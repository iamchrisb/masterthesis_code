package de.cb.ma;

import de.cb.ma.core.imu.preprocessing.AmplitudeNormalizer;
import de.cb.ma.util.Functions;

import java.math.BigDecimal;
import java.time.Instant;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.math3.complex.Complex;
import org.apache.commons.math3.transform.DftNormalization;
import org.apache.commons.math3.transform.FastFourierTransformer;
import org.apache.commons.math3.transform.TransformType;

import com.fastdtw.dtw.FastDTW;
import com.fastdtw.dtw.TimeWarpInfo;
import com.fastdtw.dtw.WarpPath;
import com.fastdtw.timeseries.TimeSeries;
import com.fastdtw.timeseries.TimeSeriesBase;
import com.fastdtw.timeseries.TimeSeriesPoint;
import com.fastdtw.util.Distances;

public class Main {

   public static void main(String[] args) {
      //      SerialMessageHandler serialMessageHandler = new SerialMessageHandler(Constants.DEBUG_MESSAGES_PATH, new SerialMessageHandler.Callback() {
      //         @Override
      //         public void onMessageReceived(final SerialMessage serialMessage) {
      //            System.out.println(serialMessage);
      //         }
      //      });

      final double map1 = Functions.map(-50, -90, 90, 0, 127);
      System.out.println(map1);
      final double map2 = Functions.map(50, -90, 90, 0, 127);
      System.out.println(map2);
      final double map3 = Functions.map(0, -90, 90, 0, 127);
      System.out.println(map3);

      System.out.println("- - - - - - Amplitude Normalization  - - - - - - ");
      System.out.println("starting with higher amps than limits...");
      AmplitudeNormalizer amplitudeNormalizer = new AmplitudeNormalizer(-1, 1);

      double[] signal = new double[13];

      signal[0] = -1.5;
      signal[1] = -1.25;
      signal[2] = -1.0;
      signal[3] = -0.75;
      signal[4] = -0.5;
      signal[5] = -0.25;
      signal[6] = 0;
      signal[7] = 0.25;
      signal[8] = 0.5;
      signal[9] = 0.75;
      signal[10] = 1.0;
      signal[11] = 1.25;
      signal[12] = 1.50;

      double[] normalizedAmps = amplitudeNormalizer.normalize(signal);
      double maxAmplitude = amplitudeNormalizer.getMaxAmps()[0];
      double minAmplitude = amplitudeNormalizer.getMinAmps()[0];

      for (int i = 0; i < signal.length; i++) {
         double currentAmp = signal[i];
         System.out.print(currentAmp);
      }

      System.out.println();

      System.out.println("max amp: " + maxAmplitude + " | min amp: " + minAmplitude);

      for (int i = 0; i < normalizedAmps.length; i++) {
         double normalizedAmp = normalizedAmps[i];
         System.out.print(normalizedAmp + ", ");
      }

      System.out.println();
      System.out.println();
      System.out.println("starting with amps lower than limits...");

      signal[0] = -0.9;
      signal[1] = -0.7;
      signal[2] = -0.5;
      signal[3] = -0.3;
      signal[4] = -0.2;
      signal[5] = -0.1;
      signal[6] = 0;
      signal[7] = 0.1;
      signal[8] = 0.2;
      signal[9] = 0.3;
      signal[10] = 0.5;
      signal[11] = 0.7;
      signal[12] = 0.9;

      for (int i = 0; i < signal.length; i++) {
         double currentAmp = signal[i];
         System.out.print(currentAmp);
      }
      System.out.println();

      normalizedAmps = amplitudeNormalizer.normalize(signal);
      maxAmplitude = amplitudeNormalizer.getMaxAmps()[0];
      minAmplitude = amplitudeNormalizer.getMinAmps()[0];

      System.out.println("max amp: " + maxAmplitude + " | min amp: " + minAmplitude);

      for (int i = 0; i < normalizedAmps.length; i++) {
         double normalizedAmp = normalizedAmps[i];
         System.out.print(normalizedAmp + ", ");
      }

      System.out.println();
      System.out.println();

      System.out.println("- - - - - - FFT  - - - - - - ");

      FastFourierTransformer fft = new FastFourierTransformer(DftNormalization.STANDARD);
      final Complex[] transform = fft.transform(new double[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 11, 12, 13, 14, 15, 16 }, TransformType.FORWARD);
      for (int i = 0; i < transform.length; i++) {
         Complex complex = transform[i];
         final double real = complex.getReal();
         final double imaginary = complex.getImaginary();
         System.out.println("real:" + real + " | img: " + imaginary);
      }

      System.out.println("- - - - - - TIME SERIES STUFF - - - - - - ");
      final BigDecimal bigDecimal = new BigDecimal("5.035401000E-3");
      System.out.println(bigDecimal);

      TimeSeries ts1 = TimeSeriesBase.builder()
            .add(0, 123.4)
            .add(1, 125)
            .add(2, 126.3)
            .build();

      TimeSeries ts2 = TimeSeriesBase.builder()
            .add(0, 123.4)
            .add(1, 125)
            .add(2, 126.3)
            .build();

      double distance = FastDTW.compare(ts1, ts2, 10, Distances.EUCLIDEAN_DISTANCE)
            .getDistance();

      System.out.println(distance);

      double[] p1Values = new double[3];
      p1Values[0] = 123.1;
      p1Values[0] = 125.1;
      p1Values[0] = 127.1;
      TimeSeriesPoint p1 = new TimeSeriesPoint(p1Values);

      double[] p2Values = new double[3];
      p2Values[0] = 123.1;
      p2Values[0] = 125.1;
      p2Values[0] = 190.1;

      TimeSeriesPoint p2 = new TimeSeriesPoint(p2Values);

      final TimeSeries ts3 = TimeSeriesBase.builder()
            .add(0, p1)
            .add(1, p2)
            .build();

      double[] p3Values = new double[3];
      p3Values[0] = 127.1;
      p3Values[0] = 129.1;
      p3Values[0] = 131.1;
      TimeSeriesPoint p3 = new TimeSeriesPoint(p3Values);

      double[] p4Values = new double[3];
      p4Values[0] = 127.1;
      p4Values[0] = 129.1;
      p4Values[0] = 194.1;

      TimeSeriesPoint p4 = new TimeSeriesPoint(p4Values);

      final TimeSeries ts4 = TimeSeriesBase.builder()
            .add(0, p3)
            .add(1, p4)
            .build();

      final TimeWarpInfo timeWarpInfo = FastDTW.compare(ts3, ts4, 10, Distances.EUCLIDEAN_DISTANCE);
      double distance2 = timeWarpInfo.getDistance();

      final WarpPath timeWarpInfoPath = timeWarpInfo.getPath();

      System.out.println(distance2);

      //      List<Gesture> gestures = new ArrayList<>();
      //      Map<Double, List<Double>> freqs = new HashMap<>();
      //      //      freqs.put(0, new ArrayList<>());
      //
      //      final Gesture drumHit = new Gesture("DRUM_HIT", freqs, "x-axis", "y-axis", "z-axis");

      //      GestureMapping mapping = new GestureMapping(gestures);
      //
      //      ObjectMapper mapper = new ObjectMapper();
      //
      //      try {
      //         mapper.writeValue(new File(Constants.GESTURE_MAPPING_PATH), gestures);
      //      } catch (IOException e) {
      //         e.printStackTrace();
      //      }

      final Instant now = Instant.now();

      CircularFifoQueue<Double> qu = new CircularFifoQueue<>(4);
      final int size = qu.size();
      System.out.println(size);

      for (int i = 8; i > 0; i--) {
         qu.add(new Double(i));
      }

      final Double peek1 = qu.poll();
      final Double peek2 = qu.poll();
      final Double peek3 = qu.poll();
      final Double peek4 = qu.poll();

      System.out.println(peek1);
      System.out.println(peek2);
      System.out.println(peek3);
      System.out.println(peek4);

      long start = System.nanoTime();

      int counter = 0;

      //      while (true) {
      //         counter++;
      //         final long nextMeasure = System.nanoTime();
      //         float passed = ((float) (nextMeasure - start)) / Constants.NANO_SECONDS_IN_SECONDS;
      //         if (passed >= 1) {
      //            System.out.println(counter);
      //            start = System.nanoTime();
      //            counter = 0;
      //         }
      //      }

   }
}
