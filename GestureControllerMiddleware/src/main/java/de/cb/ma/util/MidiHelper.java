package de.cb.ma.util;

import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.MidiMessageParameter;

import java.util.ArrayList;
import java.util.List;

import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Synthesizer;

public class MidiHelper {

   public static final Integer[] MIDI_CHANNELS = new Integer[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };

   public static String getNoteAsString(int i) {
      if (i < 0 || i > 127) {
         throw new IllegalArgumentException("i is not in range of 0 - 127");
      }
      return NOTES[i];
   }

   public static int MIDI_MAX = 127;
   public static int MIDI_MIN = 0;

   public static int CC_VOLUME_CHANGE = 7;
   public static int CC_BALANCE_CHANGE = 8;
   public static int CC_UNDEFINED_CHANGE = 9;
   public static int CC_PAN_CHANGE = 10;

   public enum CC_TYPE {
      CC_VOLUME_CHANGE(7),
      CC_BALANCE_CHANGE(8),
      CC_UNDEFINED_CHANGE(9),
      CC_PAN_CHANGE(10);

      CC_TYPE(final int i) {
         this.value = i;
      }

      private final int value;

      public int getValue() {
         return value;
      }
   }

   public static String[] OCTAVE = new String[] { "C", "C#", "D", "D#", "E", "E#", "F", "G", "G#", "A", "A#", "B" };

   public static String[] NOTES = new String[] {
         "C0", "C#0", "D0", "D#0", "E0", "F0", "F#0", "G0", "G#0", "A0", "A#0", "B0",
         "C1", "C#1", "D1", "D#1", "E1", "F1", "F#1", "G1", "G#1", "A1", "A#1", "B1",
         "C2", "C#2", "D2", "D#2", "E2", "F2", "F#2", "G2", "G#2", "A2", "A#2", "B2",
         "C3", "C#3", "D3", "D#3", "E3", "F3", "F#3", "G3", "G#3", "A3", "A#3", "B3",
         "C4", "C#4", "D4", "D#4", "E4", "F4", "F#4", "G4", "G#4", "A4", "A#4", "B4",
         "C5", "C#5", "D5", "D#5", "E5", "F5", "F#5", "G5", "G#5", "A5", "A#5", "B5",
         "C6", "C#6", "D6", "D#6", "E6", "F6", "F#6", "G6", "G#6", "A6", "A#6", "B6",
         "C7", "C#7", "D7", "D#7", "E7", "F7", "F#7", "G7", "G#7", "A7", "A#7", "B7",
         "C8", "C#8", "D8", "D#8", "E8", "F8", "F#8", "G8", "G#8", "A8", "A#8", "B8",
         "C9", "C#9", "D9", "D#9", "E9", "F9", "F#9", "G9", "G#9", "A9", "A#9", "B9",
   };

   public static final String OUT_PORT_INF_STR = " (OUT port)";
   public static final String IN_PORT_INF_STR = " (IN port)";
   List<MidiDevice.Info> ports = new ArrayList<>();
   List<MidiDevice.Info> sequencers = new ArrayList<>();
   List<MidiDevice.Info> synths = new ArrayList<>();

   public MidiHelper() {

      final MidiDevice.Info[] infos = MidiSystem.getMidiDeviceInfo();
      MidiDevice device = null;

      for (int i = 0; i < infos.length; i++) {
         try {
            device = MidiSystem.getMidiDevice(infos[i]);
         } catch (MidiUnavailableException e) {
            // Handle or throw exception...
         }

         if (device instanceof Sequencer) {
            sequencers.add(infos[i]);
         } else if (device instanceof Synthesizer) {
            synths.add(infos[i]);
         } else {
            ports.add(infos[i]);
         }
      }
   }

   /*
   take care that in- and output ports often share the same name
    */
   public MidiDevice getMidiDevice(String name) throws MidiUnavailableException {
      final MidiDevice.Info[] midiDeviceInfo = MidiSystem.getMidiDeviceInfo();

      for (int i = 0; i < midiDeviceInfo.length; i++) {
         MidiDevice.Info info = midiDeviceInfo[i];
         if (info.getName()
               .equals(name)) {
            return MidiSystem.getMidiDevice(midiDeviceInfo[i]);
         }
      }
      return null;
   }

   public MidiDevice getMidiDevice(int index) throws MidiUnavailableException {
      final MidiDevice.Info[] midiDeviceInfo = MidiSystem.getMidiDeviceInfo();
      return MidiSystem.getMidiDevice(midiDeviceInfo[index]);
   }

   public MidiDevice.Info[] getDeviceInfo() {
      return MidiSystem.getMidiDeviceInfo();
   }

   public MidiDevice.Info getDeviceInfo(String name) {
      for (MidiDevice.Info info : getDeviceInfo()) {
         if (info.getName()
               .equals(name)) {
            return info;
         }
      }
      return null;
   }

   public MidiDevice.Info getDeviceInfo(int index) {
      return MidiSystem.getMidiDeviceInfo()[index];
   }

   public List<String> getDeviceNames() {
      List<String> names = new ArrayList<>();
      for (MidiDevice.Info info : getDeviceInfo()) {
         names.add(info.getName());
      }
      return names;
   }

   public List<MidiDevice.Info> getSystemSynthezisers() {
      return synths;
   }

   public List<MidiDevice.Info> getSystemSequencers() {
      return sequencers;
   }

   public List<MidiDevice.Info> getSystemMidiPorts() {
      return ports;
   }

   public MidiDevice getSystemMidiPort(int index) {
      try {
         return MidiSystem.getMidiDevice(ports.get(index));
      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      }
      return null;
   }

   public List<String> getSystemMidiPortNames() {
      List<String> names = new ArrayList<>();
      for (MidiDevice.Info port : ports) {
         StringBuilder portName = new StringBuilder();
         try {
            portName.append(port.getName());

            final MidiDevice midiDevice = MidiSystem.getMidiDevice(port);
            if (midiDevice.getMaxReceivers() == -1) {
               portName.append(OUT_PORT_INF_STR);
            } else if (midiDevice.getMaxTransmitters() == -1) {
               portName.append(IN_PORT_INF_STR);
            }
            names.add(portName.toString());
         } catch (MidiUnavailableException e) {
            e.printStackTrace();
         }
      }
      return names;
   }

   public void useMidiDevice(int index) throws MidiUnavailableException {
      this.currentMidiDevice = getSystemMidiPort(index);
      if (!this.currentMidiDevice.isOpen()) {
         System.out.println("Open MIDI-Device: " + currentMidiDevice.getDeviceInfo()
               .getName());
         this.currentMidiDevice.open();
      }
      final List<Receiver> receivers = currentMidiDevice.getReceivers();
      for (Receiver receiver : receivers) {
         System.out.println(receiver);
      }
   }

   public void playNote(int channel, int pitch, int velocity) {
      try {
         if (currentMidiDevice != null) {
            Receiver receiver = currentMidiDevice.getReceiver();

            final ShortMessage shortMessage = new ShortMessage();
            shortMessage.setMessage(ShortMessage.NOTE_ON, channel, pitch, velocity);
            receiver.send(shortMessage, -1);

            System.out.println("using : " + currentMidiDevice.getDeviceInfo()
                  .getName() + " to send a midi message");
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   /**
    * @param channel
    * @param pitch
    *       or key number on the controller
    * @param velocity
    */
   public void stopNote(int channel, int pitch, int velocity) {
      try {
         if (currentMidiDevice != null) {
            Receiver receiver = currentMidiDevice.getReceiver();

            final ShortMessage shortMessage = new ShortMessage();
            shortMessage.setMessage(ShortMessage.NOTE_OFF, channel, pitch, velocity);
            receiver.send(shortMessage, -1);

            System.out.println("using : " + currentMidiDevice.getDeviceInfo()
                  .getName() + " to send a midi message");
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   private MidiDevice currentMidiDevice;

   public void sendChannelMessage(DAWCommand command) {
      final MidiMessageParameter midiMessageParameter = command.getMidiMessageParameter();
      final int midiType = getMidiType(midiMessageParameter.getActionType());
      sendChannelMessage(midiType, midiMessageParameter.getMidiChannel(), midiMessageParameter.getDatabyte1(), midiMessageParameter.getDatabyte2());
   }

   private int getMidiType(final MidiMessageParameter.MIDI_MESSAGE_TYPE actionType) {
      if (actionType.equals(MidiMessageParameter.MIDI_MESSAGE_TYPE.CONTROL_CHANGE)) {
         return ShortMessage.CONTROL_CHANGE;
      }

      if (actionType.equals(MidiMessageParameter.MIDI_MESSAGE_TYPE.NOTE_ON)) {
         return ShortMessage.NOTE_ON;
      }

      if (actionType.equals(MidiMessageParameter.MIDI_MESSAGE_TYPE.NOTE_OFF)) {
         return ShortMessage.NOTE_OFF;
      }

      if (actionType.equals(MidiMessageParameter.MIDI_MESSAGE_TYPE.PITCH_BEND)) {
         return ShortMessage.PITCH_BEND;
      }

      return 0;
   }

   public void sendChannelMessage(final int status, final int channel, final int databyte1, final int databyte2) {
      try {
         if (currentMidiDevice != null) {
            Receiver receiver = currentMidiDevice.getReceiver();

            final ShortMessage shortMessage = new ShortMessage();
            shortMessage.setMessage(status, channel, databyte1, databyte2);
            receiver.send(shortMessage, -1);

            System.out.println("using : " + currentMidiDevice.getDeviceInfo()
                  .getName() + " to send a midi message");
         }
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   public int getIntForMessageActionType(final MidiMessageParameter.MIDI_MESSAGE_TYPE actionType) {
      return getMidiType(actionType);
   }
}
