package de.cb.ma.util;

public class Pair<K, V> {

   private K key;
   private V value;

   public Pair(final K key, final V value) {
      this.key = key;
      this.value = value;
   }

   public Pair() {
      //json
   }

   public K getKey() {
      return key;
   }

   public void setKey(final K key) {
      this.key = key;
   }

   public V getValue() {
      return value;
   }

   public void setValue(final V value) {
      this.value = value;
   }

   @Override
   public boolean equals(final Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof Pair)) {
         return false;
      }

      final Pair<?, ?> pair = (Pair<?, ?>) o;

      return key != null ? key.equals(pair.key) : pair.key == null;
   }

   @Override
   public int hashCode() {
      return key != null ? key.hashCode() : 0;
   }
}
