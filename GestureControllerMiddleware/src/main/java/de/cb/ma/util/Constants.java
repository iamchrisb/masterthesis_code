package de.cb.ma.util;

import java.io.File;
import java.text.DecimalFormat;

public class Constants {

   public static final DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#.##");
   public static final DecimalFormat INTEGER_FORMAT = new DecimalFormat("#");

   public static final long NANO_SECONDS_IN_SECONDS = 1000000000;

   public static final String WORKING_DIR = System.getProperty("user.dir");

   // I created my own file format which stands for "intertial motion capture"
   public static final String INERTIAL_DATA_FILE_FORMAT = ".imc";

   public static final String RESOURCE_DIR = WORKING_DIR + File.separator + "resource";

   public static final String EXPERIMENT_DATA_DIR = RESOURCE_DIR + File.separator + "exp";

   public static final String MAPPING_DIR = RESOURCE_DIR + File.separator + "mapping";
   public static final String MSGS_DIR = RESOURCE_DIR + File.separator + "msgs";

   public static final String MIDI_MAPPING_PATH = MAPPING_DIR + File.separator + "midi_mapping.json";
   public static final String GESTURE_MAPPING_PATH = MAPPING_DIR + File.separator + "gesture_mapping.json";

   public static final String LATEST_GESTURE = MAPPING_DIR + File.separator + "msgs" + File.separator + "latest_gesture.msgs";

   public static final String DEBUG_MESSAGES_PATH = WORKING_DIR + File.separator + "serial.msgs";

   public static final String MAG_DATA_DIR = RESOURCE_DIR + File.separator + "mag_data";
   public static final String ACC_DATA_DIR = RESOURCE_DIR + File.separator + "acc_data";
   public static final String GYRO_DATA_DIR = RESOURCE_DIR + File.separator + "gyro_data";

   public static final String MAG_PLAIN_DATA = MAG_DATA_DIR + File.separator + "mag_plain_data.msgs";
   public static final String MAG_BIAS_CALIBRATION_DATA = MAG_DATA_DIR + File.separator + "mag_bias_calibration_data.msgs";
   public static final String MAG_BIAS_SCALE_CALIBRATION_DATA = MAG_DATA_DIR + File.separator + "mag_bias_scale_calibration_data.msgs";
   public static final String EXPERIMENT_DATA_DATE_FORMAT = "yyyy-MM-dd";
   public static final long ONE_SECOND_PERIOD = 1000;

   public static final String DB_FOLDER = RESOURCE_DIR + File.separator + "db";
   public static final String DB_PROFILES_PATH = DB_FOLDER + File.separator + "profiles.json";
   public static final String DB_GESTURE_MODELS_PATH = DB_FOLDER + File.separator + "gesture_models.json";
   public static final String DB_DAW_COMMANDS_PATH = DB_FOLDER + File.separator + "daw_commands.json";
}
