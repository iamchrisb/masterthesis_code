package de.cb.ma.util;

public class Functions {
   public static double map(double x, double inputStart, double inputEnd, double outputStart, double outputEnd) {
      return (x - inputStart) / (inputEnd - inputStart) * (outputEnd - outputStart) + outputStart;
   }
}
