package de.cb.ma.depricated.debugging.accel;

import de.cb.ma.depricated.View;
import de.cb.ma.depricated.debugging.DataPlotPresenter;

public interface AccelerationDataView extends View {

   void setPresenter(DataPlotPresenter presenter);

   void updateAccelerationGraphs(long timePassed, Double xAxisValue, Double yAxisValue, Double zAxisValue, final Double filteredX, final Double filteredY, final Double filteredZ);
}
