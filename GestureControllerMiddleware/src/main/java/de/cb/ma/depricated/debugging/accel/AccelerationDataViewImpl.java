package de.cb.ma.depricated.debugging.accel;

import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.core.widgets.file.FileChooserWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.depricated.Statistics;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import de.cb.ma.util.Constants;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections4.queue.CircularFifoQueue;
import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class AccelerationDataViewImpl implements AccelerationDataView {

   public static final int WINDOW_SIZE = 16;

   public void setPresenter(DataPlotPresenter presenter) {
      this.presenter = presenter;
   }

   private DescriptiveStatistics descriptiveStatisticsXAxis = new DescriptiveStatistics();
   private DescriptiveStatistics descriptiveStatisticsYAxis = new DescriptiveStatistics();
   private DescriptiveStatistics descriptiveStatisticsZAxis = new DescriptiveStatistics();

   private CircularFifoQueue<Double> xAxisVarianceQueue = new CircularFifoQueue<>(WINDOW_SIZE);
   private CircularFifoQueue<Double> yAxisVarianceQueue = new CircularFifoQueue<>(WINDOW_SIZE);
   private CircularFifoQueue<Double> zAxisVarianceQueue = new CircularFifoQueue<>(WINDOW_SIZE);

   @Override
   public void updateAccelerationGraphs(final long timePassed, final Double xAxisValue, final Double yAxisValue, final Double zAxisValue, final Double filteredX, final Double filteredY,
         final Double filteredZ) {
      float timePassedInSecs = ((float) timePassed) / 1000;
      //      xAccelGraph.addData(timePassedInSecs, xAxisValue);
      //      yAccelGraph.addData(timePassedInSecs, yAxisValue);
      //      zAccelGraph.addData(timePassedInSecs, zAxisValue);

      descriptiveStatisticsXAxis.addValue(filteredX);
      descriptiveStatisticsYAxis.addValue(filteredY);
      descriptiveStatisticsZAxis.addValue(filteredY);

      final double xMean = descriptiveStatisticsXAxis.getMean();
      final double yMean = descriptiveStatisticsYAxis.getMean();
      final double zMean = descriptiveStatisticsZAxis.getMean();

      //      smoothedXAccelGraph.addData(timePassedInSecs, xMean);
      //      smoothedYAccelGraph.addData(timePassedInSecs, yMean);
      //      smoothedZAccelGraph.addData(timePassedInSecs, zMean);

      smoothedXAccelGraph.addData(timePassedInSecs, filteredX);
      smoothedYAccelGraph.addData(timePassedInSecs, filteredY);
      smoothedZAccelGraph.addData(timePassedInSecs, filteredZ);

      // show variance and standard deviation
      Platform.runLater(() -> {
         showVariance(timePassedInSecs);
         //         showStandardDeviation(timePassedInSecs);
         showWelfordVariance(timePassedInSecs, xAxisValue.doubleValue(), yAxisValue.doubleValue(), zAxisValue.doubleValue());
      });
   }

   private void showWelfordVariance(final float timePassedInSecs, final double x, final double y, final double z) {
      xAxisVarianceQueue.add(x);
      yAxisVarianceQueue.add(y);
      zAxisVarianceQueue.add(z);

      if (xAxisVarianceQueue.size() != WINDOW_SIZE && yAxisVarianceQueue.size() != WINDOW_SIZE && zAxisVarianceQueue.size() != WINDOW_SIZE) {
         return;
      }

      final double xVariance = Statistics.welfordVariance(xAxisVarianceQueue);
      final double yVariance = Statistics.welfordVariance(yAxisVarianceQueue);
      final double zVariance = Statistics.welfordVariance(zAxisVarianceQueue);

      double magVariance = (xVariance + yVariance + zVariance) / 3;

      final List<Number> variances = new ArrayList<>();
      variances.add(xVariance);
      variances.add(yVariance);
      variances.add(zVariance);
      variances.add(magVariance);

      deviationGraph.addData(timePassedInSecs, variances);
   }

   private void showStandardDeviation(final float timePassedInSecs) {
      xDeviationValue.setText(Constants.DECIMAL_FORMAT
            .format(descriptiveStatisticsXAxis.getStandardDeviation()));
      yDeviationValue.setText(Constants.DECIMAL_FORMAT.format(descriptiveStatisticsYAxis.getStandardDeviation()));
      zDeviationValue.setText(Constants.DECIMAL_FORMAT.format(descriptiveStatisticsZAxis.getStandardDeviation()));

      double magDeviation = (descriptiveStatisticsXAxis.getStandardDeviation() + descriptiveStatisticsYAxis.getStandardDeviation() + descriptiveStatisticsZAxis.getStandardDeviation()) / 3;

      magDeviationValue.setText(Constants.DECIMAL_FORMAT.format(magDeviation));

      final ArrayList<Number> variances = new ArrayList<>();
      variances.add(descriptiveStatisticsXAxis.getStandardDeviation());
      variances.add(descriptiveStatisticsYAxis.getStandardDeviation());
      variances.add(descriptiveStatisticsZAxis.getStandardDeviation());
      variances.add(magDeviation);

      deviationGraph.addData(timePassedInSecs, variances);
   }

   private void showVariance(final float timePassedInSecs) {
      xVarianceValue.setText(Constants.DECIMAL_FORMAT.format(descriptiveStatisticsXAxis.getVariance()));
      yVarianceValue.setText(Constants.DECIMAL_FORMAT.format(descriptiveStatisticsYAxis.getVariance()));
      zVarianceValue.setText(Constants.DECIMAL_FORMAT.format(descriptiveStatisticsZAxis.getVariance()));

      double magVariance = (descriptiveStatisticsXAxis.getVariance() + descriptiveStatisticsYAxis.getVariance() + descriptiveStatisticsZAxis.getVariance()) / 3;
      magVarianceValue.setText(Constants.DECIMAL_FORMAT.format(magVariance));

      // analysis graphs
      final ArrayList<Number> variances = new ArrayList<>();
      variances.add(descriptiveStatisticsXAxis.getVariance());
      variances.add(descriptiveStatisticsYAxis.getVariance());
      variances.add(descriptiveStatisticsZAxis.getVariance());
      variances.add(magVariance);

      varianceGraph.addData(timePassedInSecs, variances);
   }

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Acceleration data plot");
      descriptiveStatisticsXAxis.setWindowSize(WINDOW_SIZE);
      descriptiveStatisticsYAxis.setWindowSize(WINDOW_SIZE);
      descriptiveStatisticsZAxis.setWindowSize(WINDOW_SIZE);

      VBox parentVBox = new VBox();
      parentVBox.setSpacing(20);

      serialPortWidget = new SerialPortWidget("Connect to Serialport", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            presenter.startSerial(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            presenter.stopSerial();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      FileChooserWidget fileChooserWidget = new FileChooserWidget("Choose file", primaryStage, new FileChooserWidget.Callback() {
         @Override
         public void onFileSelected(final File file) {
            presenter.startSerial(file.getAbsolutePath());
         }
      });

      /** misc **/

      VBox varianceBox = new VBox(10);
      VBox standardDeviationBox = new VBox(10);

      createVarianceLabels(varianceBox);
      createStandardDeviationLabels(standardDeviationBox);

      Button clear = new Button("Clear Graphs");

      VBox analysisContainer = new VBox(10);
      analysisContainer.getChildren()
            .addAll(varianceBox, standardDeviationBox, clear);

      HBox controlPanelContainer = new HBox(10);
      controlPanelContainer.setPadding(new Insets(10, 10, 40, 10));

      controlPanelContainer.getChildren()
            .addAll(serialPortWidget, fileChooserWidget, analysisContainer);

      parentVBox.getChildren()
            .addAll(controlPanelContainer);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      xAccelGraph = new LineGraphPlotWidget("x-Axis", "Acc x-axis data plot", "time in s", "acceleration in mG");
      yAccelGraph = new LineGraphPlotWidget("y-Axis", "Acc y-axis data plot", "time in s", "acceleration in mG");
      zAccelGraph = new LineGraphPlotWidget("z-Axis", "Acc z-axis data plot", "time in s", "acceleration in mG");

      smoothedXAccelGraph = new LineGraphPlotWidget("x-Axis", "Acc x-axis data plot", "time in s", "acceleration in mG");
      smoothedYAccelGraph = new LineGraphPlotWidget("y-Axis", "Acc y-axis data plot", "time in s", "acceleration in mG");
      smoothedZAccelGraph = new LineGraphPlotWidget("z-Axis", "Acc z-axis data plot", "time in s", "acceleration in mG");

      deviationGraph = new LineGraphPlotWidget(new String[] { "x", "y", "z", "magnitude" }, "deviation", "time in s", "");
      deviationGraph.showAxisValuesInLegend(false);

      varianceGraph = new LineGraphPlotWidget(new String[] { "x", "y", "z", "magnitude" }, "variance", "time in s", "");
      varianceGraph.showAxisValuesInLegend(false);

      HBox accelGraphContainer = new HBox();
      HBox smoothedAccelGraphContainer = new HBox();
      HBox analysisGraphContainer = new HBox();

      parentVBox.getChildren()
            .addAll(accelGraphContainer, smoothedAccelGraphContainer);

      controlPanelContainer.getChildren()
            .addAll(analysisGraphContainer);

      accelGraphContainer.getChildren()
            .addAll(xAccelGraph.getElement(), yAccelGraph.getElement(), zAccelGraph.getElement());

      smoothedAccelGraphContainer.getChildren()
            .addAll(smoothedXAccelGraph.getElement(), smoothedYAccelGraph.getElement(), smoothedZAccelGraph.getElement());

      analysisGraphContainer.getChildren()
            .addAll(varianceGraph.getElement(), deviationGraph.getElement());

      clear.setOnAction(event -> {
         xAccelGraph.clearData();
         yAccelGraph.clearData();
         zAccelGraph.clearData();
         smoothedXAccelGraph.clearData();
         smoothedYAccelGraph.clearData();
         smoothedZAccelGraph.clearData();
         varianceGraph.clearData();
         deviationGraph.clearData();
      });

      final URL resource = LineGraphPlotWidget.class.getResource("styles.css");

      final String styles = resource
            .toExternalForm();

      final Scene scene = primaryStage.getScene();

      scene.getStylesheets()
            .add(styles);

      primaryStage.show();
   }

   private void createStandardDeviationLabels(final VBox standardDeviationBox) {
      HBox xDeviationBox = new HBox(10);
      HBox yDeviationBox = new HBox(10);
      HBox zDeviationBox = new HBox(10);
      HBox magDeviationBox = new HBox(10);

      xDeviationValue = new Label();
      xDeviationBox.getChildren()
            .addAll(new Label("x deviation: "), xDeviationValue);
      yDeviationValue = new Label();

      yDeviationBox.getChildren()
            .addAll(new Label("y deviation: "), yDeviationValue);

      zDeviationValue = new Label();
      zDeviationBox.getChildren()
            .addAll(new Label("z deviation: "), zDeviationValue);

      magDeviationValue = new Label();
      magDeviationBox.getChildren()
            .addAll(new Label("mag deviation: "), magDeviationValue);

      standardDeviationBox.getChildren()
            .addAll(xDeviationBox, yDeviationBox, zDeviationBox, magDeviationBox);
   }

   private void createVarianceLabels(final VBox varianceBox) {
      HBox xVarianceBox = new HBox(10);
      HBox yVarianceBox = new HBox(10);
      HBox zVarianceBox = new HBox(10);
      HBox magVarianceBox = new HBox(10);

      xVarianceValue = new Label();
      xVarianceBox.getChildren()
            .addAll(new Label("x variance: "), xVarianceValue);
      yVarianceValue = new Label();

      yVarianceBox.getChildren()
            .addAll(new Label("y variance: "), yVarianceValue);

      zVarianceValue = new Label();
      zVarianceBox.getChildren()
            .addAll(new Label("z variance: "), zVarianceValue);

      magVarianceValue = new Label();
      magVarianceBox.getChildren()
            .addAll(new Label("mag variance: "), magVarianceValue);

      varianceBox.getChildren()
            .addAll(xVarianceBox, yVarianceBox, zVarianceBox, magVarianceBox);
   }

   private DataPlotPresenter presenter;

   private LineGraphPlotWidget xAccelGraph;
   private LineGraphPlotWidget yAccelGraph;
   private LineGraphPlotWidget zAccelGraph;

   SerialPortWidget serialPortWidget;
   private LineGraphPlotWidget smoothedXAccelGraph;
   private LineGraphPlotWidget smoothedYAccelGraph;
   private LineGraphPlotWidget smoothedZAccelGraph;

   private Label xVarianceValue;
   private Label yVarianceValue;
   private Label zVarianceValue;
   private Label magVarianceValue;

   private Label xDeviationValue;
   private Label yDeviationValue;
   private Label zDeviationValue;
   private Label magDeviationValue;

   private LineGraphPlotWidget varianceGraph;
   private LineGraphPlotWidget deviationGraph;
}
