package de.cb.ma.depricated;

import de.cb.ma.core.database.dao.GestureMappingDao;
import de.cb.ma.core.database.model.GestureMappingEntity;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.imu.motion.HandSign;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MidiMappingTableWidget extends Pane {

   public interface ButtonCallback {
      void onSelected(int index, MidiMappingModel midiModel);
   }

   public interface PlayCallback extends ButtonCallback {
   }

   public interface DeleteCallback extends ButtonCallback {
   }

   public MidiMappingTableWidget(String title, PlayCallback playCallback, DeleteCallback deleteCallback) {

      final Label label = new Label(title);

      // TODO
      //      gestureMappingDao = new GestureMappingDaoFileImpl();
      //      gestureMapping = gestureMappingDao.findAll();

      final List<MidiMappingModel> model = collectMidiModels();

      tableView = new TableView();

      TableColumn drumActionNameCol = new TableColumn("Name");
      drumActionNameCol.setMinWidth(200);
      drumActionNameCol.setCellValueFactory(new PropertyValueFactory<MidiMappingModel, String>("drumActionName"));

      TableColumn postureCol = new TableColumn("Posture");
      postureCol.setCellValueFactory(new PropertyValueFactory<MidiMappingModel, String>("posture"));
      postureCol.setMinWidth(200);

      TableColumn gestureNameCol = new TableColumn("Gesture");
      gestureNameCol.setCellValueFactory(new PropertyValueFactory<MidiMappingModel, String>("gestureName"));
      gestureNameCol.setMinWidth(200);

      TableColumn midiChannelCol = new TableColumn("MidiChannel");
      midiChannelCol.setCellValueFactory(new PropertyValueFactory<MidiMappingModel, Integer>("midiChannel"));
      midiChannelCol.setMinWidth(100);

      TableColumn midiPitchNoteCol = new TableColumn("MidiPitch");
      midiPitchNoteCol.setCellValueFactory(new PropertyValueFactory<MidiMappingModel, Integer>("midiPitchAsNote"));
      midiPitchNoteCol.setMinWidth(100);

      TableColumn midiVelocityCol = new TableColumn("MidiVelocity");
      midiVelocityCol.setCellValueFactory(new PropertyValueFactory<MidiMappingModel, Integer>("midiVelocity"));
      midiVelocityCol.setMinWidth(100);

      TableColumn sendCol = new TableColumn<>("Test MIDI");
      sendCol.setSortable(false);
      sendCol.setMinWidth(100);

      TableColumn deleteCol = new TableColumn<>("Actions");
      deleteCol.setSortable(false);
      deleteCol.setMinWidth(100);

      sendCol.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<MidiMappingModel, Boolean>,
                  ObservableValue<Boolean>>() {

               @Override
               public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<MidiMappingModel, Boolean> p) {
                  return new SimpleBooleanProperty(p.getValue() != null);
               }
            });

      sendCol.setCellFactory(
            p -> new ButtonCell("Play", playCallback));

      deleteCol.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<MidiMappingModel, Boolean>,
                  ObservableValue<Boolean>>() {

               @Override
               public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<MidiMappingModel, Boolean> p) {
                  return new SimpleBooleanProperty(p.getValue() != null);
               }
            });

      deleteCol.setCellFactory(
            p -> new ButtonCell("Remove", deleteCallback));

      tableView.getColumns()
            .addAll(sendCol, drumActionNameCol, postureCol, gestureNameCol, midiChannelCol, midiPitchNoteCol, midiVelocityCol, deleteCol);

      this.model = FXCollections.observableArrayList(
            model
      );

      tableView.setItems(this.model);

      VBox container = new VBox();

      container.getChildren()
            .addAll(label, tableView);

      this.getChildren()
            .add(container);

   }

   public void refresh() {
      clearTableView();
      gestureMapping = gestureMappingDao.findAll();
      final List<MidiMappingModel> mappingModels = collectMidiModels();
      model.setAll(mappingModels);
      tableView.setItems(model);
   }

   private void clearTableView() {
      tableView.getItems()
            .clear();
   }

   public List<MidiMappingModel> collectMidiModels() {
      final List<MidiMappingModel> models = new ArrayList<>();

      final Set<HandSign.SIGN> keys = gestureMapping.giveKeys();
      for (HandSign.SIGN key : keys) {
         final List<GestureModel> gestures = gestureMapping.getGestures(key);
         for (GestureModel gesture : gestures) {
            // TODO
            //            final MidiMessageParameter drumAction = gesture.getMidiAction();
            //            if (drumAction != null) {
            //               models.add(new MidiMappingModel(drumAction, gesture, key));
            //            }
         }
      }
      return models;
   }

   private final TableView tableView;
   private final ObservableList<MidiMappingModel> model;

   //Define the button cell
   private class ButtonCell extends TableCell<MidiMappingModel, Boolean> {
      final Button selectButton = new Button();
      final Button saveButton = new Button();

      HBox container = new HBox(10);

      ButtonCell(String text, ButtonCallback callback) {

         // add later
         saveButton.setText("Save (Draft)");

         selectButton.setText(text);
         selectButton.setOnAction(t -> {
            final MidiMappingModel midiModel = (MidiMappingModel) tableView.getItems()
                  .get(getIndex());
            callback.onSelected(getIndex(), midiModel);
         });

         container.getChildren()
               .addAll(selectButton);
      }

      //Display button if the row is not empty
      @Override
      protected void updateItem(Boolean t, boolean empty) {
         super.updateItem(t, empty);
         if (!empty) {
            setGraphic(container);
         }
      }
   }

   private GestureMappingEntity gestureMapping;
   private GestureMappingDao gestureMappingDao;
}
