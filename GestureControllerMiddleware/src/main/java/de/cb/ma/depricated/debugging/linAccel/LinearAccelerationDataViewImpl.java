package de.cb.ma.depricated.debugging.linAccel;

import de.cb.ma.core.widgets.file.FileChooserWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;

public class LinearAccelerationDataViewImpl implements LinearAccelerationDataView {

   public void setPresenter(DataPlotPresenter presenter) {
      this.presenter = presenter;
   }

   @Override
   public void updateAccelerationGraphs(final long timePassed, final Integer xAxisValue, final Integer yAxisValue, final Integer zAxisValue) {
      float timePassedInSecs = ((float) timePassed) / 1000;
      xLinAccelGraph.addData(timePassedInSecs, xAxisValue);
      yLinAccelGraph.addData(timePassedInSecs, yAxisValue);
      zLinAccelGraph.addData(timePassedInSecs, zAxisValue);
   }

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Linear acceleration data plotting");

      VBox parentVBox = new VBox();
      parentVBox.setSpacing(20);

      serialPortWidget = new SerialPortWidget("Connect to Serialport", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            presenter.startSerial(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            presenter.stopSerial();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      FileChooserWidget fileChooserWidget = new FileChooserWidget("Choose file", primaryStage, new FileChooserWidget.Callback() {
         @Override
         public void onFileSelected(final File file) {
            presenter.startSerial(file.getAbsolutePath());
         }
      });

      /** misc **/

      HBox controlPanelContainer = new HBox(10);

      controlPanelContainer.getChildren()
            .addAll(serialPortWidget, fileChooserWidget);

      parentVBox.getChildren()
            .addAll(controlPanelContainer);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      xLinAccelGraph = new LineGraphPlotWidget("x-Axis", "Linear acceleration data plotting", "Time in s", "acceleration in mG");
      yLinAccelGraph = new LineGraphPlotWidget("y-Axis", "Linear acceleration data plotting", "Time in s", "acceleration in mG");
      zLinAccelGraph = new LineGraphPlotWidget("z-Axis", "Linear acceleration data plotting", "Time in s", "acceleration in mG");

      HBox accelGraphContainer = new HBox();

      parentVBox.getChildren()
            .addAll(accelGraphContainer);

      accelGraphContainer.getChildren()
            .addAll(xLinAccelGraph.getElement(), yLinAccelGraph.getElement(), zLinAccelGraph.getElement());

      Button clear = new Button("Clear Graphs");

      parentVBox.getChildren()
            .add(clear);

      clear.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(final ActionEvent event) {
            xLinAccelGraph.clearData();
            yLinAccelGraph.clearData();
            zLinAccelGraph.clearData();
         }
      });

      final URL resource = LineGraphPlotWidget.class.getResource("styles.css");

      final String styles = resource
            .toExternalForm();

      final Scene scene = primaryStage.getScene();

      scene.getStylesheets()
            .add(styles);

      primaryStage.show();
   }

   private DataPlotPresenter presenter;

   private LineGraphPlotWidget xLinAccelGraph;
   private LineGraphPlotWidget yLinAccelGraph;
   private LineGraphPlotWidget zLinAccelGraph;

   SerialPortWidget serialPortWidget;
}
