package de.cb.ma.depricated.debugging.gravity;

import de.cb.ma.depricated.View;
import de.cb.ma.depricated.debugging.DataPlotPresenter;

public interface GravityDataView extends View {

   void setPresenter(DataPlotPresenter presenter);

   void updateAccelerationGraphs(long timePassed, Integer xAxisValue, Integer yAxisValue, Integer zAxisValue);
}
