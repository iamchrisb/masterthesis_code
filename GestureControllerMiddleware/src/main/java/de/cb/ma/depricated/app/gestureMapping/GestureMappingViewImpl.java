package de.cb.ma.depricated.app.gestureMapping;

import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.core.widgets.file.FileChooserWidget;
import de.cb.ma.core.widgets.analysis.FingerDisplayWidget;
import de.cb.ma.core.database.dao.GestureMappingDao;
import de.cb.ma.core.database.model.GestureMappingEntity;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.imu.serial.messages.application.BendMessage;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.util.Pair;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GestureMappingViewImpl implements GestureMappingView {

   @Override
   public List<Pair<Double, List<Double>>> getMatchedGestureModel() {
      return matchedGestureModel;
   }

   private List<Pair<Double, List<Double>>> matchedGestureModel = new LinkedList<>();

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Gesture Mapping");

      VBox parentVBox = new VBox();

      parentVBox.setPadding(new Insets(10, 10, 10, 10));

      serialPortWidget = new SerialPortWidget("Connect to Serialport", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            presenter.startGestureDetection(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            presenter.stopSerial();
            //            matchingGestureGraph.updateRangeSlider();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {
            presenter.muteSerial(enabled);
         }
      });

      FileChooserWidget fileChooserWidget = new FileChooserWidget("Choose file", primaryStage, new FileChooserWidget.Callback() {
         @Override
         public void onFileSelected(final File file) {
            collectAccDataBtn.setSelected(true);
            presenter.startGestureDetection(file.getAbsolutePath());
         }
      });

      postureWidget = new FingerDisplayWidget("current posture");

      VBox selectGestureContainer = new VBox(10);

      Button adjustRangeBtn = new Button("adjust");
      adjustRangeBtn.setOnAction(event -> {
         matchingGestureGraph.updateRangeSlider();
      });

      Button clearBtn = new Button("clear");
      clearBtn.setOnAction(event -> {
         clearGraphs();
      });

      collectAccDataBtn = new ToggleButton("collect acc data");
      collectAccDataBtn.selectedProperty()
            .addListener(new ChangeListener<Boolean>() {
               @Override
               public void changed(final ObservableValue<? extends Boolean> observable, final Boolean oldValue, final Boolean newValue) {
                  isCollectingAccData = newValue;
                  //                  matchingGestureGraph.clearData();
               }
            });

      Label selectGestureLabel = new Label("select trained gesture");

      // TODO
      //      GestureMappingDao gestureMappingDao = new GestureMappingDaoFileImpl();
      //      gestureMapping = gestureMappingDao.findAll();

      final Set<HandSign.SIGN> keys = gestureMapping.giveKeys();

      final ArrayList<HandSign.SIGN> postures = new ArrayList<>(keys);
      posturesObservableList = FXCollections.observableList(postures);

      postureKeySelection = new ComboBox(posturesObservableList);
      postureKeySelection.getSelectionModel()
            .selectFirst();

      postureKeySelection.valueProperty()
            .addListener((observable, oldValue, newValue) -> {
               trainedGestures.clear();
               final HandSign.SIGN value = (HandSign.SIGN) newValue;
               final List<String> trainedGesturesNames = getTrainedGesturesNames(value);
               trainedGestures.addAll(trainedGesturesNames);
               trainedGesturesToSelect.setPromptText("gesture");
            });

      postureKeySelection.setPromptText("posture");

      trainedGestures = FXCollections.observableList(new ArrayList<>());
      trainedGesturesToSelect = new ComboBox(trainedGestures);
      trainedGesturesToSelect.setPromptText("gesture");

      final Button refeshGestureMappingsBtn = new Button("refesh");
      refeshGestureMappingsBtn.setOnAction(event -> {
         updateGestureMappings();
      });

      final Button deleteGestureMappingBtn = new Button("delete");
      deleteGestureMappingBtn.setOnAction(event -> {
         deleteGestureMapping();
      });

      trainedGesturesToSelect.valueProperty()
            .addListener((observable, oldValue, newValue) -> {
               if (newValue == null) {
                  return;
               }

               final GestureModel gesture = gestureMapping.getGestures((HandSign.SIGN) postureKeySelection.getSelectionModel()
                     .getSelectedItem())
                     .get(trainedGesturesToSelect.getSelectionModel()
                           .getSelectedIndex());

               // TODO
               //               final List<Pair<Double, List<Double>>> frequencies = gesture.getFrequencies();
               //               gestureTrainedGraph.createChart(frequencies, gesture.getAxesNames());
            });

      selectGestureContainer.getChildren()
            .addAll(selectGestureLabel, postureKeySelection, trainedGesturesToSelect, refeshGestureMappingsBtn, deleteGestureMappingBtn);

      gestureTrainedGraph = new LineGraphPlotWidget(new String[] { "x-Axis", "y-Axis", "z-Axis" }, "trained gesture plot", "time in s (x)", "acceleration in mG (y)");

      matchingGestureGraph = new LineGraphPlotWidget(new String[] { "x-Axis", "y-Axis",
            "z-Axis" }, "measured gesture plot", "time in s (x)", "acceleration in mG (y)");
      matchingGestureGraph.setRangeSaveVisibility(true);

      parentVBox.setSpacing(20);

      VBox addGestureContainer = new VBox(10);
      final HBox gestureTextContainer = new HBox(10);
      newGestureName = new TextField();
      gestureTextContainer.getChildren()
            .addAll(new Label("name:"), newGestureName);

      final Button addGestureBtn = new Button("save");
      addGestureBtn.setOnAction(event -> {
         Pair<Double, Double> ranges = matchingGestureGraph.getRangeTimes();
         addNewGestureMapping(ranges.getKey(), ranges.getValue());
      });

      addGestureContainer.getChildren()
            .addAll(new Label("add data as new gestures"), gestureTextContainer,
                  addGestureBtn);

      HBox controlPanelContainer = new HBox(10);
      controlPanelContainer.getChildren()
            .addAll(selectGestureContainer, serialPortWidget, fileChooserWidget, postureWidget);

      final HBox buttonContainer = new HBox(10);
      buttonContainer.getChildren()
            .addAll(adjustRangeBtn, clearBtn, collectAccDataBtn);

      this.postureSelection = new ComboBox<>(FXCollections.observableArrayList(HandSign.SIGN.values()));
      this.postureSelection.setPromptText("Posture");

      VBox gestureMappingControlContainer = new VBox(20);
      gestureMappingControlContainer.getChildren()
            .addAll(buttonContainer, this.postureSelection, addGestureContainer);

      final HBox gestureGraphContainer = new HBox(10);
      gestureGraphContainer.getChildren()
            .addAll(gestureTrainedGraph.getElement(), matchingGestureGraph.getElement(), gestureMappingControlContainer);

      final HBox gestureComparisonControlContainer = new HBox(20);
      final Button fetchBtn = new Button("fetch DTW path");

      distanceLabel = new Label("");
      gestureComparisonControlContainer.getChildren()
            .addAll(fetchBtn, new Label("Distance: "), distanceLabel);

      parentVBox.getChildren()
            .addAll(controlPanelContainer, gestureGraphContainer, gestureComparisonControlContainer);

      parentVBox.setSpacing(20);

      fetchBtn.setOnAction(event -> {
         final GestureMappingEntity gestureMapping = getGestureMapping();

         final HandSign.SIGN selectedPosture = (HandSign.SIGN) postureKeySelection.getSelectionModel()
               .getSelectedItem();

         final List<GestureModel> gestures = gestureMapping.getGestures(selectedPosture);
         final List<Pair<Double, List<Double>>> trainedGestureFrequencies = gestures
               .get(trainedGesturesToSelect.getSelectionModel()
                     .getSelectedIndex())
               .getAccelerations()
               .getAmplitudes();
         presenter.fetchDtwPath(trainedGestureFrequencies, matchedGestureModel);
      });

      root.setCenter(parentVBox);
      primaryStage.show();
   }

   private void clearGraphs() {
      matchingGestureGraph.clearData();
      matchedGestureModel.clear();
   }

   private void updateGestureMappings() {
      trainedGestures.clear();
      trainedGestures.addAll(getTrainedGesturesNames((HandSign.SIGN) postureKeySelection.getSelectionModel()
            .getSelectedItem()));
      trainedGesturesToSelect.setItems(trainedGestures);
      trainedGesturesToSelect.getSelectionModel()
            .selectFirst();
   }

   private void deleteGestureMapping() {
      final HandSign.SIGN posture = (HandSign.SIGN) postureKeySelection.getSelectionModel()
            .getSelectedItem();

      final String gestureName = trainedGesturesToSelect.getSelectionModel()
            .getSelectedItem();

      if (posture == null || gestureName == null || gestureName.isEmpty()) {
         return;
      }
      gestureMapping = gestureMappingDao.findAll();

      final List<GestureModel> gestures = gestureMapping.getGestures(posture);
      for (int i = 0; i < gestures.size(); i++) {
         final GestureModel gestureModel = gestures.get(i);
         if (gestureModel.getName()
               .equals(gestureName)) {
            gestures.remove(i);
            // we can use for loop coz of the break
            break;
         }
      }

      gestureMappingDao.update(gestureMapping);

   }

   // TODO move to presenter
   private void addNewGestureMapping(final Double from, final Double to) {
      final GestureMappingEntity gestureMapping = getGestureMapping();

      //      int fromIndex = findIndexForTime(matchedGestureModel, from);
      //      int toIndex = findIndexForTime(matchedGestureModel, to);
      //      matchedGestureModel = matchedGestureModel.subList(fromIndex, toIndex);

      final HandSign.SIGN selectedPosture = postureSelection.getSelectionModel()
            .getSelectedItem();

      // TODO
      //      final GestureModel gestureModel = new GestureModel(newGestureName.getText(), matchedGestureModel, matchingGestureGraph.getLegendNames());
      //      gestureMapping.addGestureModel(selectedPosture, gestureModel);

      saveGestureMapping(gestureMapping);
   }

   private void saveGestureMapping(final GestureMappingEntity gestureMapping) {
      gestureMappingDao.update(gestureMapping);
   }

   private int findIndexForTime(final List<Pair<Double, List<Double>>> matchedGestureModel, final Double time) {
      int index = 0;
      for (int i = 0; i < matchedGestureModel.size(); i++) {
         if (matchedGestureModel.get(i)
               .getKey()
               .equals(time)) {
            index = i;
            break;
         }
      }
      return index;
   }

   private GestureMappingEntity getGestureMapping() {
      if (gestureMapping == null) {
         return gestureMappingDao.findAll();
      }
      return gestureMapping;
   }

   private GestureMappingPresenter presenter;

   @Override
   public void setPresenter(final GestureMappingPresenter presenter) {
      this.presenter = presenter;
   }

   @Override
   public void updateGraphs(final long timePassed, final Double xValue, final Double yValue, final Double zValue) {
      if (!this.isCollectingAccData) {
         return;
      }

      double timePassedInSecs = ((double) timePassed) / 1000;

      List<Number> values = new ArrayList<>();
      values.add(xValue);
      values.add(yValue);
      values.add(zValue);

      matchingGestureGraph.addData(timePassed, values);

      final LinkedList<Double> trainedGraphModelValues = new LinkedList<>();
      trainedGraphModelValues.add(xValue.doubleValue());
      trainedGraphModelValues.add(yValue.doubleValue());
      trainedGraphModelValues.add(zValue.doubleValue());

      matchedGestureModel.add(new Pair<>((double) timePassed, trainedGraphModelValues));

   }

   @Override
   public void showDistance(final double distance) {
      distanceLabel.setText(String.valueOf(distance));
   }

   @Override
   public void updateBendValues(final BendMessage bendMessage) {
      this.postureWidget.changeAllFingerSliderValues(bendMessage.getThumbFlexValue() / 100, bendMessage.getIndexFlexValue() / 100, bendMessage.getMiddleFlexValue() / 100, bendMessage.getRingFlexValue() / 100, bendMessage.getPinkyFlexValue() / 100);
   }

   @Override
   public void updatePosture(final HandSign.SIGN posture) {
      this.postureWidget.showHandSign(posture.name());
   }

   @Override
   public void showCapturedGesture(final HandSign.SIGN posture, final List<Pair<Double, List<Double>>> capturedGestureFrequencies) {
      if (!collectAccDataBtn.isSelected()) {
         return;
      }

      final List<String> legendNames = new ArrayList<>();
      legendNames.add("x");
      legendNames.add("y");
      legendNames.add("z");
      matchingGestureGraph.createChart(capturedGestureFrequencies, legendNames);

      matchedGestureModel = new ArrayList<>(capturedGestureFrequencies);

      Platform.runLater(() -> collectAccDataBtn.setSelected(false));
   }

   private SerialPortWidget serialPortWidget;
   private LineGraphPlotWidget gestureTrainedGraph;
   private LineGraphPlotWidget matchingGestureGraph;

   public List<String> getTrainedGesturesNames(HandSign.SIGN key) {
      List<String> names = new ArrayList<>();

      final List<GestureModel> gestures = getGestureMapping().getGestures(key);
      for (GestureModel gesture : gestures) {
         names.add(gesture.getName());
      }
      return names;
   }

   private TextField newGestureName;
   private ObjectMapper mapper;
   private ComboBox<String> trainedGesturesToSelect;
   private ObservableList<String> trainedGestures;
   private Label distanceLabel;
   private GestureMappingEntity gestureMapping;
   private FingerDisplayWidget postureWidget;
   private ToggleButton collectAccDataBtn;
   private boolean isCollectingAccData;
   private ComboBox<HandSign.SIGN> postureSelection;
   private ObservableList<HandSign.SIGN> posturesObservableList;
   private ComboBox postureKeySelection;
   private GestureMappingDao gestureMappingDao;
}
