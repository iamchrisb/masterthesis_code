package de.cb.ma.depricated.control.debug;

import de.cb.ma.core.imu.serial.messages.debug.LinearAccelerationMessage;
import de.cb.ma.depricated.debugging.linAccel.LinearAccelerationDataView;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import jssc.SerialPort;

/**
 * Hello world!
 */
public class LinearAccelerationDataActivity implements DataPlotPresenter {

   private SerialPort serialPort;

   private LinearAccelerationDataView view;

   public LinearAccelerationDataActivity(LinearAccelerationDataView view) {
      this.view = view;
   }

   public void startSerial(String portName, int baudRate) {
      timeOnSerialLoggingStart = System.currentTimeMillis();

      serialMessageHandler = new SerialMessageHandler(portName, baudRate, serialMessage -> {
         if (serialMessage instanceof LinearAccelerationMessage) {
            handleSerialMessage((LinearAccelerationMessage) serialMessage);
            return;
         }
      });

      serialMessageHandler.startSerial();
   }

   private void handleSerialMessage(final LinearAccelerationMessage serialMessage) {
      LinearAccelerationMessage linearAccelerationMessage = serialMessage;

      System.out.println(linearAccelerationMessage);
      final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      view.updateAccelerationGraphs(currentTimeMillis, Integer.valueOf(linearAccelerationMessage.getxAccelValue()), Integer.valueOf(linearAccelerationMessage.getyAccelValue()), Integer.valueOf(linearAccelerationMessage.getzAccelValue()));
   }

   @Override
   public void startSerial(final String filePath) {
      timeOnSerialLoggingStart = 0;

      serialMessageHandler = new SerialMessageHandler(filePath, serialMessage -> {
         if (serialMessage instanceof LinearAccelerationMessage) {
            LinearAccelerationMessage linearAccelerationMessage = (LinearAccelerationMessage) serialMessage;
            timeOnSerialLoggingStart += 100;

            view.updateAccelerationGraphs(timeOnSerialLoggingStart, Integer.valueOf(linearAccelerationMessage.getxAccelValue()), Integer.valueOf(linearAccelerationMessage.getyAccelValue()), Integer.valueOf(linearAccelerationMessage.getzAccelValue()));
            return;
         }
      });
   }

   @Override
   public void stopSerial() {
      serialMessageHandler.closeSerialPort();
   }

   private double stringToSliderValue(String s) {
      return Double.valueOf(s) / 100;
   }

   private long timeOnSerialLoggingStart;
   private SerialMessageHandler serialMessageHandler;
}
