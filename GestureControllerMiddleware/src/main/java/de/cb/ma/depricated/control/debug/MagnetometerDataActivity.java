package de.cb.ma.depricated.control.debug;

import de.cb.ma.core.imu.serial.messages.debug.MagnetometerMessage;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import de.cb.ma.depricated.debugging.magnetometer.MagnetometerDataView;
import de.cb.ma.util.Constants;
import jssc.SerialPort;

import java.util.UUID;

public class MagnetometerDataActivity implements DataPlotPresenter {

   private SerialPort serialPort;

   private MagnetometerDataView view;

   public MagnetometerDataActivity(MagnetometerDataView view) {
      this.view = view;
   }

   public void startSerial(String portName, int baudRate) {
      timeOnSerialLoggingStart = System.currentTimeMillis();

      int mode = view.getMode();

      String filePath = null;

      final UUID uuid = UUID.randomUUID();

      if (mode == 1) {
         filePath = Constants.MAG_BIAS_SCALE_CALIBRATION_DATA + "_" + uuid;
      }

      // TODO dont forget the filepath to logg the data all the time
      // TODO create new file with UUID

      serialMessageHandler = new SerialMessageHandler(portName, baudRate, serialMessage -> {
         if (serialMessage instanceof MagnetometerMessage) {
            handleSerialMessage((MagnetometerMessage) serialMessage);
            return;
         }
      });

      serialMessageHandler.startSerial();
   }

   private void handleSerialMessage(final MagnetometerMessage serialMessage) {
      MagnetometerMessage magnetometerMessage = serialMessage;

      final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      view.updateGraphs(currentTimeMillis, Integer.valueOf(magnetometerMessage.getxMagValue()), Integer.valueOf(magnetometerMessage.getyMagValue()), Integer.valueOf(magnetometerMessage.getzMagValue()));
   }

   @Override
   public void startSerial(final String filePath) {
      timeOnSerialLoggingStart = 0;

      serialMessageHandler = new SerialMessageHandler(filePath, serialMessage -> {
         if (serialMessage instanceof MagnetometerMessage) {
            MagnetometerMessage magnetometerMessage = (MagnetometerMessage) serialMessage;
            timeOnSerialLoggingStart += 100;
            view.updateGraphs(timeOnSerialLoggingStart, Integer.valueOf(magnetometerMessage.getxMagValue()), Integer.valueOf(magnetometerMessage.getyMagValue()), Integer.valueOf(magnetometerMessage.getzMagValue()));
            return;
         }
      });
   }

   @Override
   public void stopSerial() {
      serialMessageHandler.closeSerialPort();
   }

   private double stringToSliderValue(String s) {
      return Double.valueOf(s) / 100;
   }

   private long timeOnSerialLoggingStart;
   private SerialMessageHandler serialMessageHandler;
}
