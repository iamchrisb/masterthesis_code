package de.cb.ma.depricated.debugging.gyro;

import de.cb.ma.core.widgets.file.FileChooserWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;

public class GyroPeakDataViewImpl implements GyroPeakDataView {

   private DataPlotPresenter presenter;

   private LineGraphPlotWidget xPeakValue;
   private LineGraphPlotWidget yPeakValue;
   private LineGraphPlotWidget zPeakValue;

   SerialPortWidget serialPortWidget;

   public void setPresenter(DataPlotPresenter presenter) {
      this.presenter = presenter;
   }

   @Override
   public void updateGyroGraphs(final long timePassed, final Integer yawAngle, final Integer pitchAngle, final Integer rollAngle) {
      float timePassedInSecs = ((float) timePassed) / 1000;
      xPeakValue.addData(timePassedInSecs, yawAngle);
      yPeakValue.addData(timePassedInSecs, pitchAngle);
      zPeakValue.addData(timePassedInSecs, rollAngle);
   }

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Data Plotting");

      VBox parentVBox = new VBox();
      parentVBox.setSpacing(20);

      serialPortWidget = new SerialPortWidget("Connect to Serialport", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            presenter.startSerial(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            presenter.stopSerial();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      FileChooserWidget fileChooserWidget = new FileChooserWidget("Open serial messages", primaryStage, new FileChooserWidget.Callback() {

         @Override
         public void onFileSelected(final File file) {
            presenter.startSerial(file.getAbsolutePath());
         }
      });

      HBox controlPanelContainer = new HBox(10);

      controlPanelContainer.getChildren()
            .addAll(serialPortWidget, fileChooserWidget);

      parentVBox.getChildren()
            .addAll(controlPanelContainer);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      /**
       *  add graphs
       */
      xPeakValue = new LineGraphPlotWidget("x-Axis", "Sensor data plotting", "Time in s", "X-Peak in deg/s");
      yPeakValue = new LineGraphPlotWidget("y-Axis", "Sensor data plotting", "Time in s", "Y-Peak in deg/s");
      zPeakValue = new LineGraphPlotWidget("z-Axis", "Sensor data plotting", "Time in s", "z-Peak in deg/s");

      HBox gyroGraphContainer = new HBox();

      parentVBox.getChildren()
            .addAll(gyroGraphContainer);

      gyroGraphContainer.getChildren()
            .addAll(xPeakValue.getElement(), yPeakValue.getElement(), zPeakValue.getElement());

      Button clear = new Button("Clear Graphs");

      parentVBox.getChildren()
            .add(clear);

      clear.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(final ActionEvent event) {
            xPeakValue.clearData();
            yPeakValue.clearData();
            zPeakValue.clearData();
         }
      });

      final URL resource = LineGraphPlotWidget.class.getResource("styles.css");

      final String styles = resource
            .toExternalForm();

      final Scene scene = primaryStage.getScene();

      scene.getStylesheets()
            .add(styles);

      primaryStage.show();
   }
}
