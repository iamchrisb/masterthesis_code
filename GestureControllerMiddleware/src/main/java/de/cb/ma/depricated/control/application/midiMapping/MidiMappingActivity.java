package de.cb.ma.depricated.control.application.midiMapping;

import de.cb.ma.core.database.dao.GestureMappingDao;
import de.cb.ma.core.database.model.GestureMappingEntity;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.depricated.app.midiMapping.MidiMappingView;
import de.cb.ma.util.MidiHelper;

import java.util.List;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

/**
 * Hello world!
 */
public class MidiMappingActivity implements MidiMappingView.MidiMappingPresenter {

   private MidiMappingView view;
   private final MidiHelper midiHelper;
   private MidiDevice currentMidiDevice;
   private Receiver receiver;

   public MidiMappingActivity(MidiMappingView view) {
      this.view = view;
      this.midiHelper = new MidiHelper();

      try {
         receiver = MidiSystem.getReceiver();
      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      }

      // TODO
      //gestureMappingDao = new GestureMappingDaoFileImpl();
      //gestureMapping = gestureMappingDao.findAll();
   }

   @Override
   public void sendMidiMessage(int status, int channel, int data1, int data2) {

      try {
         if (currentMidiDevice != null) {
            receiver = currentMidiDevice.getReceiver();

            System.out.println("using : " + currentMidiDevice.getDeviceInfo()
                  .getName() + " to send a midi message");
         }

         final ShortMessage shortMessage = new ShortMessage();
         shortMessage.setMessage(status, channel, data1, data2);
         receiver.send(shortMessage, -1);

      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      } catch (InvalidMidiDataException e) {
         e.printStackTrace();
      } catch (NullPointerException e) {
         e.printStackTrace();
      }

      //      final ShortMessage changeVolume = new ShortMessage();
      //      changeVolume.setMessage(ShortMessage.CONTROL_CHANGE, 7, 0, 0);
      //      receiver.send(shortMessage, -1);
   }

   @Override
   public void playNote(int channel, int pitch, int velocity) {

      try {
         if (currentMidiDevice != null) {
            receiver = currentMidiDevice.getReceiver();

            System.out.println("using : " + currentMidiDevice.getDeviceInfo()
                  .getName() + " to send a midi message");
         }

         final ShortMessage shortMessage = new ShortMessage();
         shortMessage.setMessage(ShortMessage.NOTE_ON, channel, pitch, velocity);
         receiver.send(shortMessage, -1);

      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      } catch (InvalidMidiDataException e) {
         e.printStackTrace();
      } catch (NullPointerException e) {
         e.printStackTrace();
      }
   }

   @Override
   public void useMidiDevice(int index) throws MidiUnavailableException {
      this.currentMidiDevice = midiHelper.getSystemMidiPort(index);
      if (!this.currentMidiDevice.isOpen()) {
         System.out.println("Open MIDI-Device: " + currentMidiDevice.getDeviceInfo()
               .getName());
         this.currentMidiDevice.open();
      }
      final List<Receiver> receivers = currentMidiDevice.getReceivers();
      for (Receiver receiver : receivers) {
         System.out.println(receiver);
      }
   }

   @Override
   public void saveDrumAction(final MidiMessageParameter drumAction, HandSign.SIGN key, String gestureName) {
      gestureMapping = gestureMappingDao.findAll();
      final List<GestureModel> gestures = gestureMapping.getGestures(key);
      for (int i = 0; i < gestures.size(); i++) {
         final GestureModel gestureModel = gestures.get(i);
         if (gestureModel.getName()
               .equals(gestureName)) {
            // TODO
            //            gestureModel.setMidiAction(drumAction);
            break;
         }
      }
      gestureMappingDao.update(gestureMapping);
      view.refresh();
   }

   @Override
   public void removeDrumAction(HandSign.SIGN posture, final String gestureId) {
      gestureMapping = gestureMappingDao.findAll();
      final List<GestureModel> gestures = gestureMapping.getGestures(posture);
      for (int i = 0; i < gestures.size(); i++) {
         final GestureModel gestureModel = gestures.get(i);
         if (gestureModel.getId()
               .equals(gestureId)) {
            // TODO
            //            gestureModel.setMidiAction(null);
            // we can use for loop coz of this break
            break;
         }
      }
      gestureMappingDao.update(gestureMapping);
      view.refresh();
   }

   private GestureMappingEntity gestureMapping;
   private GestureMappingDao gestureMappingDao;
}
