package de.cb.ma.depricated.control.debug;

import de.cb.ma.core.imu.serial.messages.application.OrientationMessage;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import de.cb.ma.depricated.debugging.orientation.OrientationDataView;
import jssc.SerialPort;

/**
 * Hello world!
 */
public class OrientationDataActivity implements DataPlotPresenter {

   private SerialPort serialPort;

   private OrientationDataView view;

   public OrientationDataActivity(OrientationDataView view) {
      this.view = view;
   }

   public void startSerial(String portName, int baudRate) {
      timeOnSerialLoggingStart = System.currentTimeMillis();

      serialMessageHandler = new SerialMessageHandler(portName, baudRate, serialMessage -> {
         if (serialMessage instanceof OrientationMessage) {
            handleSerialMessage((OrientationMessage) serialMessage);
            return;
         }
      });

      serialMessageHandler.startSerial();
   }

   private void handleSerialMessage(final OrientationMessage serialMessage) {
      OrientationMessage orientationMessage = serialMessage;

      final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      view.updateOrientationGraphs(currentTimeMillis, Integer.valueOf(orientationMessage.getYawValue()), Integer.valueOf(orientationMessage.getPitchValue()), Integer.valueOf(orientationMessage.getRollValue()));
   }

   @Override
   public void startSerial(final String filePath) {
      timeOnSerialLoggingStart = 0;

      serialMessageHandler = new SerialMessageHandler(filePath, serialMessage -> {
         if (serialMessage instanceof OrientationMessage) {
            OrientationMessage accelerationCommand = (OrientationMessage) serialMessage;
            timeOnSerialLoggingStart += 100;

            view.updateOrientationGraphs(timeOnSerialLoggingStart, Integer.valueOf(accelerationCommand.getYawValue()), Integer.valueOf(accelerationCommand.getPitchValue()), Integer.valueOf(accelerationCommand.getRollValue()));
            return;
         }
      });
   }

   @Override
   public void stopSerial() {
      serialMessageHandler.closeSerialPort();
   }

   private double stringToSliderValue(String s) {
      return Double.valueOf(s) / 100;
   }

   private long timeOnSerialLoggingStart;
   private SerialMessageHandler serialMessageHandler;
}
