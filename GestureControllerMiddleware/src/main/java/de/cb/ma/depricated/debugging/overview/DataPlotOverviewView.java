package de.cb.ma.depricated.debugging.overview;

import de.cb.ma.depricated.View;

public interface DataPlotOverviewView extends View {

   public interface DataPlotOverviewPresenter {
      void sendMidiMessage(int command, int channel, int data1, int data2);

      void startSerial(String COMM_PORT, int BAUD_RATE);

      void stopRecognition();

      void startSerial(String msgsPath);
   }

   void setPresenter(DataPlotOverviewPresenter presenter);

   void changeAllFingerSliderValues(double thumbValue, double indexValue, double middleValue, double ringValue, double pinkyValue);

   void changeShownHandsign(String currentHandSign);

   void updateGyroGraphs(long currentTimeMillis, Integer yawAngle, Integer pitchAngle, Integer rollAngle);

   void updateAccelerationGraphs(long timePassed, Double xAxisValue, Double yAxisValue, Double zAxisValue);

   void updateGravityGraphs(long timePassed, Integer xAxisValue, Integer yAxisValue, Integer zAxisValue);

   void updateLinearAccelerationyGraphs(long timePassed, Integer xAxisValue, Integer yAxisValue, Integer zAxisValue);

}
