package de.cb.ma.depricated.debugging.overview;

import de.cb.ma.core.widgets.file.FileChooserWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.core.widgets.analysis.FingerDisplayWidget;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;

public class DataPlottingOverviewViewImpl implements DataPlotOverviewView {

   public void setPresenter(DataPlotOverviewPresenter presenter) {
      this.presenter = presenter;
   }

   @Override
   public void changeAllFingerSliderValues(final double thumbValue, final double indexValue, final double middleValue, final double ringValue, final double pinkyValue) {
      fingerWidget.changeAllFingerSliderValues(thumbValue, indexValue, middleValue, ringValue, pinkyValue);
   }

   @Override
   public void changeShownHandsign(final String currentHandSign) {
      fingerWidget.showHandSign(currentHandSign);
   }

   @Override
   public void updateGyroGraphs(final long timePassed, final Integer yawAngle, final Integer pitchAngle, final Integer rollAngle) {
      float timePassedInSecs = ((float) timePassed) / 1000;
      final ArrayList<Number> numbers = new ArrayList<>();
      numbers.add(yawAngle);
      numbers.add(pitchAngle);
      numbers.add(rollAngle);

      orientationGraph.addData(timePassed, numbers);
   }

   @Override
   public void updateAccelerationGraphs(final long timePassed, final Double xAxisValue, final Double yAxisValue, final Double zAxisValue) {
      float timePassedInSecs = ((float) timePassed) / 1000;

      final ArrayList<Number> numbers = new ArrayList<>();
      numbers.add(xAxisValue);
      numbers.add(yAxisValue);
      numbers.add(zAxisValue);

      accelerationGraph.addData(timePassedInSecs, numbers);
   }

   @Override
   public void updateGravityGraphs(final long timePassed, final Integer xAxisValue, final Integer yAxisValue, final Integer zAxisValue) {
      float timePassedInSecs = ((float) timePassed) / 1000;

      final ArrayList<Number> numbers = new ArrayList<>();
      numbers.add(xAxisValue);
      numbers.add(yAxisValue);
      numbers.add(zAxisValue);

      gravityGraph.addData(timePassedInSecs, numbers);
   }

   @Override
   public void updateLinearAccelerationyGraphs(final long timePassed, final Integer xAxisValue, final Integer yAxisValue, final Integer zAxisValue) {
      float timePassedInSecs = ((float) timePassed) / 1000;

      final ArrayList<Number> numbers = new ArrayList<>();
      numbers.add(xAxisValue);
      numbers.add(yAxisValue);
      numbers.add(zAxisValue);

      linearAccelerationGraph.addData(timePassedInSecs, numbers);
   }

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Overview several data plotting");

      VBox parentVBox = new VBox();
      parentVBox.setSpacing(20);

      fingerWidget = new FingerDisplayWidget("Represents the finger positions");
      serialPortWidget = new SerialPortWidget("Connect to Serialport", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            presenter.startSerial(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            presenter.stopRecognition();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      new FileChooserWidget("Choose serial msgs", primaryStage, new FileChooserWidget.Callback() {
         @Override
         public void onFileSelected(final File file) {
            presenter.startSerial(file.getAbsolutePath());
         }
      });

      /** misc **/

      HBox controlPanelContainer = new HBox(10);

      controlPanelContainer.getChildren()
            .addAll(serialPortWidget, fingerWidget);

      parentVBox.getChildren()
            .addAll(controlPanelContainer);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      /**
       *  add graphs
       */
      orientationGraph = new LineGraphPlotWidget(new String[] { "Yaw", "Pitch", "Roll" }, "Orientation data plotting", "Time in s", "Yaw, pitch, roll in degree");
      accelerationGraph = new LineGraphPlotWidget(new String[] { "x-Axis", "y-Axis", "z-Axis" }, "Acceleration data plotting", "Time in s", "acceleration in mg");
      gravityGraph = new LineGraphPlotWidget(new String[] { "x-Axis", "y-Axis", "z-Axis" }, "Gravity data plotting", "Time in s", "acceleration in mg");
      linearAccelerationGraph = new LineGraphPlotWidget(new String[] { "x-Axis", "y-Axis", "z-Axis" }, "Linear acceleration data plotting", "Time in s", "acceleration in mg w/o gravity");

      HBox upperGraphContainer = new HBox();
      HBox lowerGraphContainer = new HBox();

      parentVBox.getChildren()
            .addAll(upperGraphContainer, lowerGraphContainer);

      upperGraphContainer.getChildren()
            .addAll(orientationGraph.getElement(), accelerationGraph.getElement());

      lowerGraphContainer.getChildren()
            .addAll(gravityGraph.getElement(), linearAccelerationGraph.getElement());

      Button clear = new Button("Clear Graphs");

      parentVBox.getChildren()
            .add(clear);

      clear.setOnAction(event -> {
         orientationGraph.clearData();
         accelerationGraph.clearData();
         gravityGraph.clearData();
         linearAccelerationGraph.clearData();
      });

      final URL resource = LineGraphPlotWidget.class.getResource("styles.css");

      final String styles = resource
            .toExternalForm();

      final Scene scene = primaryStage.getScene();

      scene.getStylesheets()
            .add(styles);

      primaryStage.show();
   }

   private DataPlotOverviewPresenter presenter;

   private LineGraphPlotWidget orientationGraph;

   private LineGraphPlotWidget accelerationGraph;

   private LineGraphPlotWidget gravityGraph;

   private LineGraphPlotWidget linearAccelerationGraph;

   FingerDisplayWidget fingerWidget;
   SerialPortWidget serialPortWidget;
}
