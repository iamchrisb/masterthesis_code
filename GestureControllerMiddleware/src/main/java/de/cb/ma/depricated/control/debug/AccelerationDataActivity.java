package de.cb.ma.depricated.control.debug;

import de.cb.ma.core.imu.serial.messages.debug.AccelerationMessage;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import de.cb.ma.depricated.debugging.accel.AccelerationDataView;
import jssc.SerialPort;

/**
 * Hello world!
 */
public class AccelerationDataActivity implements DataPlotPresenter {

   private SerialPort serialPort;

   private AccelerationDataView view;

   public AccelerationDataActivity(AccelerationDataView view) {
      this.view = view;
   }

   public void startSerial(String portName, int baudRate) {
      timeOnSerialLoggingStart = System.currentTimeMillis();

      serialMessageHandler = new SerialMessageHandler(portName, baudRate, serialMessage -> {
         if (serialMessage instanceof AccelerationMessage) {
            handleSerialMessage((AccelerationMessage) serialMessage);
            return;
         }
      });

      serialMessageHandler.startSerial();
   }

   private void handleSerialMessage(final AccelerationMessage serialMessage) {
      AccelerationMessage accelerationMessage = serialMessage;
      final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      view.updateAccelerationGraphs(currentTimeMillis, accelerationMessage.getxAccelValue(), accelerationMessage.getyAccelValue(), accelerationMessage.getzAccelValue(), accelerationMessage.getFilteredX(), accelerationMessage.getFilteredY(), accelerationMessage.getFilteredZ());
   }

   @Override
   public void startSerial(final String filePath) {
      timeOnSerialLoggingStart = 0;

      serialMessageHandler = new SerialMessageHandler(filePath, serialMessage -> {
         if (serialMessage instanceof AccelerationMessage) {
            AccelerationMessage accelerationMessage = (AccelerationMessage) serialMessage;
            timeOnSerialLoggingStart += 100;

            view.updateAccelerationGraphs(timeOnSerialLoggingStart, accelerationMessage.getxAccelValue(), accelerationMessage.getyAccelValue(), accelerationMessage.getzAccelValue(), accelerationMessage.getFilteredX(), accelerationMessage.getFilteredY(), accelerationMessage.getFilteredZ());
            return;
         }
      });
   }

   @Override
   public void stopSerial() {
      serialMessageHandler.closeSerialPort();
   }

   private double stringToSliderValue(String s) {
      return Double.valueOf(s) / 100;
   }

   private long timeOnSerialLoggingStart;
   private SerialMessageHandler serialMessageHandler;
}
