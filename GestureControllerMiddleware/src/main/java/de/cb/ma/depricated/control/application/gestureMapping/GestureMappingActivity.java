package de.cb.ma.depricated.control.application.gestureMapping;

import de.cb.ma.core.imu.DynamicTimeWarpComparator;
import de.cb.ma.core.imu.GestureComparatorAlgo;
import de.cb.ma.core.imu.serial.messages.debug.AccelerationMessage;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.depricated.app.gestureMapping.GestureMappingView;
import de.cb.ma.util.Pair;
import jssc.SerialPort;

import java.util.List;

import javax.sound.midi.Receiver;

import com.fastdtw.util.Distances;

public class GestureMappingActivity implements GestureMappingView.GestureMappingPresenter {

   private SerialPort serialPort;

   private long timeOnSerialLoggingStart;

   private Receiver receiver;

   public GestureMappingActivity(GestureMappingView view) {
      this.view = view;
   }

   private GestureMappingView view;

   @Override
   public void sendMidiMessage(final int command, final int channel, final int data1, final int data2) {

   }

   @Override
   public void startGestureDetection(final String COMM_PORT, final int BAUD_RATE) {

      //            view.showCapturedGesture(posture, recentCapturedAmplitudeValues);
   }

   //         @Override
   //         public void onBendMessageReceived(final BendMessage bendMessage) {
   //            view.updateBendValues(bendMessage);
   //         }

   //      timeOnSerialLoggingStart = System.currentTimeMillis();
   //
   //      final HandPostureDetector handPostureDetector = new HandPostureDetector(0.3, new HandPostureDetector.Callback() {
   //         @Override
   //         public void onPostureDetected(final HandPosture.POSTURES posture, final Integer[] binaryPattern) {
   //            view.updateBendValues(posture);
   //         }
   //      });
   //
   //      serialMessageHandler = new SerialMessageHandler(Constants.LATEST_GESTURE, COMM_PORT, BAUD_RATE, serialMessage -> {
   //         if (serialMessage instanceof AccelerationMessage) {
   //            AccelerationMessage accelerationMessage = (AccelerationMessage) serialMessage;
   //            final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
   //            view.updateGraphs(currentTimeMillis, accelerationMessage.getFilteredX(), accelerationMessage.getFilteredY(), accelerationMessage.getFilteredZ());
   //            return;
   //         }
   //         if (serialMessage instanceof BendMessage) {
   //            BendMessage bendMessage = (BendMessage) serialMessage;
   //            //            final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
   //            view.updateBendValues(bendMessage);
   //            handPostureDetector.updateValues(bendMessage.getThumbFlexValue(), bendMessage.getIndexFlexValue(), bendMessage.getMiddleFlexValue(), bendMessage.getRingFlexValue(), bendMessage.getPinkyFlexValue());
   //            return;
   //         }
   //      });
   //
   //      serialMessageHandler.startGestureDetection();

   @Override
   public void stopSerial() {

   }

   @Override
   public void startGestureDetection(final String filePath) {
      timeOnSerialLoggingStart = 0;

      serialMessageHandler = new SerialMessageHandler(filePath, serialMessage -> {
         if (serialMessage instanceof AccelerationMessage) {
            AccelerationMessage accelerationCommand = (AccelerationMessage) serialMessage;
            timeOnSerialLoggingStart += 10;

            view.updateGraphs(timeOnSerialLoggingStart, accelerationCommand.getFilteredX(), accelerationCommand.getFilteredY(), accelerationCommand.getFilteredZ());
            return;
         }
      });
   }

   @Override
   public void fetchDtwPath(List<Pair<Double, List<Double>>> firstGesture, List<Pair<Double, List<Double>>> secondGesture) {
      final GestureComparatorAlgo gestureComperator = new DynamicTimeWarpComparator(Distances.EUCLIDEAN_DISTANCE, DynamicTimeWarpComparator.MODE.DTW);
      final double distance = gestureComperator.compare(firstGesture, secondGesture);
      view.showDistance(distance);
      //      view.showWarpGraph(timeWarpInfo.getPath().get(1))
   }

   @Override
   public void muteSerial(final boolean enabled) {

   }

   private SerialMessageHandler serialMessageHandler;
}
