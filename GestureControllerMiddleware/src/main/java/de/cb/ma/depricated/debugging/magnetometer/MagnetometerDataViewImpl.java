package de.cb.ma.depricated.debugging.magnetometer;

import de.cb.ma.core.widgets.file.FileChooserWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.analysis.charts.DotPlotWidget;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;

public class MagnetometerDataViewImpl implements MagnetometerDataView {

   public void setPresenter(DataPlotPresenter presenter) {
      this.presenter = presenter;
   }

   @Override
   public void updateGraphs(final long timePassed, final Integer xAxisValue, final Integer yAxisValue, final Integer zAxisValue) {
      float timePassedInSecs = ((float) timePassed) / 1000;

      int[] mXY = new int[] { xAxisValue, yAxisValue };
      int[] mXZ = new int[] { xAxisValue, zAxisValue };
      int[] mYZ = new int[] { yAxisValue, zAxisValue };

      final int[][] scatterData = new int[3][2];

      scatterData[0] = mXY;
      scatterData[1] = mXZ;
      scatterData[2] = mYZ;

      dotPlottingWidget.addData(scatterData);
   }

   @Override
   public int getMode() {
      return 0;
   }

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Magnetic field calibration");

      VBox parentVBox = new VBox();
      parentVBox.setSpacing(20);

      serialPortWidget = new SerialPortWidget("Connect to Serialport", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            presenter.startSerial(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            presenter.stopSerial();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      FileChooserWidget fileChooserWidget = new FileChooserWidget("Choose file", primaryStage, new FileChooserWidget.Callback() {
         @Override
         public void onFileSelected(final File file) {
            presenter.startSerial(file.getAbsolutePath());
         }
      });

      /** misc **/

      HBox controlPanelContainer = new HBox(10);

      controlPanelContainer.getChildren()
            .addAll(serialPortWidget, fileChooserWidget);

      parentVBox.getChildren()
            .addAll(controlPanelContainer);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      HBox magGraphContainer = new HBox();
      Button clearBtn = new Button("Clear Graphs");

      dotPlottingWidget = new DotPlotWidget(new String[] { "mXY", "mXZ", "mYZ" }, "Calibrate magnetometer", "", "magnetic field in mG");

      parentVBox.getChildren()
            .addAll(dotPlottingWidget, clearBtn);

      dotPlottingWidget.setWidth(800);

      clearBtn.setOnAction(event -> dotPlottingWidget.clearData());

      final URL resource = LineGraphPlotWidget.class.getResource("styles.css");

      final String styles = resource
            .toExternalForm();

      final Scene scene = primaryStage.getScene();

      scene.getStylesheets()
            .add(styles);

      primaryStage.show();
   }

   private ScatterChart createPlot() {
      final NumberAxis xAxis = new NumberAxis(0, 10, 1);
      final NumberAxis yAxis = new NumberAxis(-100, 500, 100);
      final ScatterChart<Number, Number> sc = new
            ScatterChart<>(xAxis, yAxis);
      xAxis.setLabel("Age (years)");
      yAxis.setLabel("Returns to date");
      sc.setTitle("Investment Overview");

      XYChart.Series series1 = new XYChart.Series();
      series1.setName("Equities");

      series1.getData()
            .add(new XYChart.Data(4.2, 193.2));
      series1.getData()
            .add(new XYChart.Data(2.8, 33.6));
      series1.getData()
            .add(new XYChart.Data(6.2, 24.8));
      series1.getData()
            .add(new XYChart.Data(1, 14));
      series1.getData()
            .add(new XYChart.Data(1.2, 26.4));
      series1.getData()
            .add(new XYChart.Data(4.4, 114.4));
      series1.getData()
            .add(new XYChart.Data(8.5, 323));
      series1.getData()
            .add(new XYChart.Data(6.9, 289.8));
      series1.getData()
            .add(new XYChart.Data(9.9, 287.1));
      series1.getData()
            .add(new XYChart.Data(0.9, -9));
      series1.getData()
            .add(new XYChart.Data(3.2, 150.8));
      series1.getData()
            .add(new XYChart.Data(4.8, 20.8));
      series1.getData()
            .add(new XYChart.Data(7.3, -42.3));
      series1.getData()
            .add(new XYChart.Data(1.8, 81.4));
      series1.getData()
            .add(new XYChart.Data(7.3, 110.3));
      series1.getData()
            .add(new XYChart.Data(2.7, 41.2));

      XYChart.Series series2 = new XYChart.Series();
      series2.setName("Mutual funds");

      series2.getData()
            .add(new XYChart.Data(5.2, 229.2));
      series2.getData()
            .add(new XYChart.Data(2.4, 37.6));
      series2.getData()
            .add(new XYChart.Data(3.2, 49.8));
      series2.getData()
            .add(new XYChart.Data(1.8, 134));
      series2.getData()
            .add(new XYChart.Data(3.2, 236.2));
      series2.getData()
            .add(new XYChart.Data(7.4, 114.1));
      series2.getData()
            .add(new XYChart.Data(3.5, 323));
      series2.getData()
            .add(new XYChart.Data(9.3, 29.9));
      series2.getData()
            .add(new XYChart.Data(8.1, 287.4));

      sc.getData()
            .addAll(series1, series2);
      return sc;
   }

   private DataPlotPresenter presenter;

   SerialPortWidget serialPortWidget;
   private DotPlotWidget dotPlottingWidget;
}
