package de.cb.ma.depricated.app.midiMapping;

import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.depricated.View;

import javax.sound.midi.MidiUnavailableException;

public interface MidiMappingView extends View {

   public interface MidiMappingPresenter {
      void sendMidiMessage(int command, int channel, int data1, int data2);

      //      void startGestureDetection(String COMM_PORT, int BAUD_RATE);

      void playNote(int channel, int pitch, int velocity);

      void useMidiDevice(int selectedIndex) throws MidiUnavailableException;

      void saveDrumAction(MidiMessageParameter drumAction, HandSign.SIGN key, String gestureId);

      void removeDrumAction(HandSign.SIGN posture, String gestureId);

      //      void stopSerial();
   }

   void setPresenter(MidiMappingPresenter presenter);

   void refresh();
}
