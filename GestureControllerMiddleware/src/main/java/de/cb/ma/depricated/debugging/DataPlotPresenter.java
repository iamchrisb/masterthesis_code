package de.cb.ma.depricated.debugging;

public interface DataPlotPresenter {

   void startSerial(String COMM_PORT, int BAUD_RATE);

   void startSerial(String filePath);

   void stopSerial();
}
