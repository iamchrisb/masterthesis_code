package de.cb.ma.depricated.debugging.linAccel;

import de.cb.ma.depricated.View;
import de.cb.ma.depricated.debugging.DataPlotPresenter;

public interface LinearAccelerationDataView extends View {

   void setPresenter(DataPlotPresenter presenter);

   void updateAccelerationGraphs(long timePassed, Integer xAxisValue, Integer yAxisValue, Integer zAxisValue);
}
