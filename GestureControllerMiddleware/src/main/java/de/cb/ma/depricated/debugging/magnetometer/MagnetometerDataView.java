package de.cb.ma.depricated.debugging.magnetometer;

import de.cb.ma.depricated.View;
import de.cb.ma.depricated.debugging.DataPlotPresenter;

public interface MagnetometerDataView extends View {

   void setPresenter(DataPlotPresenter presenter);

   void updateGraphs(long timePassed, Integer xAxisValue, Integer yAxisValue, Integer zAxisValue);

   int getMode();
}
