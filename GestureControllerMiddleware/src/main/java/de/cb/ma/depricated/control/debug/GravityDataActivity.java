package de.cb.ma.depricated.control.debug;

import de.cb.ma.core.imu.serial.messages.debug.GravityMessage;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import de.cb.ma.depricated.debugging.gravity.GravityDataView;
import jssc.SerialPort;

/**
 * Hello world!
 */
public class GravityDataActivity implements DataPlotPresenter {

   private SerialPort serialPort;

   private GravityDataView view;

   public GravityDataActivity(GravityDataView view) {
      this.view = view;
   }

   public void startSerial(String portName, int baudRate) {
      timeOnSerialLoggingStart = System.currentTimeMillis();

      serialMessageHandler = new SerialMessageHandler(portName, baudRate, serialMessage -> {
         if (serialMessage instanceof GravityMessage) {
            handleSerialMessage((GravityMessage) serialMessage);
            return;
         }
      });

      serialMessageHandler.startSerial();
   }

   private void handleSerialMessage(final GravityMessage serialMessage) {
      GravityMessage gravityMessage = serialMessage;

      final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      view.updateAccelerationGraphs(currentTimeMillis, Integer.valueOf(gravityMessage.getxValue()), Integer.valueOf(gravityMessage.getyValue()), Integer.valueOf(gravityMessage.getzValue()));
   }

   @Override
   public void startSerial(final String filePath) {
      timeOnSerialLoggingStart = 0;

      serialMessageHandler = new SerialMessageHandler(filePath, serialMessage -> {
         if (serialMessage instanceof GravityMessage) {
            GravityMessage gravityMessage = (GravityMessage) serialMessage;
            timeOnSerialLoggingStart += 100;

            view.updateAccelerationGraphs(timeOnSerialLoggingStart, Integer.valueOf(gravityMessage.getxValue()), Integer.valueOf(gravityMessage.getyValue()), Integer.valueOf(gravityMessage.getzValue()));
            return;
         }
      });
   }

   @Override
   public void stopSerial() {
      serialMessageHandler.closeSerialPort();
   }

   private double stringToSliderValue(String s) {
      return Double.valueOf(s) / 100;
   }

   private long timeOnSerialLoggingStart;
   private SerialMessageHandler serialMessageHandler;
}
