package de.cb.ma.depricated.debugging.orientation;

import de.cb.ma.depricated.View;
import de.cb.ma.depricated.debugging.DataPlotPresenter;

public interface OrientationDataView extends View {

   void setPresenter(DataPlotPresenter presenter);

   void updateOrientationGraphs(long timePassed, Integer xAxisValue, Integer yAxisValue, Integer zAxisValue);
}
