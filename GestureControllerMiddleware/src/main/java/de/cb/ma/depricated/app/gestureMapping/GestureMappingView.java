package de.cb.ma.depricated.app.gestureMapping;

import de.cb.ma.core.imu.serial.messages.application.BendMessage;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.util.Pair;
import de.cb.ma.depricated.View;

import java.util.List;

public interface GestureMappingView extends View {

   public interface GestureMappingPresenter {
      void sendMidiMessage(int command, int channel, int data1, int data2);

      void startGestureDetection(String COMM_PORT, int BAUD_RATE);

      void stopSerial();

      void startGestureDetection(String filePath);

      void fetchDtwPath(List<Pair<Double, List<Double>>> firstGesture, List<Pair<Double, List<Double>>> secondGesture);

      void muteSerial(boolean enabled);
   }

   List<Pair<Double, List<Double>>> getMatchedGestureModel();

   void setPresenter(GestureMappingPresenter presenter);

   void updateGraphs(long timeOnSerialLoggingStart, Double xValue, Double yValue, Double zValue);

   void showDistance(double distance);

   void updateBendValues(BendMessage bendMessage);

   void updatePosture(HandSign.SIGN posture);

   void showCapturedGesture(HandSign.SIGN posture, List<Pair<Double, List<Double>>> capturedGestureFrequencies);
}
