package de.cb.ma.depricated.app.midiMapping;

import de.cb.ma.depricated.MidiMappingTableWidget;
import de.cb.ma.depricated.AddDrumActionWidget;
import de.cb.ma.core.widgets.communication.MidiPortWidget;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.util.MidiHelper;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.ShortMessage;

public class MidiMappingViewImpl implements MidiMappingView {

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Midi Mapping");

      Button testMidiNoteBtn = new Button();
      testMidiNoteBtn.setText("Play Note");

      testMidiNoteBtn.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(ActionEvent event) {
            presenter.sendMidiMessage(ShortMessage.NOTE_ON, 0, 60, 93);
         }
      });

      VBox parentVBox = new VBox();

      parentVBox.setPadding(new Insets(10, 10, 10, 10));

      parentVBox.setSpacing(20);

      midiPortWidget = new MidiPortWidget("Choose a midi port", midiHelper, new MidiPortWidget.Callback() {
         @Override
         public void onMidiPortSelected(final int index) {
            try {
               presenter.useMidiDevice(index);
            } catch (MidiUnavailableException e) {
               e.printStackTrace();
            }
         }
      });

      AddDrumActionWidget addDrumActionWidget = new AddDrumActionWidget("Add new mappings for drums");
      addDrumActionWidget.setOnAction(event -> {
         final MidiMessageParameter drumAction = addDrumActionWidget.getCurrentDrumAction();
         final HandSign.SIGN selectedPosture = addDrumActionWidget.getSelectedPosture();
         final String selectedGestureName = addDrumActionWidget.getSelectedGestureName();
         presenter.saveDrumAction(drumAction, selectedPosture, selectedGestureName);
      });

      midiMappingWidget = new MidiMappingTableWidget("Map motions to MIDI", (index, action) -> {
         final int midiVelocity = action.getMidiVelocity();
         final int midiPitch = action.getMidiPitch();
         final int midiChannel = action.getMidiChannel();

         presenter.playNote(midiChannel, midiPitch, midiVelocity);
      }, (index, model) -> {
         presenter.removeDrumAction(model.getPosture(), model.getGestureId());
      });

      HBox controlPanelContainer = new HBox(10);

      controlPanelContainer.getChildren()
            .addAll(midiPortWidget, addDrumActionWidget);

      parentVBox.getChildren()
            .addAll(controlPanelContainer, midiMappingWidget);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      primaryStage.show();
   }

   private MidiMappingPresenter presenter;

   MidiPortWidget midiPortWidget;

   // @Inject :D
   public void setMidiHelper(MidiHelper midiHelper) {
      this.midiHelper = midiHelper;
   }

   private MidiHelper midiHelper;

   @Override
   public void setPresenter(final MidiMappingPresenter presenter) {
      this.presenter = presenter;
   }

   @Override
   public void refresh() {
      midiMappingWidget.refresh();
   }

   private MidiMappingTableWidget midiMappingWidget;
}
