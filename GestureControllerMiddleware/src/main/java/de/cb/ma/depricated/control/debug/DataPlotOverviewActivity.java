package de.cb.ma.depricated.control.debug;

import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.depricated.debugging.overview.DataPlotOverviewView;
import de.cb.ma.core.imu.motion.HandSignDetector;
import de.cb.ma.util.MidiHelper;
import jssc.SerialPort;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

/**
 * Hello world!
 */
public class DataPlotOverviewActivity implements DataPlotOverviewView.DataPlotOverviewPresenter {

   private SerialPort serialPort;

   private DataPlotOverviewView view;
   private final MidiHelper midiHelper;
   private MidiDevice currentMidiDevice;
   private final HandSignDetector gestureDictionary;
   private Receiver receiver;

   public DataPlotOverviewActivity(DataPlotOverviewView view) {
      this.view = view;
      this.midiHelper = new MidiHelper();
      this.gestureDictionary = new HandSignDetector(0.3, new HandSignDetector.Callback() {
         @Override
         public void onPostureDetected(final HandSign.SIGN posture, final Integer[] binaryPattern) {
            view.changeShownHandsign(posture.name());
         }
      });

      try {
         receiver = MidiSystem.getReceiver();
      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      }
   }

   public void sendMidiMessage(int status, int channel, int data1, int data2) {

      try {
         if (currentMidiDevice != null) {
            receiver = currentMidiDevice.getReceiver();

            System.out.println("using : " + currentMidiDevice.getDeviceInfo()
                  .getName() + " to send a midi message");
         }

         final ShortMessage shortMessage = new ShortMessage();
         shortMessage.setMessage(status, channel, data1, data2);
         receiver.send(shortMessage, -1);

      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      } catch (InvalidMidiDataException e) {
         e.printStackTrace();
      } catch (NullPointerException e) {
         e.printStackTrace();
      }

      //      final ShortMessage changeVolume = new ShortMessage();
      //      changeVolume.setMessage(ShortMessage.CONTROL_CHANGE, 7, 0, 0);
      //      receiver.send(shortMessage, -1);
   }

   public void startSerial(String portName, int baudRate) {
      timeOnSerialLoggingStart = System.currentTimeMillis();

      //               view.changeShownHandsign(currentHandSign);
      System.out.println("start serial presenter");

      //      motionRecognitionEngine = new GestureDetector(portName, baudRate, new GestureDetectorEngineCallbackImpl() {
      //         @Override
      //         public void onPostureDetected(final PostureMotion gesture) {
      //            if (gesture.getType()
      //                  .equals(PostureMotion.SWIPE)) {
      //               Random ran = new Random();
      //               final int nextInt = ran.nextInt(127);
      //               sendMidiMessage(ShortMessage.NOTE_ON, 0, nextInt, nextInt);
      //            }
      //         }
      //
      //         @Override
      //         public void onPostureDetected(final HandPosture.SIGN gesture, final Integer[] binaryPattern) {
      //
      //         }
      //
      //         @Override
      //         public void onDetectionStart(final double varianceX, final double varianceY, final double varianceZ) {
      //
      //         }
      //
      //         @Override
      //         public void onDetectionEnd(final double varianceX, final double varianceY, final double varianceZ) {
      //
      //         }
      //
      //         @Override
      //         public void onAcceleration(final double x, final double y, final double z) {
      //
      //         }
      //
      //         @Override
      //         public void onBendMessageReceived(final BendMessage bendMessage) {
      //
      //         }
      //
      //         @Override
      //         public void onGestureRecognized(final HandPosture.SIGN posture, final List<Pair<Double, List<Double>>> capturedGestureFrequencies) {
      //
      //         }
      //
      //         @Override
      //         public void onActionDetected(final MidiMessageParameter drumAction) {
      //
      //         }
      //
      //         @Override
      //         public void onMagneticSensing(final String x, final String y, final String z) {
      //
      //         }
      //      }, serialMessage -> {
      //         if (serialMessage instanceof BendMessage) {
      //            BendMessage flexCommand = (BendMessage) serialMessage;
      //
      //            final Double thumbFlexValue = flexCommand.getThumbFlexValue();
      //            final Double indexFlexValue = flexCommand.getIndexFlexValue();
      //            final Double middleFlexValue = flexCommand.getMiddleFlexValue();
      //            final Double ringFlexValue = flexCommand.getRingFlexValue();
      //            final Double pinkyFlexValue = flexCommand.getPinkyFlexValue();
      //
      //            view.changeAllFingerSliderValues(toSliderValue(thumbFlexValue), toSliderValue(indexFlexValue), toSliderValue(middleFlexValue), toSliderValue(ringFlexValue), toSliderValue(pinkyFlexValue));
      //
      //            gestureDictionary.updateValues(thumbFlexValue, indexFlexValue, middleFlexValue, ringFlexValue, pinkyFlexValue);
      //
      //            //               view.changeShownHandsign(currentHandSign);
      //            return;
      //         }
      //         if (serialMessage instanceof OrientationMessage) {
      //            OrientationMessage orientationCommand = (OrientationMessage) serialMessage;
      //            final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      //
      //            view.updateGyroGraphs(currentTimeMillis, Integer.valueOf(orientationCommand.getYawValue()), Integer.valueOf(orientationCommand.getPitchValue()), Integer.valueOf(orientationCommand.getRollValue()));
      //            return;
      //         }
      //
      //         if (serialMessage instanceof AccelerationMessage) {
      //            AccelerationMessage accelerationCommand = (AccelerationMessage) serialMessage;
      //
      //            final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      //            view.updateAccelerationGraphs(currentTimeMillis, accelerationCommand.getxAccelValue(), accelerationCommand.getyAccelValue(), accelerationCommand.getzAccelValue());
      //            return;
      //         }
      //
      //         if (serialMessage instanceof LinearAccelerationMessage) {
      //            LinearAccelerationMessage linearAccelerationMessage = (LinearAccelerationMessage) serialMessage;
      //
      //            final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      //            view.updateLinearAccelerationyGraphs(currentTimeMillis, Integer.valueOf(linearAccelerationMessage.getxAccelValue()), Integer.valueOf(linearAccelerationMessage.getyAccelValue()), Integer.valueOf(linearAccelerationMessage.getzAccelValue()));
      //            return;
      //         }
      //
      //         if (serialMessage instanceof GravityMessage) {
      //            GravityMessage gravityMessage = (GravityMessage) serialMessage;
      //
      //            final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      //            view.updateGravityGraphs(currentTimeMillis, Integer.valueOf(gravityMessage.getxValue()), Integer.valueOf(gravityMessage.getyValue()), Integer.valueOf(gravityMessage.getzValue()));
      //            return;
      //         }
      //
      //         if (serialMessage instanceof DrumMessage) {
      //            return;
      //         }
      //
      //         if (serialMessage instanceof DirectionMessage) {
      //            return;
      //         }
      //
      //         if (serialMessage instanceof GyroPeakMessage) {
      //            return;
      //         }
      //      });
      //   }
      //
      //   @Override
      //   public void stopRecognition() {
      //      motionRecognitionEngine.stopRightHandPort();
      //   }
      //
      //   @Override
      //   public void startSerial(final String msgsPath) {
      //      timeOnSerialLoggingStart = 0;
      //      new SerialMessageHandler(msgsPath, serialMessage -> {
      //         if (serialMessage instanceof GyroPeakValueMessage) {
      //            timeOnSerialLoggingStart += 100;
      //         }
      //      });
   }

   @Override
   public void stopRecognition() {

   }

   @Override
   public void startSerial(final String msgsPath) {

   }

   private double toSliderValue(Double value) {
      return value / 100;
   }

   private long timeOnSerialLoggingStart;
   //   private GestureDetector motionRecognitionEngine;
}
