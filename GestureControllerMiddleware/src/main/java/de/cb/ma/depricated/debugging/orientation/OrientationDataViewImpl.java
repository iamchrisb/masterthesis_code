package de.cb.ma.depricated.debugging.orientation;

import de.cb.ma.core.widgets.file.FileChooserWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;

public class OrientationDataViewImpl implements OrientationDataView {

   public void setPresenter(DataPlotPresenter presenter) {
      this.presenter = presenter;
   }

   @Override
   public void updateOrientationGraphs(final long timePassed, final Integer xAxisValue, final Integer yAxisValue, final Integer zAxisValue) {
      float timePassedInSecs = ((float) timePassed) / 1000;
      xOriGraph.addData(timePassedInSecs, xAxisValue);
      yOriGraph.addData(timePassedInSecs, yAxisValue);
      zOriGraph.addData(timePassedInSecs, zAxisValue);
   }

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Orientation-Data Plotting");

      VBox parentVBox = new VBox();
      parentVBox.setSpacing(20);

      serialPortWidget = new SerialPortWidget("Connect to Serialport", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            presenter.startSerial(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            presenter.stopSerial();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      FileChooserWidget fileChooserWidget = new FileChooserWidget("Choose file", primaryStage, new FileChooserWidget.Callback() {
         @Override
         public void onFileSelected(final File file) {
            presenter.startSerial(file.getAbsolutePath());
         }
      });

      HBox controlPanelContainer = new HBox(10);

      controlPanelContainer.getChildren()
            .addAll(serialPortWidget, fileChooserWidget);

      parentVBox.getChildren()
            .addAll(controlPanelContainer);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      xOriGraph = new LineGraphPlotWidget("Yaw", "", "Time in s", "rotation in degree");
      yOriGraph = new LineGraphPlotWidget("Pitch", "", "Time in s", "rotation in degree");
      zOriGraph = new LineGraphPlotWidget("Roll", "", "Time in s", "rotation in degree");

      HBox accelGraphContainer = new HBox();

      parentVBox.getChildren()
            .addAll(accelGraphContainer);

      accelGraphContainer.getChildren()
            .addAll(xOriGraph.getElement(), yOriGraph.getElement(), zOriGraph.getElement());

      Button clear = new Button("Clear graphs");

      parentVBox.getChildren()
            .add(clear);

      clear.setOnAction(event -> {
         xOriGraph.clearData();
         yOriGraph.clearData();
         zOriGraph.clearData();
      });

      final URL resource = LineGraphPlotWidget.class.getResource("styles.css");

      final String styles = resource
            .toExternalForm();

      final Scene scene = primaryStage.getScene();

      scene.getStylesheets()
            .add(styles);

      primaryStage.show();
   }

   private DataPlotPresenter presenter;

   private LineGraphPlotWidget xOriGraph;
   private LineGraphPlotWidget yOriGraph;
   private LineGraphPlotWidget zOriGraph;

   SerialPortWidget serialPortWidget;
}
