package de.cb.ma.depricated;

import java.util.Queue;

public class Statistics {

   public static double welfordVariance(double[] samples) {
      double mean = 0;
      double sum = 0;

      for (int i = 1; i < samples.length; i++) {
         double sample = samples[i];
         double oldMean = mean;
         mean = mean + (sample - mean) / samples.length;
         sum = sum + (sample - mean) * (sample - oldMean);
      }

      return sum / samples.length - 1;
   }

   public static double welfordVariance(Queue<Double> samples) {
      double mean = 0;
      double sum = 0;

      final Double[] sampleArr = samples.toArray(new Double[samples.size()]);

      for (int i = 1; i < sampleArr.length; i++) {
         double sample = sampleArr[i];
         double oldMean = mean;
         mean = mean + (sample - mean) / samples.size();
         sum = sum + (sample - mean) * (sample - oldMean);
      }

      return sum / samples.size() - 1;
   }

   private int windowSize;

   public double movingVariance(double newSample) {
      return 0;
   }

}
