package de.cb.ma.depricated;

import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.core.imu.motion.HandSign;

public class MidiMappingModel {

   private String drumActionId;
   private String drumActionName;
   private int midiChannel;
   private int midiPitch;
   private int midiVelocity;
   private String midiPitchAsNote;
   private String midiVelocityAsNote;

   private String gestureName;
   private String gestureId;
   private HandSign.SIGN posture;

   public MidiMappingModel(final String drumActionId, final String drumActionName, final int midiChannel, final int midiPitch, final int midiVelocity, final String midiPitchAsNote,
         final String midiVelocityAsNote,
         final String gestureId, final String gestureName, final HandSign.SIGN posture) {
      this.drumActionId = drumActionId;
      this.drumActionName = drumActionName;
      this.midiChannel = midiChannel;
      this.midiPitch = midiPitch;
      this.midiVelocity = midiVelocity;
      this.midiPitchAsNote = midiPitchAsNote;
      this.midiVelocityAsNote = midiVelocityAsNote;
      this.gestureName = gestureName;
      this.gestureId = gestureId;
      this.posture = posture;
   }

   public MidiMappingModel(MidiMessageParameter drumAction, GestureModel gestureModel, HandSign.SIGN posture) {
      //      this(drumAction.getId(), drumAction.getName(), drumAction.getMidiChannel(), drumAction.getDatabyte1(), drumAction.getDatabyte2(), MidiHelper.getNoteAsString(drumAction.getDatabyte1()), MidiHelper.getNoteAsString(drumAction.getDatabyte2()), gestureModel.getId(), gestureModel.getName(), posture);
   }

   public String getDrumActionId() {
      return drumActionId;
   }

   public void setDrumActionId(final String drumActionId) {
      this.drumActionId = drumActionId;
   }

   public String getDrumActionName() {
      return drumActionName;
   }

   public void setDrumActionName(final String drumActionName) {
      this.drumActionName = drumActionName;
   }

   public int getMidiChannel() {
      return midiChannel;
   }

   public void setMidiChannel(final int midiChannel) {
      this.midiChannel = midiChannel;
   }

   public int getMidiPitch() {
      return midiPitch;
   }

   public void setMidiPitch(final int midiPitch) {
      this.midiPitch = midiPitch;
   }

   public int getMidiVelocity() {
      return midiVelocity;
   }

   public void setMidiVelocity(final int midiVelocity) {
      this.midiVelocity = midiVelocity;
   }

   public String getMidiPitchAsNote() {
      return midiPitchAsNote;
   }

   public void setMidiPitchAsNote(final String midiPitchAsNote) {
      this.midiPitchAsNote = midiPitchAsNote;
   }

   public String getMidiVelocityAsNote() {
      return midiVelocityAsNote;
   }

   public void setMidiVelocityAsNote(final String midiVelocityAsNote) {
      this.midiVelocityAsNote = midiVelocityAsNote;
   }

   public String getGestureName() {
      return gestureName;
   }

   public void setGestureName(final String gestureName) {
      this.gestureName = gestureName;
   }

   public String getGestureId() {
      return gestureId;
   }

   public void setGestureId(final String gestureId) {
      this.gestureId = gestureId;
   }

   public HandSign.SIGN getPosture() {
      return posture;
   }

   public void setPosture(final HandSign.SIGN posture) {
      this.posture = posture;
   }
}
