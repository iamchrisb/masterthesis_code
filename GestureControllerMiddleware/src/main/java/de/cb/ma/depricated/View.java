package de.cb.ma.depricated;

import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public interface View {
   void show(Stage primaryStage, BorderPane root);
}
