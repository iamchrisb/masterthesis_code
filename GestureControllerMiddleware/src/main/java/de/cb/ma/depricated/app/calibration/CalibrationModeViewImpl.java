package de.cb.ma.depricated.app.calibration;

import javafx.geometry.Insets;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class CalibrationModeViewImpl implements CalibrationModeView {

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Calibration Mode");

      VBox parentVBox = new VBox();

      parentVBox.setPadding(new Insets(10, 10, 10, 10));

      parentVBox.setSpacing(20);

      HBox controlPanelContainer = new HBox(10);

      parentVBox.getChildren()
            .addAll(controlPanelContainer);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      primaryStage.show();
   }

   private CalibrationPresenter presenter;

   @Override
   public void setPresenter(final CalibrationPresenter presenter) {
      this.presenter = presenter;
   }

}
