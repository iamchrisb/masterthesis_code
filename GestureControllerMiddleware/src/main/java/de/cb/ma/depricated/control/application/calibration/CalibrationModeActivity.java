package de.cb.ma.depricated.control.application.calibration;

import de.cb.ma.depricated.app.calibration.CalibrationModeView;
import jssc.SerialPort;

import javax.sound.midi.Receiver;

/**
 * Hello world!
 */
public class CalibrationModeActivity implements CalibrationModeView.CalibrationPresenter {

   private SerialPort serialPort;

   private Receiver receiver;

   public CalibrationModeActivity(CalibrationModeView view) {
      this.view = view;
   }

   private CalibrationModeView view;
}
