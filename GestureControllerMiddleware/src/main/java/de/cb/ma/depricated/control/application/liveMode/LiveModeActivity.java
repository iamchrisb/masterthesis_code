package de.cb.ma.depricated.control.application.liveMode;

import de.cb.ma.depricated.app.liveMode.LiveModeView;
import de.cb.ma.util.MidiHelper;
import jssc.SerialPort;

import java.io.IOException;
import java.util.List;
import java.util.Timer;

import javax.sound.midi.InvalidMidiDataException;
import javax.sound.midi.MidiDevice;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.MidiUnavailableException;
import javax.sound.midi.Receiver;
import javax.sound.midi.ShortMessage;

public class LiveModeActivity implements LiveModeView.LiveModePresenter {

   private SerialPort serialPort;

   private LiveModeView view;
   private final MidiHelper midiHelper;
   private MidiDevice currentMidiDevice;
   private Receiver receiver;

   public LiveModeActivity(LiveModeView view) {
      this.view = view;
      this.midiHelper = new MidiHelper();

      try {
         receiver = MidiSystem.getReceiver();
      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      }

      rightHandPortCheckTimer = new Timer();
   }

   @Override
   public void sendMidiMessage(int status, int channel, int data1, int data2) {

      try {
         if (currentMidiDevice != null) {
            receiver = currentMidiDevice.getReceiver();

            System.out.println("using : " + currentMidiDevice.getDeviceInfo()
                  .getName() + " to send a midi message");
         }

         final ShortMessage shortMessage = new ShortMessage();
         shortMessage.setMessage(status, channel, data1, data2);
         receiver.send(shortMessage, -1);

      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      } catch (InvalidMidiDataException e) {
         e.printStackTrace();
      } catch (NullPointerException e) {
         e.printStackTrace();
      }

      //      final ShortMessage changeVolume = new ShortMessage();
      //      changeVolume.setMessage(ShortMessage.CONTROL_CHANGE, 7, 0, 0);
      //      receiver.send(shortMessage, -1);
   }

   @Override
   public void playNote(int channel, int pitch, int velocity) {

      try {
         if (currentMidiDevice != null) {
            receiver = currentMidiDevice.getReceiver();

            System.out.println("using : " + currentMidiDevice.getDeviceInfo()
                  .getName() + " to send a midi message");
         }

         final ShortMessage shortMessage = new ShortMessage();
         shortMessage.setMessage(ShortMessage.NOTE_ON, channel, pitch, velocity);
         receiver.send(shortMessage, -1);

         final ShortMessage shortMessageOff = new ShortMessage();
         shortMessageOff.setMessage(ShortMessage.NOTE_OFF, channel, pitch, velocity);
         receiver.send(shortMessageOff, -1);

      } catch (MidiUnavailableException e) {
         e.printStackTrace();
      } catch (InvalidMidiDataException e) {
         e.printStackTrace();
      } catch (NullPointerException e) {
         e.printStackTrace();
      }
   }

   @Override
   public void useMidiDevice(int index) throws MidiUnavailableException {
      this.currentMidiDevice = midiHelper.getSystemMidiPort(index);
      if (!this.currentMidiDevice.isOpen()) {
         System.out.println("Open MIDI-Device: " + currentMidiDevice.getDeviceInfo()
               .getName());
         this.currentMidiDevice.open();
      }
      final List<Receiver> receivers = currentMidiDevice.getReceivers();
      for (Receiver receiver : receivers) {
         System.out.println(receiver);
      }
   }

   @Override
   public void startSerial(final String COMM_PORT, final int BAUD_RATE) {
      try {
         startGestureDetection(COMM_PORT, BAUD_RATE);
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   private void startGestureDetection(final String COMM_PORT, final int BAUD_RATE) throws IOException {
      //      rightHandGestureDetector = new GestureDetector(COMM_PORT, BAUD_RATE, new GestureDetectorEngineCallbackImpl() {
      //         @Override
      //         public void onPostureDetected(final HandPosture.SIGN posture, final Integer[] binaryPattern) {
      //            view.updateHandSign(posture);
      //         }
      //
      //         @Override
      //         public void onDetectionStart(final double varianceX, final double varianceY, final double varianceZ) {
      //            System.out.println("Gesture started with variances: x: " + Constants.DECIMAL_FORMAT.format(varianceX) + "\t y: " + Constants.DECIMAL_FORMAT.format(varianceY) + " \t z: " + Constants.DECIMAL_FORMAT.format(varianceZ));
      //            view.addGestureEventLogClearLine();
      //            view.addGestureEventLog("Start:\t x:" + Constants.DECIMAL_FORMAT.format(varianceX) + "\t y: " + Constants.DECIMAL_FORMAT.format(varianceY) + " \t z: " + Constants.DECIMAL_FORMAT.format(varianceZ));
      //         }
      //
      //         @Override
      //         public void onDetectionEnd(final double varianceX, final double varianceY, final double varianceZ) {
      //            System.out.println("Gesture end with variances: x: " + Constants.DECIMAL_FORMAT.format(varianceX) + "\t y: " + Constants.DECIMAL_FORMAT.format(varianceY) + " \t z: " + Constants.DECIMAL_FORMAT.format(varianceZ));
      //            System.out.println();
      //            view.addGestureEventLog("Stop:\t x:" + Constants.DECIMAL_FORMAT.format(varianceX) + "\t y: " + Constants.DECIMAL_FORMAT.format(varianceY) + " \t z: " + Constants.DECIMAL_FORMAT.format(varianceZ));
      //         }
      //
      //         @Override
      //         public void onAcceleration(final double x, final double y, final double z) {
      //            final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      //            //            view.updateAccelerationGraph(currentTimeMillis, x, y, z);
      //         }
      //
      //         @Override
      //         public void onBendMessageReceived(final BendMessage bendMessage) {
      //            view.updateBendValues(bendMessage.getThumbFlexValue() / 100, bendMessage.getIndexFlexValue() / 100, bendMessage.getMiddleFlexValue() / 100, bendMessage.getRingFlexValue() / 100, bendMessage.getPinkyFlexValue() / 100);
      //         }
      //
      //         @Override
      //         public void onActionDetected(final MidiMessageParameter drumAction) {
      //            playNote(drumAction.getMidiChannel(), drumAction.getDatabyte1(), drumAction.getDatabyte2());
      //            // TODO
      //            //            view.logMidiEvent(drumAction.getName(), drumAction.getMidiChannel(), MidiHelper.getNoteAsString(drumAction.getDatabyte1()));
      //         }
      //
      //         @Override
      //         public void onMagneticSensing(final String x, final String y, final String z) {
      //            final Integer magneticX = Integer.valueOf(x);
      //            final Integer magneticY = Integer.valueOf(y);
      //
      //            if (Math.abs(latestMagneticX - magneticX) > 10) {
      //               final int pitch = Math.abs(magneticX % 48) + 48;
      //               playNote(currentMagneticNoteChannel, pitch, 64);
      //               latestMagneticX = magneticX;
      //            }
      //
      //            //            if (Math.abs(latestMagneticY - magneticY) > 10) {
      //            //               final int pitch = Math.abs(magneticX % 48) + 48;
      //            //               playNote(12, pitch, 50);
      //            //               latestMagneticY = magneticY;
      //            //            }
      //         }
      //      });
      //
      //      rightHandGestureDetector.startRightHandRecognizing();
      //      timeOnSerialLoggingStart = System.currentTimeMillis();
      //
      //      rightHandPortCheckTimer.scheduleAtFixedRate(new TimerTask() {
      //         @Override
      //         public void run() {
      //            rightHandPortIsOpened = rightHandGestureDetector.isRightHandPortOpened();
      //            if (rightHandPortIsOpened) {
      //               view.switchStatusStartButton(!rightHandPortIsOpened);
      //               view.switchStatusStopButton(rightHandPortIsOpened);
      //            } else {
      //               view.switchStatusStopButton(rightHandPortIsOpened);
      //               view.switchStatusStartButton(!rightHandPortIsOpened);
      //            }
      //         }
      //      }, 0, 500);
   }

   @Override
   public void stopRecognition() {

   }

   @Override
   public void startSerial(final String msgsPath) {

   }

   @Override
   public void muteSerial(final boolean enabled) {
      //      rightHandGestureDetector.mute(enabled);
   }

   @Override
   public void changeMagneticNodeChannel(final int channelNumber) {
      this.currentMagneticNoteChannel = channelNumber;
   }

   private final Timer rightHandPortCheckTimer;
   private boolean rightHandPortIsOpened;
   private long timeOnSerialLoggingStart;
   private int latestMagneticX;
   private int latestMagneticY;
   private int currentMagneticNoteChannel = 11;
}
