package de.cb.ma.depricated.debugging.gyro;

import de.cb.ma.depricated.View;
import de.cb.ma.depricated.debugging.DataPlotPresenter;

public interface GyroPeakDataView extends View {

   void setPresenter(DataPlotPresenter presenter);

   void updateGyroGraphs(long currentTimeMillis, Integer yawAngle, Integer pitchAngle, Integer rollAngle);
}
