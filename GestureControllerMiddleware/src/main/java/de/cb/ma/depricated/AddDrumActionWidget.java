package de.cb.ma.depricated;

import de.cb.ma.core.database.model.GestureMappingEntity;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.database.dao.GestureMappingDao;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.util.MidiHelper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

public class AddDrumActionWidget extends Pane {

   public AddDrumActionWidget(String title) {

      final Label label = new Label(title);

      VBox container = new VBox(10);

      HBox sideContainer = new HBox(10);

      ObservableList<String> notes =
            FXCollections.observableArrayList(
                  MidiHelper.NOTES
            );

      //      gestureMappingDao = new GestureMappingDaoFileImpl();
      final GestureMappingEntity gestureMapping = getGestureMapping();

      postureComboBox = new ComboBox<>(FXCollections.observableArrayList(HandSign.SIGN.values()));
      postureComboBox.setPromptText("posture");
      postureComboBox.valueProperty()
            .addListener(new ChangeListener<HandSign.SIGN>() {
               @Override
               public void changed(final ObservableValue<? extends HandSign.SIGN> observable, final HandSign.SIGN oldValue, final HandSign.SIGN newValue) {
                  if (newValue == null) {
                     return;
                  }
                  gestureObservableList.clear();
                  final HandSign.SIGN value = (HandSign.SIGN) newValue;
                  final List<String> gesturesNames = getGesturesNames(newValue);
                  gestureObservableList.addAll(gesturesNames);
                  gestureSelection.setPromptText("choose gesture");
               }
            });

      gestureObservableList = FXCollections.observableArrayList(new ArrayList<>());
      gestureSelection = new ComboBox<>(gestureObservableList);
      gestureSelection.setPromptText("choose gesture");

      midiChannelSelection = new ComboBox<>(FXCollections.observableArrayList(
            MidiHelper.MIDI_CHANNELS
      ));
      midiChannelSelection.setPromptText("MIDI Channel");

      sampleIdentifierSelection = new ComboBox<>(notes);
      sampleIdentifierSelection.setPromptText("Sample Identifier (Pitch Note)");

      velocitySelection = new ComboBox<>(notes);
      velocitySelection.setPromptText("Velocity");

      nameField = new TextField();
      nameField.setPromptText("Name");

      sideContainer.getChildren()
            .addAll(nameField, postureComboBox, gestureSelection, midiChannelSelection, sampleIdentifierSelection, velocitySelection);

      addBtn = new Button("Add");

      container.getChildren()
            .addAll(label, sideContainer, addBtn);

      this.getChildren()
            .add(container);

   }

   public List<String> getGesturesNames(HandSign.SIGN key) {
      List<String> names = new ArrayList<>();

      final List<GestureModel> gestures = getGestureMapping().getGestures(key);
      for (GestureModel gesture : gestures) {
         names.add(gesture.getName());
      }
      return names;
   }

   private GestureMappingEntity getGestureMapping() {
      return gestureMappingDao.findAll();
   }

   private final Button addBtn;

   public void setOnAction(EventHandler<ActionEvent> eventEventHandler) {
      addBtn.setOnAction(eventEventHandler);
   }

   public MidiMessageParameter getCurrentDrumAction() {
      final String name = nameField.getText();

      final String gestureName = gestureSelection.getSelectionModel()
            .getSelectedItem();

      // choose index cause midi channels go from 0 - 15, but names are 1 - 16
      final int midiChannel = midiChannelSelection.getSelectionModel()
            .getSelectedIndex();

      final Integer sampleIdentifier = sampleIdentifierSelection.getSelectionModel()
            .getSelectedIndex();

      final Integer velocity = velocitySelection.getSelectionModel()
            .getSelectedIndex();

      // TODO
      //      return new MidiMessageParameter(name, midiChannel, sampleIdentifier, velocity, MidiMessageParameter.MIDI_MESSAGE_TYPE.NOTE_ON);
      return null;
   }

   private final TextField nameField;
   private final ComboBox<String> velocitySelection;
   private final ComboBox<String> sampleIdentifierSelection;
   private final ComboBox<Integer> midiChannelSelection;

   private final ComboBox<String> gestureSelection;
   private final ComboBox<HandSign.SIGN> postureComboBox;
   private final ObservableList<String> gestureObservableList;

   public HandSign.SIGN getSelectedPosture() {
      return this.postureComboBox.getSelectionModel()
            .getSelectedItem();
   }

   public String getSelectedGestureName() {
      return this.gestureSelection.getSelectionModel()
            .getSelectedItem();
   }

   private GestureMappingDao gestureMappingDao;
}
