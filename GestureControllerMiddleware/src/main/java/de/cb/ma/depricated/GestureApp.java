package de.cb.ma.depricated;

import de.cb.ma.depricated.app.calibration.CalibrationModeView;
import de.cb.ma.depricated.app.gestureMapping.GestureMappingView;
import de.cb.ma.depricated.app.gestureMapping.GestureMappingViewImpl;
import de.cb.ma.depricated.app.liveMode.LiveModeView;
import de.cb.ma.depricated.app.liveMode.LiveModeViewImpl;
import de.cb.ma.depricated.app.midiMapping.MidiMappingViewImpl;
import de.cb.ma.depricated.control.application.calibration.CalibrationModeActivity;
import de.cb.ma.depricated.control.application.midiMapping.MidiMappingActivity;
import de.cb.ma.depricated.control.debug.AccelerationDataActivity;
import de.cb.ma.depricated.control.debug.GravityDataActivity;
import de.cb.ma.depricated.control.debug.GyroPeakValueDataActivity;
import de.cb.ma.depricated.control.debug.LinearAccelerationDataActivity;
import de.cb.ma.depricated.control.debug.MagnetometerDataActivity;
import de.cb.ma.depricated.debugging.accel.AccelerationDataView;
import de.cb.ma.depricated.debugging.gravity.GravityDataViewImpl;
import de.cb.ma.depricated.debugging.gyro.GyroPeakDataViewImpl;
import de.cb.ma.depricated.debugging.linAccel.LinearAccelerationDataViewImpl;
import de.cb.ma.depricated.debugging.magnetometer.MagnetometerDataViewImpl;
import de.cb.ma.depricated.debugging.overview.DataPlotOverviewView;
import de.cb.ma.depricated.debugging.overview.DataPlottingOverviewViewImpl;
import de.cb.ma.depricated.control.application.gestureMapping.GestureMappingActivity;
import de.cb.ma.depricated.control.application.liveMode.LiveModeActivity;
import de.cb.ma.depricated.control.debug.DataPlotOverviewActivity;
import de.cb.ma.depricated.control.debug.OrientationDataActivity;
import de.cb.ma.depricated.app.calibration.CalibrationModeViewImpl;
import de.cb.ma.depricated.debugging.accel.AccelerationDataViewImpl;
import de.cb.ma.depricated.debugging.orientation.OrientationDataViewImpl;
import de.cb.ma.util.MidiHelper;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.layout.BorderPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class GestureApp extends Application {

   public static void main(String[] args) {
      launch(args);
   }

   private static Stage stage;

   @Override
   public void start(final Stage primaryStage) throws Exception {
      this.primaryStage = primaryStage;
      this.midiHelper = new MidiHelper();
      this.rootNode = new BorderPane();
      this.menuBar = createMenu(primaryStage);

      GestureApp.stage = primaryStage;

      setFullScreenMode(primaryStage);

      /**
       * data plotting part
       */
      dataPlottingView = new DataPlottingOverviewViewImpl();
      DataPlotOverviewActivity presenter = new DataPlotOverviewActivity(dataPlottingView);
      dataPlottingView.setPresenter(presenter);

      /**
       * midi mapping part
       */
      midiMappingView = new MidiMappingViewImpl();
      midiMappingView.setMidiHelper(midiHelper);
      final MidiMappingActivity midiMappingActivity = new MidiMappingActivity(midiMappingView);
      midiMappingView.setPresenter(midiMappingActivity);

      /**
       * calibration part
       */
      calibrationView = new CalibrationModeViewImpl();
      final CalibrationModeActivity calibrationModeActivity = new CalibrationModeActivity(calibrationView);
      calibrationView.setPresenter(calibrationModeActivity);

      /**
       * gesture mapping part
       */
      gestureMappingView = new GestureMappingViewImpl();
      final GestureMappingActivity gestureMappingActivity = new GestureMappingActivity(gestureMappingView);
      gestureMappingView.setPresenter(gestureMappingActivity);

      /**
       * live mode part
       */

      liveModeView = new LiveModeViewImpl();
      final LiveModeActivity liveModeActivity = new LiveModeActivity(liveModeView);
      liveModeView.setPresenter(liveModeActivity);
      liveModeView.setMidiHelper(midiHelper);

      /**
       * acceleration data part
       */
      accelerationDataView = new AccelerationDataViewImpl();
      AccelerationDataActivity accelerationDataActivity = new AccelerationDataActivity(accelerationDataView);
      accelerationDataView.setPresenter(accelerationDataActivity);

      /**
       * linear acceleration data part
       */
      linearAccelerationDataView = new LinearAccelerationDataViewImpl();
      final LinearAccelerationDataActivity linearAccelerationDataActivity = new LinearAccelerationDataActivity(linearAccelerationDataView);
      linearAccelerationDataView.setPresenter(linearAccelerationDataActivity);

      /**
       * gravity data part
       */
      gravityDataView = new GravityDataViewImpl();
      final GravityDataActivity gravityDataActivity = new GravityDataActivity(gravityDataView);
      gravityDataView.setPresenter(gravityDataActivity);

      /**
       * gyro peak data part
       */
      gyroPeakDataView = new GyroPeakDataViewImpl();
      final GyroPeakValueDataActivity gyroPeakDataActivity = new GyroPeakValueDataActivity(gyroPeakDataView);
      gyroPeakDataView.setPresenter(gyroPeakDataActivity);

      /**
       * magnetometer data part
       */
      magnetometerDataView = new MagnetometerDataViewImpl();
      final MagnetometerDataActivity magnetometerDataActivity = new MagnetometerDataActivity(magnetometerDataView);
      magnetometerDataView.setPresenter(magnetometerDataActivity);

      /**
       * orientation data part
       */
      orientationDataView = new OrientationDataViewImpl();
      final OrientationDataActivity orientationDataActivity = new OrientationDataActivity(orientationDataView);
      orientationDataView.setPresenter(orientationDataActivity);

      rootNode.setTop(menuBar);

      /**
       * the timing is important
       */
      final Scene scene = new Scene(rootNode, 700, 500);
      primaryStage.setScene(scene);

      //dataPlottingView.show(primaryStage, rootNode);
      //      gestureMappingView.show(primaryStage, rootNode);
      //      gestureMappingView.show(primaryStage, rootNode);
      liveModeView.show(primaryStage, rootNode);
   }

   private void setFullScreenMode(final Stage primaryStage) {
      Rectangle2D primaryScreenBounds = Screen.getPrimary()
            .getVisualBounds();
      primaryStage.setX(primaryScreenBounds.getMinX());
      primaryStage.setY(primaryScreenBounds.getMinY());
      primaryStage.setWidth(primaryScreenBounds.getWidth());
      primaryStage.setHeight(primaryScreenBounds.getHeight());
   }

   private MenuBar createMenu(Stage stage) {
      MenuBar menuBar = new MenuBar();
      menuBar.prefWidthProperty()
            .bind(stage.widthProperty());

      Menu debuggingMenu = new Menu("Debugging");
      MenuItem overviewDataPlottingMenuItem = new MenuItem("Data Plotting (Overview)");
      MenuItem gyroDataPlottingMenuItem = new MenuItem("Gyropeak Data");
      MenuItem orientationDataPlottingMenuItem = new MenuItem("Orientation Data");
      MenuItem accelerationDataPlottingMenuItem = new MenuItem("Acceleration Data");
      MenuItem linearAccelerationDataPlottingMenuItem = new MenuItem("Linear Acceleration Data");
      MenuItem gravityDataPlottingMenuItem = new MenuItem("Gravity Data");
      MenuItem magnetometerDataPlottingMenuItem = new MenuItem("Magnetometer Data");

      Menu mappingMenu = new Menu("Actions");
      MenuItem quitMenuItem = new MenuItem("Quit");
      MenuItem midiMappingMenuItem = new MenuItem("Midi Mapping");
      MenuItem gestureMappingMenuItem = new MenuItem("Gesture Mapping");
      MenuItem calibrationMenuItem = new MenuItem("Calibration Mode");
      MenuItem liveModeMenuItem = new MenuItem("Live Mode");

      menuBar.getMenus()
            .addAll(mappingMenu, debuggingMenu);

      mappingMenu.getItems()
            .addAll(liveModeMenuItem, calibrationMenuItem, gestureMappingMenuItem, midiMappingMenuItem, new SeparatorMenuItem(), quitMenuItem);

      debuggingMenu.getItems()
            .addAll(overviewDataPlottingMenuItem, new SeparatorMenuItem(), orientationDataPlottingMenuItem, new SeparatorMenuItem(), gyroDataPlottingMenuItem, accelerationDataPlottingMenuItem, linearAccelerationDataPlottingMenuItem, gravityDataPlottingMenuItem, magnetometerDataPlottingMenuItem);

      /*
      adding actions
       */

      /**
       * debugging part
       */

      accelerationDataPlottingMenuItem.setOnAction(actionEvent -> {
         accelerationDataView.show(primaryStage, rootNode);
      });

      linearAccelerationDataPlottingMenuItem.setOnAction(actionEvent -> {
         linearAccelerationDataView.show(primaryStage, rootNode);
      });

      gravityDataPlottingMenuItem.setOnAction(actionEvent -> {
         gravityDataView.show(primaryStage, rootNode);
      });

      gyroDataPlottingMenuItem.setOnAction(actionEvent -> {
         gyroPeakDataView.show(primaryStage, rootNode);
      });

      magnetometerDataPlottingMenuItem.setOnAction(actionEvent -> {
         magnetometerDataView.show(primaryStage, rootNode);
      });

      orientationDataPlottingMenuItem.setOnAction(actionEvent -> {
         orientationDataView.show(primaryStage, rootNode);
      });

      /**
       * app part
       */
      quitMenuItem.setOnAction(actionEvent -> Platform.exit());

      midiMappingMenuItem.setOnAction(actionEvent -> {
         midiMappingView.show(primaryStage, rootNode);
      });

      calibrationMenuItem.setOnAction(actionEvent -> {
         // show calibration view
         calibrationView.show(primaryStage, rootNode);
      });

      liveModeMenuItem.setOnAction(actionEvent -> {
         // show mapping view
         liveModeView.show(primaryStage, rootNode);
      });

      gestureMappingMenuItem.setOnAction(actionEvent -> {
         // show mapping view
         gestureMappingView.show(primaryStage, rootNode);
      });

      overviewDataPlottingMenuItem.setOnAction(actionEvent -> {
         dataPlottingView.show(primaryStage, rootNode);
      });

      return menuBar;
   }

   public static Stage getStage() {
      return stage;
   }

   private BorderPane rootNode;
   private Stage primaryStage;
   private MidiHelper midiHelper;
   private MenuBar menuBar;

   private DataPlotOverviewView dataPlottingView;
   private MidiMappingViewImpl midiMappingView;
   private AccelerationDataView accelerationDataView;
   private GyroPeakDataViewImpl gyroPeakDataView;
   private MagnetometerDataViewImpl magnetometerDataView;
   private OrientationDataViewImpl orientationDataView;
   private GravityDataViewImpl gravityDataView;
   private LinearAccelerationDataViewImpl linearAccelerationDataView;
   private LiveModeView liveModeView;
   private CalibrationModeView calibrationView;
   private GestureMappingView gestureMappingView;
}
