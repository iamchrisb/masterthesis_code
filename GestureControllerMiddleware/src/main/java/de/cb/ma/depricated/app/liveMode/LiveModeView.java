package de.cb.ma.depricated.app.liveMode;

import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.depricated.View;
import de.cb.ma.util.MidiHelper;

import javax.sound.midi.MidiUnavailableException;

public interface LiveModeView extends View {

   public interface LiveModePresenter {
      void sendMidiMessage(int command, int channel, int data1, int data2);

      void playNote(int channel, int pitch, int velocity);

      void useMidiDevice(int selectedIndex) throws MidiUnavailableException;

      void startSerial(String COMM_PORT, int BAUD_RATE);

      void stopRecognition();

      void startSerial(String msgsPath);

      void muteSerial(boolean enabled);

      void changeMagneticNodeChannel(int channelNumber);
   }

   void setPresenter(LiveModePresenter presenter);

   void setMidiHelper(MidiHelper midiHelper);

   void switchStatusStartButton(final boolean status);

   void switchStatusStopButton(final boolean rightHandPortIsOpened);

   void addGestureEventLog(String logMsg);

   void addMidiEventLog(String logMsg);

   void addGestureEventLogClearLine();

   void updateAccelerationGraph(long currentTimeMillis, double x, double y, double z);

   void updateBendValues(Double thumbFlexValue, Double indexFlexValue, Double middleFlexValue, Double ringFlexValue, Double pinkyFlexValue);

   void updateHandSign(HandSign.SIGN posture);

   void logMidiEvent(String name, int midiChannel, String noteAsString);
}
