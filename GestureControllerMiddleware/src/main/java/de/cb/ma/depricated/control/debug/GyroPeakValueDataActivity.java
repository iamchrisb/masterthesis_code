package de.cb.ma.depricated.control.debug;

import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.depricated.debugging.gyro.GyroPeakDataView;
import de.cb.ma.core.imu.serial.messages.SerialMessage;
import de.cb.ma.core.imu.serial.messages.debug.GyroPeakValueMessage;
import de.cb.ma.depricated.debugging.DataPlotPresenter;
import jssc.SerialPort;

public class GyroPeakValueDataActivity implements DataPlotPresenter {

   private SerialPort serialPort;

   private GyroPeakDataView view;

   public GyroPeakValueDataActivity(GyroPeakDataView view) {
      this.view = view;
   }

   public void startSerial(String portName, int baudRate) {
      timeOnSerialLoggingStart = System.currentTimeMillis();

      serialMessageHandler = new SerialMessageHandler(portName, baudRate, serialMessage -> {
         if (serialMessage instanceof GyroPeakValueMessage) {
            handleGyroPeak(serialMessage);
            return;
         }
      });

      serialMessageHandler.startSerial();
   }

   @Override
   public void startSerial(final String filePath) {
      timeOnSerialLoggingStart = 0;

      serialMessageHandler = new SerialMessageHandler(filePath, serialMessage -> {
         if (serialMessage instanceof GyroPeakValueMessage) {
            GyroPeakValueMessage gyroPeakValueMessage = (GyroPeakValueMessage) serialMessage;

            timeOnSerialLoggingStart += 100;
            view.updateGyroGraphs(timeOnSerialLoggingStart, Integer.valueOf(gyroPeakValueMessage.getxPeakValue()), Integer.valueOf(gyroPeakValueMessage.getyPeakValue()), Integer.valueOf(gyroPeakValueMessage.getzPeakValue()));
            return;
         }
      });
   }

   private void handleGyroPeak(final SerialMessage serialMessage) {
      GyroPeakValueMessage gyroPeakValueMessage = (GyroPeakValueMessage) serialMessage;

      System.out.println(gyroPeakValueMessage);
      final long currentTimeMillis = (System.currentTimeMillis() - timeOnSerialLoggingStart);
      view.updateGyroGraphs(currentTimeMillis, Integer.valueOf(gyroPeakValueMessage.getxPeakValue()), Integer.valueOf(gyroPeakValueMessage.getyPeakValue()), Integer.valueOf(gyroPeakValueMessage.getzPeakValue()));
   }

   @Override
   public void stopSerial() {
      serialMessageHandler.closeSerialPort();
   }

   private double stringToSliderValue(String s) {
      return Double.valueOf(s) / 100;
   }

   private long timeOnSerialLoggingStart;
   private SerialMessageHandler serialMessageHandler;
}
