package de.cb.ma.depricated.app.liveMode;

import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.widgets.analysis.FingerDisplayWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.core.widgets.logging.MidiEventLogWidget;
import de.cb.ma.core.widgets.communication.MidiPortWidget;
import de.cb.ma.core.widgets.logging.GestureEventLogWidget;
import de.cb.ma.util.MidiHelper;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.LinkedList;

import javax.sound.midi.MidiUnavailableException;

public class LiveModeViewImpl implements LiveModeView {

   @Override
   public void show(Stage primaryStage, BorderPane root) {
      primaryStage.setTitle("Live Mode");

      VBox parentVBox = new VBox();

      parentVBox.setPadding(new Insets(10, 10, 10, 10));

      parentVBox.setSpacing(20);

      postureWidget = new FingerDisplayWidget("Current Posture");

      midiPortWidget = new MidiPortWidget("Choose a midi port", midiHelper, new MidiPortWidget.Callback() {
         @Override
         public void onMidiPortSelected(final int index) {
            try {
               presenter.useMidiDevice(index);
            } catch (MidiUnavailableException e) {
               e.printStackTrace();
            }
         }
      });

      serialPortWidget = new SerialPortWidget("Connect to serial port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            presenter.startSerial(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            presenter.stopRecognition();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {
            presenter.muteSerial(enabled);
         }
      });

      serialPortWidget.setStartButtonText("start gesture regocnition");

      varianceGraph = new LineGraphPlotWidget(new String[] { "x", "y", "z", "treshhold" }, "variance", "time in s", "");
      varianceGraph.showAxisValuesInLegend(false);

      accelerationGraph = new LineGraphPlotWidget(new String[] { "x", "y", "z", "treshhold" }, "acceleration", "time in s", "");
      accelerationGraph.showAxisValuesInLegend(false);

      HBox graphContainer = new HBox(10);
      graphContainer.getChildren()
            .addAll(accelerationGraph.getElement(), varianceGraph.getElement());

      //      midiMappingWidget = new MidiMappingTableWidget("Map motions to MIDI", (index, action) -> {
      //         presenter.playNote(action.getMidiChannel(), action.getMidiPitch(), action.getDatabyte2());
      //      }, (index, action) -> {
      //         midiMappingWidget.refresh();
      //      });

      HBox controlPanelContainer = new HBox(10);

      final Button clearGraphsBtn = new Button("clear graphs");
      clearGraphsBtn.setOnAction(event -> {
         accelerationGraph.clearData();
         varianceGraph.clearData();
      });

      final Button channel10 = new Button("Channel 10");
      channel10.setOnAction(event -> {
         presenter.changeMagneticNodeChannel(10);
      });

      final Button channel11 = new Button("Channel 11");
      channel11.setOnAction(event -> {
         presenter.changeMagneticNodeChannel(11);
      });

      final Button channel12 = new Button("Channel 12");
      channel12.setOnAction(event -> {
         presenter.changeMagneticNodeChannel(12);
      });

      controlPanelContainer.getChildren()
            .addAll(serialPortWidget, midiPortWidget, postureWidget, clearGraphsBtn, channel10, channel11, channel12);

      midiEventLogWidget = new MidiEventLogWidget();
      gestureEventLogWidget = new GestureEventLogWidget();

      HBox logContainer = new HBox(10);

      logContainer.getChildren()
            .addAll(midiEventLogWidget, gestureEventLogWidget);

      parentVBox.getChildren()
            .addAll(controlPanelContainer, logContainer);
      //            .addAll(controlPanelContainer, logContainer, graphContainer);

      parentVBox.setSpacing(20);

      root.setCenter(parentVBox);

      primaryStage.show();
   }

   private LiveModePresenter presenter;

   MidiPortWidget midiPortWidget;

   // @Inject :D
   @Override
   public void setMidiHelper(MidiHelper midiHelper) {
      this.midiHelper = midiHelper;
   }

   @Override
   public void switchStatusStartButton(final boolean status) {
      serialPortWidget.enableStartButton(status);
   }

   @Override
   public void switchStatusStopButton(final boolean status) {
      serialPortWidget.enableStopButton(status);
   }

   @Override
   public void addGestureEventLog(final String logMsg) {
      gestureEventLogWidget.addLog(logMsg);
   }

   @Override
   public void addMidiEventLog(final String logMsg) {
      midiEventLogWidget.addLog(logMsg);
   }

   @Override
   public void addGestureEventLogClearLine() {
      gestureEventLogWidget.addNewLine();
   }

   @Override
   public void updateAccelerationGraph(final long currentTimeMillis, final double x, final double y, final double z) {
      final LinkedList<Number> list = new LinkedList<>();
      list.add(x);
      list.add(y);
      list.add(z);
      //      accelerationGraph.addData(currentTimeMillis, list);
   }

   @Override
   public void updateBendValues(final Double thumbFlexValue, final Double indexFlexValue, final Double middleFlexValue, final Double ringFlexValue, final Double pinkyFlexValue) {
      postureWidget.changeAllFingerSliderValues(thumbFlexValue, indexFlexValue, middleFlexValue, ringFlexValue, pinkyFlexValue);
   }

   @Override
   public void updateHandSign(final HandSign.SIGN posture) {
      postureWidget.showHandSign(posture.name());
   }

   @Override
   public void logMidiEvent(final String name, final int midiChannel, final String pitchNote) {
      final long l = System.currentTimeMillis();
      midiEventLogWidget.addLog(l + ": name = " + name + " - chan = " + " note = " + pitchNote);
   }

   private MidiHelper midiHelper;

   @Override
   public void setPresenter(final LiveModePresenter presenter) {
      this.presenter = presenter;
   }

   private GestureEventLogWidget gestureEventLogWidget;
   private MidiEventLogWidget midiEventLogWidget;
   private SerialPortWidget serialPortWidget;
   private LineGraphPlotWidget varianceGraph;
   private LineGraphPlotWidget accelerationGraph;
   private FingerDisplayWidget postureWidget;
}
