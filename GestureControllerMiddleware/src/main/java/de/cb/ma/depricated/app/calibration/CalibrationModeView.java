package de.cb.ma.depricated.app.calibration;

import de.cb.ma.depricated.View;

public interface CalibrationModeView extends View {

   public interface CalibrationPresenter {
      //      void startGestureDetection(String COMM_PORT, int BAUD_RATE);

      //      void stopSerial();
   }

   void setPresenter(CalibrationPresenter presenter);

}
