package de.cb.ma.apps;

import de.cb.ma.core.imu.preprocessing.filter.sensorfusion.ComplementaryFilter;
import de.cb.ma.core.imu.preprocessing.filter.sensorfusion.SensorFusionFilter;
import de.cb.ma.core.widgets.analysis.HertzCounterWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.analysis.charts.RunningLineGraphPlotWidget;
import de.cb.ma.core.imu.serial.messages.application.SuperMessage;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.core.imu.preprocessing.filter.ApacheAmplitudeSmoother;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import org.apache.commons.math3.util.FastMath;

public class SensorFusionApp extends Pane {

   SerialMessageHandler serialMessageHandler;

   private long startedTime;

   ApacheAmplitudeSmoother accelDataSmoother = new ApacheAmplitudeSmoother(32, 3);

   private RunningLineGraphPlotWidget rollComparisonGraph;
   private RunningLineGraphPlotWidget pitchComparisonGraph;
   private RunningLineGraphPlotWidget rollDeltaGraph;
   private RunningLineGraphPlotWidget pitchDeltaGraph;

   SensorFusionFilter sensorFusionFilter = new ComplementaryFilter(200);

   final HertzCounterWidget hertzCounterWidget;

   public SensorFusionApp() {

      rollComparisonGraph = new RunningLineGraphPlotWidget(new String[] { "y accel", "gyro x", "roll", "compl. roll" }, "Compare roll values", "time in ms", "-", 600);
      pitchComparisonGraph = new RunningLineGraphPlotWidget(new String[] { "x accel", "gyro y", "pitch", "compl. pitch" }, "Compare pitch values", "time in ms", "-", 600);
      rollDeltaGraph = new RunningLineGraphPlotWidget(new String[] { "delta roll" }, "observe roll drift", "time in ms", "-", 600);
      pitchDeltaGraph = new RunningLineGraphPlotWidget(new String[] { "delta pitch" }, "observe pitch drift", "time in ms", "-", 600);

      SerialPortWidget serialPortWidget = new SerialPortWidget("Choose serial port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {

            if (serialMessageHandler != null && serialMessageHandler.isSerialPortOpened()) {
               return;
            }

            startedTime = System.currentTimeMillis();
            hertzCounterWidget.start();

            serialMessageHandler = new SerialMessageHandler(COM_PORT, BAUD_RATE, serialMessage -> {
               final long passedTime = getPassedTime();
               hertzCounterWidget.update();

               if (serialMessage instanceof SuperMessage) {
                  final SuperMessage superMessage = (SuperMessage) serialMessage;

                  final double accelX = superMessage.getAccelX();
                  final double accelY = superMessage.getAccelY();
                  final double accelZ = superMessage.getAccelZ();

                  final double gyroX = superMessage.getGyroX();
                  final double gyroY = superMessage.getGyroY();
                  final double gyroZ = superMessage.getGyroZ();

                  final double[] smooth = accelDataSmoother.smooth(accelX, accelY, accelZ);
                  sensorFusionFilter.update(accelX, accelY, accelZ, gyroX, gyroY, gyroZ);
                  final double integratedGyroX = ((ComplementaryFilter) sensorFusionFilter).getIntegratedGyroX();
                  final double integratedGyroY = ((ComplementaryFilter) sensorFusionFilter).getIntegratedGyroY();

                  // scale down acceleration to fit in orientation graph
                  rollComparisonGraph.addData(passedTime, accelY / 10, gyroX, integratedGyroX, sensorFusionFilter.getRoll());
                  pitchComparisonGraph.addData(passedTime, accelX / 10, gyroY, integratedGyroY, sensorFusionFilter.getPitch());

                  // display drift
                  rollDeltaGraph.addData(passedTime, FastMath.abs(sensorFusionFilter.getRoll() - integratedGyroX));
                  pitchDeltaGraph.addData(passedTime, FastMath.abs(sensorFusionFilter.getPitch() - integratedGyroY));
               }
            });
            serialMessageHandler.startSerial();
         }

         @Override
         public void onStopSerial() {
            serialMessageHandler.closeSerialPort();
            hertzCounterWidget.stop();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {
            // not implemented
         }
      });

      VBox vContainer = new VBox(20);
      HBox header = new HBox(20);

      hertzCounterWidget = new HertzCounterWidget();
      header.getChildren()
            .addAll(serialPortWidget, hertzCounterWidget);

      HBox fusionContainer = new HBox(20);
      fusionContainer.getChildren()
            .addAll(rollComparisonGraph.getElement(), pitchComparisonGraph.getElement());

      HBox driftContainer = new HBox(20);
      driftContainer.getChildren()
            .addAll(rollDeltaGraph.getElement(), pitchDeltaGraph.getElement());

      vContainer.getChildren()
            .addAll(header, fusionContainer, driftContainer);

      this.getChildren()
            .add(vContainer);

      rollComparisonGraph.setWidth(700);
      pitchComparisonGraph.setWidth(700);
      rollDeltaGraph.setWidth(700);
      pitchDeltaGraph.setWidth(700);

   }

   private long getPassedTime() {
      final long passedTime = System.currentTimeMillis() - startedTime;
      return passedTime;
   }
}
