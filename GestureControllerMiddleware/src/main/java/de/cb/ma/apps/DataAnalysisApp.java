package de.cb.ma.apps;

import de.cb.ma.MainDataAnalysisApp;
import de.cb.ma.core.widgets.analysis.OfflineDataExtractor;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.core.widgets.file.FileChooserWidget;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class DataAnalysisApp extends Pane {

   private TextField experimentDescriptionTF;

   private LineGraphPlotWidget accelGraph;

   public DataAnalysisApp() {

      OfflineDataExtractor dataExtractor = new OfflineDataExtractor(new OfflineDataExtractor.Callback() {
         @Override
         public void onLineExtracted(final int passedTime, final double[] values) {
            List<Number> accelData = new ArrayList<>(3);
            accelData.add(values[0]);
            accelData.add(values[1]);
            accelData.add(values[1]);

            accelGraph.addData((Number) passedTime, accelData);
         }

         @Override
         public void onError() {
            //
         }
      });

      FileChooserWidget fileChooserWidget = new FileChooserWidget("Choose file", MainDataAnalysisApp.CURRENT_STAGE, new FileChooserWidget.Callback() {
         @Override
         public void onFileSelected(final File file) {
            dataExtractor.load(file);
         }
      });

      accelGraph = new LineGraphPlotWidget(new String[] { "x", "y", "z" }, "chart title", "x label", "y label");
      accelGraph.getLineChart()
            .setCreateSymbols(false);

      Button createFileBtn = new Button("Choose");
      experimentDescriptionTF = new TextField();
      experimentDescriptionTF.setPromptText("Enter some descriptive words");
      Label currentFilePathLabel = new Label();

      experimentDescriptionTF.textProperty()
            .addListener((observable, oldValue, newValue) -> {
            });

      VBox vContainer = new VBox(20);
      HBox hContainer = new HBox(20);

      HBox fileControl = new HBox(20);
      fileControl.getChildren()
            .addAll(fileChooserWidget);

      vContainer.getChildren()
            .addAll(hContainer, fileControl, accelGraph.getElement());

      this.getChildren()
            .add(vContainer);
   }

}
