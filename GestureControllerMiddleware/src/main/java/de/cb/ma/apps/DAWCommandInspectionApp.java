package de.cb.ma.apps;

import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.widgets.communication.MidiPortWidget;
import de.cb.ma.core.widgets.contentmanagement.daw.CreateDAWCommandWidget;
import de.cb.ma.core.widgets.contentmanagement.daw.DAWCommandListWidget;
import de.cb.ma.core.widgets.contentmanagement.daw.DAWCommandMappingModel;
import de.cb.ma.util.MidiHelper;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javax.sound.midi.MidiUnavailableException;

public class DAWCommandInspectionApp extends Pane {

   public DAWCommandInspectionApp(DAWCommandDao dawCommandDao, MidiHelper midiHelper) {
      this.dawCommandDao = dawCommandDao;

      HBox header = new HBox(20);

      final MidiPortWidget midiPortWidget = new MidiPortWidget("choose MIDI port", midiHelper, index -> {
         try {
            midiHelper.useMidiDevice(index);
         } catch (MidiUnavailableException e) {
            e.printStackTrace();
         }
      });

      header.getChildren()
            .addAll(midiPortWidget);

      HBox body = new HBox(20);
      dawCommandListWidget = new DAWCommandListWidget(dawCommandDao, new DAWCommandListWidget.PlayCallback() {
         @Override
         public void onSelected(final int index, final DAWCommandMappingModel dawCommand) {
            final DAWCommand command = dawCommandDao.findById(dawCommand.getDawCommandId());
            midiHelper.sendChannelMessage(command);
         }
      }, new DAWCommandListWidget.DeleteCallback() {
         @Override
         public void onSelected(final int index, final DAWCommandMappingModel dawCommand) {
            final DAWCommand byId = dawCommandDao.findById(dawCommand.getDawCommandId());
            dawCommandDao.delete(byId);
            dawCommandListWidget.refresh();
         }
      });

      createDAWCommandWidget = new CreateDAWCommandWidget(dawCommandDao, new CreateDAWCommandWidget.Callback() {
         public void onCreation(final DAWCommand dawCommand) {
            dawCommandListWidget.refresh();
         }
      });

      body.getChildren()
            .addAll(dawCommandListWidget, createDAWCommandWidget);

      final VBox container = new VBox(20);
      container.getChildren()
            .addAll(header, body);

      this.getChildren()
            .addAll(container);
   }

   private DAWCommandDao dawCommandDao;
   private final CreateDAWCommandWidget createDAWCommandWidget;
   private final DAWCommandListWidget dawCommandListWidget;
}
