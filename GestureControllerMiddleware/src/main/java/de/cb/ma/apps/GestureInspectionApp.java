package de.cb.ma.apps;

import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.dao.ProfileDao;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.widgets.gesture.DisplayGestureWidget;
import de.cb.ma.core.widgets.analysis.SelectedGestureInfoWidget;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.List;

public class GestureInspectionApp extends Pane {

   public GestureInspectionApp(GestureModelDao gestureModelDao, ProfileDao profileDao) {
      this.gestureModelDao = gestureModelDao;
      this.profileDao = profileDao;

      final Label title = new Label("choose a gesture to display");

      gestureModels = gestureModelDao.findAll();

      List<String> gestureNames = new ArrayList<>();

      for (GestureModel gestureModel : gestureModels) {
         gestureNames.add(gestureModel.getName());
      }

      final ComboBox<String> gestureSelectionBox = new ComboBox<>(FXCollections.observableArrayList(gestureNames));
      gestureSelectionBox.valueProperty()
            .addListener((observable, oldValue, newValue) -> {
               final int selectedIndex = gestureSelectionBox.getSelectionModel()
                     .getSelectedIndex();
               final GestureModel selectedGesture = gestureModels.get(selectedIndex);
               displayGestureWidget.displayGesture(selectedGesture.getAccelerations()
                     .getAmplitudes());
               singleGestureInfoWidget.showInfo(selectedGesture);
            });

      gestureSelectionBox.setPromptText("Select Gesture");

      final VBox body = new VBox(20);

      displayGestureWidget = new DisplayGestureWidget();

      HBox infoContainer = new HBox(20);

      singleGestureInfoWidget = new SelectedGestureInfoWidget("gesture infos");

      infoContainer.getChildren()
            .addAll(displayGestureWidget, singleGestureInfoWidget);

      deleteBtn = new Button("delete");
      deleteBtn.setOnAction(event -> {
         final int selectedIndex = gestureSelectionBox.getSelectionModel()
               .getSelectedIndex();

         // remove gesture model from the database
         final GestureModel gestureModel = gestureModels.get(selectedIndex);
         gestureModelDao.delete(gestureModel);

         // remove gesture mapping from profile, cause the gesture does not exist anymore
         profileDao.findAll()
               .get(0)
               .getGestureMapping()
               .remove(gestureModel.getId());

         updateWidget();
      });

      body.getChildren()
            .addAll(title, gestureSelectionBox, infoContainer, deleteBtn);

      this.getChildren()
            .addAll(body);
   }

   private void updateWidget() {
      // update graph widget
      // update model list
      // update selection box
   }

   private GestureModelDao gestureModelDao;
   private ProfileDao profileDao;
   private final List<GestureModel> gestureModels;
   private DisplayGestureWidget displayGestureWidget;
   private final Button deleteBtn;
   private SelectedGestureInfoWidget singleGestureInfoWidget;
}
