package de.cb.ma.apps;

import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.dao.ProfileDao;
import de.cb.ma.core.database.model.Profile;
import de.cb.ma.core.widgets.communication.MidiPortWidget;
import de.cb.ma.core.widgets.contentmanagement.profile.CreateProfileWidget;
import de.cb.ma.core.widgets.contentmanagement.profile.ProfileMappingModel;
import de.cb.ma.core.widgets.contentmanagement.profile.ProfileMappingTableWidget;
import de.cb.ma.core.widgets.performance.ProfileSelectionWidget;
import de.cb.ma.util.MidiHelper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javax.sound.midi.MidiUnavailableException;

/**
 * under construction
 */
public class ProfileInspectionApp extends Pane {

   public ProfileInspectionApp(MidiHelper midiHelper, ProfileDao profileDao, GestureModelDao gestureModelDao, DAWCommandDao dawCommandDao) {
      this.midiHelper = midiHelper;

      HBox header = new HBox(20);

      HBox body = new HBox(20);

      MidiPortWidget midiPortWidget = new MidiPortWidget("choose midi port", midiHelper, new MidiPortWidget.Callback() {
         @Override
         public void onMidiPortSelected(final int index) {
            try {
               midiHelper.useMidiDevice(index);
            } catch (MidiUnavailableException e) {
               e.printStackTrace();
            }
         }
      });

      profileMappingTableWidget = new ProfileMappingTableWidget(gestureModelDao, dawCommandDao, null, new ProfileMappingTableWidget.DeleteCallback() {
         @Override
         public void onSelected(final int index, final ProfileMappingModel profileMappingModel) {
            selectedProfile.getGestureMapping()
                  .remove(profileMappingModel.getGestureId());
            profileMappingTableWidget.refresh(selectedProfile);
         }
      });

      profileSelectionWidget = new ProfileSelectionWidget(profileDao, new ChangeListener<Profile>() {
         @Override
         public void changed(final ObservableValue<? extends Profile> observable, final Profile oldValue, final Profile newValue) {
            selectedProfile = newValue;
            profileMappingTableWidget.refresh(newValue);
         }
      });

      header.getChildren()
            .addAll(midiPortWidget, profileSelectionWidget);

      final HBox hBox = new HBox(20);
      hBox.getChildren()
            .addAll(profileMappingTableWidget, new CreateProfileWidget(gestureModelDao, dawCommandDao, new CreateProfileWidget.Callback() {
               @Override
               public void onCreation(final String gestureModelId, final String dawCommandId) {
                  selectedProfile.getGestureMapping()
                        .put(gestureModelId, dawCommandId);
                  final Profile update = profileDao.update(selectedProfile);
                  profileMappingTableWidget.refresh(update);

               }
            }));
      body.getChildren()
            .addAll(hBox);

      final VBox container = new VBox(20);
      container.getChildren()
            .addAll(header, body);

      this.getChildren()
            .addAll(container);
   }

   private MidiHelper midiHelper;
   private final ProfileMappingTableWidget profileMappingTableWidget;
   private Profile selectedProfile;
   private final ProfileSelectionWidget profileSelectionWidget;
}
