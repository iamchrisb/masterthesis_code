package de.cb.ma.apps;

import de.cb.ma.core.database.InMemoryGestureMapping;
import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.model.AmplitudeModel;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.imu.GestureDetectorEngine;
import de.cb.ma.core.imu.GestureDetectorEngineCallbackImpl;
import de.cb.ma.core.imu.InertialDataInterface;
import de.cb.ma.core.imu.RichSensorDataFacade;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.imu.serial.SerialDataInterface;
import de.cb.ma.core.widgets.analysis.FingerDisplayWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.contentmanagement.GestureSaveWidget;
import de.cb.ma.core.widgets.gesture.DisplayGestureWidget;
import de.cb.ma.core.widgets.gesture.MatchedGestureInfoWidget;
import de.cb.ma.core.widgets.gesture.RecognizedGestureInfoWidget;
import de.cb.ma.util.Pair;
import de.cb.ma.util.UUIDGenerator;
import javafx.application.Platform;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class GestureDetectionApp extends Pane {

   public GestureDetectionApp(final InMemoryGestureMapping inMemoryGestureMapping, GestureModelDao gestureModelDao) {
      this.gestureModelDao = gestureModelDao;

      GestureDetectorEngine gestureDetectorEngine = new GestureDetectorEngine(inMemoryGestureMapping, new GestureDetectorEngineCallbackImpl() {
         @Override
         public void onGestureRecognized(final HandSign.SIGN posture, final GestureModel recognizedGestureModel, final List<Pair<Double, List<Double>>> recentCapturedAmplitudeValues,
               final double distance) {
            if (showInfoCheckbox.isSelected()) {
               System.out.println("on gesture recognized, updateing infos");
               recognizedGestureInfoWidget.showInfo(recognizedGestureModel, distance);
            }
         }

         @Override
         public void onAnyGestureCaptured(final HandSign.SIGN sign, final List<Pair<Double, List<Double>>> recentCapturedAmplitudeValues) {
            System.out.println("on any gesture captured");
            if (captureCheckBox.isSelected()) {
               List<Pair<Double, List<Double>>> clonedList = deepArCopy(recentCapturedAmplitudeValues);
               gestureMappingWidget.displayGesture(clonedList);
               matchedGestureInfoWidget.showInfo(recentCapturedAmplitudeValues.size(), sign);
               recognizedGestureInfoWidget.clear();
               Platform.runLater(() -> {
                  captureCheckBox.setSelected(false);
               });
            }
         }

         @Override
         public void onDetectionStart(final double varianceX, final double varianceY, final double varianceZ) {
            System.out.println("gesture detection started");
         }

         @Override
         public void onDetectionEnd(final double varianceX, final double varianceY, final double varianceZ) {
            System.out.println("gesture detection ended");
         }

         @Override
         public void onPostureDetected(final HandSign.SIGN sign, final Integer[] binaryPattern) {
            postureWidget.showHandSign(sign.name());
         }

      });

      inertialDataInterface = new RichSensorDataFacade(new RichSensorDataFacade.ImuCallback() {
         @Override
         public void onUpdate(final long processedTimeInMs) {
            gestureDetectorEngine.handleUpdate(processedTimeInMs);
            changeFingerAngles();
         }

         @Override
         public void onError() {
            gestureDetectorEngine.handleError();
         }
      });

      gestureDetectorEngine.setInertialDataInterface(inertialDataInterface);

      final SerialPortWidget serialPortWidget = new SerialPortWidget("Choose port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            ((SerialDataInterface) inertialDataInterface).start(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            ((SerialDataInterface) inertialDataInterface).stop();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      postureWidget = new FingerDisplayWidget("Bend angles");

      /**
       * header
       */
      HBox header = new HBox(20);
      captureCheckBox = new CheckBox("capture");
      showInfoCheckbox = new CheckBox("show info");

      gestureSaveWidget = new GestureSaveWidget();
      gestureSaveWidget.setButtonAction(event -> {
         onSaveSelected();
      });

      header.getChildren()
            .addAll(serialPortWidget, postureWidget, gestureSaveWidget, captureCheckBox, showInfoCheckbox);

      /**
       * body
       */
      VBox body = new VBox(20);

      HBox fatchedGestureCont = new HBox(20);
      gestureMappingWidget = new DisplayGestureWidget();

      VBox infoBox = new VBox(20);
      recognizedGestureInfoWidget = new RecognizedGestureInfoWidget("recognized gesture info");
      matchedGestureInfoWidget = new MatchedGestureInfoWidget("matched gesture info");

      infoBox.getChildren()
            .addAll(matchedGestureInfoWidget, recognizedGestureInfoWidget);

      fatchedGestureCont.getChildren()
            .addAll(gestureMappingWidget, infoBox);

      body.getChildren()
            .addAll(fatchedGestureCont);

      /**
       * end
       */

      VBox container = new VBox(20);
      container.getChildren()
            .addAll(header, body);

      this.getChildren()
            .add(container);
   }

   private List<Pair<Double, List<Double>>> deepArCopy(final List<Pair<Double, List<Double>>> list) {
      List<Pair<Double, List<Double>>> cloneList = new ArrayList<>();
      for (Pair<Double, List<Double>> pair : list) {

         final Double time = pair.getKey();
         final List<Double> samples = pair.getValue();
         final Pair<Double, List<Double>> clonedPair = new Pair<>();

         List<Double> clonedSamples = new ArrayList<>();
         for (Double sample : samples) {
            clonedSamples.add(new Double(sample));
         }

         clonedPair.setKey(new Double(time));
         clonedPair.setValue(clonedSamples);

         cloneList.add(clonedPair);
      }
      return cloneList;
   }

   private void onSaveSelected() {
      final String gestureName = gestureSaveWidget.getGestureName();
      final HandSign.SIGN identifier = gestureSaveWidget.getIdentifier();
      final AmplitudeModel amplitudes = gestureMappingWidget.getAmplitudes();

      final LinkedList<HandSign.SIGN> signs = new LinkedList<>();
      signs.add(identifier);

      final GestureModel gestureModel = new GestureModel(UUIDGenerator.uuid(), gestureName, amplitudes, signs);
      gestureModelDao.create(gestureModel);
   }

   private void changeFingerAngles() {
      final double[] flexAngles = this.inertialDataInterface.getFlexAngles();
      this.postureWidget.changeAllFingerSliderValues(flexAngles);
   }

   private final FingerDisplayWidget postureWidget;
   private final InertialDataInterface inertialDataInterface;
   private final DisplayGestureWidget gestureMappingWidget;
   private final CheckBox captureCheckBox;
   private final GestureSaveWidget gestureSaveWidget;
   private GestureModelDao gestureModelDao;
   private final RecognizedGestureInfoWidget recognizedGestureInfoWidget;
   private final MatchedGestureInfoWidget matchedGestureInfoWidget;
   private CheckBox showInfoCheckbox;
}
