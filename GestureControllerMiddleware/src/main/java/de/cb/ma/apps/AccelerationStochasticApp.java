package de.cb.ma.apps;

import de.cb.ma.core.imu.preprocessing.ApacheMathAmplitudeStochastic;
import de.cb.ma.core.imu.preprocessing.filter.ApacheAmplitudeSmoother;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.core.imu.serial.messages.application.SuperMessage;
import de.cb.ma.core.widgets.analysis.HertzCounterWidget;
import de.cb.ma.core.widgets.analysis.charts.RunningLineGraphPlotWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class AccelerationStochasticApp extends Pane {

   private static final int LASTEST_MSGS_COUNT = 70;
   public static final int ONE_SECOND_PERIOD = 1000;
   SerialMessageHandler serialMessageHandler;

   private long startedTime;

   ApacheAmplitudeSmoother accelDataSmoother = new ApacheAmplitudeSmoother(16, 3);
   ApacheMathAmplitudeStochastic amplitudeCharacterizer = new ApacheMathAmplitudeStochastic(16, 3);

   private RunningLineGraphPlotWidget smoothedAccelerationAmplitudeGraph;
   private RunningLineGraphPlotWidget meanGraph;
   private RunningLineGraphPlotWidget varianceGraph;
   private RunningLineGraphPlotWidget standardDeviationGraph;

   private HertzCounterWidget hertzCounterWidget;

   private int windowSize = 1200;

   public AccelerationStochasticApp() {

      smoothedAccelerationAmplitudeGraph = new RunningLineGraphPlotWidget(new String[] { "x", "y", "z" }, "Acceleration data", "time in ms", "acceleration in mG", windowSize);
      meanGraph = new RunningLineGraphPlotWidget(new String[] { "meanX", "meanY", "meanZ" }, "means of the 3 axes", "time in ms", "acceleration in mG", windowSize);
      varianceGraph = new RunningLineGraphPlotWidget(new String[] { "varianceX", "varianceY", "varianceZ" }, "variances of the 3 axes", "time in ms", "acceleration in mG", windowSize);
      standardDeviationGraph = new RunningLineGraphPlotWidget(new String[] { "stdDevX", "stdDevY",
            "stdDevZ" }, "standard deviations of the 3 axes", "time in ms", "acceleration in mG", windowSize);

      SerialPortWidget serialPortWidget = new SerialPortWidget("Choose serial port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {

            if (serialMessageHandler != null && serialMessageHandler.isSerialPortOpened()) {
               return;
            }

            startedTime = System.currentTimeMillis();

            hertzCounterWidget.start();

            serialMessageHandler = new SerialMessageHandler(COM_PORT, BAUD_RATE, serialMessage -> {
               final long passedTime = getPassedTime();
               hertzCounterWidget.update();

               if (serialMessage instanceof SuperMessage) {
                  final SuperMessage superMessage = (SuperMessage) serialMessage;

                  final double accelerationX = superMessage.getAccelX();
                  final double accelerationY = superMessage.getAccelY();
                  final double accelerationZ = superMessage.getAccelZ();

                  final double[] smooth = accelDataSmoother.smooth(accelerationX, accelerationY, accelerationZ);

                  smoothedAccelerationAmplitudeGraph.addData(passedTime, smooth[0], smooth[1], smooth[2]);

                  amplitudeCharacterizer.update(smooth[0], smooth[1], smooth[2]);

                  final double meanX = amplitudeCharacterizer.getMean(0);
                  final double varianceX = amplitudeCharacterizer.getVariance(0);
                  final double standardDeviationX = amplitudeCharacterizer.getStandardDeviation(0);

                  final double meanY = amplitudeCharacterizer.getMean(1);
                  final double varianceY = amplitudeCharacterizer.getVariance(1);
                  final double standardDeviationY = amplitudeCharacterizer.getStandardDeviation(1);

                  final double meanZ = amplitudeCharacterizer.getMean(2);
                  final double varianceZ = amplitudeCharacterizer.getVariance(2);
                  final double standardDeviationZ = amplitudeCharacterizer.getStandardDeviation(2);

                  meanGraph.addData(passedTime, meanX, meanY, meanZ);
                  varianceGraph.addData(passedTime, varianceX, varianceY, varianceZ);
                  standardDeviationGraph.addData(passedTime, standardDeviationX, standardDeviationY, standardDeviationZ);
               }
            });

            serialMessageHandler.startSerial();
         }

         @Override
         public void onStopSerial() {
            serialMessageHandler.closeSerialPort();
            hertzCounterWidget.stop();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      smoothedAccelerationAmplitudeGraph.getLineChart()
            .setCreateSymbols(false);

      VBox vContainer = new VBox(20);
      HBox header = new HBox(20);

      hertzCounterWidget = new HertzCounterWidget();

      header.getChildren()
            .addAll(serialPortWidget, hertzCounterWidget);

      HBox imuGraphContainer = new HBox(20);
      imuGraphContainer.getChildren()
            .addAll(meanGraph.getElement(), varianceGraph.getElement(), standardDeviationGraph.getElement());

      vContainer.getChildren()
            .addAll(header, smoothedAccelerationAmplitudeGraph.getElement(), imuGraphContainer);

      this.getChildren()
            .add(vContainer);

      smoothedAccelerationAmplitudeGraph.getLineChart()
            .setPrefWidth(1400);
      meanGraph.getLineChart()
            .setMinWidth(500);
      varianceGraph.getLineChart()
            .setMinWidth(500);
      standardDeviationGraph.getLineChart()
            .setMinWidth(500);
   }

   private long getPassedTime() {
      final long passedTime = System.currentTimeMillis() - startedTime;
      return passedTime;
   }

}
