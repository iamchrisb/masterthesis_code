package de.cb.ma.apps;

import de.cb.ma.MainDataAnalysisApp;
import de.cb.ma.core.imu.InertialDataInterface;
import de.cb.ma.core.imu.RichSensorDataFacade;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.imu.motion.HandSignDetector;
import de.cb.ma.core.imu.serial.SerialDataInterface;
import de.cb.ma.core.widgets.analysis.FingerDisplayWidget;
import de.cb.ma.core.widgets.communication.MidiPortWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.performance.MagneticSynthesizer;
import de.cb.ma.core.widgets.performance.TiltConsumerWidget;
import de.cb.ma.util.MidiHelper;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javax.sound.midi.MidiUnavailableException;

public class MagneticInstrumentApp extends Pane {

   public MagneticInstrumentApp() {

      HBox header = new HBox(20);

      final MidiHelper midiHelper = MainDataAnalysisApp.midiHelper;

      magneticSynthesizer = new MagneticSynthesizer(midiHelper);

      inertialDataFacade = new RichSensorDataFacade(new RichSensorDataFacade.ImuCallback() {
         @Override
         public void onUpdate(final long processedTimeInMs) {
            if (isActive.isSelected()) {
               magneticSynthesizer.handleUpdate(processedTimeInMs);
            }
            updateBendAngles();
            updateOrientationWidget();
            updateHandSignDetector();
         }

         @Override
         public void onError() {
            magneticSynthesizer.handleError();
         }
      });

      magneticSynthesizer.setInertialDataInterface(inertialDataFacade);

      MidiPortWidget midiPortWidget = new MidiPortWidget("Select midi Port", midiHelper, index -> {
         try {
            midiHelper.useMidiDevice(index);
         } catch (MidiUnavailableException e) {
            e.printStackTrace();
         }
      });

      SerialPortWidget serialPortWidget = new SerialPortWidget("Choose serial port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            ((SerialDataInterface) magneticSynthesizer.getInertialDataInterface()).start(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            ((SerialDataInterface) magneticSynthesizer.getInertialDataInterface()).stop();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {
            // do nothing
         }
      });

      handSignDetector = new HandSignDetector(0.3, new HandSignDetector.Callback() {
         @Override
         public void onPostureDetected(final HandSign.SIGN posture, final Integer[] binaryPattern) {
            currentPosture = posture;
            magneticSynthesizer.updatePosture(posture);
         }
      });

      postureWidget = new FingerDisplayWidget("Bend angles");

      pitchWidget = new TiltConsumerWidget("Control pitch", TiltConsumerWidget.AXIS.ROLL, HandSign.SIGN.POINT, TiltConsumerWidget.CONTROL_PARAM.PITCH, midiHelper);

      this.isActive = new CheckBox(
            "Instrument is Active");

      header.getChildren()
            .addAll(serialPortWidget, midiPortWidget, postureWidget, pitchWidget, isActive);

      final HBox body = new HBox();

      //      body.getChildren()
      //            .addAll(isActive);

      final VBox container = new VBox(20);
      container.getChildren()
            .addAll(header, body);

      this.getChildren()
            .addAll(container);

   }

   private void updateHandSignDetector() {
      this.handSignDetector.updateValues(this.inertialDataFacade.getFlexAngles());
   }

   private void updateOrientationWidget() {
      this.pitchWidget.updateAngle(this.currentPosture, this.inertialDataFacade.getRoll(), this.inertialDataFacade.getPitch());
   }

   private void updateBendAngles() {
      postureWidget.changeAllFingerSliderValues(this.inertialDataFacade.getFlexAngles());
      postureWidget.showHandSign(this.currentPosture.name());
   }

   private MagneticSynthesizer magneticSynthesizer;

   private final FingerDisplayWidget postureWidget;
   private final InertialDataInterface inertialDataFacade;
   private final TiltConsumerWidget pitchWidget;
   private final HandSignDetector handSignDetector;
   private HandSign.SIGN currentPosture = HandSign.SIGN.UNDETECTED;
   private CheckBox isActive;
}
