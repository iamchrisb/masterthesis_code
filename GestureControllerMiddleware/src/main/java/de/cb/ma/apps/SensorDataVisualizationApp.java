package de.cb.ma.apps;

import de.cb.ma.core.imu.InertialDataInterface;
import de.cb.ma.core.imu.RichSensorDataFacade;
import de.cb.ma.core.imu.preprocessing.filter.sensorfusion.ComplementaryFilter;
import de.cb.ma.core.imu.serial.SerialDataInterface;
import de.cb.ma.core.widgets.analysis.charts.RunningLineGraphPlotWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class SensorDataVisualizationApp extends Pane {

   private String REAL_ACCELEROMETER = "-accel";
   private String SMOOTH_ACCELEROMETER = "-accelsmooth";
   private String GYROMETER = "-gyro";
   private String MAGNETOMETER = "-mag";
   private String BEND_ANGLES = "-flex";

   final ComplementaryFilter sensorFusionFilter;

   public SensorDataVisualizationApp(String subStatus) {

      String[] legendNames = { "x", "y", "z" };
      String yAxisLabel = "-";
      String[] xLegendNames = new String[] { "x" };
      String[] yLegendNames = new String[] { "y" };
      String[] zLegendNames = new String[] { "z" };

      if (subStatus.equals(REAL_ACCELEROMETER)) {
         yAxisLabel = "acceleration in milliGaus";
      } else if (subStatus.equals(SMOOTH_ACCELEROMETER)) {
         yAxisLabel = "acceleration in milliGaus";
      } else if (subStatus.equals(GYROMETER)) {
         yAxisLabel = "deg/sec";
//         legendNames = new String[] { "x", "y", "z", "roll", "pitch", "yaw" };
      } else if (subStatus.equals(MAGNETOMETER)) {
         yAxisLabel = "milliVolt/Gaus";
      } else if (subStatus.equals(BEND_ANGLES)) {
         yAxisLabel = "angle in degree";
         legendNames = new String[] { "thumb", "index", "middle", "ring", "pinky" };
      }

      int windowSize = 2000;
      axesGraph = new RunningLineGraphPlotWidget(legendNames, "Compare values", "time in ms", yAxisLabel, windowSize);
      xAxisGraph = new RunningLineGraphPlotWidget(xLegendNames, "x axis", "time in ms", yAxisLabel, windowSize);
      yAxisGraph = new RunningLineGraphPlotWidget(yLegendNames, "y axis", "time in ms", yAxisLabel, windowSize);
      zAxisGraph = new RunningLineGraphPlotWidget(zLegendNames, "z axis", "time in ms", yAxisLabel, windowSize);

      HBox header = new HBox(20);

      richSensorDataFacade = new RichSensorDataFacade(new RichSensorDataFacade.ImuCallback() {
         @Override
         public void onUpdate(final long processedTimeInMs) {
            if (isDisplaying.isSelected()) {
               if (subStatus.equals(REAL_ACCELEROMETER)) {
                  final double[] accelerationReal = richSensorDataFacade.getAccelerationReal();
                  axesGraph.addData(processedTimeInMs, accelerationReal[0], accelerationReal[1], accelerationReal[2]);
                  xAxisGraph.addData(processedTimeInMs, accelerationReal[0]);
                  yAxisGraph.addData(processedTimeInMs, accelerationReal[1]);
                  zAxisGraph.addData(processedTimeInMs, accelerationReal[2]);
               } else if (subStatus.equals(SMOOTH_ACCELEROMETER)) {
                  final double[] accelerationSmoothed = richSensorDataFacade.getAccelerationSmoothed();
                  axesGraph.addData(processedTimeInMs, accelerationSmoothed[0], accelerationSmoothed[1], accelerationSmoothed[2]);
                  xAxisGraph.addData(processedTimeInMs, accelerationSmoothed[0]);
                  yAxisGraph.addData(processedTimeInMs, accelerationSmoothed[1]);
                  zAxisGraph.addData(processedTimeInMs, accelerationSmoothed[2]);
               } else if (subStatus.equals(GYROMETER)) {
                  final double[] gyro = richSensorDataFacade.getGyro();
//                  axesGraph.addData(processedTimeInMs, gyro[0], gyro[1], gyro[2], sensorFusionFilter.getIntegratedGyroX(), sensorFusionFilter.getIntegratedGyroY(), sensorFusionFilter.getIntegratedGyroZ());
//                  axesGraph.addData(processedTimeInMs, gyro[0], gyro[1], gyro[2]);
                  axesGraph.addData(processedTimeInMs, sensorFusionFilter.getIntegratedGyroX(), sensorFusionFilter.getIntegratedGyroY(), sensorFusionFilter.getIntegratedGyroZ());
//                  xAxisGraph.addData(processedTimeInMs, gyro[0]);
//                  yAxisGraph.addData(processedTimeInMs, gyro[1]);
//                  zAxisGraph.addData(processedTimeInMs, gyro[2]);
                  xAxisGraph.addData(processedTimeInMs, sensorFusionFilter.getIntegratedGyroX());
                  yAxisGraph.addData(processedTimeInMs, sensorFusionFilter.getIntegratedGyroY());
                  zAxisGraph.addData(processedTimeInMs, sensorFusionFilter.getIntegratedGyroZ());
               } else if (subStatus.equals(MAGNETOMETER)) {
                  final double[] magnetometer = richSensorDataFacade.getMagnetometer();
                  axesGraph.addData(processedTimeInMs, magnetometer[0], magnetometer[1], magnetometer[2]);
                  xAxisGraph.addData(processedTimeInMs, magnetometer[0]);
                  yAxisGraph.addData(processedTimeInMs, magnetometer[1]);
                  zAxisGraph.addData(processedTimeInMs, magnetometer[2]);
               } else if (subStatus.equals(BEND_ANGLES)) {
                  final double[] flexis = richSensorDataFacade.getFlexAngles();
                  axesGraph.addData(processedTimeInMs, flexis[0], flexis[1], flexis[2], flexis[3], flexis[4]);
               }
            }
         }

         @Override
         public void onError() {
         }
      });

      sensorFusionFilter = (ComplementaryFilter) richSensorDataFacade.getSensorFusionFilter();

      SerialPortWidget serialPortWidget = new SerialPortWidget("Choose serial port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            ((SerialDataInterface) richSensorDataFacade).start(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            ((SerialDataInterface) richSensorDataFacade).stop();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {
            // do nothing
         }
      });

      this.isDisplaying = new CheckBox("capture data");

      final Button clearBtn = new Button("Clear");

      header.getChildren()
            .addAll(serialPortWidget, isDisplaying, clearBtn);

      final VBox body = new VBox(20);

      axesGraph.setWidth(1500);

      xAxisGraph.setWidth(500);
      yAxisGraph.setWidth(500);
      zAxisGraph.setWidth(500);

      HBox graphContainer = new HBox(20);
      graphContainer.getChildren()
            .addAll(xAxisGraph.getElement(), yAxisGraph.getElement(), zAxisGraph.getElement());

      body.getChildren()
            .addAll(axesGraph.getElement(), graphContainer);

      final VBox container = new VBox(20);
      container.getChildren()
            .addAll(header, body);

      this.getChildren()
            .addAll(container);

   }

   private final InertialDataInterface richSensorDataFacade;
   private CheckBox isDisplaying;
   private final RunningLineGraphPlotWidget axesGraph;
   private final RunningLineGraphPlotWidget yAxisGraph;
   private final RunningLineGraphPlotWidget zAxisGraph;
   private final RunningLineGraphPlotWidget xAxisGraph;
}
