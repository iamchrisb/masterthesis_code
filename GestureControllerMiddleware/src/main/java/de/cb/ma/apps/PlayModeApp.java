package de.cb.ma.apps;

import de.cb.ma.core.database.InMemoryGestureMapping;
import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.dao.ProfileDao;
import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.database.model.Profile;
import de.cb.ma.core.imu.GestureDetectorEngine;
import de.cb.ma.core.imu.GestureDetectorEngineCallbackImpl;
import de.cb.ma.core.imu.InertialDataInterface;
import de.cb.ma.core.imu.RichSensorDataFacade;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.imu.serial.SerialDataInterface;
import de.cb.ma.core.widgets.analysis.FingerDisplayWidget;
import de.cb.ma.core.widgets.communication.MidiPortWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.widgets.contentmanagement.profile.ProfileMappingTableWidget;
import de.cb.ma.core.widgets.logging.GestureEventLogWidget;
import de.cb.ma.core.widgets.logging.MidiEventLogWidget;
import de.cb.ma.core.widgets.gesture.RecognizedGestureInfoWidget;
import de.cb.ma.core.widgets.performance.ProfileSelectionWidget;
import de.cb.ma.core.widgets.performance.TiltConsumerWidget;
import de.cb.ma.util.MidiHelper;
import de.cb.ma.util.Pair;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.List;

import javax.sound.midi.MidiUnavailableException;

public class PlayModeApp extends Pane {

   public PlayModeApp(InMemoryGestureMapping inMemoryGestureMapping, MidiHelper midiHelper, ProfileDao profileDao, DAWCommandDao dawCommandDao, GestureModelDao gestureModelDao) {

      final ProfileSelectionWidget profileSelectionWidget = new ProfileSelectionWidget(profileDao, (observable, oldValue, newValue) -> {
         currentProfile = newValue;
      });

      gestureDetectorEngine = new GestureDetectorEngine(inMemoryGestureMapping, new GestureDetectorEngineCallbackImpl() {
         @Override
         public void onPostureDetected(final HandSign.SIGN sign, final Integer[] binaryPattern) {
            currentHandSign = sign;
            postureWidget.showHandSign(sign.name());
         }

         @Override
         public void onGestureRecognized(final HandSign.SIGN posture, final GestureModel recognizedGestureModel, final List<Pair<Double, List<Double>>> recentCapturedAmplitudeValues,
               final double distance) {
            recognizedGestureInfoWidget.showInfo(recognizedGestureModel, distance);

            final long time = System.currentTimeMillis();

            gestureEventLogWidget.addLog("Gesture recognized: " + recognizedGestureModel.getName());
            final String dawCommandId = currentProfile.getGestureMapping()
                  .get(recognizedGestureModel.getId());

            final DAWCommand dawCommand = dawCommandDao.findById(dawCommandId);
            if (dawCommand == null) {
               return;
            }

            dawCommandEventLogWidget.addLog(time + ": " + dawCommand.getCommand());

            midiHelper.sendChannelMessage(dawCommand);
         }
      });

      this.dawCommandDao = dawCommandDao;

      final MidiPortWidget midiPortWidget = new MidiPortWidget("choose midi port", midiHelper, index -> {
         try {
            midiHelper.useMidiDevice(index);
         } catch (MidiUnavailableException e) {
            e.printStackTrace();
         }
      });

      inertialDataFacade = new RichSensorDataFacade(new RichSensorDataFacade.ImuCallback() {
         @Override
         public void onUpdate(final long processedTimeInMs) {
            updateOrientationConsumer(processedTimeInMs);
            updateBendAngles();
            gestureDetectorEngine.handleUpdate(processedTimeInMs);
         }

         @Override
         public void onError() {

         }
      });

      gestureDetectorEngine.setInertialDataInterface(inertialDataFacade);

      serialPortWidget = new SerialPortWidget("choose serial port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {
            ((SerialDataInterface) inertialDataFacade).start(COM_PORT, BAUD_RATE);
         }

         @Override
         public void onStopSerial() {
            ((SerialDataInterface) inertialDataFacade).stop();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      dawCommandEventLogWidget = new MidiEventLogWidget();
      gestureEventLogWidget = new GestureEventLogWidget();

      recognizedGestureInfoWidget = new RecognizedGestureInfoWidget("Recognized gesture infos");

      postureWidget = new FingerDisplayWidget("Bend angles");

      volumeWidget = new TiltConsumerWidget("control volume", TiltConsumerWidget.AXIS.ROLL, HandSign.SIGN.POINT, TiltConsumerWidget.CONTROL_PARAM.VOLUME, midiHelper);
      pitchWidget = new TiltConsumerWidget("control pitch", TiltConsumerWidget.AXIS.PITCH, HandSign.SIGN.POINT, TiltConsumerWidget.CONTROL_PARAM.PITCH, midiHelper);

      /**
       * header
       */

      HBox header = new HBox(20);

      header.getChildren()
            //            .addAll(serialPortWidget, midiPortWidget, volumeWidget, pitchWidget, postureWidget, profileSelectionWidget);
            .addAll(serialPortWidget, midiPortWidget, postureWidget, profileSelectionWidget);

      /**
       * body
       */

      VBox body = new VBox(20);
      HBox infoAndLogContainer = new HBox(20);

      infoAndLogContainer.getChildren()
            .addAll(gestureEventLogWidget, dawCommandEventLogWidget, recognizedGestureInfoWidget);

      HBox someContainer = new HBox(20);
      profileMappingTableWidget = new ProfileMappingTableWidget(gestureModelDao, dawCommandDao, null, null);

      someContainer.getChildren()
            .addAll(profileMappingTableWidget);

      body.getChildren()
            .addAll(infoAndLogContainer, someContainer);

      final VBox container = new VBox(20);
      container.getChildren()
            .addAll(header, body);

      this.getChildren()
            .addAll(container);
   }

   private void updateBendAngles() {
      this.postureWidget.changeAllFingerSliderValues(inertialDataFacade.getFlexAngles());
   }

   private void updateOrientationConsumer(final long processedTimeInMs) {
      final double roll = inertialDataFacade.getRoll();
      final double pitch = inertialDataFacade.getPitch();

      this.volumeWidget.updateAngle(this.currentHandSign, roll, pitch);
      this.pitchWidget.updateAngle(this.currentHandSign, roll, pitch);
   }

   private final SerialPortWidget serialPortWidget;
   private final InertialDataInterface inertialDataFacade;
   private HandSign.SIGN currentHandSign;
   private final TiltConsumerWidget volumeWidget;
   private final TiltConsumerWidget pitchWidget;
   private final GestureDetectorEngine gestureDetectorEngine;
   private DAWCommandDao dawCommandDao;
   private final FingerDisplayWidget postureWidget;
   private Profile currentProfile;
   private final MidiEventLogWidget dawCommandEventLogWidget;
   private final GestureEventLogWidget gestureEventLogWidget;
   private final RecognizedGestureInfoWidget recognizedGestureInfoWidget;
   private final ProfileMappingTableWidget profileMappingTableWidget;
}
