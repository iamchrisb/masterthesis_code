package de.cb.ma.apps;

import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.core.widgets.analysis.HertzCounterWidget;
import de.cb.ma.core.widgets.analysis.charts.RunningLineGraphPlotWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class DataFrequencyMeasurementApp extends Pane {

   SerialMessageHandler serialMessageHandler;

   private RunningLineGraphPlotWidget hertzGraph;

   final HertzCounterWidget hertzCounterWidget;

   public DataFrequencyMeasurementApp() {

      hertzGraph = new RunningLineGraphPlotWidget(new String[] { "frequency" }, "display data frequency of the last 60 seconds", "time in s", "frequency in hertz", 60);

      SerialPortWidget serialPortWidget = new SerialPortWidget("Choose serial port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {

            if (serialMessageHandler != null && serialMessageHandler.isSerialPortOpened()) {
               return;
            }

            hertzCounterWidget.start();

            serialMessageHandler = new SerialMessageHandler(COM_PORT, BAUD_RATE, serialMessage -> {
               hertzCounterWidget.update();
            });

            serialMessageHandler.startSerial();

         }

         @Override
         public void onStopSerial() {
            serialMessageHandler.closeSerialPort();
            hertzCounterWidget.stop();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      VBox vContainer = new VBox(20);
      HBox header = new HBox(20);

      hertzCounterWidget = new HertzCounterWidget((passedSecs, ticks) -> hertzGraph.addData(passedSecs, ticks));
      header.getChildren()
            .addAll(serialPortWidget, hertzCounterWidget);

      vContainer.getChildren()
            .addAll(header, hertzGraph.getElement());

      this.getChildren()
            .add(vContainer);

      hertzGraph.setWidth(1400);
   }
}
