package de.cb.ma;

import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.dao.ProfileDao;
import de.cb.ma.core.database.dao.fileimpl.GestureModelDaoFileImpl;
import de.cb.ma.core.database.dao.fileimpl.ProfileDaoFileImpl;
import de.cb.ma.core.database.dao.fileimpl.DAWCommandDaoFileImpl;
import de.cb.ma.util.Constants;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GestureTest {

   public static void main(String[] args) {

      String portName = "COM12";
      int baudRate = 1152000;

      ObjectMapper objectMapper = new ObjectMapper();

      DAWCommandDao dawCommandDao = new DAWCommandDaoFileImpl(Constants.DB_DAW_COMMANDS_PATH, objectMapper);
      //      final MidiMessageParameter midiMessageParameter = new MidiMessageParameter(1, 0, 0, MidiMessageParameter.MIDI_MESSAGE_TYPE.CONTROL_CHANGE);
      //      dawCommandDao.create(new DAWCommand(UUIDGenerator.uuid(), "VOLUME CHANGE", "Changes the main volume of the DAW", DAWCommand.MANUFACTURER.FL_STUDIO, midiMessageParameter));

      ProfileDao profileDao = new ProfileDaoFileImpl(Constants.DB_PROFILES_PATH, objectMapper);
      //      profileDao.create(new Profile(UUIDGenerator.uuid(), "FL-Studio Profile"));

      GestureModelDao gestureModelDao = new GestureModelDaoFileImpl(Constants.DB_GESTURE_MODELS_PATH, objectMapper);

      //      final Profile profile = profileDao.findAll()
      //            .get(0);
      //
      //      final HashMap<String, String> gestureMapping = new HashMap<>();
      //      final List<DAWCommand> dawCommands = dawCommandDao.findAll();
      //
      //      final List<GestureModel> gestureModels = gestureModelDao.findAll();
      //
      //      gestureMapping.put(gestureModels
      //            .get(0)
      //            .getId(), dawCommands
      //            .get(0)
      //            .getId());
      //      profile.setGestureMapping(gestureMapping);
      //
      //      profileDao.update(profile);

      //      final LinkedList<HandPosture.SIGN> identifiers = new LinkedList<>();
      //      identifiers.add(HandPosture.SIGN.FIST);
      //      final LinkedList<String> axesNames = new LinkedList<>();
      //      axesNames.add("x");
      //      axesNames.add("y");
      //      axesNames.add("z");
      //
      //      final LinkedList<Pair<Double, List<Double>>> amplitudes = new LinkedList<>();
      //      for (int i = 0; i < 10; i++) {
      //         final LinkedList<Double> dataPoint = new LinkedList<>();
      //         dataPoint.add(Math.random());
      //         dataPoint.add(Math.random());
      //         dataPoint.add(Math.random());
      //
      //         final Pair<Double, List<Double>> timePoint = new Pair<>((double) i, dataPoint);
      //         amplitudes.add(timePoint);
      //      }
      //
      //      final AmplitudeSpectrum amplitudeSpectrum = new AmplitudeSpectrum(3, amplitudes, axesNames);
      //
      //      gestureModelDao.create(new GestureModel(UUIDGenerator.uuid(), "VERTICAL_DRUM_HIT", amplitudeSpectrum, identifiers));

      //      InMemoryMapping inMemoryMapping = new InMemoryMapping(gestureModelDao);

      //      final List<GestureModel> gesturesByHandSign = inMemoryMapping.getGesturesByHandSign(HandPosture.SIGN.FIST);

      //      GestureDetectorEngine gestureDetectorEngine = new GestureDetectorEngine(gestureMappingDao, new GestureDetectorEngineCallbackImpl() {
      //         @Override
      //         public void onGestureRecognized(final HandPosture.SIGN posture, final List<Pair<Double, List<Double>>> capturedGestureFrequencies) {
      //            super.onGestureRecognized(posture, capturedGestureFrequencies);
      //         }
      //      });
      //
      //      final MidiHelper midiHelper = new MidiHelper();
      //
      //      MagneticSynthesizer magneticSynthesizer = new MagneticSynthesizer(midiHelper);
      //
      //      final InertialDataInterface imuDataFacade = new RichInertialDataFacade(new RichInertialDataFacade.ImuCallback() {
      //         @Override
      //         public void onUpdate(final long processedTimeInMilliSeconds) {
      //            gestureDetectorEngine.handleUpdate(processedTimeInMilliSeconds);
      //            magneticSynthesizer.handleUpdate(processedTimeInMilliSeconds);
      //         }
      //
      //         @Override
      //         public void onError() {
      //            gestureDetectorEngine.handleError();
      //            magneticSynthesizer.handleError();
      //         }
      //      });
      //
      //      // couple elements
      //      gestureDetectorEngine.setInertialDataInterface(imuDataFacade);
      //      magneticSynthesizer.setInertialDataInterface(imuDataFacade);
      //
      //      // start data collecting
      //      ((SerialDataInterface) imuDataFacade).start(portName, baudRate);
   }
}
