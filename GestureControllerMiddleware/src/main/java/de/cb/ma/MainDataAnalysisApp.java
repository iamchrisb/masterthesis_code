package de.cb.ma;

import de.cb.ma.apps.AccelerationStochasticApp;
import de.cb.ma.apps.DAWCommandInspectionApp;
import de.cb.ma.apps.DataAnalysisApp;
import de.cb.ma.apps.DataFrequencyMeasurementApp;
import de.cb.ma.apps.GestureDetectionApp;
import de.cb.ma.apps.GestureInspectionApp;
import de.cb.ma.apps.MagneticInstrumentApp;
import de.cb.ma.apps.PlayModeApp;
import de.cb.ma.apps.ProfileInspectionApp;
import de.cb.ma.apps.SensorDataVisualizationApp;
import de.cb.ma.apps.SensorFusionApp;
import de.cb.ma.core.database.InMemoryGestureMapping;
import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.dao.ProfileDao;
import de.cb.ma.core.database.dao.fileimpl.DAWCommandDaoFileImpl;
import de.cb.ma.core.database.dao.fileimpl.GestureModelDaoFileImpl;
import de.cb.ma.core.database.dao.fileimpl.ProfileDaoFileImpl;
import de.cb.ma.core.widgets.analysis.SerialDataMinerWidget;
import de.cb.ma.core.widgets.analysis.midi.MidiExplorerApp;
import de.cb.ma.util.Constants;
import de.cb.ma.util.MidiHelper;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import com.fasterxml.jackson.databind.ObjectMapper;

public class MainDataAnalysisApp extends Application {

   public static MidiHelper midiHelper = new MidiHelper();
   public static String APP_STATUS = "";
   public static String SUB_STATUS = "";

   private String MINER_STATUS = "-miner";

   private String VISUALIZATION_STATUS = "-visualization";
   private String MIDI_STATUS = "-midi";
   private String ANALYSIS_STATUS = "-analysis";
   private String ACCELERATION_STOCHASTIC_STATUS = "-acceleration";
   private String SENSOR_FUSION_STATUS = "-sensorfusion";
   private String DATA_FREQ_MEASUREMENT_STATUS = "-datameasurements";
   private String MAGNET_INSTRUMENT_STATUS = "-magneticinstrument";
   private String GESTURE_DETECTION_STATUS = "-gesturedetection";
   private String GESTURE_INSPECTION_STATUS = "-gestureinspection";
   private String PLAY_MODE_STATUS = "-playmode";
   private String DAW_MAPPING_STATUS = "-profile";
   private String DAW_COMMAND_STATUS = "-dawcommand";

   public static void main(String[] args) {
      if (args.length > 0) {
         // this must come first
         APP_STATUS = args[0];

         if (args.length > 1) {
            SUB_STATUS = args[1];
         }

         launch(args);
      }
      System.err.println("No status parameter provided");
   }

   @Override
   public void start(final Stage primaryStage) throws Exception {
      CURRENT_STAGE = primaryStage;

      MidiExplorerApp explorer = new MidiExplorerApp();
      DataAnalysisApp dataAnalysisApp = new DataAnalysisApp();

      SerialDataMinerWidget dataMinerApp = new SerialDataMinerWidget();

      ObjectMapper objectMapper = new ObjectMapper();

      DAWCommandDao dawCommandDao = new DAWCommandDaoFileImpl(Constants.DB_DAW_COMMANDS_PATH, objectMapper);
      ProfileDao profileDao = new ProfileDaoFileImpl(Constants.DB_PROFILES_PATH, objectMapper);
      GestureModelDao gestureModelDao = new GestureModelDaoFileImpl(Constants.DB_GESTURE_MODELS_PATH, objectMapper);

      InMemoryGestureMapping inMemoryGestureMapping = new InMemoryGestureMapping(gestureModelDao);

      final BorderPane borderPane = new BorderPane();
      final Scene scene = new Scene(borderPane, 1600, 900);
      primaryStage.setScene(scene);

      primaryStage.show();

      if (APP_STATUS.equals(MINER_STATUS)) {
         borderPane.setCenter(dataMinerApp);
      } else if (APP_STATUS.equals(MIDI_STATUS)) {
         borderPane.setCenter(explorer);
      } else if (APP_STATUS.equals(ANALYSIS_STATUS)) {
         borderPane.setCenter(dataAnalysisApp);
      } else if (APP_STATUS.equals(ACCELERATION_STOCHASTIC_STATUS)) {
         borderPane.setCenter(new AccelerationStochasticApp());
      } else if (APP_STATUS.equals(SENSOR_FUSION_STATUS)) {
         borderPane.setCenter(new SensorFusionApp());
      } else if (APP_STATUS.equals(DATA_FREQ_MEASUREMENT_STATUS)) {
         borderPane.setCenter(new DataFrequencyMeasurementApp());
      } else if (APP_STATUS.equals(MAGNET_INSTRUMENT_STATUS)) {
         MagneticInstrumentApp magneticInstrumentWidget = new MagneticInstrumentApp();
         borderPane.setCenter(magneticInstrumentWidget);
      } else if (APP_STATUS.equals(DAW_MAPPING_STATUS)) {
         final ProfileInspectionApp profileCreationWidget = new ProfileInspectionApp(midiHelper, profileDao, gestureModelDao, dawCommandDao);
         borderPane.setCenter(profileCreationWidget);
      } else if (APP_STATUS.equals(GESTURE_DETECTION_STATUS)) {
         final GestureDetectionApp gestureDetectionWidget = new GestureDetectionApp(inMemoryGestureMapping, gestureModelDao);
         borderPane.setCenter(gestureDetectionWidget);
      } else if (APP_STATUS.equals(GESTURE_INSPECTION_STATUS)) {
         borderPane.setCenter(new GestureInspectionApp(gestureModelDao, profileDao));
      } else if (APP_STATUS.equals(PLAY_MODE_STATUS)) {
         borderPane.setCenter(new PlayModeApp(inMemoryGestureMapping, midiHelper, profileDao, dawCommandDao, gestureModelDao));
      } else if (APP_STATUS.equals(DAW_COMMAND_STATUS)) {
         borderPane.setCenter(new DAWCommandInspectionApp(dawCommandDao, midiHelper));
      } else if (APP_STATUS.equals(VISUALIZATION_STATUS)) {
         borderPane.setCenter(new SensorDataVisualizationApp(SUB_STATUS));
      }
   }

   public static Stage CURRENT_STAGE = null;
}
