package de.cb.ma.core.imu;

import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.imu.motion.PostureMotion;
import de.cb.ma.util.Pair;

import java.util.List;

public interface GestureDetectorEngineCallback {

   void onPostureDetected(PostureMotion postureMotion);

   void onPostureDetected(HandSign.SIGN sign, final Integer[] binaryPattern);

   void onDetectionStart(final double varianceX, final double varianceY, final double varianceZ);

   void onDetectionEnd(final double varianceX, final double varianceY, final double varianceZ);

   void onGestureRecognized(HandSign.SIGN posture, GestureModel recognizedGestureModel, List<Pair<Double, List<Double>>> recentCapturedAmplitudeValues, double distance);

   void onAnyGestureCaptured(HandSign.SIGN posture, List<Pair<Double, List<Double>>> recentCapturedAmplitudeValues);

}
