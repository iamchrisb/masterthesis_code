package de.cb.ma.core.database.dao;

import de.cb.ma.core.database.model.DAWCommand;

import java.util.List;

public interface DAWCommandDao {

   List<DAWCommand> findAll();

   DAWCommand findById(String id);

   DAWCommand create(DAWCommand dawCommand);

   DAWCommand update(DAWCommand dawCommand);

   DAWCommand delete(DAWCommand dawCommand);
}
