package de.cb.ma.core.imu.serial.messages.application;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class DrumMessage extends SerialMessage {

   public enum DRUM_AXIS {
      NONE,
      DRUM_X,
      DRUM_Y,
      DRUM_Z,
   }

   private final Integer region;
   private final Integer intensity;

   public DrumMessage(Integer region, Integer intensity) {
      this.region = region;
      this.intensity = intensity;
   }

   public Integer getRegion() {
      return region;
   }

   public Integer getIntensity() {
      return intensity;
   }
}
