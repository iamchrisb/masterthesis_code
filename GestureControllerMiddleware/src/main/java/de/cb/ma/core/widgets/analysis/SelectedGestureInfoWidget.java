package de.cb.ma.core.widgets.analysis;

import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.imu.motion.HandSign;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.List;

public class SelectedGestureInfoWidget extends Pane {
   public SelectedGestureInfoWidget(String title) {
      final VBox gestureInfos = new VBox(20);

      final HBox identifierContainer = new HBox(20);
      identifierLabel = new Label();

      identifierContainer.getChildren()
            .addAll(new Label("identifier: "), identifierLabel);

      final HBox sampleContainer = new HBox(20);
      sampleLabel = new Label();
      sampleContainer.getChildren()
            .addAll(new Label("samples: "), sampleLabel);

      gestureInfos.getChildren()
            .addAll(new Label(title), identifierContainer, sampleContainer);

      this.getChildren()
            .addAll(gestureInfos);
   }

   private String getIdentifierAsString(final List<HandSign.SIGN> identifiers) {
      StringBuilder sb = new StringBuilder();
      for (HandSign.SIGN identifier : identifiers) {
         sb.append(identifier.name() + ",");
      }
      return sb.toString();
   }

   private final Label identifierLabel;
   private final Label sampleLabel;

   public void showInfo(final GestureModel selectedGesture) {
      final List<HandSign.SIGN> identifiers = selectedGesture.getIdentifiers();
      identifierLabel.setText(getIdentifierAsString(identifiers));
      sampleLabel.setText(new Integer(selectedGesture.getAccelerations()
            .getTimeKeys().length).toString());
   }
}
