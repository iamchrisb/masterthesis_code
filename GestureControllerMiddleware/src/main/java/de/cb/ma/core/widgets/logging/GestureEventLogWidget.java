package de.cb.ma.core.widgets.logging;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class GestureEventLogWidget extends Pane implements LiveLogWidget {

   public GestureEventLogWidget() {

      VBox container = new VBox();
      container.setSpacing(10);

      final Label titleLabel = new Label("Gesture event log");

      eventLog = new TextArea();

      final Button clearBtn = new Button("clear");
      clearBtn.setOnAction(event -> eventLog.clear());

      container.getChildren()
            .addAll(titleLabel, eventLog, clearBtn);

      this.getChildren()
            .add(container);
   }

   @Override
   public void addLog(final String logEntry) {
      Platform.runLater(() -> eventLog.setText(logEntry + "\n" + eventLog.getText()));
   }

   @Override
   public void addNewLine() {
      Platform.runLater(() -> {
         eventLog.setText("\n" + eventLog.getText());
      });
   }

   private final TextArea eventLog;
}
