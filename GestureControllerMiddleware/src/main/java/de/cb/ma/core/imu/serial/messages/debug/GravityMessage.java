package de.cb.ma.core.imu.serial.messages.debug;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class GravityMessage extends SerialMessage {

   public GravityMessage(final String xValue, final String yValue, final String zValue) {
      this.xValue = xValue;
      this.yValue = yValue;
      this.zValue = zValue;
   }

   private String xValue;
   private String yValue;
   private String zValue;

   public String getxValue() {
      return xValue;
   }

   public String getyValue() {
      return yValue;
   }

   public String getzValue() {
      return zValue;
   }

   @Override
   public String toString() {
      return "GravityMessage [" +
            "xValue='" + xValue +
            ", yValue='" + yValue +
            ", zValue='" + zValue +
            ']';
   }
}
