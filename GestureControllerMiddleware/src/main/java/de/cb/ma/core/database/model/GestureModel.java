package de.cb.ma.core.database.model;

import de.cb.ma.core.imu.motion.HandSign;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GestureModel {

   private String id;
   private String name;

   private AmplitudeModel accelerations;

   private List<HandSign.SIGN> identifiers = new ArrayList<>();

   public GestureModel() {
      // JSON
   }

   public GestureModel(String id, final String name, AmplitudeModel accelerations, List<HandSign.SIGN> identifiers) {
      Validate.notNull(id);
      Validate.notEmpty(id);
      Validate.notNull(name);
      Validate.notEmpty(name);
      Validate.notNull(identifiers);
      Validate.notEmpty(identifiers);

      this.id = id;
      this.name = name;
      this.accelerations = accelerations;
      this.identifiers = identifiers;
   }

   @Override
   public boolean equals(final Object o) {
      if (this == o) {
         return true;
      }
      if (!(o instanceof GestureModel)) {
         return false;
      }

      final GestureModel that = (GestureModel) o;

      return id != null ? id.equals(that.id) : that.id == null;
   }

   public String getId() {
      return id;
   }

   public void setId(final String id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   public AmplitudeModel getAccelerations() {
      return accelerations;
   }

   public void setAccelerations(final AmplitudeModel accelerations) {
      this.accelerations = accelerations;
   }

   public List<HandSign.SIGN> getIdentifiers() {
      return identifiers;
   }

   public void setIdentifiers(final List<HandSign.SIGN> identifiers) {
      this.identifiers = identifiers;
   }

   @Override
   public String toString() {
      return name;
   }
}
