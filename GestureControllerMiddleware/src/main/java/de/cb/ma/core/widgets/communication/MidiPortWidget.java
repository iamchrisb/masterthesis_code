package de.cb.ma.core.widgets.communication;

import de.cb.ma.util.MidiHelper;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javax.sound.midi.MidiDevice;

public class MidiPortWidget extends Pane {

   public interface Callback {
      void onMidiPortSelected(int index);
   }

   // inject midi helper
   public MidiPortWidget(final String title, MidiHelper midiHelper, Callback callback) {

      VBox container = new VBox();
      container.setSpacing(10);

      final Label titleLabel = new Label(title);
      container.getChildren()
            .addAll(titleLabel);

      container.setSpacing(10);

      ObservableList<String> midiDevicesOptions =
            FXCollections.observableList(midiHelper.getSystemMidiPortNames());

      ComboBox midiPortsComboBox = new ComboBox(midiDevicesOptions);

      container.getChildren()
            .add(midiPortsComboBox);

      Platform.runLater(() -> midiPortsComboBox.getSelectionModel()
            .selectLast());

      HBox midiDeviceDescriptionBox = new HBox(10);
      Label midiDeviceDescriptionLabel = new Label("Description: ");
      Label midiDeviceDescriptionValue = new Label();
      midiDeviceDescriptionBox.getChildren()
            .addAll(midiDeviceDescriptionLabel, midiDeviceDescriptionValue);

      HBox midiDeviceVersionBox = new HBox(10);
      Label midiDeviceVersionLabel = new Label("Version: ");
      Label midiDeviceVersionValue = new Label();
      midiDeviceVersionBox.getChildren()
            .addAll(midiDeviceVersionLabel, midiDeviceVersionValue);

      HBox midiDeviceVendorBox = new HBox(10);
      Label midiDeviceVendorLabel = new Label("Vendor: ");
      Label midiDeviceVendorValue = new Label();
      midiDeviceVendorBox.getChildren()
            .addAll(midiDeviceVendorLabel, midiDeviceVendorValue);

      container.getChildren()
            .addAll(midiDeviceDescriptionBox, midiDeviceVendorBox, midiDeviceVersionBox);

      midiPortsComboBox.getSelectionModel()
            .selectFirst();

      midiPortsComboBox.valueProperty()
            .addListener((ov, t, t1) -> {
               final int selectedIndex = midiPortsComboBox.getSelectionModel()
                     .getSelectedIndex();

               final MidiDevice midiDevice = midiHelper.getSystemMidiPort(selectedIndex);
               final MidiDevice.Info midiDeviceInfo = midiDevice.getDeviceInfo();
               midiDeviceDescriptionValue.setText(midiDeviceInfo.getDescription());
               midiDeviceVendorValue.setText(midiDeviceInfo.getVendor());
               midiDeviceVersionValue.setText(midiDeviceInfo.getVersion());

               callback.onMidiPortSelected(selectedIndex);
            });

      this.getChildren()
            .add(container);
   }
}
