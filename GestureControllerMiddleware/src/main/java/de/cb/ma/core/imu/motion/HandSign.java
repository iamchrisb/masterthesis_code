package de.cb.ma.core.imu.motion;

public abstract class HandSign extends Gesture {

   public enum SIGN {
      FIST,
      PALM,
      ONE_FINGER_POINT,
      TELEPHONE,
      PIECE,
      HEAVYMETAL,
      POINT,
      OKAY,
      PERFECT,
      UNDETECTED
   }

   /**
    * @param type
    *       one of the listed gesture-types, listed in the gesture class
    */
   public HandSign(final String type) {
      super(type);
   }

   public String getType() {
      return this.type;
   }

}
