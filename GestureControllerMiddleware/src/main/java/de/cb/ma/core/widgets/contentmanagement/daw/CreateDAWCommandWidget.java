package de.cb.ma.core.widgets.contentmanagement.daw;

import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.util.UUIDGenerator;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class CreateDAWCommandWidget extends Pane {

   public interface Callback {
      void onCreation(final DAWCommand dawCommand);
   }

   public CreateDAWCommandWidget(DAWCommandDao dawCommandDao, Callback callback) {
      dawInfoTitle = new Label("daw information");

      HBox nameContainer = new HBox(20);
      commandTF = new TextField();
      final Label commandLabel = new Label("command");

      nameContainer.getChildren()
            .addAll(commandLabel, commandTF);

      HBox descriptionContainer = new HBox(20);
      descriptionTF = new TextField();
      descriptionContainer.getChildren()
            .addAll(new Label("description"), descriptionTF);

      createBtn = new Button("create");

      createBtn.setOnAction(event -> {
         final MidiMessageParameter.MIDI_MESSAGE_TYPE selectedActionType = midiMessageCreateWidget.getSelectedMidiActionType();
         final Integer channel = midiMessageCreateWidget.getMidiChannel();

         final Integer dataByteOne = midiMessageCreateWidget.getDataByte1();
         final Integer dataByteTwo = midiMessageCreateWidget.getDataByte2();

         final MidiMessageParameter midiMessageParameter = new MidiMessageParameter(selectedActionType, channel, dataByteOne, dataByteTwo);
         final DAWCommand dawCommand = new DAWCommand(UUIDGenerator.uuid(), commandTF.getText(), descriptionTF.getText(), DAWCommand.MANUFACTURER.FL_STUDIO, midiMessageParameter);
         dawCommandDao.create(dawCommand);
         if (callback != null) {
            callback.onCreation(dawCommand);
         }
      });

      VBox container = new VBox(20);
      midiMessageCreateWidget = new CreateSingleMidiMessageWidget();
      container.getChildren()
            .addAll(dawInfoTitle, nameContainer, descriptionContainer, midiMessageCreateWidget, createBtn);

      this.getChildren()
            .addAll(container);
   }

   private final Label dawInfoTitle;
   private final TextField commandTF;
   private final TextField descriptionTF;
   private final Button createBtn;

   private CreateSingleMidiMessageWidget midiMessageCreateWidget;
}
