package de.cb.ma.core.widgets.contentmanagement.daw;

import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.MidiMessageParameter;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.util.LinkedList;
import java.util.List;

public class DAWCommandListWidget extends Pane {

   public interface ButtonCallback {
      void onSelected(int index, DAWCommandMappingModel dawCommand);
   }

   public interface PlayCallback extends ButtonCallback {
   }

   public interface DeleteCallback extends ButtonCallback {
   }

   public DAWCommandListWidget(final DAWCommandDao dawCommandDao, PlayCallback playCallback, DeleteCallback deleteCallback) {

      this.dawCommandDao = dawCommandDao;

      String title = "DAWCommand mapping";
      final Label label = new Label(title);

      final List<DAWCommandMappingModel> model = collectDAWMappingModels();

      tableView = new TableView();

      TableColumn commandColumn = new TableColumn("Command");
      commandColumn.setMinWidth(200);
      commandColumn.setCellValueFactory(new PropertyValueFactory<DAWCommandMappingModel, String>("dawCommandName"));

      TableColumn idColumn = new TableColumn("ID");
      idColumn.setCellValueFactory(new PropertyValueFactory<DAWCommandMappingModel, String>("dawCommandId"));
      idColumn.setMinWidth(200);

      TableColumn midiChannelCol = new TableColumn("MIDI channel");
      midiChannelCol.setCellValueFactory(new PropertyValueFactory<DAWCommandMappingModel, String>("displayedMidiChannel"));
      midiChannelCol.setMinWidth(100);

      TableColumn midiMessageTypeCol = new TableColumn("MIDI message type");
      midiMessageTypeCol.setCellValueFactory(new PropertyValueFactory<DAWCommandMappingModel, MidiMessageParameter.MIDI_MESSAGE_TYPE>("midiMessageType"));
      midiMessageTypeCol.setMinWidth(100);

      TableColumn midiDataByteOneCol = new TableColumn("MIDI databyte 1");
      midiDataByteOneCol.setCellValueFactory(new PropertyValueFactory<DAWCommandMappingModel, String>("displayDatabyte1Note"));
      midiDataByteOneCol.setMinWidth(100);

      TableColumn midiDatabyteTwoCol = new TableColumn("MIDI databyte 2");
      midiDatabyteTwoCol.setCellValueFactory(new PropertyValueFactory<DAWCommandMappingModel, String>("displayDatabyte2Note"));
      midiDatabyteTwoCol.setMinWidth(100);

      TableColumn sendCol = new TableColumn<>("Test MIDI");
      sendCol.setSortable(false);
      sendCol.setMinWidth(100);

      TableColumn deleteColumn = new TableColumn<>("Actions");
      deleteColumn.setSortable(false);
      deleteColumn.setMinWidth(100);

      sendCol.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<DAWCommandMappingModel, Boolean>,
                  ObservableValue<Boolean>>() {

               @Override
               public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<DAWCommandMappingModel, Boolean> p) {
                  return new SimpleBooleanProperty(p.getValue() != null);
               }
            });

      sendCol.setCellFactory(
            p -> new DAWCommandListWidget.ButtonCell("Play", playCallback));

      deleteColumn.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<DAWCommandMappingModel, Boolean>,
                  ObservableValue<Boolean>>() {

               @Override
               public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<DAWCommandMappingModel, Boolean> p) {
                  return new SimpleBooleanProperty(p.getValue() != null);
               }
            });

      deleteColumn.setCellFactory(
            p -> new DAWCommandListWidget.ButtonCell("Remove", deleteCallback));

      tableView.getColumns()
            .addAll(sendCol, commandColumn, midiMessageTypeCol, midiChannelCol, midiDataByteOneCol, midiDatabyteTwoCol, deleteColumn);

      this.model = FXCollections.observableArrayList(
            model
      );

      tableView.setItems(this.model);

      VBox container = new VBox();

      container.getChildren()
            .addAll(label, tableView);

      this.getChildren()
            .add(container);
   }

   private List<DAWCommandMappingModel> collectDAWMappingModels() {
      final List<DAWCommand> dawCommands = dawCommandDao.findAll();
      List<DAWCommandMappingModel> dawCommandMappingModels = new LinkedList<>();
      for (DAWCommand dawCommand : dawCommands) {
         dawCommandMappingModels.add(new DAWCommandMappingModel(dawCommand));
      }
      return dawCommandMappingModels;
   }

   public void refresh() {
      clearTableView();
      final List<DAWCommandMappingModel> mappingModels = collectDAWMappingModels();
      model.setAll(mappingModels);
      tableView.setItems(model);
   }

   private void clearTableView() {
      tableView.getItems()
            .clear();
   }

   private final TableView tableView;
   private final ObservableList<DAWCommandMappingModel> model;

   //Define the button cell
   private class ButtonCell extends TableCell<DAWCommandMappingModel, Boolean> {
      final Button selectButton = new Button();
      final Button saveButton = new Button();

      HBox container = new HBox(10);

      ButtonCell(String text, DAWCommandListWidget.ButtonCallback callback) {

         // add later
         saveButton.setText("Save (Draft)");

         selectButton.setText(text);
         selectButton.setOnAction(t -> {
            final DAWCommandMappingModel dawCommand = (DAWCommandMappingModel) tableView.getItems()
                  .get(getIndex());
            callback.onSelected(getIndex(), dawCommand);
         });

         container.getChildren()
               .addAll(selectButton);
      }

      //Display button if the row is not empty
      @Override
      protected void updateItem(Boolean t, boolean empty) {
         super.updateItem(t, empty);
         if (!empty) {
            setGraphic(container);
         }
      }
   }

   private final DAWCommandDao dawCommandDao;
}
