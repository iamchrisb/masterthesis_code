package de.cb.ma.core.imu.preprocessing.filter.sensorfusion;

public interface SensorFusionFilter {

   void update(double accelX, double accelY, double accelZ, double gyroX, double gyroY, double gyroZ);

   void update(double accelX, double accelY, double accelZ, double gyroX, double gyroY, double gyroZ, double magX, double magY, double magZ);

   /**
    * @return returns roll, pitch and yaw as a double array
    */
   public double[] getAngles();

   /**
    * @return returns the yaw value (current angle around z-axis)
    */
   public double getYaw();

   /**
    * @return returns the pitch value (current angle around y-axis)
    */
   public double getPitch();

   /**
    * @return returns the roll value (current angle around x-axis)
    */
   public double getRoll();
}
