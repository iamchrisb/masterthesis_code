package de.cb.ma.core.database.model;

import de.cb.ma.core.imu.motion.HandSign;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GestureMappingEntity {

   private Map<HandSign.SIGN, List<GestureModel>> mappings = new HashMap<>();

   public GestureMappingEntity() {
      // json
   }

   public GestureMappingEntity(final Map<HandSign.SIGN, List<GestureModel>> mappings) {
      this.mappings = mappings;
   }

   public Map<HandSign.SIGN, List<GestureModel>> getMappings() {
      return mappings;
   }

   public void setMappings(final Map<HandSign.SIGN, List<GestureModel>> mappings) {
      this.mappings = mappings;
   }

   public void addGestureModel(HandSign.SIGN posture, GestureModel gestureModel) {
      final boolean containsKey = mappings.containsKey(posture);
      if (containsKey) {
         mappings.get(posture)
               .add(gestureModel);
      } else {
         final ArrayList<GestureModel> gestureModels = new ArrayList<>();
         gestureModels.add(gestureModel);
         mappings.put(posture, gestureModels);
      }
   }

   public Set<HandSign.SIGN> giveKeys() {
      return mappings.keySet();
   }

   public List<GestureModel> getGestures(HandSign.SIGN key) {
      final List<GestureModel> gestureModels = mappings.get(key);
      if (gestureModels == null) {
         return new ArrayList<>();
      }
      return gestureModels;
   }

   public void updateGestures(HandSign.SIGN key, List<GestureModel> gestures) {
      mappings.get(key)
            .clear();
      mappings.get(key)
            .addAll(gestures);
   }
}
