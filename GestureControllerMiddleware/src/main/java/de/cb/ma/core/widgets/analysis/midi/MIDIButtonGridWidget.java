package de.cb.ma.core.widgets.analysis.midi;

import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.util.MidiHelper;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class MIDIButtonGridWidget extends Pane {

   Button[][] grid;

   private final ComboBox midiChannelSelection;
   private final ComboBox midiMessageParameterTypeSelect;

   public MIDIButtonGridWidget(int rows, int columns, MidiHelper midiHelper) {
      this.midiHelper = midiHelper;

      if (rows > 10) {
         throw new IllegalArgumentException("not more rows than 10 possible");
      }

      if (columns > 12) {
         throw new IllegalArgumentException("not more columns than 12 possible");
      }

      grid = new Button[rows][columns];

      VBox gridContainer = new VBox(20);

      HBox midiChannelContainer = new HBox(20);
      final Label midiChannelLbl = new Label("MIDI channel: ");
      midiChannelSelection = new ComboBox<>(FXCollections.observableArrayList(
            MidiHelper.MIDI_CHANNELS));
      midiChannelSelection.getSelectionModel()
            .selectFirst();

      midiChannelContainer.getChildren()
            .addAll(midiChannelLbl, midiChannelSelection);

      HBox midiTypeContainer = new HBox(20);
      final Label midiTypeLbl = new Label("MIDI type: ");
      midiMessageParameterTypeSelect = new ComboBox<>(FXCollections.observableArrayList(MidiMessageParameter.MIDI_MESSAGE_TYPE.values()));
      midiMessageParameterTypeSelect.getSelectionModel()
            .selectFirst();

      midiTypeContainer.getChildren()
            .addAll(midiTypeLbl, midiMessageParameterTypeSelect);

      for (int i = 0; i < grid.length; i++) {
         final HBox nextHbox = new HBox(20);
         gridContainer.getChildren()
               .addAll(nextHbox);

         final Button[] row = grid[i];
         for (int j = 0; j < row.length; j++) {
            final String text = MidiHelper.OCTAVE[j] + i;
            MIDIButton buttonIJ = new MIDIButton(text, i, j);
            row[j] = buttonIJ;
            nextHbox.getChildren()
                  .addAll(buttonIJ);

            buttonIJ.setOnAction(new EventHandler<ActionEvent>() {
               @Override
               public void handle(final ActionEvent event) {
                  final int index = buttonIJ.getRow() * columns + buttonIJ.getColumn();
                  midiHelper.playNote(0, index, 64);
               }
            });
         }
      }

      VBox container = new VBox(20);
      container.getChildren()
            .addAll(new Label("MIDI button grid for FL perfomance mode"), midiChannelContainer, midiTypeContainer, gridContainer);

      this.getChildren()
            .addAll(container);

   }

   private MidiHelper midiHelper;
}
