package de.cb.ma.core.database.model;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * more of a gestureMapping for gestures to MIDI signals
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Profile {

   private String id;
   private String name;

   /**
    * gesture to command mapping
    */
   private Map<String, String> gestureMapping = new HashMap<>();

   //   /**
   //    * command to gesture mapping
   //    */
   //   private Map<Long, Long> commandMapping = new HashMap<>();

   /**
    * @param id
    * @param name
    */
   public Profile(final String id, final String name) {
      this.id = id;
      this.name = name;
   }

   public Profile() {
      // JSON
   }

   public Map<String, String> getGestureMapping() {
      return gestureMapping;
   }

   public void setGestureMapping(final Map<String, String> gestureMapping) {
      this.gestureMapping = gestureMapping;
   }

   public String getId() {
      return id;
   }

   public void setId(final String id) {
      this.id = id;
   }

   public String getName() {
      return name;
   }

   public void setName(final String name) {
      this.name = name;
   }

   //   public Map<Long, Long> getCommandMapping() {
   //      return commandMapping;
   //   }
   //
   //   public void setCommandMapping(final Map<Long, Long> commandMapping) {
   //      this.commandMapping = commandMapping;
   //   }

   @Override
   public String toString() {
      return name;
   }
}

