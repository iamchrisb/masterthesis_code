package de.cb.ma.core.imu.preprocessing.filter.sensorfusion;

import org.apache.commons.math3.util.FastMath;

/**
 * This sensor fusion algorithm provides in general the roll and pitch value of an object using the accelerometer and gyroscope measurements.
 * Its implementation is based on the following papers about acceleration and gyroscope tilt compensation and complementary filters:
 * https://www.nxp.com/docs/en/application-note/AN3461.pdf
 * http://ieeexplore.ieee.org/document/4101411/
 */
public class ComplementaryFilter implements SensorFusionFilter {

   private double[] angles;

   private static final double LOWPASS_CONSTANT = 0.02;
   private static final double HIGHPASS_CONSTANT = 0.98;

   private double dt;

   private double integratedGyroX;
   private double integratedGyroY;

   public ComplementaryFilter(double sampleRate) {
      this.angles = new double[3];
      this.dt = 1 / sampleRate;
   }

   @Override
   public void update(double accelX, double accelY, double accelZ, double gyroX, double gyroY, double gyroZ) {
      double rollGyro = gyroX * dt;
      double pitchGyro = gyroY * dt;
      double yawGyro = gyroZ * dt;

      // save values for scientific purpose
      this.integratedGyroX = integratedGyroX + rollGyro;
      this.integratedGyroY = integratedGyroY + pitchGyro;
      this.integratedGyroZ = integratedGyroZ + yawGyro;

      double x = accelX;
      double y = accelY;
      double z = accelZ;

      double rollAccel = FastMath.atan2(y, z) * 180 / FastMath.PI; // gravity occurs on y axis, rotating around the x axis
      double pitchAccel = FastMath.atan2(-x, FastMath.sqrt(y * y + z * z)) * 180 / FastMath.PI; // gravity occurs on x axis, rotating around the y axis

      angles[0] = HIGHPASS_CONSTANT * (angles[0] + rollGyro) + (rollAccel * LOWPASS_CONSTANT);
      angles[1] = HIGHPASS_CONSTANT * (angles[1] + pitchGyro) + (pitchAccel * LOWPASS_CONSTANT);

      angles[2] = angles[2] + yawGyro;

      return;
   }

   @Override
   public void update(final double accelX, final double accelY, final double accelZ, final double gyroX, final double gyroY, final double gyroZ, final double magX, final double magY,
         final double magZ) {
      throw new UnsupportedOperationException("complementary filter do not use magnetic values to compute orientation");
   }

   @Override
   public double getRoll() {
      return angles[0];
   }

   @Override
   public double getPitch() {
      return angles[1];
   }

   @Override
   public double getYaw() {
      return angles[2];
   }

   @Override
   public double[] getAngles() {
      return angles;
   }

   public double getIntegratedGyroX() {
      return integratedGyroX;
   }

   public double getIntegratedGyroY() {
      return integratedGyroY;
   }

   public double getIntegratedGyroZ() {
      return integratedGyroZ;
   }

   private double integratedGyroZ;
}
