package de.cb.ma.core.widgets.analysis;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

public class OfflineDataExtractor {

   public interface Callback {
      void onLineExtracted(int passedTime, double[] values);

      void onError();
   }

   public OfflineDataExtractor(Callback callback) {
      this.callback = callback;
   }

   public void load(File file) {
      try {
         final List<String> lines = Files.readAllLines(file.toPath());
         for (String line : lines) {
            final String[] split = line.split(":");
            int passedTime = Integer.valueOf(split[0]);
            double[] values = extractValues(split[1]);
            this.callback.onLineExtracted(passedTime, values);
         }
      } catch (IOException e) {
         e.printStackTrace();
         this.callback.onError();
      }
   }

   private double[] extractValues(final String valueString) {
      final String[] stringedValues = valueString.split(",");

      double[] values = new double[14];

      for (int i = 0; i < stringedValues.length; i++) {
         String stringedValue = stringedValues[i];
         values[i] = Double.valueOf(stringedValue);
      }
      return values;
   }

   private Callback callback;
}
