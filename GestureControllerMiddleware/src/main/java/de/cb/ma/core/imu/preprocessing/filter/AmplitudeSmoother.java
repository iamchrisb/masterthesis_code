package de.cb.ma.core.imu.preprocessing.filter;

public interface AmplitudeSmoother {
   double smooth(double value);

   double[] smooth(double... values);
}
