package de.cb.ma.core.database.model;

import de.cb.ma.util.Pair;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.Validate;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AmplitudeModel {

   private int dimension;
   private List<Pair<Double, List<Double>>> amplitudes;

   private double[] timeKeys;
   private double[][] amplitudeValues;

   private List<String> axesNames = new ArrayList<>();

   public AmplitudeModel() {
      // JSON
   }

   public AmplitudeModel(final int dimension, final List<Pair<Double, List<Double>>> amplitudes, final List<String> axesNames) {
      Validate.notEmpty(amplitudes);
      Validate.notEmpty(axesNames);

      this.dimension = dimension;
      this.amplitudes = amplitudes;
      this.axesNames = axesNames;

      fillValues(amplitudes);
   }

   private void fillValues(final List<Pair<Double, List<Double>>> frequencies) {
      timeKeys = new double[frequencies.size()];
      amplitudeValues = new double[frequencies.size()][frequencies.get(0)
            .getValue()
            .size()];

      for (int i = 0; i < timeKeys.length; i++) {
         timeKeys[i] = frequencies.get(i)
               .getKey();
      }

      for (int i = 0; i < frequencies.size(); i++) {
         final Pair<Double, List<Double>> frequency = frequencies.get(i);
         final List<Double> value = frequency.getValue();
         amplitudeValues[i] = new double[value.size()];
         for (int j = 0; j < value.size(); j++) {
            amplitudeValues[i][j] = value.get(j);
         }
      }

   }

   public void addAxesNames(String... axesNames) {
      this.axesNames.addAll(Arrays.asList(axesNames));
   }

   public void setAxesNames(String... axesNames) {
      this.axesNames.clear();
      this.axesNames.addAll(Arrays.asList(axesNames));
   }

   public List<String> getAxesNames() {
      return axesNames;
   }

   public void removeAxesNames() {
      this.axesNames.clear();
   }

   public List<Pair<Double, List<Double>>> getAmplitudes() {
      return amplitudes;
   }

   public void setAmplitudes(List<Pair<Double, List<Double>>> amplitudes) {
      this.amplitudes = amplitudes;
      fillValues(amplitudes);
   }

   public int getDimension() {
      return dimension;
   }

   public void setDimension(final int dimension) {
      this.dimension = dimension;
   }

   public double[] getTimeKeys() {
      return timeKeys;
   }

   public double[][] getAmplitudeValues() {
      return amplitudeValues;
   }
}
