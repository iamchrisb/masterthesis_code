package de.cb.ma.core.database.dao;

import de.cb.ma.core.database.model.GestureMappingEntity;

public interface GestureMappingDao {

   GestureMappingEntity findAll();

   GestureMappingEntity findById(long id);

   boolean update(GestureMappingEntity gestureMapping);

   boolean delete(GestureMappingEntity gestureMapping);

}
