package de.cb.ma.core.imu;

import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.util.Pair;

import java.util.List;

public interface GestureComparatorAlgo {

   /**
    * compares to gesturural datasets based on a 3 dimensional acceleration time series dataset
    *
    * @param referenceGesture
    * @param observedGesture
    * @return returns the distance between the two timeseries datasets
    */
   double compare(GestureModel referenceGesture, GestureModel observedGesture);

   /**
    * compares to gesturural datasets based on a 3 dimensional acceleration time series dataset
    *
    * @param referenceGesture
    * @param observedGesture
    * @return returns the distance between the two timeseries datasets
    */
   double compare(List<Pair<Double, List<Double>>> referenceGesture, List<Pair<Double, List<Double>>> observedGesture);
}
