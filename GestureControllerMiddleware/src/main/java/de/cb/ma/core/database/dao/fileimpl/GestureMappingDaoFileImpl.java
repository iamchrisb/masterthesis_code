package de.cb.ma.core.database.dao.fileimpl;

import de.cb.ma.core.database.dao.GestureMappingDao;
import de.cb.ma.core.database.model.GestureMappingEntity;
import de.cb.ma.util.Constants;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class GestureMappingDaoFileImpl implements GestureMappingDao {

   private String filePath;
   private ObjectMapper objectMapper;

   public GestureMappingDaoFileImpl(String filePath, ObjectMapper objectMapper) {
      this.filePath = filePath;
      this.objectMapper = objectMapper;
   }

   @Override
   public GestureMappingEntity findAll() {
      try {
         return objectMapper.readValue(new File(Constants.GESTURE_MAPPING_PATH), GestureMappingEntity.class);
      } catch (IOException e) {
         e.printStackTrace();
      }
      return null;
   }

   @Override
   public GestureMappingEntity findById(final long id) {
      throw new UnsupportedOperationException("not implemented yet");
   }

   public boolean update(GestureMappingEntity mapping) {
      try {
         objectMapper.writeValue(new File(Constants.GESTURE_MAPPING_PATH), mapping);
      } catch (IOException e) {
         e.printStackTrace();
         return false;
      }
      return true;
   }

   @Override
   public boolean delete(final GestureMappingEntity gestureMapping) {
      throw new UnsupportedOperationException("not implemented yet");
   }
}
