package de.cb.ma.core.imu.serial;

import de.cb.ma.core.imu.serial.messages.application.GyroPeakMessage;
import de.cb.ma.core.imu.serial.messages.debug.FrequencyMessage;
import de.cb.ma.core.imu.serial.messages.debug.GravityMessage;
import de.cb.ma.core.imu.serial.messages.debug.LinearAccelerationMessage;
import de.cb.ma.core.imu.serial.messages.debug.MagnetometerMessage;
import de.cb.ma.core.imu.serial.messages.SerialMessage;
import de.cb.ma.core.imu.serial.messages.application.BendMessage;
import de.cb.ma.core.imu.serial.messages.application.DirectionMessage;
import de.cb.ma.core.imu.serial.messages.application.OrientationMessage;
import de.cb.ma.core.imu.serial.messages.application.SuperMessage;
import de.cb.ma.core.imu.serial.messages.debug.AccelerationMessage;
import de.cb.ma.core.imu.serial.messages.debug.GyroPeakValueMessage;
import de.cb.ma.core.imu.serial.messages.debug.TemperatureMessage;
import jssc.SerialPort;
import jssc.SerialPortException;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class SerialMessageHandler {

   public static final int LAST_TEN_MSGS = 10;
   private static int MSG_TYPE_INDEX = 1;
   private static int MSG_SUBTYPE_INDEX = 2;
   private static int MSG_VALUE_INDEX = 3;

   public static int MSG_NO_SUBTYPE_INDEX = 2;

   private static String MSG_TYPE_SEPERATOR = ":";
   private static String MSG_VALUE_SEPERATOR = ";";

   private SerialPort serialPort;
   private int baudRate;
   private Callback callback;

   private List<String> latestMessages;
   private Path messageFile;

   public interface Callback {
      void onMessageReceived(SerialMessage serialMessage);
   }

   /**
    * Save all incoming gesture messages to a file
    *
    * @param filePath
    *       file path where all incoming data should be saved to
    * @param portName
    *       the serial port name e.g. "COM4" on a windows machine
    * @param baudRate
    *       the set baudrate for the com port for example 384000
    * @param callback
    *       the callback object that receives every update
    */
   public SerialMessageHandler(String filePath, String portName, int baudRate, Callback callback) {
      this.baudRate = baudRate;
      this.callback = callback;
      serialPort = new SerialPort(portName);
      messageFile = Paths.get(filePath);
      latestMessages = new ArrayList<>();
   }

   /**
    * read messages from file instead of a serial connection, so this is not really a serial handler
    *
    * @param filePath
    *       path to the file with saved messages
    * @param callback
    */
   public SerialMessageHandler(String filePath, Callback callback) {
      this.callback = callback;

      try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
         stream.forEach(this::processSerialMessageInternal);
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   /**
    * the most used constructor
    *
    * @param portName
    *       the serial port name e.g. "COM4" on a windows machine
    * @param baudRate
    *       the set baudrate for the com port for example 384000
    * @param callback
    *       the callback object that receives every update
    */
   public SerialMessageHandler(String portName, int baudRate, Callback callback) {
      serialPort = new SerialPort(portName);
      this.baudRate = baudRate;
      this.callback = callback;
   }

   public void startSerial() {
      if (isSerialPortOpened()) {
         closeSerialPort();
      }

      startSerial(baudRate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
   }

   public void startSerial(final int baudRate, final int dataBits, final int stopBit, final int parityBit) {
      if (isSerialPortOpened()) {
         closeSerialPort();
      }

      try {
         serialPort.openPort();
         serialPort.setParams(baudRate,
               dataBits,
               stopBit,
               parityBit);

         serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_RTSCTS_IN |
               SerialPort.FLOWCONTROL_RTSCTS_OUT);

         serialPort.addEventListener(new PortReader(serialPort, message -> {
            try {
               processSerialMessage(message);
            } catch (Exception e) {
               // simply ignore this errorF
               e.printStackTrace();
            }
         }), SerialPort.MASK_RXCHAR);
      } catch (SerialPortException e) {
         e.printStackTrace();
      }
   }

   public void closeSerialPort() {
      try {
         serialPort.closePort();
      } catch (SerialPortException e) {
         e.printStackTrace();
      }
   }

   public boolean isSerialPortOpened() {
      return serialPort.isOpened();
   }

   private void processSerialMessageInternal(final String serialMessage) {
      try {
         processSerialMessage(serialMessage);
      } catch (Exception e) {
         // ignore error
         e.printStackTrace();
      }
   }

   private void processSerialMessage(final String serialMessage) throws Exception {

      if (messageFile != null) {
         saveMessage(serialMessage);
      }

      if (serialMessage == null || serialMessage.isEmpty()) {
         return;
      }

      final String[] splittedCommandMessage = serialMessage.split(MSG_TYPE_SEPERATOR);

      String type = splittedCommandMessage[MSG_TYPE_INDEX];
      String subType = splittedCommandMessage[MSG_SUBTYPE_INDEX];

      if (type == null || type.isEmpty()) {
         return;
      }

      if (type.equals(SerialMessage.SUPER_TYPE)) {
         final String[] values = splittedCommandMessage[MSG_NO_SUBTYPE_INDEX].split(MSG_VALUE_SEPERATOR);

         if (values.length != 14) {
            return;
         }

         SuperMessage superMessage
               = new SuperMessage(values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7], values[8], values[9], values[10], values[11], values[12], values[13]);
         callback.onMessageReceived(superMessage);
         return;
      }

      if (type.equals(SerialMessage.APP_TYPE)) {
         if (subType.equals(SerialMessage.APP_SUBTYPE_BEND)) {
            final String commandValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = commandValues.split(MSG_VALUE_SEPERATOR);

            String thumbValue = values[0];
            String indexValue = values[1];
            String middleValue = values[2];
            String ringValue = values[3];
            String pinkyValue = values[4];
            //            String averageValue = values[5];

            SerialMessage message = new BendMessage(thumbValue, indexValue, middleValue, ringValue, pinkyValue, "0");
            callback.onMessageReceived(message);
            return;
         }

         if (subType.equals(SerialMessage.APP_SUBTYPE_ORIENTATION)) {
            final String commandValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = commandValues.split(MSG_VALUE_SEPERATOR);

            String yawValue = values[0];
            String pitchValue = values[1];
            String rollValue = values[2];

            SerialMessage command = new OrientationMessage(yawValue, pitchValue, rollValue);
            callback.onMessageReceived(command);
            return;
         }

         if (subType.equals(SerialMessage.APP_SUBTYPE_GYRO_PEAK_HAPPENED)) {
            final String commandValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = commandValues.split(MSG_VALUE_SEPERATOR);

            Boolean isGyroPeakOnX = new Boolean(values[0]);
            Boolean isGyroPeakOnY = new Boolean(values[1]);
            Boolean isGyroPeakOnZ = new Boolean(values[2]);

            SerialMessage command = new GyroPeakMessage(isGyroPeakOnX, isGyroPeakOnY, isGyroPeakOnZ);
            callback.onMessageReceived(command);
            return;
         }

         if (subType.equals(SerialMessage.APP_SUBTYPE_DIRECTION)) {
            final String commandValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];

            String directionValue = commandValues;

            callback.onMessageReceived(new DirectionMessage(new Integer(directionValue)));
            return;
         }
         return;
      }

      if (type.equals(SerialMessage.DEBUG_TYPE)) {
         if (subType.equals(SerialMessage.DEBUG_SUBTYPE_ACC)) {

            final String commandValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = commandValues.split(MSG_VALUE_SEPERATOR);

            String xAccelValue = values[0];
            String yAccelValue = values[1];
            String zAccelValue = values[2];
            String filteredX = values[3];
            String filteredY = values[4];
            String filteredZ = values[5];

            callback.onMessageReceived(new AccelerationMessage(xAccelValue, yAccelValue, zAccelValue, filteredX, filteredY, filteredZ));
            return;
         }
         if (subType.equals(SerialMessage.DEBUG_SUBTYPE_GYRO_PEAK_VALUE)) {

            final String messageValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = messageValues.split(MSG_VALUE_SEPERATOR);

            String xGyroPeakValue = values[0];
            String yGyroPeakValue = values[1];
            String zGyroPeakValue = values[2];

            callback.onMessageReceived(new GyroPeakValueMessage(xGyroPeakValue, yGyroPeakValue, zGyroPeakValue));
            return;
         }
         if (subType.equals(SerialMessage.DEBUG_SUBTYPE_MAG)) {

            final String messageValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = messageValues.split(MSG_VALUE_SEPERATOR);

            String xMagValue = values[0];
            String yMagValue = values[1];
            String zMagValue = values[2];

            callback.onMessageReceived(new MagnetometerMessage(xMagValue, yMagValue, zMagValue));
            return;
         }
         if (subType.equals(SerialMessage.DEBUG_SUBTYPE_TEMP)) {

            final String messageValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = messageValues.split(MSG_VALUE_SEPERATOR);

            String temp = values[0];

            callback.onMessageReceived(new TemperatureMessage(temp));
            return;
         }
         if (subType.equals(SerialMessage.DEBUG_SUBTYPE_FREQ)) {

            final String messageValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = messageValues.split(MSG_VALUE_SEPERATOR);

            String freq = values[0];

            callback.onMessageReceived(new FrequencyMessage(freq));
            return;
         }
         if (subType.equals(SerialMessage.DEBUG_SUBTYPE_GRAV)) {

            final String messageValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = messageValues.split(MSG_VALUE_SEPERATOR);

            callback.onMessageReceived(new GravityMessage(values[0], values[1], values[2]));
            return;
         }
         if (subType.equals(SerialMessage.DEBUG_SUBTYPE_LINACC)) {

            final String messageValues = serialMessage.split(MSG_TYPE_SEPERATOR)[MSG_VALUE_INDEX];
            final String[] values = messageValues.split(MSG_VALUE_SEPERATOR);

            callback.onMessageReceived(new LinearAccelerationMessage(values[0], values[1], values[2]));
            return;
         }
         return;
      }
   }

   private void saveMessage(final String serialMessage) {
      if (latestMessages.size() == LAST_TEN_MSGS) {
         try {
            Files.write(messageFile, latestMessages, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
            latestMessages.clear();
         } catch (IOException e) {
            e.printStackTrace();
         }
      } else {
         latestMessages.add(serialMessage);
      }
   }
}
