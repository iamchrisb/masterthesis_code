package de.cb.ma.core.widgets.gesture;

import de.cb.ma.core.imu.motion.HandSign;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class MatchedGestureInfoWidget extends Pane {

   public MatchedGestureInfoWidget(String title) {

      final Label titleLbl = new Label(title);

      HBox sampleContainer = new HBox(20);
      sampleLabel = new Label();
      sampleContainer.getChildren()
            .addAll(new Label("samples: "), sampleLabel);

      HBox identifierContainer = new HBox(20);
      identifierLabel = new Label();
      identifierContainer.getChildren()
            .addAll(new Label("identifier: "), identifierLabel);

      final VBox body = new VBox(20);

      body.getChildren()
            .addAll(titleLbl, sampleContainer, identifierContainer);

      this.getChildren()
            .addAll(body);
   }

   public void showInfo(int count, final HandSign.SIGN sign) {
      Platform.runLater(() -> {
         sampleLabel.setText(new Integer(count).toString());
         identifierLabel.setText(sign.name());
      });
   }

   private final Label sampleLabel;
   private final Label identifierLabel;
}
