package de.cb.ma.core.widgets.analysis.midi;

import de.cb.ma.core.widgets.contentmanagement.daw.CreateSingleMidiMessageWidget;
import de.cb.ma.core.widgets.communication.MidiPortWidget;
import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.util.MidiHelper;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javax.sound.midi.MidiUnavailableException;

public class MidiExplorerApp extends Pane {

   final CreateSingleMidiMessageWidget midiMessageCreateWidget;

   public MidiExplorerApp() {
      final MidiHelper midiHelper = new MidiHelper();

      MidiPortWidget midiPortWidget = new MidiPortWidget("choose midi port", midiHelper, new MidiPortWidget.Callback() {
         @Override
         public void onMidiPortSelected(final int index) {
            try {
               midiHelper.useMidiDevice(index);
            } catch (MidiUnavailableException e) {
               e.printStackTrace();
            }
         }
      });

      midiMessageCreateWidget = new CreateSingleMidiMessageWidget();

      HBox midiMessageCont = new HBox(20);
      Button sendBtn = new Button("Send message");
      midiMessageCont.getChildren()
            .addAll(midiMessageCreateWidget);

      final MIDIButtonGridWidget midiButtonGridWidget = new MIDIButtonGridWidget(10, 12, midiHelper);

      MIDIKnobWidget midiKnobWidget = new MIDIKnobWidget(midiHelper);

      sendBtn.setOnAction(event -> {
         final MidiMessageParameter.MIDI_MESSAGE_TYPE selectedMidiActionType = midiMessageCreateWidget.getSelectedMidiActionType();
         final Integer midiChannel = midiMessageCreateWidget.getMidiChannel();
         final Integer dataByte1 = midiMessageCreateWidget.getDataByte1();
         final Integer dataByte2 = midiMessageCreateWidget.getDataByte2();
         midiHelper.sendChannelMessage(midiHelper.getIntForMessageActionType(selectedMidiActionType), midiChannel, dataByte1, dataByte2);
      });

      VBox container = new VBox(20);

      HBox header = new HBox(20);
      header.getChildren()
            .addAll(midiPortWidget);

      HBox body = new HBox(20);

      final VBox rightCont = new VBox(20);
      rightCont.getChildren()
            .addAll(midiKnobWidget, midiMessageCreateWidget, sendBtn);

      body.getChildren()
            .addAll(rightCont, midiButtonGridWidget);

      container.getChildren()
            .addAll(header, body);

      this.getChildren()
            .addAll(container);
   }

}
