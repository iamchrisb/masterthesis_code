package de.cb.ma.core.database;

import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.imu.motion.HandSign;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class InMemoryGestureMapping {

   /**
    * ids of the ..
    */
   private Map<HandSign.SIGN, List<GestureModel>> signaturesToGestureMapping = new HashMap<>();

   public InMemoryGestureMapping(GestureModelDao dao) {
      this.signaturesToGestureMapping = new HashMap<>();
      gestureModelDao = dao;

      createMapping();
   }

   private void createMapping() {
      final List<GestureModel> allGestureModels = gestureModelDao.findAll();

      for (GestureModel currentGestureModel : allGestureModels) {
         final List<HandSign.SIGN> identifiers = currentGestureModel.getIdentifiers();
         for (HandSign.SIGN identifier : identifiers) {
            List<GestureModel> gestureModels = signaturesToGestureMapping.get(identifier);
            if (gestureModels == null) {
               gestureModels = new LinkedList<>();
            }

            gestureModels.add(currentGestureModel);
            signaturesToGestureMapping.put(identifier, gestureModels);
         }
      }
   }

   public Map<HandSign.SIGN, List<GestureModel>> getSignaturesToGestureMapping() {
      return signaturesToGestureMapping;
   }

   public List<GestureModel> getGesturesByHandSign(HandSign.SIGN sign) {
      return signaturesToGestureMapping.get(sign);
   }

   private GestureModelDao gestureModelDao;
}
