package de.cb.ma.core.widgets.analysis.charts;

import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;

public interface PlotWidget {
   void updateRangeSlider(double from, double to);

   void clearData();

   ObservableList<XYChart.Series<Number, Number>> getData();

   void shrinkData(Number from, Number to);

   void setRangeSaveVisibility(boolean value);

   void setWidth(double width);

   void setHeight(double height);
}
