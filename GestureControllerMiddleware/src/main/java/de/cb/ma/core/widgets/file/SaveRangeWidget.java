package de.cb.ma.core.widgets.file;

import de.cb.ma.depricated.GestureApp;
import de.cb.ma.util.Constants;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;

import java.io.File;

import org.controlsfx.control.RangeSlider;

public class SaveRangeWidget extends Pane {

   public interface Callback {
      public void onShrinkSelected(double lowValue, double highValue);

      public void onSaveSelected(double lowValue, double highValue, final String absolutePath);

      public void onResetSelected();
   }

   public static final String CURRENT_FILE = "Current file: ";

   private final Button saveBtn;

   public SaveRangeWidget(Callback callback) {
      this.callback = callback;
      HBox container = new HBox(10);
      container.setPadding(new Insets(15, 10, 15, 80));

      saveBtn = new Button("save sequence");
      shrinkBtn = new Button("shrink");
      resetBtn = new Button("reset");

      FileChooser chooser = new FileChooser();
      chooser.setInitialDirectory(new File(Constants.WORKING_DIR));
      chooser.setInitialFileName("selected-range.txt");

      rangeSlider = new RangeSlider(0, 100, 0, 100);
      rangeSlider.setShowTickMarks(true);
      rangeSlider.setShowTickLabels(true);
      rangeSlider.setBlockIncrement(10);
      rangeSlider.setMajorTickUnit(10);
      rangeSlider.setSnapToTicks(true);

      saveBtn.setOnAction(event -> {
         final File file = chooser.showSaveDialog(GestureApp.getStage());
         if (file != null) {
            callback.onSaveSelected(0, 0, file.getAbsolutePath());
         }
      });

      //      saveBtn.setOnAction(new EventHandler<ActionEvent>() {
      //         @Override
      //         public void handle(final ActionEvent event) {
      //            final File file = chooser.showOpenDialog(stage);
      //            if (file != null) {
      //               callback.onFileSelected(file);
      //               currentFileLabel.setText(CURRENT_FILE + file.getName());
      //            }
      //         }
      //      });

      shrinkBtn.setOnAction(event -> callback.onShrinkSelected(rangeSlider.getLowValue(), rangeSlider.getHighValue()));
      resetBtn.setOnAction(event -> callback.onResetSelected());

      container.getChildren()
            .addAll(saveBtn, rangeSlider, shrinkBtn);

      //      container.getChildren().add(resetBtn);

      this.getChildren()
            .addAll(container);
   }

   private final Button shrinkBtn;

   public void setRange(double min, double max, double lowValue, double highValue) {
      rangeSlider.setMin(min);
      rangeSlider.setMax(max);
      rangeSlider.setLowValue(lowValue);
      rangeSlider.setHighValue(highValue);
   }

   private final RangeSlider rangeSlider;
   private final Button resetBtn;
   private Callback callback;
}
