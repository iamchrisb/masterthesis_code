package de.cb.ma.core.imu.serial;

import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;

public class PortReader implements SerialPortEventListener {

   private final String MSG_DETECTION_STRING = "MSG:";
   private final char MSG_END_RETURN = '\r';
   private final char MSG_END_LINE_FEED = '\n';

   public interface Callback {
      void onSerialEvent(String commandMessage);
   }

   public PortReader(SerialPort serialPort, Callback callback) {
      this.serialPort = serialPort;
      this.callback = callback;
   }

   StringBuilder message = new StringBuilder();

   @Override
   public void serialEvent(SerialPortEvent event) {
      if (event.isRXCHAR() && event.getEventValue() > 0) {
         byte buffer[] = new byte[0];
         try {
            buffer = serialPort.readBytes();
            for (byte b : buffer) {
               if ((b == MSG_END_RETURN || b == MSG_END_LINE_FEED) && message.length() > 0) {
                  String serialMessage = message.toString();
                  if (serialMessage.contains(MSG_DETECTION_STRING)) {
                     callback.onSerialEvent(serialMessage);
                  }
                  message.setLength(0);
               } else {
                  message.append((char) b);
               }
            }
         } catch (SerialPortException e) {
            e.printStackTrace();
         }
      }
   }

   private SerialPort serialPort;
   private Callback callback;
}