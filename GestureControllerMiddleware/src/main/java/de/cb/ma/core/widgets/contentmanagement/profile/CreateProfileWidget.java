package de.cb.ma.core.widgets.contentmanagement.profile;

import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.GestureModel;
import javafx.collections.FXCollections;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.List;

public class CreateProfileWidget extends Pane {

   public interface Callback {
      void onCreation(String gestureModelId, String dawCommandId);
   }

   public CreateProfileWidget(GestureModelDao gestureModelDao, DAWCommandDao dawCommandDao, Callback callback) {
      this.callback = callback;
      final VBox container = new VBox(20);

      final Label title = new Label("Add profile entry");

      final List<GestureModel> gestureModels = gestureModelDao.findAll();
      selectGesture = new ComboBox<>(FXCollections.observableArrayList(gestureModels));

      final List<DAWCommand> dawCommands = dawCommandDao.findAll();
      selectDAWCommand = new ComboBox<>(FXCollections.observableArrayList(dawCommands));

      if (dawCommands != null && !dawCommands.isEmpty()) {
         selectDAWCommand.getSelectionModel()
               .selectFirst();
      }

      if (gestureModels != null && !gestureModels.isEmpty()) {
         selectGesture.getSelectionModel()
               .selectFirst();
      }

      createMappingBtn = new Button("ceate mapping");

      final HBox hBox = new HBox(20);

      hBox.getChildren()
            .addAll(selectGesture, selectDAWCommand);

      container.getChildren()
            .addAll(title, hBox, createMappingBtn);

      this.getChildren()
            .addAll(container);

      createMappingBtn.setOnAction(event -> {
         final String dawId = selectDAWCommand.getSelectionModel()
               .getSelectedItem()
               .getId();
         final String gestureModelId = selectGesture.getSelectionModel()
               .getSelectedItem()
               .getId();
         callback.onCreation(gestureModelId, dawId);
      });
   }

   private final ComboBox<GestureModel> selectGesture;
   private final ComboBox<DAWCommand> selectDAWCommand;
   private final Button createMappingBtn;
   private Callback callback;
}
