package de.cb.ma.core.database.dao;

import de.cb.ma.core.database.model.GestureModel;

import java.util.List;

public interface GestureModelDao {

   List<GestureModel> findAll();

   GestureModel findById(String id);

   GestureModel create(GestureModel gestureModel);

   GestureModel update(GestureModel gestureModel);

   GestureModel delete(GestureModel gestureModel);
}
