package de.cb.ma.core.imu;

import de.cb.ma.core.imu.serial.messages.SerialMessage;
import de.cb.ma.core.imu.serial.messages.application.SuperMessage;
import de.cb.ma.core.imu.serial.SerialDataInterface;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.core.imu.preprocessing.AmplitudeStochastic;
import de.cb.ma.core.imu.preprocessing.ApacheMathAmplitudeStochastic;
import de.cb.ma.core.imu.preprocessing.filter.ApacheAmplitudeSmoother;
import de.cb.ma.core.imu.preprocessing.filter.sensorfusion.ComplementaryFilter;
import de.cb.ma.core.imu.preprocessing.filter.sensorfusion.SensorFusionFilter;

import java.util.LinkedList;
import java.util.List;

/**
 * In contrast to a slim data facade, the smoothing and sensorfusion algorithms are implemented in the middleware.
 * A slim facade would not provide any own algorithmic implementations due to the mcu limit
 * This facade is designed for slower or weaker mcus like the arduino micro and gives access to all necessary imu data and even more.
 * Algorithmic implementations could be easily changed.
 */
public class RichSensorDataFacade implements InertialDataInterface, SerialDataInterface {
   private static final int ACCELERATION_SMOOTH_WINDOW_SIZE = 16;
   private static final int ACCELERATION_STOCHASTIC_WINDOW_SIZE = 16;
   private static final int FLEX_ANGLE_SMOOTH_WINDOW_SIZE = 32;

   private static final int SENSOR_FUSION_SAMPLE_RATE = 200;

   private ImuCallback callback;

   private long startedTime;

   private double[] currentSmoothedAccelerationValues;
   private double[] currentRealAccelerationValues;
   private double[] currentGyroValues;
   private double[] currentMagneticValues;
   private double[] currentOrientationValues;
   private double[] currentFlexAngleValues;

   private SerialMessageHandler serialMessageHandler;

   private ApacheAmplitudeSmoother accelerationSmoother;
   private ApacheAmplitudeSmoother flexAngleSmoother;
   private AmplitudeStochastic accelerationStochastic;

   List<InertialDataReceiver> receivers = new LinkedList<>();

   private SensorFusionFilter sensorFusionFilter;

   public interface ImuCallback {
      void onUpdate(long processedTimeInMs);

      void onError();
   }

   public RichSensorDataFacade(ImuCallback callback) {
      this.callback = callback;

      currentSmoothedAccelerationValues = new double[3];
      currentRealAccelerationValues = new double[3];
      currentGyroValues = new double[3];
      currentMagneticValues = new double[3];
      currentOrientationValues = new double[3];
      currentFlexAngleValues = new double[5];

      accelerationSmoother = new ApacheAmplitudeSmoother(ACCELERATION_SMOOTH_WINDOW_SIZE, 3);
      flexAngleSmoother = new ApacheAmplitudeSmoother(FLEX_ANGLE_SMOOTH_WINDOW_SIZE, 5);
      accelerationStochastic = new ApacheMathAmplitudeStochastic(ACCELERATION_STOCHASTIC_WINDOW_SIZE, 3);

      sensorFusionFilter = new ComplementaryFilter(SENSOR_FUSION_SAMPLE_RATE);
   }

   @Override
   public void start(final String serialPortName, final int baudRate) {
      if (serialMessageHandler == null) {
         serialMessageHandler = new SerialMessageHandler(serialPortName, baudRate, serialMessage -> {
            handleSerialMessage(serialMessage);
         });
         serialMessageHandler.startSerial();
         startedTime = System.currentTimeMillis();
         return;
      }

      if (serialMessageHandler.isSerialPortOpened()) {
         serialMessageHandler.closeSerialPort();

         serialMessageHandler = new SerialMessageHandler(serialPortName, baudRate, serialMessage -> {
            handleSerialMessage(serialMessage);
         });

         serialMessageHandler.startSerial();
         startedTime = System.currentTimeMillis();
      }
   }

   private void handleSerialMessage(final SerialMessage serialMessage) {
      if (serialMessage instanceof SuperMessage) {
         SuperMessage superMessage = (SuperMessage) serialMessage;

         final double accelX = superMessage.getAccelX();
         final double accelY = superMessage.getAccelY();
         final double accelZ = superMessage.getAccelZ();

         this.currentRealAccelerationValues[0] = accelX;
         this.currentRealAccelerationValues[1] = accelY;
         this.currentRealAccelerationValues[2] = accelZ;

         final double[] accelerationSmooth = accelerationSmoother.smooth(accelX, accelY, accelZ);

         this.currentSmoothedAccelerationValues[0] = accelerationSmooth[0];
         this.currentSmoothedAccelerationValues[1] = accelerationSmooth[1];
         this.currentSmoothedAccelerationValues[2] = accelerationSmooth[2];

         accelerationStochastic.update(accelerationSmooth[0], accelerationSmooth[1], accelerationSmooth[2]);

         final double gyroX = superMessage.getGyroX();
         final double gyroY = superMessage.getGyroY();
         final double gyroZ = superMessage.getGyroZ();

         this.currentGyroValues[0] = gyroX;
         this.currentGyroValues[1] = gyroY;
         this.currentGyroValues[2] = gyroZ;

         sensorFusionFilter.update(accelerationSmooth[0], accelerationSmooth[1], accelerationSmooth[2], gyroX, gyroY, gyroZ);
         this.currentOrientationValues[0] = sensorFusionFilter.getRoll();
         this.currentOrientationValues[1] = sensorFusionFilter.getPitch();
         this.currentOrientationValues[2] = sensorFusionFilter.getYaw();

         currentMagneticValues[0] = superMessage.getMagX();
         currentMagneticValues[1] = superMessage.getMagY();
         currentMagneticValues[2] = superMessage.getMagZ();

         final double thumbFlexValue = superMessage.getThumbFlexValue();
         final double indexFlexValue = superMessage.getIndexFlexValue();
         final double middleFlexValue = superMessage.getMiddleFlexValue();
         final double ringFlexValue = superMessage.getRingFlexValue();
         final double pinkyFlexValue = superMessage.getPinkyFlexValue();

         final double[] flexSmooth = flexAngleSmoother.smooth(thumbFlexValue, indexFlexValue, middleFlexValue, ringFlexValue, pinkyFlexValue);
         this.currentFlexAngleValues[0] = flexSmooth[0];
         this.currentFlexAngleValues[1] = flexSmooth[1];
         this.currentFlexAngleValues[2] = flexSmooth[2];
         this.currentFlexAngleValues[3] = flexSmooth[3];
         this.currentFlexAngleValues[4] = flexSmooth[4];

         // update all components in the end
         this.callback.onUpdate(getPassedTime());

         // TODO should I really use a subscription model?
         for (InertialDataReceiver receiver : receivers) {
            receiver.handleUpdate(getPassedTime());
         }
      }
   }

   @Override
   public void stop() {
      if (serialMessageHandler != null && serialMessageHandler.isSerialPortOpened()) {
         serialMessageHandler.closeSerialPort();
      }
   }

   @Override
   public boolean isConnected() {
      return serialMessageHandler != null && serialMessageHandler.isSerialPortOpened();
   }

   @Override
   public double[] getOrientationAngles() {
      return currentOrientationValues;
   }

   @Override
   public double getRoll() {
      return currentOrientationValues[0];
   }

   @Override
   public double getPitch() {
      return currentOrientationValues[1];
   }

   @Override
   public double getYaw() {
      return currentOrientationValues[2];
   }

   @Override
   public double[] getAccelerationSmoothed() {
      return currentSmoothedAccelerationValues;
   }

   @Override
   public double[] getAccelerationReal() {
      return this.currentRealAccelerationValues;
   }

   @Override
   public double[] getAccelerationMeans() {
      return this.accelerationStochastic.getMeans();
   }

   @Override
   public double[] getAccelerationVariances() {
      return this.accelerationStochastic.getVariances();
   }

   @Override
   public double[] getAccelerationStdDeviations() {
      return this.accelerationStochastic.getStandardDeviations();
   }

   @Override
   public double[] getGyro() {
      return currentGyroValues;
   }

   @Override
   public double[] getMagnetometer() {
      return currentMagneticValues;
   }

   @Override
   public double[] getFlexAngles() {
      return currentFlexAngleValues;
   }

   @Override
   public double getThumbAngle() {
      return currentFlexAngleValues[0];
   }

   @Override
   public double getIndexAngle() {
      return currentFlexAngleValues[1];
   }

   @Override
   public double getMiddleAngle() {
      return currentFlexAngleValues[2];
   }

   @Override
   public double getRingAngle() {
      return currentFlexAngleValues[3];
   }

   @Override
   public double getPinkyAngle() {
      return currentFlexAngleValues[4];
   }

   @Override
   public void addSubscriber(final InertialDataReceiver receiver) {
      this.receivers.add(receiver);
   }

   private long getPassedTime() {
      final long passedTime = System.currentTimeMillis() - startedTime;
      return passedTime;
   }

   @Override
   public SensorFusionFilter getSensorFusionFilter() {
      return this.sensorFusionFilter;
   }
}
