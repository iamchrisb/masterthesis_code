package de.cb.ma.core.database.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DAWCommand implements Command {
   public enum MANUFACTURER {
      ABLETON_LIVE,
      FL_STUDIO,
      LOGIX
   }

   private String id;
   private String command;
   private String description;
   private MANUFACTURER manufacturer;

   private MidiMessageParameter midiMessageParameter;

   public DAWCommand(String id, final String command, String description, MANUFACTURER manufacturer, MidiMessageParameter midiMessageParameter) {
      this.id = id;
      this.command = command;
      this.description = description;
      this.manufacturer = manufacturer;
      this.midiMessageParameter = midiMessageParameter;
   }

   public DAWCommand() {
      // JSON
   }

   public String getCommand() {
      return command;
   }

   public void setCommand(final String command) {
      this.command = command;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(final String description) {
      this.description = description;
   }

   public MANUFACTURER getManufacturer() {
      return manufacturer;
   }

   public void setManufacturer(final MANUFACTURER manufacturer) {
      this.manufacturer = manufacturer;
   }

   public MidiMessageParameter getMidiMessageParameter() {
      return midiMessageParameter;
   }

   public void setMidiMessageParameter(final MidiMessageParameter midiMessageParameter) {
      this.midiMessageParameter = midiMessageParameter;
   }

   public String getId() {
      return id;
   }

   public void setId(final String id) {
      this.id = id;
   }

   @Override
   public String toString() {
      return command;
   }
}
