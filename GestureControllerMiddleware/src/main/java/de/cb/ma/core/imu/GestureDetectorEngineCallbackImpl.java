package de.cb.ma.core.imu;

import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.imu.motion.PostureMotion;
import de.cb.ma.util.Pair;

import java.util.List;

public class GestureDetectorEngineCallbackImpl implements GestureDetectorEngineCallback {

   @Override
   public void onPostureDetected(final PostureMotion postureMotion) {

   }

   @Override
   public void onPostureDetected(final HandSign.SIGN sign, final Integer[] binaryPattern) {

   }

   @Override
   public void onDetectionStart(final double varianceX, final double varianceY, final double varianceZ) {
      // do nothing
   }

   @Override
   public void onDetectionEnd(final double varianceX, final double varianceY, final double varianceZ) {
      // do nothing
   }

   @Override
   public void onGestureRecognized(final HandSign.SIGN posture, final GestureModel recognizedGestureModel, final List<Pair<Double, List<Double>>> recentCapturedAmplitudeValues,
         final double distance) {

   }

   @Override
   public void onAnyGestureCaptured(final HandSign.SIGN posture, final List<Pair<Double, List<Double>>> recentCapturedAmplitudeValues) {
      // do nothing
   }

}
