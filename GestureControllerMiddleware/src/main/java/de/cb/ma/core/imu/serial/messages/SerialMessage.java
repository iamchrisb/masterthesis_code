package de.cb.ma.core.imu.serial.messages;

public abstract class SerialMessage {

   // this message type carries all information [spr:accX;accY;accZ;gyroX;gyroY;gyroZ;magX;magY;magZ] in one message
   public static final String SUPER_TYPE = "SPR";

   public static final String APP_TYPE = "APP";
   public static final String DEBUG_TYPE = "DBG";

   public static final String APP_SUBTYPE_BEND = "BEND";
   public static final String APP_SUBTYPE_ORIENTATION = "ORI";
   public static final String APP_SUBTYPE_GYRO_PEAK_HAPPENED = "GYROPKH";    // indicates on which axis a gyropeak happened
   public static final String APP_SUBTYPE_DIRECTION = "DIR";      // movement in a certain direction
   public static final String APP_SUBTYPE_DRUM = "DRUM";          // drum hit with an intensity

   public static final String DEBUG_SUBTYPE_GYRO_PEAK_VALUE = "GYROPKV";        // the instensity of the gyropeak
   public static final String DEBUG_SUBTYPE_MAG = "MAG";
   public static final String DEBUG_SUBTYPE_TEMP = "TMP";
   public static final String DEBUG_SUBTYPE_FREQ = "FREQ";
   public static final String DEBUG_SUBTYPE_ACC = "ACC";          // plain acceleration affected gravity
   public static final String DEBUG_SUBTYPE_GRAV = "GRAV";        // the gravity value, affecting the accelerometer
   public static final String DEBUG_SUBTYPE_LINACC = "LINACC";    // linear acceleration - basically acceleration w/o gravity
}
