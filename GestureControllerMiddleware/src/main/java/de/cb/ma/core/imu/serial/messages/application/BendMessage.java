package de.cb.ma.core.imu.serial.messages.application;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class BendMessage extends SerialMessage {

   public BendMessage(final String thumbFlexValue, final String indexFlexValue, final String middleFlexValue,
         final String ringFlexValue, final String pinkyFlexValue, final String averageValue) {
      this.thumbFlexValue = Double.valueOf(thumbFlexValue);
      this.indexFlexValue = Double.valueOf(indexFlexValue);
      this.middleFlexValue = Double.valueOf(middleFlexValue);
      this.ringFlexValue = Double.valueOf(ringFlexValue);
      this.pinkyFlexValue = Double.valueOf(pinkyFlexValue);
      this.averageValue = Double.valueOf(averageValue);
   }

   private Double thumbFlexValue;
   private Double indexFlexValue;
   private Double middleFlexValue;
   private Double ringFlexValue;
   private Double pinkyFlexValue;
   private Double averageValue;

   public Double getThumbFlexValue() {
      return thumbFlexValue;
   }

   public Double getIndexFlexValue() {
      return indexFlexValue;
   }

   public Double getMiddleFlexValue() {
      return middleFlexValue;
   }

   public Double getRingFlexValue() {
      return ringFlexValue;
   }

   public Double getPinkyFlexValue() {
      return pinkyFlexValue;
   }

   public Double getAverageValue() {
      return averageValue;
   }

   @Override
   public String toString() {
      return "BendMessage [" +
            "thumbFlexValue=" + thumbFlexValue +
            ", indexFlexValue=" + indexFlexValue +
            ", middleFlexValue=" + middleFlexValue +
            ", ringFlexValue=" + ringFlexValue +
            ", pinkyFlexValue=" + pinkyFlexValue +
            ", averageValue=" + averageValue +
            ']';
   }
}
