package de.cb.ma.core.imu.serial.messages.debug;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class LinearAccelerationMessage extends SerialMessage {

   public LinearAccelerationMessage(final String xAccelValue, final String yAccelValue, final String zAccelValue) {
      this.xAccelValue = xAccelValue;
      this.yAccelValue = yAccelValue;
      this.zAccelValue = zAccelValue;
   }

   private String xAccelValue;
   private String yAccelValue;
   private String zAccelValue;

   public String getxAccelValue() {
      return xAccelValue;
   }

   public String getyAccelValue() {
      return yAccelValue;
   }

   public String getzAccelValue() {
      return zAccelValue;
   }

   @Override
   public String toString() {
      return "AccelerationMessage [" +
            "xAccelValue='" + xAccelValue +
            ", yAccelValue='" + yAccelValue +
            ", zAccelValue='" + zAccelValue +
            ']';
   }
}
