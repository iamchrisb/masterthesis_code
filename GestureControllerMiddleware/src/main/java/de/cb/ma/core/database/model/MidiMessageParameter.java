package de.cb.ma.core.database.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MidiMessageParameter {

   public enum MIDI_MESSAGE_TYPE {
      NOTE_ON,
      NOTE_OFF,
      PITCH_BEND,
      CONTROL_CHANGE,
      PROGRAM_CHANGE,
   }

   private MIDI_MESSAGE_TYPE actionType;
   private int midiChannel;
   private int databyte1;
   private int databyte2;

   public MidiMessageParameter() {
      // JSON
   }

   /**
    * @param midiChannel
    * @param dataByte1
    * @param dataByte2
    * @param messageType
    */
   public MidiMessageParameter(final MIDI_MESSAGE_TYPE messageType, final int midiChannel,
         final int dataByte1, final int dataByte2) {
      this.actionType = messageType;
      this.midiChannel = midiChannel;
      this.databyte1 = dataByte1;
      this.databyte2 = dataByte2;
   }

   public int getMidiChannel() {
      return midiChannel;
   }

   public void setMidiChannel(final int midiChannel) {
      this.midiChannel = midiChannel;
   }

   public int getDatabyte1() {
      return databyte1;
   }

   public void setDatabyte1(final int databyte1) {
      this.databyte1 = databyte1;
   }

   public int getDatabyte2() {
      return databyte2;
   }

   public void setDatabyte2(final int databyte2) {
      this.databyte2 = databyte2;
   }

   public MIDI_MESSAGE_TYPE getActionType() {
      return actionType;
   }

   public void setActionType(final MIDI_MESSAGE_TYPE actionType) {
      this.actionType = actionType;
   }
}
