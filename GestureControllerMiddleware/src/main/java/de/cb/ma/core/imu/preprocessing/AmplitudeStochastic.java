package de.cb.ma.core.imu.preprocessing;

public interface AmplitudeStochastic {

   void update(double amplitude);

   void update(double... amplitudes);

   double getMean(int index);

   double getVariance(int index);

   double getStandardDeviation(int index);

   double[] getMeans();

   double[] getVariances();

   double[] getStandardDeviations();
}
