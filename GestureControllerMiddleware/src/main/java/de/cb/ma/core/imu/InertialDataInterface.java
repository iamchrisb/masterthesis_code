package de.cb.ma.core.imu;

import de.cb.ma.core.imu.preprocessing.filter.sensorfusion.SensorFusionFilter;

public interface InertialDataInterface {

   double[] getOrientationAngles();

   double getRoll();

   double getPitch();

   double getYaw();

   double[] getAccelerationSmoothed();

   double[] getAccelerationReal();

   double[] getAccelerationMeans();

   double[] getAccelerationVariances();

   double[] getAccelerationStdDeviations();

   double[] getGyro();

   double[] getMagnetometer();

   double[] getFlexAngles();

   double getThumbAngle();

   double getIndexAngle();

   double getMiddleAngle();

   double getRingAngle();

   double getPinkyAngle();

   void addSubscriber(InertialDataReceiver receiver);

   SensorFusionFilter getSensorFusionFilter();
}
