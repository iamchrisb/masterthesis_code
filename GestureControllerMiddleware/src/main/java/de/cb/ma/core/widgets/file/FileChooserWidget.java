package de.cb.ma.core.widgets.file;

import de.cb.ma.util.Constants;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;

public class FileChooserWidget extends Pane {

   public static final String CURRENT_FILE = "Current file: ";

   public interface Callback {
      void onFileSelected(File file);
   }

   private final Button openBtn;

   public FileChooserWidget(String title, Stage stage, Callback callback) {
      VBox container = new VBox(10);

      Label label = new Label(title);

      openBtn = new Button("open");

      currentFileLabel = new Label(CURRENT_FILE);

      FileChooser chooser = new FileChooser();
      chooser.setInitialDirectory(new File(Constants.WORKING_DIR));
      chooser.setTitle(title);

      openBtn.setOnAction(new EventHandler<ActionEvent>() {
         @Override
         public void handle(final ActionEvent event) {
            final File file = chooser.showOpenDialog(stage);
            if (file != null) {
               callback.onFileSelected(file);
               currentFileLabel.setText(CURRENT_FILE + file.getName());
            }
         }
      });

      container.getChildren()
            .addAll(label, openBtn, currentFileLabel);

      this.getChildren()
            .addAll(container);
   }

   private final Label currentFileLabel;
}
