package de.cb.ma.core.widgets.contentmanagement.daw;

import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.util.MidiHelper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class CreateSingleMidiMessageWidget extends Pane {

   private final Label midiParameterTitle;
   private final ComboBox midiMessageParameterTypeSelect;
   private final ComboBox midiChannelSelection;

   public CreateSingleMidiMessageWidget() {
      midiParameterTitle = new Label("midi message parameter (channel messages)");

      ObservableList<String> notes =
            FXCollections.observableArrayList(
                  MidiHelper.NOTES
            );

      HBox databyteOneContainer = new HBox(20);

      databyte1Selection = new ComboBox<>(notes);
      databyte1Selection.getSelectionModel()
            .selectFirst();

      databyteOneContainer.getChildren()
            .addAll(new Label("databyte1 "), databyte1Selection);

      HBox databyteTwoContainer = new HBox(20);

      final Label databyte2Label = new Label("databyte2 ");
      databyte2Selection = new ComboBox<>(notes);
      databyte2Selection.getSelectionModel()
            .selectFirst();

      databyteTwoContainer.getChildren()
            .addAll(databyte2Label, databyte2Selection);

      HBox midiTypeContainer = new HBox(20);
      midiMessageParameterTypeSelect = new ComboBox<>(FXCollections.observableArrayList(MidiMessageParameter.MIDI_MESSAGE_TYPE.values()));
      midiMessageParameterTypeSelect.getSelectionModel()
            .selectFirst();

      midiTypeContainer.getChildren()
            .addAll(new Label("MIDI Type"), midiMessageParameterTypeSelect);

      HBox midiChannelContainer = new HBox(20);
      final Label midiChannelLbl = new Label("MIDI channel: ");
      midiChannelSelection = new ComboBox<>(FXCollections.observableArrayList(
            MidiHelper.MIDI_CHANNELS));
      midiChannelSelection.getSelectionModel()
            .selectFirst();

      midiChannelContainer.getChildren()
            .addAll(midiChannelLbl, midiChannelSelection);

      final VBox body = new VBox(20);
      body.getChildren()
            .addAll(midiParameterTitle, midiTypeContainer, midiChannelContainer, databyteOneContainer, databyteTwoContainer);

      this.getChildren()
            .addAll(body);
   }

   public MidiMessageParameter.MIDI_MESSAGE_TYPE getSelectedMidiActionType() {
      final MidiMessageParameter.MIDI_MESSAGE_TYPE midiMessageType = (MidiMessageParameter.MIDI_MESSAGE_TYPE) midiMessageParameterTypeSelect.getSelectionModel()
            .getSelectedItem();
      return midiMessageType;
   }

   public Integer getMidiChannel() {
      return (Integer) midiChannelSelection.getSelectionModel()
            .getSelectedItem() - 1;
   }

   public Integer getDataByte1() {
      // return index as needed int value 0 - 127
      return databyte1Selection.getSelectionModel()
            .getSelectedIndex();
   }

   public Integer getDataByte2() {
      // return index as needed int value 0 - 127
      return databyte2Selection.getSelectionModel()
            .getSelectedIndex();
   }

   private final ComboBox databyte1Selection;
   private final ComboBox databyte2Selection;
}
