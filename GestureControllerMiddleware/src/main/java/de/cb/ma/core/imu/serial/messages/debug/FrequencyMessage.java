package de.cb.ma.core.imu.serial.messages.debug;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class FrequencyMessage extends SerialMessage {

   public FrequencyMessage(final String freqValue) {
      this.freqValue = freqValue;
   }

   private String freqValue;

   public String getFreqValue() {
      return freqValue;
   }

}
