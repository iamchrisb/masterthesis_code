package de.cb.ma.core.widgets.analysis.charts;

import de.cb.ma.core.widgets.file.SaveRangeWidget;
import de.cb.ma.util.Constants;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class RunningLineGraphPlotWidget implements PlotWidget {

   Pane container;

   private long oldSystemTime;
   private long pastSeconds = 0;

   private static final int xAxisRange = 50;

   private boolean isAnimated = false;

   private final NumberAxis xAxis;
   private final NumberAxis yAxis;
   private LineChart<Number, Number> lineChart;
   private String[] legendNames;
   private int windowSize;
   private SaveRangeWidget rangeSaveWidget;

   public RunningLineGraphPlotWidget(final String legendName, final String chartTitle, final String xAxisLabel, final String yAxisLabel, int windowSize) {
      this(new String[] { legendName }, chartTitle, xAxisLabel, yAxisLabel, windowSize);
   }

   public RunningLineGraphPlotWidget(final String[] legendNames, final String chartTitle, final String xAxisLabel, final String yAxisLabel, int windowSize) {

      this.legendNames = legendNames;
      this.windowSize = windowSize;
      xAxis = new NumberAxis();

      xAxis.setLabel(xAxisLabel);
      xAxis.setAnimated(true);
      xAxis.setLowerBound(0);
      xAxis.setUpperBound(xAxisRange);
      //      xAxis.setAutoRanging(false);

      xAxis.setForceZeroInRange(false);

      yAxis = new NumberAxis();

      lineChart = new LineChart<Number, Number>(xAxis, yAxis);

      //      yAxis.setUpperBound(90);
      //      yAxis.setLowerBound(-90);
      //      yAxis.setAutoRanging(false);

      yAxis.setLabel(yAxisLabel);
      yAxis.setAnimated(true);

      lineChart.setTitle(chartTitle);
      lineChart.setAnimated(false);

      for (String legendName : legendNames) {
         XYChart.Series dataSeries = new XYChart.Series();
         dataSeries.setName(legendName);

         lineChart.getData()
               .addAll(dataSeries);
      }

      container = new VBox();
      final HBox controllContainer = new HBox(10);

      container.getChildren()
            .addAll(lineChart, controllContainer);

      //      lineChart.getData().addAll(series1, series2, series3);

      //      HBox buttonContainer = new HBox();
      //      container.getChildren()
      //            .add(buttonContainer);
      //      Button addData = new Button("Clear");
      //      buttonContainer.getChildren()
      //            .add(addData);

      //      addData.setOnAction(new EventHandler<ActionEvent>() {
      //         @Override
      //         public void handle(final ActionEvent event) {
      //            //            lineChart.getData().clear();
      //         }
      //      });

      lineChart.getStyleClass()
            .add("thin-lines");
      lineChart.setCreateSymbols(false);
   }

   @Override
   public void setRangeSaveVisibility(boolean value) {
      this.rangeSaveWidget.setVisible(value);
   }

   @Override
   public void setWidth(final double width) {
      this.lineChart.setPrefWidth(width);
      this.lineChart.setMinWidth(width);
      this.lineChart.setMaxWidth(width);
   }

   @Override
   public void setHeight(final double height) {
      this.lineChart.setPrefHeight(height);
      this.lineChart.setMaxHeight(height);
      this.lineChart.setMinHeight(height);
   }

   public Pane getElement() {
      return container;
   }

   public void addData(Number yValue, Number... xValue) {
      Platform.runLater(() -> {

         final ObservableList<XYChart.Series<Number, Number>> lineChartData = lineChart.getData();

         for (int i = 0; i < xValue.length; i++) {
            final XYChart.Series<Number, Number> dataSeries = lineChartData.get(i);
            final int currentDataSize = dataSeries
                  .getData()
                  .size();

            if (currentDataSize == windowSize) {
               dataSeries.getData()
                     .remove(0);
            }

            if (showAxisValues) {
               dataSeries.setName(legendNames[i] + ": " + Constants.INTEGER_FORMAT.format(xValue[i].doubleValue()));
            } else {
               dataSeries.setName(legendNames[i]);
            }

            dataSeries
                  .getData()
                  .add(new XYChart.Data<>(yValue, xValue[i]));
         }
      });
   }

   @Override
   public void updateRangeSlider(final double from, final double to) {
      // do nothing
   }

   public void clearData() {
      final ObservableList<XYChart.Series<Number, Number>> data = lineChart.getData();

      for (XYChart.Series<Number, Number> datum : data) {
         datum.getData()
               .clear();
      }
   }

   public LineChart getLineChart() {
      return this.lineChart;
   }

   @Override
   public ObservableList<XYChart.Series<Number, Number>> getData() {
      return lineChart.getData();
   }

   @Override
   public void shrinkData(final Number from, final Number to) {
      // do nothing
   }

   public String[] getLegendNames() {
      return legendNames;
   }

   private boolean showAxisValues = true;

}
