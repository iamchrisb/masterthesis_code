package de.cb.ma.core.widgets.performance;

import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.util.Functions;
import de.cb.ma.util.MidiHelper;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javax.sound.midi.ShortMessage;

public class TiltConsumerWidget extends Pane {

   public static final int SLIDER_MIN_RANGE = -90;
   public static final int SLIDER_MAX_RANGE = 90;

   public enum AXIS {
      ROLL,
      PITCH
   }

   public enum CONTROL_PARAM {
      VOLUME,
      PITCH
   }

   public TiltConsumerWidget(String title, AXIS axis, HandSign.SIGN sign, CONTROL_PARAM controlParam, MidiHelper midiHelper) {
      this.axis = axis;
      this.midiHelper = midiHelper;
      HBox titleContainer = new HBox(20);

      final Label titleLbl = new Label(title);
      isActiveCheckBox = new CheckBox("active");
      isActiveCheckBox.setSelected(true);
      titleContainer.getChildren()
            .addAll(titleLbl, isActiveCheckBox);

      selectHandSign = new ComboBox<>(FXCollections.observableArrayList(HandSign.SIGN.values()));
      selectHandSign.getSelectionModel()
            .selectFirst();

      selectAxis = new ComboBox<>(FXCollections.observableArrayList(AXIS.values()));

      if (axis.equals(AXIS.ROLL)) {
         selectAxis.getSelectionModel()
               .selectFirst();
      } else {
         selectAxis.getSelectionModel()
               .select(1);
      }

      HBox techConfig = new HBox(20);
      techConfig.getChildren()
            .addAll(selectHandSign, selectAxis);

      final HBox dawConfig = new HBox(20);
      selectConsumer = new ComboBox<>(FXCollections.observableArrayList(CONTROL_PARAM.values()));

      if (controlParam.equals(CONTROL_PARAM.VOLUME)) {
         selectConsumer.getSelectionModel()
               .selectFirst();
      } else {
         selectConsumer.getSelectionModel()
               .select(1);
      }

      angleSlider = new Slider();
      angleSlider.setMax(SLIDER_MAX_RANGE);
      angleSlider.setMin(SLIDER_MIN_RANGE);
      angleSlider.setValue(0);
      angleSlider.setShowTickMarks(true);
      angleSlider.setMajorTickUnit(15);
      angleSlider.setMajorTickUnit(30);
      angleSlider.setShowTickLabels(true);

      dawConfig.getChildren()
            .addAll(selectConsumer, angleSlider);

      VBox body = new VBox(20);
      body.getChildren()
            .addAll(titleContainer, techConfig, dawConfig);

      this.getChildren()
            .addAll(body);
   }

   public void updateAngle(HandSign.SIGN sign, double rollAngle, double pitchAngle) {
      if (!isActiveCheckBox.isSelected()) {
         return;
      }

      final HandSign.SIGN selectedSign = (HandSign.SIGN) selectHandSign.getSelectionModel()
            .getSelectedItem();

      if (!selectedSign.equals(sign)) {
         return;
      }

      AXIS selectedAxis = (AXIS) selectAxis.getSelectionModel()
            .getSelectedItem();

      if (selectedAxis.equals(AXIS.ROLL)) {
         // change with roll angle
         double adjusted = adjustValue(rollAngle);
         changeSliderValue(adjusted);
         // release midi signal
         useRollAsMidiInput(rollAngle);
      } else {
         // change with pitch angle
         changeSliderValue(adjustValue(pitchAngle));
         // release midi signal
         usePitchAsMidiInput(pitchAngle);
      }

   }

   private void usePitchAsMidiInput(final double angle) {
      final double map = Functions.map(angle, SLIDER_MIN_RANGE, SLIDER_MAX_RANGE, MidiHelper.MIDI_MIN, MidiHelper.MIDI_MAX);
      midiHelper.sendChannelMessage(ShortMessage.CONTROL_CHANGE, 0, MidiHelper.CC_VOLUME_CHANGE, new Double(map).intValue());
   }

   private void useRollAsMidiInput(final double angle) {
      final double map = Functions.map(angle, SLIDER_MIN_RANGE, SLIDER_MAX_RANGE, MidiHelper.MIDI_MIN, MidiHelper.MIDI_MAX);
      midiHelper.sendChannelMessage(ShortMessage.CONTROL_CHANGE, 0, MidiHelper.CC_BALANCE_CHANGE, new Double(map).intValue());
   }

   private void changeSliderValue(final double adjusted) {
      Platform.runLater(() -> {
         angleSlider.setValue(adjusted);
      });
   }

   private double adjustValue(final double angle) {
      if (angle > SLIDER_MAX_RANGE) {
         return SLIDER_MAX_RANGE;
      } else if (angle < SLIDER_MIN_RANGE) {
         return SLIDER_MIN_RANGE;
      } else {
         return angle;
      }
   }

   private final ComboBox selectHandSign;
   private final ComboBox selectAxis;
   private final ComboBox selectConsumer;
   private final Slider angleSlider;
   private AXIS axis;
   private MidiHelper midiHelper;
   private final CheckBox isActiveCheckBox;

}
