package de.cb.ma.core.imu.preprocessing.filter.sensorfusion;

public class MadgewickFilter implements SensorFusionFilter {

   private double[] angles;
   private double[] quaternion;

   /**
    * UNDER CONSTRUCTION - NOT IMPLEMENTED YET
    */
   public MadgewickFilter() {

   }

   @Override
   public void update(final double accelX, final double accelY, final double accelZ, final double gyroX, final double gyroY, final double gyroZ) {

   }

   @Override
   public void update(final double accelX, final double accelY, final double accelZ, final double gyroX, final double gyroY, final double gyroZ, final double magX, final double magY,
         final double magZ) {

   }

   @Override
   public double[] getAngles() {
      return new double[0];
   }

   @Override
   public double getYaw() {
      return 0;
   }

   @Override
   public double getPitch() {
      return 0;
   }

   @Override
   public double getRoll() {
      return 0;
   }
}
