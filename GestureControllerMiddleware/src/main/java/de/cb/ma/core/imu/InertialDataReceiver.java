package de.cb.ma.core.imu;

public interface InertialDataReceiver {

   void handleUpdate(long processedTimeInMilliSeconds);

   void handleError();

   void setInertialDataInterface(InertialDataInterface inertialDataInterface);
}
