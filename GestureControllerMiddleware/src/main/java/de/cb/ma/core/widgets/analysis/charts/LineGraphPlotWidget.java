package de.cb.ma.core.widgets.analysis.charts;

import de.cb.ma.core.widgets.file.SaveRangeWidget;
import de.cb.ma.util.Pair;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.Validate;

public class LineGraphPlotWidget implements PlotWidget {

   Pane container;

   private long oldSystemTime;
   private long pastSeconds = 0;

   private static final int xAxisRange = 50;

   private boolean isAnimated = false;

   private final NumberAxis xAxis;
   private final NumberAxis yAxis;
   private LineChart<Number, Number> lineChart;
   private String[] legendNames;
   private SaveRangeWidget rangeSaveWidget;

   public LineGraphPlotWidget(final String legendName, final String chartTitle, final String xAxisLabel, final String yAxisLabel) {
      this(new String[] { legendName }, chartTitle, xAxisLabel, yAxisLabel);
   }

   public LineGraphPlotWidget(final String[] legendNames, final String chartTitle, final String xAxisLabel, final String yAxisLabel) {

      this.legendNames = legendNames;
      xAxis = new NumberAxis();

      xAxis.setLabel(xAxisLabel);
      xAxis.setAnimated(true);
      xAxis.setLowerBound(0);
      xAxis.setUpperBound(xAxisRange);
      //      xAxis.setAutoRanging(false);

      xAxis.setForceZeroInRange(false);

      yAxis = new NumberAxis();

      lineChart = new LineChart<Number, Number>(xAxis, yAxis);

      //      yAxis.setUpperBound(90);
      //      yAxis.setLowerBound(-90);
      //      yAxis.setAutoRanging(false);

      yAxis.setLabel(yAxisLabel);
      yAxis.setAnimated(true);

      lineChart.setTitle(chartTitle);
      lineChart.setAnimated(false);

      for (String legendName : legendNames) {
         XYChart.Series dataSeries = new XYChart.Series();
         dataSeries.setName(legendName);

         lineChart.getData()
               .addAll(dataSeries);
      }

      container = new VBox();
      final HBox controllContainer = new HBox(10);

      container.getChildren()
            .addAll(lineChart, controllContainer);

      rangeSaveWidget = new SaveRangeWidget(new SaveRangeWidget.Callback() {
         @Override
         public void onShrinkSelected(final double lowValue, final double highValue) {
            shrinkData(lowValue, highValue);
         }

         @Override
         public void onSaveSelected(final double lowValue, final double highValue, final String absolutePath) {
            //            final ObservableList<XYChart.Series<Number, Number>> lineChartData = lineChart.getData();
         }

         @Override
         public void onResetSelected() {

         }
      });
      rangeSaveWidget.setVisible(false);

      controllContainer.getChildren()
            .addAll(rangeSaveWidget);

      //      lineChart.getData().addAll(series1, series2, series3);

      //      HBox buttonContainer = new HBox();
      //      container.getChildren()
      //            .add(buttonContainer);
      //      Button addData = new Button("Clear");
      //      buttonContainer.getChildren()
      //            .add(addData);

      //      addData.setOnAction(new EventHandler<ActionEvent>() {
      //         @Override
      //         public void handle(final ActionEvent event) {
      //            //            lineChart.getData().clear();
      //         }
      //      });

      lineChart.getStyleClass()
            .add("thin-lines");
      lineChart.setCreateSymbols(false);
   }

   @Override
   public void setRangeSaveVisibility(boolean value) {
      this.rangeSaveWidget.setVisible(value);
   }

   @Override
   public void setWidth(final double width) {
      this.lineChart.setPrefWidth(width);
      this.lineChart.setMinWidth(width);
      this.lineChart.setMaxWidth(width);
   }

   @Override
   public void setHeight(final double height) {
      this.lineChart.setPrefHeight(height);
      this.lineChart.setMaxHeight(height);
      this.lineChart.setMinHeight(height);
   }

   public Pane getElement() {
      return container;
   }

   public void addData(Number yValue, Number xValue) {
      Platform.runLater(() -> {

         final ObservableList<XYChart.Series<Number, Number>> lineChartData = lineChart.getData();
         final XYChart.Series<Number, Number> dataSeries = lineChartData.get(0);

         final int currentDataSize = dataSeries
               .getData()
               .size();

         //                  if (currentDataSize >= xAxisRange) {
         //                     lineChartData.get(0)
         //                           .getData()
         //                           .remove(0);
         //                  }

         if (showAxisValues) {
            dataSeries.setName(legendNames[0] + ": " + xValue);
         } else {
            dataSeries.setName(legendNames[0]);
         }

         dataSeries
               .getData()
               .addAll(new XYChart.Data<Number, Number>(yValue, xValue));

         //updateRangeSlider(0, yValue.doubleValue());
      });
   }

   public void addData(int index, Number yValue, Number xValue) {
      Platform.runLater(() -> {

         final ObservableList<XYChart.Series<Number, Number>> lineChartData = lineChart.getData();
         final XYChart.Series<Number, Number> dataSeries = lineChartData.get(index);

         final int currentDataSize = dataSeries
               .getData()
               .size();

         if (showAxisValues) {
            dataSeries.setName(legendNames[index] + ": " + xValue);
         } else {
            dataSeries.setName(legendNames[index]);
         }

         dataSeries
               .getData()
               .addAll(new XYChart.Data<>(yValue, xValue));
      });
   }

   public void addData(Number yValue, List<Number> xValue) {
      Platform.runLater(() -> {

         final ObservableList<XYChart.Series<Number, Number>> lineChartData = lineChart.getData();

         for (int i = 0; i < xValue.size(); i++) {
            final XYChart.Series<Number, Number> dataSeries = lineChartData.get(i);
            final int currentDataSize = dataSeries
                  .getData()
                  .size();

            if (showAxisValues) {
               dataSeries.setName(legendNames[i] + ": " + xValue);
            } else {
               dataSeries.setName(legendNames[i]);
            }

            dataSeries
                  .getData()
                  .addAll(new XYChart.Data<>(yValue, xValue.get(i)));
         }

         //updateRangeSlider(0, yValue.doubleValue());
      });
   }

   @Override
   public void updateRangeSlider(double from, double to) {
      rangeSaveWidget.setRange(from, to, from, to);
   }

   public void clearData() {
      final ObservableList<XYChart.Series<Number, Number>> data = lineChart.getData();

      for (XYChart.Series<Number, Number> datum : data) {
         datum.getData()
               .clear();
      }
   }

   @Override
   public ObservableList<XYChart.Series<Number, Number>> getData() {
      return lineChart.getData();
   }

   @Override
   public void shrinkData(final Number from, final Number to) {
      final ObservableList<XYChart.Series<Number, Number>> dotChartData = lineChart.getData();

      for (int j = 0; j < dotChartData.size(); j++) {
         final XYChart.Series<Number, Number> currentSerie = dotChartData.get(j);

         int fromIndex = -1;
         int toIndex = -1;

         for (int i = 0; i < currentSerie.getData()
               .size(); i++) {
            final XYChart.Data<Number, Number> currentData = currentSerie.getData()
                  .get(i);
            final double timeAxisValue = currentData.getXValue()
                  .doubleValue();

            if (fromIndex == -1 && timeAxisValue >= from.doubleValue() && timeAxisValue <= to.doubleValue()) {
               fromIndex = i;
               continue;
            }
            if (toIndex == -1 && timeAxisValue >= to.doubleValue()) {
               toIndex = i;
               break;
            }
         }

         updateRangeSlider(currentSerie.getData()
               .get(fromIndex)
               .getXValue()
               .doubleValue(), currentSerie.getData()
               .get(toIndex)
               .getXValue()
               .doubleValue());

         currentSerie.getData()
               .remove(0, fromIndex);

         currentSerie.getData()
               .remove(toIndex, currentSerie.getData()
                     .size());

      }
   }

   public void updateRangeSlider() {
      final ObservableList<XYChart.Data<Number, Number>> data = lineChart.getData()
            .get(0)
            .getData();
      final int toIndex = data
            .size() - 1;

      final double fromValue = data.get(0)
            .getXValue()
            .doubleValue();
      final double toValue = data.get(toIndex)
            .getXValue()
            .doubleValue();

      System.out.println("update slider range from: " + fromValue + " / to: " + toValue);

      updateRangeSlider(fromValue, toValue);
   }

   public String[] getLegendNames() {
      return legendNames;
   }

   public void createChart(final List<Pair<Double, List<Double>>> frequencies, List<String> legendNames) {
      Validate.notEmpty(frequencies);
      Platform.runLater(new Runnable() {
         @Override
         public void run() {
            createChartInner(frequencies, legendNames);
         }
      });

   }

   private void createChartInner(final List<Pair<Double, List<Double>>> frequencies, final List<String> legendNames) {
      lineChart.getData()
            .clear();

      List<XYChart.Series> series = new LinkedList<>();

      List<Pair<Double, List<Double>>> copiedFrequencies = new ArrayList<>(frequencies);

      final Pair<Double, List<Double>> doubleListPair1 = copiedFrequencies.get(0);
      final List<Double> value1 = doubleListPair1
            .getValue();
      final int size = value1
            .size();

      for (int i = 0; i < size; i++) {
         series.add(new XYChart.Series());
      }

      for (int i = 0; i < frequencies.size(); i++) {
         final Pair<Double, List<Double>> doubleListPair = frequencies.get(i);
         final List<Double> value = doubleListPair.getValue();
         for (int j = 0; j < value.size(); j++) {
            series.get(j)
                  .getData()
                  .add(new XYChart.Data<Number, Number>(doubleListPair.getKey(), value.get(j)));
         }
      }

      for (int i = 0; i < series.size(); i++) {
         final XYChart.Series currentSeries = series.get(i);
         currentSeries.setName(legendNames.get(i));
         lineChart.getData()
               .add(currentSeries);
      }
   }

   public SaveRangeWidget getRangeWidget() {
      return this.rangeSaveWidget;
   }

   public Pair<Double, Double> getRangeTimes() {
      final ObservableList<XYChart.Data<Number, Number>> firstSeries = lineChart.getData()
            .get(0)
            .getData();

      final double from = firstSeries
            .get(0)
            .getXValue()
            .doubleValue();

      final double to = firstSeries.get(firstSeries.size() - 1)
            .getXValue()
            .doubleValue();
      return new Pair<Double, Double>(from, to);
   }

   public void showAxisValuesInLegend(boolean value) {
      this.showAxisValues = value;
   }

   private boolean showAxisValues = true;

   public LineChart<Number, Number> getLineChart() {
      return lineChart;
   }
}
