package de.cb.ma.core.widgets.analysis.midi;

import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.util.Constants;
import de.cb.ma.util.MidiHelper;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class MIDIKnobWidget extends Pane {
   public MIDIKnobWidget(final MidiHelper midiHelper) {

      VBox container = new VBox(20);
      HBox midiKnobParameter = new HBox(20);

      Slider mainVolumeSlider = new Slider();
      mainVolumeSlider.setMin(0);
      mainVolumeSlider.setMax(127);
      mainVolumeSlider.setValue(100);
      mainVolumeSlider.setShowTickLabels(true);
      mainVolumeSlider.setShowTickMarks(true);
      mainVolumeSlider.setMajorTickUnit(50);
      mainVolumeSlider.setMinorTickCount(5);
      mainVolumeSlider.setBlockIncrement(1);

      final Label sliderLabel = new Label();

      midiChannelSelection = new ComboBox<>(FXCollections.observableArrayList(
            MidiHelper.MIDI_CHANNELS));
      midiChannelSelection.getSelectionModel()
            .selectFirst();

      final MidiMessageParameter.MIDI_MESSAGE_TYPE[] values = MidiMessageParameter.MIDI_MESSAGE_TYPE.values();
      int ccIndex = 0;

      for (int i = 0; i < values.length; i++) {
         MidiMessageParameter.MIDI_MESSAGE_TYPE value = values[i];
         if (value.equals(MidiMessageParameter.MIDI_MESSAGE_TYPE.CONTROL_CHANGE)) {
            ccIndex = i;
         }
      }

      midiMessageParameterTypeSelect = new ComboBox<>(FXCollections.observableArrayList(values));
      midiMessageParameterTypeSelect.getSelectionModel()
            .select(ccIndex);
      midiMessageParameterTypeSelect.setEditable(false);
      midiMessageParameterTypeSelect.setDisable(true);

      midiCCParameterTypeSelect = new ComboBox<>(FXCollections.observableArrayList(MidiHelper.CC_TYPE.values()));
      midiCCParameterTypeSelect.getSelectionModel()
            .selectFirst();

      mainVolumeSlider.valueProperty()
            .addListener((ov, old_val, new_val) -> {
               Platform.runLater(() -> {
                  sliderLabel.setText(Constants.INTEGER_FORMAT.format(new_val));
               });

               final MidiMessageParameter.MIDI_MESSAGE_TYPE messageType = (MidiMessageParameter.MIDI_MESSAGE_TYPE) midiMessageParameterTypeSelect.getSelectionModel()
                     .getSelectedItem();

               midiHelper.sendChannelMessage(midiHelper.getIntForMessageActionType(messageType), midiChannelSelection.getSelectionModel()
                     .getSelectedIndex(), ((MidiHelper.CC_TYPE) midiCCParameterTypeSelect.getSelectionModel()
                     .getSelectedItem()).getValue(), new_val.intValue());
            });

      midiKnobParameter.getChildren()
            .addAll(midiChannelSelection, midiMessageParameterTypeSelect, midiCCParameterTypeSelect, mainVolumeSlider, sliderLabel);
      container.getChildren()
            .addAll(new Label("Parameters for MIDI knobs (CC only)"), midiKnobParameter);

      this.getChildren()
            .add(container);
   }

   private final ComboBox midiMessageParameterTypeSelect;
   private final ComboBox midiChannelSelection;
   private final ComboBox midiCCParameterTypeSelect;
}
