package de.cb.ma.core.widgets.performance;

import de.cb.ma.core.imu.InertialDataInterface;
import de.cb.ma.core.imu.InertialDataReceiver;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.util.MidiHelper;

/**
 * This class functions as a magic instrument using magnetic data to control the DAW.
 * It requires the configuration of the current MIDI port for the DAW through the using UI.
 */
public class MagneticSynthesizer implements InertialDataReceiver {

   public enum MODE {
      CHORD,
      RANDOM,
      HOLD
   }

   private MidiHelper midiHelper;
   private InertialDataInterface inertialDataInterface;

   private int latestMagneticX;
   private int latestMagneticY;
   private int currentMagneticNoteChannel = 11;

   /**
    * @param midiHelper
    *       to send MIDI signals way more easier
    */
   public MagneticSynthesizer(MidiHelper midiHelper) {
      this.midiHelper = midiHelper;
   }

   public void setMode(MODE mode) {
      this.currentMode = mode;
   }

   @Override
   public void handleUpdate(long processedTimeInMilliSeconds) {
      final double[] magnetometer = this.inertialDataInterface.getMagnetometer();

      final Integer magneticX = new Double(magnetometer[0]).intValue();
      final Integer magneticY = new Double(magnetometer[1]).intValue();

      if (Math.abs(latestMagneticX - magneticX) > 10) {
         final int pitch = Math.abs(magneticX % 48) + 48;
         midiHelper.playNote(currentMagneticNoteChannel, pitch, 64);
         latestMagneticX = magneticX;
      }

      //            if (Math.abs(latestMagneticY - magneticY) > 10) {
      //               final int pitch = Math.abs(magneticX % 48) + 48;
      //               playNote(12, pitch, 50);
      //               latestMagneticY = magneticY;
      //            }
   }

   @Override
   public void handleError() {
      throw new UnsupportedOperationException("not implemented yet.");
   }

   @Override
   public void setInertialDataInterface(final InertialDataInterface inertialDataInterface) {
      this.inertialDataInterface = inertialDataInterface;
   }

   public InertialDataInterface getInertialDataInterface() {
      return this.inertialDataInterface;
   }

   private MODE currentMode;

   public void updatePosture(final HandSign.SIGN posture) {

   }
}
