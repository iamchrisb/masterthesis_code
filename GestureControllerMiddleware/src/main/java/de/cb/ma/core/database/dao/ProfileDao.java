package de.cb.ma.core.database.dao;

import de.cb.ma.core.database.model.Profile;

import java.util.List;

public interface ProfileDao {
   List<Profile> findAll();

   Profile findById(String id);

   Profile update(Profile profile);

   Profile delete(Profile profile);

   Profile create(Profile profile);
}
