package de.cb.ma.core.imu.serial;

public interface SerialDataInterface {
   void start(final String serialPortName, final int baudRate);

   void stop();

   boolean isConnected();
}
