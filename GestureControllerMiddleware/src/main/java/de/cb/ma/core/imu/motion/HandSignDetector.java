package de.cb.ma.core.imu.motion;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class HandSignDetector {

   public interface Callback {
      void onPostureDetected(HandSign.SIGN posture, Integer[] binaryPattern);
   }

   private enum FINGER {
      THUMB,
      INDEX,
      MIDDLE,
      RING,
      PINKY;
   }

   private int deciderAngleThumb = 30;
   private int deciderAngleIndex = 70;
   private int deciderAngleMiddle = 50;
   private int deciderAngleRing = 50;
   private int deciderAnglePinky = 50;
   /**
    * accuracy in percent
    */
   private double accuracy = 0.3;
   private Callback callback;

   private Map<HandSign.SIGN, Integer[]> binaryGestureProfile = new HashMap<>();

   private Enum latestPosture = HandSign.SIGN.UNDETECTED;

   public HandSignDetector(final double accuracy, Callback callback) {
      this.accuracy = accuracy;
      this.callback = callback;
      binaryGestureProfile.put(HandSign.SIGN.FIST, new Integer[] { 1, 1, 1, 1, 1 });
      binaryGestureProfile.put(HandSign.SIGN.PALM, new Integer[] { 0, 0, 0, 0, 0 });
      binaryGestureProfile.put(HandSign.SIGN.TELEPHONE, new Integer[] { 0, 1, 1, 1, 0 });
      binaryGestureProfile.put(HandSign.SIGN.PIECE, new Integer[] { 1, 0, 0, 1, 1 });
      binaryGestureProfile.put(HandSign.SIGN.HEAVYMETAL, new Integer[] { 0, 0, 1, 1, 0 });
      binaryGestureProfile.put(HandSign.SIGN.POINT, new Integer[] { 1, 0, 1, 1, 1 });
      binaryGestureProfile.put(HandSign.SIGN.OKAY, new Integer[] { 0, 1, 1, 1, 1 });
      binaryGestureProfile.put(HandSign.SIGN.PERFECT, new Integer[] { 1, 1, 0, 0, 0 });
   }

   public void updateValues(double... angles) {

      Integer[] binaryPattern = getBinaryPattern(angles);

      for (Map.Entry<HandSign.SIGN, Integer[]> posture : binaryGestureProfile.entrySet()) {
         if (Arrays.equals(posture.getValue(), binaryPattern)) {
            if (!posture.getKey()
                  .equals(latestPosture)) {
               callback.onPostureDetected(posture.getKey(), posture.getValue());
               this.latestPosture = posture.getKey();
               break;
            }
         }
      }
   }

   private Integer[] getBinaryPattern(final double... angles) {
      double thumbAngle = angles[0];
      double indexFingerAngle = angles[1];
      double middleFingerAngle = angles[2];
      double ringFingerAngle = angles[3];
      double pinkyAngle = angles[4];

      Integer[] binaryPattern = new Integer[5];

      binaryPattern[0] = getBinaryValue(thumbAngle, FINGER.THUMB);
      binaryPattern[1] = getBinaryValue(indexFingerAngle, FINGER.INDEX);
      binaryPattern[2] = getBinaryValue(middleFingerAngle, FINGER.MIDDLE);
      binaryPattern[3] = getBinaryValue(ringFingerAngle, FINGER.RING);
      binaryPattern[4] = getBinaryValue(pinkyAngle, FINGER.PINKY);

      return binaryPattern;
   }

   private int getBinaryValue(final double fingerAngle, FINGER finger) {
      switch (finger) {
         case THUMB:
            return findBinaryValue(fingerAngle, deciderAngleThumb);
         case INDEX:
            return findBinaryValue(fingerAngle, deciderAngleIndex);
         case MIDDLE:
            return findBinaryValue(fingerAngle, deciderAngleMiddle);
         case RING:
            return findBinaryValue(fingerAngle, deciderAngleRing);
         case PINKY:
            return findBinaryValue(fingerAngle, deciderAnglePinky);
      }
      return 0;
   }

   private int findBinaryValue(final double fingerAngle, double deciderAngle) {
      if (fingerAngle < deciderAngle) {
         return 0;
      } else {
         return 1;
      }
   }

   public double getAccuracy() {
      return accuracy;
   }

   public void setAccuracy(final double accuracy) {
      this.accuracy = accuracy;
   }

   public int getDeciderAngleThumb() {
      return deciderAngleThumb;
   }

   public void setDeciderAngleThumb(final int deciderAngleThumb) {
      this.deciderAngleThumb = deciderAngleThumb;
   }

   public int getDeciderAngleIndex() {
      return deciderAngleIndex;
   }

   public void setDeciderAngleIndex(final int deciderAngleIndex) {
      this.deciderAngleIndex = deciderAngleIndex;
   }

   public int getDeciderAngleMiddle() {
      return deciderAngleMiddle;
   }

   public void setDeciderAngleMiddle(final int deciderAngleMiddle) {
      this.deciderAngleMiddle = deciderAngleMiddle;
   }

   public int getDeciderAngleRing() {
      return deciderAngleRing;
   }

   public void setDeciderAngleRing(final int deciderAngleRing) {
      this.deciderAngleRing = deciderAngleRing;
   }

   public int getDeciderAnglePinky() {
      return deciderAnglePinky;
   }

   public void setDeciderAnglePinky(final int deciderAnglePinky) {
      this.deciderAnglePinky = deciderAnglePinky;
   }
}
