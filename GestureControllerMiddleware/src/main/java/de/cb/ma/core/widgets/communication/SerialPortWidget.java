package de.cb.ma.core.widgets.communication;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import jssc.SerialPortList;

import java.util.Arrays;

public class SerialPortWidget extends Pane {

   public interface Callback {
      void onStartSerial(String COM_PORT, Integer BAUD_RATE);

      void onStopSerial();

      void onMuteSerial(boolean enabled);
   }

   public SerialPortWidget(String title, Callback callback) {

      VBox container = new VBox();
      container.setSpacing(10);

      final Label titleLabel = new Label(title);

      container.getChildren()
            .addAll(titleLabel);

      container.setSpacing(10);
      container.setMinWidth(250);

      ObservableList<Integer> baudrateOptions =
            FXCollections.observableArrayList(
                  4800,
                  9600,
                  19200,
                  38400,
                  57600,
                  115200,
                  230400,
                  250000,
                  500000
            );

      final HBox buttonContainer = new HBox(20);

      final ComboBox baudrateComboBox = new ComboBox(baudrateOptions);

      ObservableList<String> commPortOptions =
            FXCollections.observableList(Arrays.asList(SerialPortList.getPortNames()));
      ComboBox commPortComboBox = new ComboBox(commPortOptions);

      Platform.runLater(new Runnable() {
         @Override
         public void run() {
            baudrateComboBox.getSelectionModel()
                  .select(5);

            commPortComboBox.getSelectionModel()
                  .selectLast();
         }
      });

      createSerialConnectionBtn = new Button();
      createSerialConnectionBtn.setText("start");

      createSerialConnectionBtn.setOnAction(event -> {
         final String selectedItem = (String) commPortComboBox.getSelectionModel()
               .getSelectedItem();
         final Integer selectedBaudRate = (Integer) baudrateComboBox.getSelectionModel()
               .getSelectedItem();
         callback.onStartSerial(selectedItem, selectedBaudRate);
      });

      closeConnectionButton = new Button("stop");
      closeConnectionButton.setOnAction(event -> {
         callback.onStopSerial();
      });

      muteButton = new Button("isActive");
      muteButton.setOnAction(event -> {
         enabled = !enabled;
         callback.onMuteSerial(enabled);
      });

      buttonContainer.getChildren()
            .addAll(createSerialConnectionBtn, closeConnectionButton, muteButton);

      container.getChildren()
            .addAll(baudrateComboBox, commPortComboBox, buttonContainer);

      this.getChildren()
            .add(container);
   }

   public void setStartButtonText(String title) {
      this.createSerialConnectionBtn.setText(title);
   }

   public void setStopButtonText(String title) {
      this.closeConnectionButton.setText(title);
   }

   private final Button createSerialConnectionBtn;
   private final Button closeConnectionButton;

   public void enableStopButton(final boolean status) {
      //      createSerialConnectionBtn.setDisable(status);
   }

   public void enableStartButton(final boolean status) {
      //      closeConnectionButton.setDisable(status);
   }

   private final Button muteButton;
   private boolean enabled;
}
