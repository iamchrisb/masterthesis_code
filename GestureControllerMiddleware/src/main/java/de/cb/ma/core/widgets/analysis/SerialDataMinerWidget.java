package de.cb.ma.core.widgets.analysis;

import de.cb.ma.core.widgets.analysis.charts.RunningLineGraphPlotWidget;
import de.cb.ma.core.widgets.communication.SerialPortWidget;
import de.cb.ma.core.imu.serial.messages.application.SuperMessage;
import de.cb.ma.core.imu.serial.SerialMessageHandler;
import de.cb.ma.core.imu.preprocessing.filter.ApacheAmplitudeSmoother;
import de.cb.ma.util.Constants;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SerialDataMinerWidget extends Pane {

   private static final int LASTEST_MSGS_COUNT = 70;
   SerialMessageHandler serialMessageHandler;

   private TextField experimentDescriptionTF;

   private long startedTime;

   CheckBox captureAccelerometerData;
   CheckBox captureGyrometerData;
   CheckBox captureMagnetometerData;
   CheckBox captureFlexData;

   ApacheAmplitudeSmoother accelDataSmoother = new ApacheAmplitudeSmoother(32, 3);
   ApacheAmplitudeSmoother flexDataSmoother = new ApacheAmplitudeSmoother(32, 5);

   private RunningLineGraphPlotWidget cleanAccelerationDataGraph;
   private RunningLineGraphPlotWidget cleanGyroDataGraph;
   private RunningLineGraphPlotWidget flexGraph;

   final HertzCounterWidget hertzCounterWidget;

   public SerialDataMinerWidget() {

      cleanAccelerationDataGraph = new RunningLineGraphPlotWidget(new String[] { "x", "y", "z" }, "Acceleration data (smoothed)", "time in ms", "acceleration in mG", 600);
      cleanGyroDataGraph = new RunningLineGraphPlotWidget(new String[] { "x", "y", "z" }, "Gyro data", "time in ms", "cock", 600);
      flexGraph = new RunningLineGraphPlotWidget(new String[] { "thum", "index", "middle", "ring", "pinky" }, "Finger angles", "time in ms", "angle in degree", 600);

      SerialPortWidget serialPortWidget = new SerialPortWidget("Choose serial port", new SerialPortWidget.Callback() {
         @Override
         public void onStartSerial(final String COM_PORT, final Integer BAUD_RATE) {

            if (serialMessageHandler != null && serialMessageHandler.isSerialPortOpened()) {
               return;
            }

            startedTime = System.currentTimeMillis();
            hertzCounterWidget.start();

            serialMessageHandler = new SerialMessageHandler(COM_PORT, BAUD_RATE, serialMessage -> {
               final long passedTime = getPassedTime();
               hertzCounterWidget.update();

               if (serialMessage instanceof SuperMessage) {
                  final SuperMessage superMessage = (SuperMessage) serialMessage;

                  StringBuilder serialDataStringBuilder = new StringBuilder();

                  serialDataStringBuilder.append(passedTime)
                        .append(":");

                  final double accelX = superMessage.getAccelX();
                  final double accelY = superMessage.getAccelY();
                  final double accelZ = superMessage.getAccelZ();

                  final double gyroX = superMessage.getGyroX();
                  final double gyroY = superMessage.getGyroY();
                  final double gyroZ = superMessage.getGyroZ();

                  final double[] smooth = accelDataSmoother.smooth(accelX, accelY, accelZ);

                  if (captureAccelerometerData.isSelected()) {
                     serialDataStringBuilder.append(accelX + "," + accelY + "," + accelZ + ",");
                     cleanAccelerationDataGraph.addData(passedTime, smooth[0], smooth[1], smooth[2]);
                  }

                  if (captureGyrometerData.isSelected()) {
                     serialDataStringBuilder.append(gyroX + "," + gyroY + "," + gyroZ + ",");
                     cleanGyroDataGraph.addData(passedTime, gyroX, gyroY, gyroZ);
                  }

                  if (captureMagnetometerData.isSelected()) {
                     final double magX = superMessage.getMagX();
                     final double magY = superMessage.getMagY();
                     final double magZ = superMessage.getMagZ();
                     serialDataStringBuilder.append(magX + "," + magY + "," + magZ + ",");
                  }

                  if (captureFlexData.isSelected()) {
                     final double thumbFlexValue = superMessage.getThumbFlexValue();
                     final double indexFlexValue = superMessage.getIndexFlexValue();
                     final double middleFlexValue = superMessage.getMiddleFlexValue();
                     final double ringFlexValue = superMessage.getRingFlexValue();
                     final double pinkyFlexValue = superMessage.getPinkyFlexValue();
                     serialDataStringBuilder.append(thumbFlexValue + "," + indexFlexValue + "," + middleFlexValue + "," + ringFlexValue + "," + pinkyFlexValue + ",");

                     final double[] smoothedFlex = flexDataSmoother.smooth(thumbFlexValue, indexFlexValue, middleFlexValue, ringFlexValue, pinkyFlexValue);
                     flexGraph.addData(passedTime, smoothedFlex[0], smoothedFlex[1], smoothedFlex[2], smoothedFlex[3], smoothedFlex[4]);
                  }

                  if (!stopCapturingData.isSelected()) {
                     saveToFile(serialDataStringBuilder.toString());
                  }
               }
            });

            serialMessageHandler.startSerial();
         }

         @Override
         public void onStopSerial() {
            serialMessageHandler.closeSerialPort();
         }

         @Override
         public void onMuteSerial(final boolean enabled) {

         }
      });

      VBox selectDataContainer = new VBox(20);
      Label checkBoxLabel = new Label("Capture serial sata");

      captureAccelerometerData = new CheckBox("Accelerometer");
      captureAccelerometerData.setSelected(true);

      captureGyrometerData = new CheckBox("Gyrometer");
      captureGyrometerData.setSelected(true);

      captureMagnetometerData = new CheckBox("Magnetometer");
      captureMagnetometerData.setSelected(true);

      captureFlexData = new CheckBox("Finger flex");
      captureFlexData.setSelected(true);

      hertzCounterWidget = new HertzCounterWidget();

      stopCapturingData = new CheckBox("Pause file writing");
      stopCapturingData.setSelected(true);
      stopCapturingData.selectedProperty()
            .addListener(observable -> {
               if (!stopCapturingData.isSelected()) {
                  startedTime = timeAtStopping;
               } else {
                  timeAtStopping = startedTime;
               }
            });

      selectDataContainer.getChildren()
            .addAll(checkBoxLabel, captureAccelerometerData, captureGyrometerData, captureMagnetometerData, captureFlexData);

      cleanAccelerationDataGraph.getLineChart()
            .setCreateSymbols(false);

      experimentDescriptionTF = new TextField();
      experimentDescriptionTF.setPromptText("Enter some descriptive words");
      Label currentFilePathLabel = new Label();

      experimentDescriptionTF.textProperty()
            .addListener((observable, oldValue, newValue) -> {
               currentFilePathLabel.setText("..." + File.separator + getCurrentFileName());
            });

      VBox vContainer = new VBox(20);
      HBox hContainer = new HBox(20);

      HBox fileControl = new HBox(20);
      fileControl.getChildren()
            .addAll(experimentDescriptionTF, currentFilePathLabel);

      hContainer.getChildren()
            .addAll(serialPortWidget, selectDataContainer, stopCapturingData, hertzCounterWidget, fileControl);

      HBox imuGraphContainer = new HBox(20);
      imuGraphContainer.getChildren()
            .addAll(cleanAccelerationDataGraph.getElement(), cleanGyroDataGraph.getElement());

      HBox orientationGraphContainer = new HBox(20);

      vContainer.getChildren()
            .addAll(hContainer, imuGraphContainer, orientationGraphContainer, flexGraph.getElement());

      this.getChildren()
            .add(vContainer);

      cleanAccelerationDataGraph.setWidth(700);
      cleanGyroDataGraph.setWidth(700);
      flexGraph.setWidth(1400);
   }

   private long getPassedTime() {
      final long passedTime = System.currentTimeMillis() - startedTime;
      return passedTime;
   }

   private List<String> latestMessages = new ArrayList<>(LASTEST_MSGS_COUNT);
   private Path messageFile;

   /**
    * saves the latest time series data in chunks of 70 datasets
    *
    * @param latestSerialMessage
    *       the latest time series data from the sensors
    */
   public void saveToFile(String latestSerialMessage) {

      messageFile = Paths.get(getCurrentFilePath());
      //      Files.write
      if (Files.notExists(messageFile)) {
         try {
            Files.createFile(messageFile);
         } catch (IOException e) {
            e.printStackTrace();
         }
      }

      if (latestMessages.size() == LASTEST_MSGS_COUNT) {
         try {
            Files.write(messageFile, latestMessages, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
            latestMessages.clear();
         } catch (IOException e) {
            e.printStackTrace();
         }
      } else {
         latestMessages.add(latestSerialMessage);
      }
   }

   public String getCurrentFilePath() {
      final String filePath = Constants.EXPERIMENT_DATA_DIR + File.separator + getCurrentFileName();
      return filePath;
   }

   private String getCurrentFileName() {
      DateFormat dateFormat = new SimpleDateFormat(Constants.EXPERIMENT_DATA_DATE_FORMAT);
      Date date = new Date();
      final String dateStr = dateFormat.format(date);

      String fileName = dateStr + "-" + experimentDescriptionTF.getText() + Constants.INERTIAL_DATA_FILE_FORMAT;
      return fileName;
   }

   private final CheckBox stopCapturingData;
   private long timeAtStopping;
}
