package de.cb.ma.core.imu.preprocessing;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

public class ApacheMathAmplitudeStochastic implements AmplitudeStochastic {

   private int windowSize;
   DescriptiveStatistics[] descriptiveStatistics;
   private int dimension;

   private double[] currentMeans;
   private double[] currentVariances;
   private double[] currentStdDevs;

   public ApacheMathAmplitudeStochastic(int windowSize) {
      this(windowSize, 1);
   }

   public ApacheMathAmplitudeStochastic(int windowSize, int dimension) {
      this.windowSize = windowSize;
      this.dimension = dimension;

      descriptiveStatistics = new DescriptiveStatistics[dimension];

      for (int i = 0; i < descriptiveStatistics.length; i++) {
         descriptiveStatistics[i] = new DescriptiveStatistics();
         descriptiveStatistics[i].setWindowSize(this.windowSize);
      }

      currentMeans = new double[dimension];
      currentVariances = new double[dimension];
      currentStdDevs = new double[dimension];
   }

   @Override
   public void update(double amplitude) {
      descriptiveStatistics[0].addValue(amplitude);
      currentMeans[0] = descriptiveStatistics[0].getMean();
      currentVariances[0] = descriptiveStatistics[0].getVariance();
      currentStdDevs[0] = descriptiveStatistics[0].getStandardDeviation();
   }

   @Override
   public void update(double... amplitudes) {
      if (amplitudes.length != dimension) {
         throw new IllegalArgumentException("dimension of parameter and field must be equal!");
      }

      for (int i = 0; i < amplitudes.length; i++) {
         double amplitude = amplitudes[i];
         descriptiveStatistics[i].addValue(amplitude);

         currentMeans[i] = descriptiveStatistics[i].getMean();
         currentVariances[i] = descriptiveStatistics[i].getVariance();
         currentStdDevs[i] = descriptiveStatistics[i].getStandardDeviation();
      }
   }

   @Override
   public double getMean(int index) {
      return descriptiveStatistics[index].getMean();
   }

   @Override
   public double getVariance(int index) {
      return descriptiveStatistics[index].getVariance();
   }

   @Override
   public double getStandardDeviation(int index) {
      return descriptiveStatistics[index].getStandardDeviation();
   }

   @Override
   public double[] getMeans() {
      return currentMeans;
   }

   @Override
   public double[] getVariances() {
      return currentVariances;
   }

   @Override
   public double[] getStandardDeviations() {
      return currentStdDevs;
   }
}
