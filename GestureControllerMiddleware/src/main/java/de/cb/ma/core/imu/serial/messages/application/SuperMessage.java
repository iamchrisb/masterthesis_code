package de.cb.ma.core.imu.serial.messages.application;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class SuperMessage extends SerialMessage {

   public SuperMessage(final String accelX, final String accelY, final String accelZ, final String gyroX, final String gyroY, final String gyroZ, final String magX, final String magY,
         final String magZ, final String thumbFlexValue, final String indexFlexValue, final String middleFlexValue,
         final String ringFlexValue, final String pinkyFlexValue) {

      this.accelX = Double.valueOf(accelX);
      this.accelY = Double.valueOf(accelY);
      this.accelZ = Double.valueOf(accelZ);

      this.gyroX = Double.valueOf(gyroX);
      this.gyroY = Double.valueOf(gyroY);
      this.gyroZ = Double.valueOf(gyroZ);

      this.magX = Double.valueOf(magX);
      this.magY = Double.valueOf(magY);
      this.magZ = Double.valueOf(magZ);

      this.thumbFlexValue = Double.valueOf(thumbFlexValue);
      this.indexFlexValue = Double.valueOf(indexFlexValue);
      this.middleFlexValue = Double.valueOf(middleFlexValue);
      this.ringFlexValue = Double.valueOf(ringFlexValue);
      this.pinkyFlexValue = Double.valueOf(pinkyFlexValue);

      this.averageFlexValue = (this.thumbFlexValue + this.indexFlexValue + this.middleFlexValue + this.ringFlexValue + this.pinkyFlexValue) / 5;
   }

   private double averageFlexValue;
   private double thumbFlexValue;
   private double indexFlexValue;
   private double middleFlexValue;
   private double ringFlexValue;
   private double pinkyFlexValue;
   private double accelX;
   private double accelY;
   private double accelZ;
   private double gyroX;
   private double gyroY;
   private double gyroZ;
   private double magX;
   private double magY;
   private double magZ;

   public double getAverageFlexValue() {
      return averageFlexValue;
   }

   public void setAverageFlexValue(final double averageFlexValue) {
      this.averageFlexValue = averageFlexValue;
   }

   public double getThumbFlexValue() {
      return thumbFlexValue;
   }

   public void setThumbFlexValue(final double thumbFlexValue) {
      this.thumbFlexValue = thumbFlexValue;
   }

   public double getIndexFlexValue() {
      return indexFlexValue;
   }

   public void setIndexFlexValue(final double indexFlexValue) {
      this.indexFlexValue = indexFlexValue;
   }

   public double getMiddleFlexValue() {
      return middleFlexValue;
   }

   public void setMiddleFlexValue(final double middleFlexValue) {
      this.middleFlexValue = middleFlexValue;
   }

   public double getRingFlexValue() {
      return ringFlexValue;
   }

   public void setRingFlexValue(final double ringFlexValue) {
      this.ringFlexValue = ringFlexValue;
   }

   public double getPinkyFlexValue() {
      return pinkyFlexValue;
   }

   public void setPinkyFlexValue(final double pinkyFlexValue) {
      this.pinkyFlexValue = pinkyFlexValue;
   }

   public double getAccelX() {
      return accelX;
   }

   public void setAccelX(final double accelX) {
      this.accelX = accelX;
   }

   public double getAccelY() {
      return accelY;
   }

   public void setAccelY(final double accelY) {
      this.accelY = accelY;
   }

   public double getAccelZ() {
      return accelZ;
   }

   public void setAccelZ(final double accelZ) {
      this.accelZ = accelZ;
   }

   public double getGyroX() {
      return gyroX;
   }

   public void setGyroX(final double gyroX) {
      this.gyroX = gyroX;
   }

   public double getGyroY() {
      return gyroY;
   }

   public void setGyroY(final double gyroY) {
      this.gyroY = gyroY;
   }

   public double getGyroZ() {
      return gyroZ;
   }

   public void setGyroZ(final double gyroZ) {
      this.gyroZ = gyroZ;
   }

   public double getMagX() {
      return magX;
   }

   public void setMagX(final double magX) {
      this.magX = magX;
   }

   public double getMagY() {
      return magY;
   }

   public void setMagY(final double magY) {
      this.magY = magY;
   }

   public double getMagZ() {
      return magZ;
   }

   public void setMagZ(final double magZ) {
      this.magZ = magZ;
   }

   @Override
   public String toString() {
      return "SuperMessage [" +
            "averageFlexValue=" + averageFlexValue +
            ", thumbFlexValue=" + thumbFlexValue +
            ", indexFlexValue=" + indexFlexValue +
            ", middleFlexValue=" + middleFlexValue +
            ", ringFlexValue=" + ringFlexValue +
            ", pinkyFlexValue=" + pinkyFlexValue +
            ", accelX=" + accelX +
            ", accelY=" + accelY +
            ", accelZ=" + accelZ +
            ", gyroX=" + gyroX +
            ", gyroY=" + gyroY +
            ", gyroZ=" + gyroZ +
            ", magX=" + magX +
            ", magY=" + magY +
            ", magZ=" + magZ +
            ']';
   }
}
