package de.cb.ma.core.imu.motion;

public abstract class Gesture {

   /**
    * @param type
    *       one of the listed gesture-types, listed in the gesture class
    */
   public Gesture(final String type) {
      this.type = type;
   }

   protected String type;
}
