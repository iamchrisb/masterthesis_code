package de.cb.ma.core.database.model.jackson;

import de.cb.ma.core.database.model.Profile;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Profiles {

   Map<String, Profile> profiles;

   public Profiles() {
      // JSON
   }

   public Profiles(final Map<String, Profile> profiles) {
      this.profiles = profiles;
   }

   public Map<String, Profile> getProfiles() {
      return profiles;
   }

   public void setProfiles(final Map<String, Profile> profiles) {
      this.profiles = profiles;
   }
}
