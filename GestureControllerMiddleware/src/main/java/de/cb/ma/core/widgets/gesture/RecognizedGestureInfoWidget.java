package de.cb.ma.core.widgets.gesture;

import de.cb.ma.core.database.model.GestureModel;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class RecognizedGestureInfoWidget extends Pane {

   public RecognizedGestureInfoWidget(String title) {

      final Label titleLbl = new Label(title);

      HBox distanceContainer = new HBox(20);
      distanceLabel = new Label();
      distanceContainer.getChildren()
            .addAll(new Label("distance: "), distanceLabel);

      HBox nameContainer = new HBox(20);
      nameLabel = new Label();
      nameContainer.getChildren()
            .addAll(new Label("name: "), nameLabel);

      HBox sampleContainer = new HBox(20);
      sampleLabel = new Label();
      sampleContainer.getChildren()
            .addAll(new Label("samples: "), sampleLabel);

      final VBox body = new VBox(20);

      body.getChildren()
            .addAll(titleLbl, distanceContainer, nameContainer, sampleContainer);

      this.getChildren()
            .addAll(body);
   }

   public void showInfo(GestureModel gestureModel, double distance) {
      Platform.runLater(() -> {
         distanceLabel.setText(String.valueOf(distance));
         nameLabel.setText(gestureModel.getName());
         sampleLabel.setText(new Integer(gestureModel.getAccelerations()
               .getTimeKeys().length).toString());
      });
   }

   void setDistanceVisibility(boolean visibility) {
      this.distanceLabel.setVisible(visibility);
   }

   private final Label distanceLabel;
   private final Label nameLabel;
   private final Label sampleLabel;

   public void clear() {
      Platform.runLater(() -> {
         distanceLabel.setText("");
         nameLabel.setText("");
         sampleLabel.setText("");
      });
   }
}
