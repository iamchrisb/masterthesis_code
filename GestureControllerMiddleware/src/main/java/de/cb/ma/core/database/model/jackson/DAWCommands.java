package de.cb.ma.core.database.model.jackson;

import de.cb.ma.core.database.model.DAWCommand;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DAWCommands {

   Map<String, DAWCommand> dawCommands;

   public DAWCommands() {
      // JSON
   }

   public DAWCommands(final Map<String, DAWCommand> dawCommands) {
      this.dawCommands = dawCommands;
   }

   public Map<String, DAWCommand> getDawCommands() {
      return dawCommands;
   }

   public void setDawCommands(final Map<String, DAWCommand> dawCommands) {
      this.dawCommands = dawCommands;
   }
}
