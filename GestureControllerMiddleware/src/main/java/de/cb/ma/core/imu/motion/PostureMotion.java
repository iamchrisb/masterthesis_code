package de.cb.ma.core.imu.motion;

public class PostureMotion extends Gesture {

   public static final String SWIPE = "SWIPE";

   public enum MOTIONS {
      SCALE,
      CLICK,
      DOUBLE_CLICK,
   }

   /**
    * @param type
    *       one of the listed gesture-types, listed in the gesture class
    * @param velocity
    * @param direction
    *       a positive or negative value on a certain axis (x,y,z)
    */
   public PostureMotion(final String type, final float velocity, final int direction) {
      super(type);
      this.velocity = velocity;
      this.direction = direction;
   }

   private float velocity;
   private int direction;

   public String getType() {
      return type;
   }

   public float getVelocity() {
      return velocity;
   }

   public int getDirection() {
      return direction;
   }
}
