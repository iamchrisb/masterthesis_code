package de.cb.ma.core.imu.serial.messages.debug;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class TemperatureMessage extends SerialMessage {

   public TemperatureMessage(final String tempValue) {
      this.tempValue = tempValue;
   }

   private String tempValue;

   public String getTempValue() {
      return tempValue;
   }

}
