package de.cb.ma.core.imu.preprocessing;

import de.cb.ma.util.Pair;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.Validate;

public class AmplitudeNormalizer {

   private int minLimit;
   private int maxLimit;
   private double[] minAmps;
   private double[] maxAmps;
   private int dimension;

   /**
    * @param minLimit
    * @param maxLimit
    */
   public AmplitudeNormalizer(int minLimit, int maxLimit) {
      this(minLimit, maxLimit, 1);
   }

   /**
    * @param minLimit
    *       the minimum limit for example -1
    * @param maxLimit
    *       the maximum limit for example 1
    * @param dimensions
    *       the dimension of the data signal
    */
   public AmplitudeNormalizer(int minLimit, int maxLimit, int dimensions) {
      this.minLimit = minLimit;
      this.maxLimit = maxLimit;
      this.minAmps = new double[dimensions];
      this.maxAmps = new double[dimensions];
      this.dimension = dimensions;
   }

   public double[] normalize(double[] signal) {
      if (dimension != 1) {
         throw new IllegalArgumentException("Wrong dimension for signal array");
      }
      return normalize(signal, 0);
   }

   private double[] normalize(double[] signal, int axisIndex) {
      double[] normalizedAmps = new double[signal.length];
      computeAmplitudeMaxAndMin(signal, axisIndex);

      for (int i = 0; i < signal.length; i++) {
         double currentAmp = signal[i];
         final double normalizedAmp = normlizeValue(currentAmp, axisIndex);
         normalizedAmps[i] = normalizedAmp;
      }
      return normalizedAmps;
   }

   private void computeAmplitudeMaxAndMin(final double[] signal, int axisIndex) {
      double currentMinAmp = 0;
      double currentMaxAmp = 0;

      for (int i = 0; i < signal.length; i++) {
         double currentAmp = signal[i];
         if (currentAmp > currentMaxAmp) {
            currentMaxAmp = currentAmp;
         }
         if (currentAmp < currentMinAmp) {
            currentMinAmp = currentAmp;
         }
      }

      this.minAmps[axisIndex] = currentMinAmp;
      this.maxAmps[axisIndex] = currentMaxAmp;
   }

   /**
    * under construction!!!
    *
    * @param signals
    * @return
    */
   public double[][] normalize(double[][] signals) {
      if (signals.length != dimension) {
         throw new IllegalArgumentException("Wrong dimension for signal array");
      }

      //      double[][] normalizedSignals = Arrays.copyOf(signals);

      for (int i = 0; i < signals.length; i++) {
         double[] signal = signals[i];
         final double[] normalizedSignal = normalize(signal, i);
      }
      return null;
   }

   public List<Pair<Double, List<Double>>> normalize(final List<Pair<Double, List<Double>>> amplitudes) {
      Validate.notNull(amplitudes);
      Validate.notEmpty(amplitudes);

      computeAmplitudeMaxAndMin(amplitudes);

      List<Pair<Double, List<Double>>> normalizedAmplitudes = new ArrayList<>(400);

      for (Pair<Double, List<Double>> amplitude : amplitudes) {

         final Pair<Double, List<Double>> normalizedAmpliduteValue = new Pair<>();

         final Double time = amplitude.getKey();
         final List<Double> amplitudeValues = amplitude.getValue();
         for (int i = 0; i < amplitudeValues.size(); i++) {
            if (i == 0) {
               normalizedAmpliduteValue.setKey(time);
               normalizedAmpliduteValue.setValue(new ArrayList<>());
            }

            double ampValueDbl = amplitudeValues.get(i)
                  .doubleValue();

            final double normalizedDbl = normlizeValue(ampValueDbl, i);

            normalizedAmpliduteValue.getValue()
                  .add(normalizedDbl);
         }

         normalizedAmplitudes.add(normalizedAmpliduteValue);
      }

      return normalizedAmplitudes;
   }

   private void computeAmplitudeMaxAndMin(final List<Pair<Double, List<Double>>> amplitudes) {
      double[] currentMinAmp = new double[dimension];
      double[] currentMaxAmp = new double[dimension];

      for (Pair<Double, List<Double>> amplitude : amplitudes) {
         final List<Double> amplitudeValues = amplitude.getValue();
         for (int i = 0; i < amplitudeValues.size(); i++) {
            final double newValue = amplitudeValues.get(i)
                  .doubleValue();

            if (newValue < currentMinAmp[i]) {
               currentMinAmp[i] = newValue;
            }

            if (newValue > currentMaxAmp[i]) {
               currentMaxAmp[i] = newValue;
            }
         }
      }

      this.maxAmps = currentMaxAmp;
      this.minAmps = currentMinAmp;
   }

   // m = minLimit
   // n = maxLimit
   // A = maxAmp
   // B = minAmp
   // y = m + (x - A) * (n - m) / (A-B)
   private double normlizeValue(double ampValue, int axisIndex) {
      double y = minLimit + ((ampValue - maxAmps[axisIndex]) * (maxLimit - minLimit)) / (minAmps[axisIndex] - maxAmps[axisIndex]);
      return y * -1;
   }

   public int getMinLimit() {
      return minLimit;
   }

   public int getMaxLimit() {
      return maxLimit;
   }

   public double[] getMinAmps() {
      return minAmps;
   }

   public double[] getMaxAmps() {
      return maxAmps;
   }

   public int getDimension() {
      return dimension;
   }

}
