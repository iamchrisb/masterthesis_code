package de.cb.ma.core.database.dao.fileimpl;

import de.cb.ma.core.database.dao.ProfileDao;
import de.cb.ma.core.database.model.Profile;
import de.cb.ma.core.database.model.jackson.Profiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProfileDaoFileImpl implements ProfileDao {

   private ObjectMapper objectMapper;

   public ProfileDaoFileImpl(String filePath, ObjectMapper objectMapper) {
      this.objectMapper = objectMapper;
      try {
         src = new File(filePath);
         profiles = objectMapper.readValue(src, Profiles.class);
      } catch (JsonProcessingException | FileNotFoundException e) {
         e.printStackTrace();
         createFile();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   private void createFile() {
      profiles = new Profiles(new HashMap<String, Profile>());
      try {
         objectMapper.writeValue(src, profiles);
      } catch (IOException e1) {
         e1.printStackTrace();
      }
   }

   @Override
   public List<Profile> findAll() {
      return new LinkedList<>(profiles.getProfiles()
            .values());
   }

   @Override
   public Profile findById(final String id) {
      return profiles.getProfiles()
            .get(id);
   }

   @Override
   public Profile update(final Profile profile) {
      profiles.getProfiles()
            .put(profile.getId(), profile);
      return updateFile(profile);
   }

   private Profile updateFile(final Profile profile) {
      try {
         objectMapper.writeValue(src, profiles);
      } catch (IOException e) {
         e.printStackTrace();
         return null;
      }
      return profile;
   }

   @Override
   public Profile delete(final Profile profile) {
      profiles.getProfiles()
            .remove(profile.getId());
      return update(profile);
   }

   @Override
   public Profile create(final Profile profile) {
      profiles.getProfiles()
            .put(profile.getId(), profile);
      return update(profile);
   }

   private Profiles profiles;
   private File src;
}
