package de.cb.ma.core.imu.serial.messages.debug;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class AccelerationMessage extends SerialMessage {

   public AccelerationMessage(final String xAccelValue, final String yAccelValue, final String zAccelValue, final String filteredX, final String filteredY, final String filteredZ) {
      this.xAccelValue = Double.valueOf(xAccelValue);
      this.yAccelValue = Double.valueOf(yAccelValue);
      this.zAccelValue = Double.valueOf(zAccelValue);
      this.filteredX = Double.valueOf(filteredX);
      this.filteredY = Double.valueOf(filteredY);
      this.filteredZ = Double.valueOf(filteredZ);
   }

   private Double xAccelValue;
   private Double yAccelValue;
   private Double zAccelValue;
   private Double filteredX;
   private Double filteredY;
   private Double filteredZ;

   public Double getxAccelValue() {
      return xAccelValue;
   }

   public Double getyAccelValue() {
      return yAccelValue;
   }

   public Double getzAccelValue() {
      return zAccelValue;
   }

   public Double getFilteredX() {
      return filteredX;
   }

   public Double getFilteredY() {
      return filteredY;
   }

   public Double getFilteredZ() {
      return filteredZ;
   }

   @Override
   public String toString() {
      return "AccelerationMessage [" +
            "xAccelValue=" + xAccelValue +
            ", yAccelValue=" + yAccelValue +
            ", zAccelValue=" + zAccelValue +
            ", filteredX=" + filteredX +
            ", filteredY=" + filteredY +
            ", filteredZ=" + filteredZ +
            ']';
   }
}
