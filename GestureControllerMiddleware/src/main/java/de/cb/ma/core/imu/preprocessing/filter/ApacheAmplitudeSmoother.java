package de.cb.ma.core.imu.preprocessing.filter;

import org.apache.commons.math3.stat.descriptive.DescriptiveStatistics;

/**
 * represents a moving average or rolling window smoothing filter
 */
public class ApacheAmplitudeSmoother implements AmplitudeSmoother {

   private int windowSize;
   DescriptiveStatistics[] descriptiveStatistics;
   private int dimension;

   /**
    * inits 1-dimensional smoothing filter
    *
    * @param windowSize
    *       determines how many values should be used for the moving average
    */
   public ApacheAmplitudeSmoother(int windowSize) {
      this(windowSize, 1);
   }

   /**
    * @param windowSize
    *       determines how many values should be used for the moving average
    * @param dimension
    *       determines how many dimensions the signals offers and can not be changed
    */
   public ApacheAmplitudeSmoother(int windowSize, int dimension) {
      this.windowSize = windowSize;
      this.dimension = dimension;

      descriptiveStatistics = new DescriptiveStatistics[dimension];

      for (int i = 0; i < descriptiveStatistics.length; i++) {
         descriptiveStatistics[i] = new DescriptiveStatistics();
         descriptiveStatistics[i].setWindowSize(windowSize);
      }
   }

   @Override
   public double smooth(double value) {
      descriptiveStatistics[0].addValue(value);
      final double mean = descriptiveStatistics[0].getMean();
      return mean;
   }

   @Override
   public double[] smooth(double... values) {
      double[] smoothedValues = new double[dimension];
      for (int i = 0; i < smoothedValues.length; i++) {
         final double currentValue = values[i];
         descriptiveStatistics[i].addValue(currentValue);
         smoothedValues[i] = descriptiveStatistics[i].getMean();
      }
      return smoothedValues;
   }

}
