package de.cb.ma.core.widgets.analysis.midi;

import javafx.scene.control.Button;

public class MIDIButton extends Button {

   public MIDIButton(String text, int row, int column) {
      super(text);

      this.row = row;
      this.column = column;
   }

   private int row;
   private int column;

   public int getRow() {
      return row;
   }

   public void setRow(final int row) {
      this.row = row;
   }

   public int getColumn() {
      return column;
   }

   public void setColumn(final int column) {
      this.column = column;
   }
}
