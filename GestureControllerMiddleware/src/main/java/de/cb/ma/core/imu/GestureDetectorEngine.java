package de.cb.ma.core.imu;

import de.cb.ma.core.database.InMemoryGestureMapping;
import de.cb.ma.core.imu.motion.HandSign;
import de.cb.ma.core.imu.motion.HandSignDetector;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.util.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

import com.fastdtw.util.Distances;

public class GestureDetectorEngine implements InertialDataReceiver {

   public static final double HIGH_TRESH_HOLD = 10000; // when gesture starts
   public static final double LOW_TRESH_HOLD = 1000; // when gesture ends
   public static final int MIN_SIZE = 65; // when gesture ends
   public static final int MAX_SIZE = 200; // when gesture ends
   private static final Double DISTANCE_TRESHOLD = 60000.00;

   private boolean isDetectionStarted;
   private boolean detectionOnX;
   private boolean detectionOnY;
   private boolean detectionOnZ;

   private HandSignDetector rightHandPostureDetector;

   private HandSign.SIGN rightHandCurrentPosture;
   private HandSign.SIGN latestRightHandPosture;

   private long rightHandCaptureCurrentPassedTime;

   List<Pair<Double, List<Double>>> capturedGestureFrequencies = new ArrayList<>(MAX_SIZE);

   private Queue<Double> capturedDistances = new PriorityQueue<>();

   private InMemoryGestureMapping inGestureMemoryMapping;
   private GestureDetectorEngineCallback callback;

   private GestureComparatorAlgo gestureComparator;

   /**
    * @param callback
    */
   public GestureDetectorEngine(InMemoryGestureMapping inGestureMemoryMapping, final GestureDetectorEngineCallback callback) {
      this.inGestureMemoryMapping = inGestureMemoryMapping;
      this.callback = callback;

      this.rightHandPostureDetector = new HandSignDetector(0.3, (posture, binaryPattern) -> {
         callback.onPostureDetected(posture, binaryPattern);
         rightHandCurrentPosture = posture;
      });

      gestureComparator = new DynamicTimeWarpComparator(Distances.EUCLIDEAN_DISTANCE, DynamicTimeWarpComparator.MODE.DTW);
   }

   @Override
   public void handleError() {
      throw new UnsupportedOperationException("not implemented yet.");
   }

   @Override
   public void handleUpdate(final long processedTimeInMilliSeconds) {
      detectGesture(processedTimeInMilliSeconds);
      this.rightHandPostureDetector.updateValues(rightHandRichImuDataFacade.getFlexAngles());
   }

   private void detectGesture(final long processedTimeInMilliSeconds) {
      double varianceX = rightHandRichImuDataFacade.getAccelerationVariances()[0];
      double varianceY = rightHandRichImuDataFacade.getAccelerationVariances()[1];
      double varianceZ = rightHandRichImuDataFacade.getAccelerationVariances()[2];

      double x = rightHandRichImuDataFacade.getAccelerationSmoothed()[0];
      double y = rightHandRichImuDataFacade.getAccelerationSmoothed()[1];
      double z = rightHandRichImuDataFacade.getAccelerationSmoothed()[2];

      if (!isDetectionStarted) {
         if (varianceX > HIGH_TRESH_HOLD) {
            detectionOnX = true;
            onRightHandGestureStart(x, y, z, varianceX, varianceY, varianceZ, processedTimeInMilliSeconds);
         } else if (varianceY > HIGH_TRESH_HOLD) {
            detectionOnY = true;
            onRightHandGestureStart(x, y, z, varianceX, varianceY, varianceZ, processedTimeInMilliSeconds);
         } else if (varianceZ > HIGH_TRESH_HOLD) {
            detectionOnZ = true;
            onRightHandGestureStart(x, y, z, varianceX, varianceY, varianceZ, processedTimeInMilliSeconds);
         }
      }

      if (isDetectionStarted) {
         rightHandCaptureCurrentPassedTime = processedTimeInMilliSeconds;
         captureValues(rightHandCaptureCurrentPassedTime, x, y, z);

         // check if min size for a gesture has been reached this could lead to
         if (capturedGestureFrequencies.size() < MIN_SIZE) {
            // collect more data
            return;
         }

         if (capturedGestureFrequencies.size() > MAX_SIZE) {
            // TODO clear old gesture
         }

         if (varianceX < LOW_TRESH_HOLD && detectionOnX) {
            onRightHandGestureEnd(varianceX, varianceY, varianceZ);
            detectionOnX = false;
         } else if (varianceY < LOW_TRESH_HOLD && detectionOnY) {
            onRightHandGestureEnd(varianceX, varianceY, varianceZ);
            detectionOnY = false;
         } else if (varianceZ < LOW_TRESH_HOLD && detectionOnZ) {
            onRightHandGestureEnd(varianceX, varianceY, varianceZ);
            detectionOnZ = false;
         }

      }

   }

   private void onRightHandGestureEnd(final double varianceX, final double varianceY, final double varianceZ) {
      this.callback.onDetectionEnd(varianceX, varianceY, varianceZ);
      this.callback.onAnyGestureCaptured(this.latestRightHandPosture, this.capturedGestureFrequencies);
      recognizePattern();

      this.capturedDistances.clear();

      // reset for new gesture
      this.isDetectionStarted = false;
      // reset time
      //      rightHandCaptureCurrentPassedTime = System.currentTimeMillis();
   }

   private void clearOldCapturedFrequencies() {
      this.capturedGestureFrequencies.clear();
   }

   private void onRightHandGestureStart(final double x, final double y, final double z, final double varianceX, final double varianceY, final double varianceZ,
         final long processedTimeInMilliSeconds) {
      // clear old values at new gesture start because of currency problems in the view
      clearOldCapturedFrequencies();

      rightHandCaptureStartedTime = System.currentTimeMillis();
      rightHandCaptureCurrentPassedTime = processedTimeInMilliSeconds;

      this.latestRightHandPosture = rightHandCurrentPosture;
      this.isDetectionStarted = true;
      this.callback.onDetectionStart(varianceX, varianceY, varianceZ);
      captureValues(rightHandCaptureCurrentPassedTime, x, y, z);
      //      System.out.println("x: " + detectionOnX + " y: " + detectionOnY + " z: " + detectionOnZ);
   }

   private void recognizePattern() {
      final List<GestureModel> trainedGestures = inGestureMemoryMapping.getGesturesByHandSign(latestRightHandPosture);

      if (trainedGestures == null) {
         return;
      }

      GestureModel closestGesture = null;
      Double closestDistance = Double.MAX_VALUE;

      System.out.println(latestRightHandPosture);

      for (GestureModel currentGesture : trainedGestures) {
         final List<Pair<Double, List<Double>>> observedGestureFrequencies = currentGesture.getAccelerations()
               .getAmplitudes();

         final double distance = gestureComparator.compare(this.capturedGestureFrequencies, observedGestureFrequencies);
         if (distance < closestDistance) {
            closestGesture = currentGesture;
            closestDistance = distance;
         }
         capturedDistances.add(distance);
      }

      if (closestGesture != null) {
         System.out.println("### GestureDetector: found clostest distance: " + closestDistance);
         if (closestDistance <= DISTANCE_TRESHOLD) {
            callback.onGestureRecognized(this.latestRightHandPosture, closestGesture, this.capturedGestureFrequencies, closestDistance);
         }
      }
   }

   private void captureValues(final double time, final double x, final double y, final double z) {
      final ArrayList<Double> doubles = new ArrayList<>(3);
      doubles.add(x);
      doubles.add(y);
      doubles.add(z);

      final Pair<Double, List<Double>> pair = new Pair<>(time, doubles);
      this.capturedGestureFrequencies.add(pair);
   }

   private long rightHandCaptureStartedTime;

   public void isActive(final boolean isActive) {
      this.isActive = isActive;
   }

   @Override
   public void setInertialDataInterface(final InertialDataInterface inertialDataInterface) {
      this.rightHandRichImuDataFacade = inertialDataInterface;
   }

   private boolean isActive;
   private InertialDataInterface rightHandRichImuDataFacade;
}
