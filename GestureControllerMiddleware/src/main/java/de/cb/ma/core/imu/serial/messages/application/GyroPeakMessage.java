package de.cb.ma.core.imu.serial.messages.application;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class GyroPeakMessage extends SerialMessage {

   public enum GYRO_PEAK_AXIS {
      NONE,
      GYRO_PEAK_X,
      GYRO_PEAK_Y,
      GYRO_PEAK_Z,
   }

   public GyroPeakMessage(boolean xPeak, boolean yPeak, boolean zPeak) {
      this.xPeak = xPeak;
      this.yPeak = yPeak;
      this.zPeak = zPeak;
   }

   private final boolean xPeak;
   private final boolean yPeak;
   private final boolean zPeak;

   public boolean isxPeak() {
      return xPeak;
   }

   public boolean isyPeak() {
      return yPeak;
   }

   public boolean iszPeak() {
      return zPeak;
   }

   @Override
   public String toString() {
      return "GyroPeakMessage [" +
            "xPeak=" + xPeak +
            ", yPeak=" + yPeak +
            ", zPeak=" + zPeak +
            ']';
   }
}
