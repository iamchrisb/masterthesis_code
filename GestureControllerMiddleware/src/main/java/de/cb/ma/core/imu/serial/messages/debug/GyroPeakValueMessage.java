package de.cb.ma.core.imu.serial.messages.debug;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class GyroPeakValueMessage extends SerialMessage {

   public GyroPeakValueMessage(final String xPeakValue, final String yPeakValue, final String zPeakValue) {
      this.xPeakValue = xPeakValue;
      this.yPeakValue = yPeakValue;
      this.zPeakValue = zPeakValue;
   }

   private String xPeakValue;
   private String yPeakValue;
   private String zPeakValue;

   public String getxPeakValue() {
      return xPeakValue;
   }

   public String getyPeakValue() {
      return yPeakValue;
   }

   public String getzPeakValue() {
      return zPeakValue;
   }

   @Override
   public String toString() {
      return "GyroPeakValueMessage [" +
            "xPeakValue='" + xPeakValue +
            ", yPeakValue='" + yPeakValue +
            ", zPeakValue='" + zPeakValue +
            ']';
   }
}
