package de.cb.ma.core.database.dao.fileimpl;

import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.jackson.DAWCommands;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DAWCommandDaoFileImpl implements DAWCommandDao {

   private ObjectMapper objectMapper;

   private DAWCommands dawCommands;
   private File src;

   public DAWCommandDaoFileImpl(String filePath, ObjectMapper objectMapper) {
      this.objectMapper = objectMapper;
      try {
         src = new File(filePath);
         dawCommands = objectMapper.readValue(src, DAWCommands.class);
      } catch (JsonProcessingException | FileNotFoundException e) {
         e.printStackTrace();
         createFile();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   private void createFile() {
      dawCommands = new DAWCommands(new HashMap<String, DAWCommand>());
      try {
         objectMapper.writeValue(src, dawCommands);
      } catch (IOException e1) {
         e1.printStackTrace();
      }
   }

   @Override
   public List<DAWCommand> findAll() {
      return new LinkedList<>(dawCommands.getDawCommands()
            .values());
   }

   @Override
   public DAWCommand findById(final String id) {
      return dawCommands.getDawCommands()
            .get(id);
   }

   @Override
   public DAWCommand create(final DAWCommand dawCommand) {
      dawCommands.getDawCommands()
            .put(dawCommand.getId(), dawCommand);
      return updateFile(dawCommand);
   }

   private DAWCommand updateFile(DAWCommand dawCommand) {
      try {
         objectMapper.writeValue(src, dawCommands);
      } catch (IOException e) {
         e.printStackTrace();
         return null;
      }
      return dawCommand;
   }

   @Override
   public DAWCommand update(final DAWCommand dawCommand) {
      dawCommands.getDawCommands()
            .put(dawCommand.getId(), dawCommand);
      return updateFile(dawCommand);
   }

   @Override
   public DAWCommand delete(final DAWCommand dawCommand) {
      dawCommands.getDawCommands()
            .remove(dawCommand.getId());
      return updateFile(dawCommand);
   }

}
