package de.cb.ma.core.imu.preprocessing;

import org.tools4j.meanvar.MeanVarianceSlidingWindow;

public class WelfordAmplitudeStochastic implements AmplitudeStochastic {

   private int windowSize;
   private int dimension;

   private MeanVarianceSlidingWindow[] meanVarianceSlidingWindows;

   private double[] currentMeans;
   private double[] currentVariances;
   private double[] currentStdDevs;

   public WelfordAmplitudeStochastic(int windowSize) {
      this(windowSize, 1);
   }

   public WelfordAmplitudeStochastic(int windowSize, int dimension) {
      this.windowSize = windowSize;
      this.dimension = dimension;

      meanVarianceSlidingWindows = new MeanVarianceSlidingWindow[dimension];

      for (int i = 0; i < meanVarianceSlidingWindows.length; i++) {
         meanVarianceSlidingWindows[i] = new MeanVarianceSlidingWindow(this.windowSize);
      }

      this.currentMeans = new double[dimension];
      this.currentVariances = new double[dimension];
      this.currentStdDevs = new double[dimension];
   }

   @Override
   public void update(double amplitude) {
      meanVarianceSlidingWindows[0].update(amplitude);

      this.currentMeans[0] = meanVarianceSlidingWindows[0].getMean();
      this.currentVariances[0] = meanVarianceSlidingWindows[0].getVariance();
      this.currentStdDevs[0] = meanVarianceSlidingWindows[0].getStdDev();
   }

   @Override
   public void update(double... amplitudes) {
      if (amplitudes.length != dimension) {
         throw new IllegalArgumentException("dimension of parameter and field must be equal!");
      }

      for (int i = 0; i < amplitudes.length; i++) {
         double amplitude = amplitudes[i];
         meanVarianceSlidingWindows[i].update(amplitude);

         this.currentMeans[i] = meanVarianceSlidingWindows[i].getMean();
         this.currentVariances[i] = meanVarianceSlidingWindows[i].getVariance();
         this.currentStdDevs[i] = meanVarianceSlidingWindows[i].getStdDev();
      }
   }

   @Override
   public double getMean(int index) {
      return meanVarianceSlidingWindows[index].getMean();
   }

   @Override
   public double getVariance(int index) {
      return meanVarianceSlidingWindows[index].getVariance();
   }

   @Override
   public double getStandardDeviation(int index) {
      return meanVarianceSlidingWindows[index].getStdDev();
   }

   @Override
   public double[] getMeans() {
      return currentMeans;
   }

   @Override
   public double[] getVariances() {
      return currentVariances;
   }

   @Override
   public double[] getStandardDeviations() {
      return currentStdDevs;
   }
}
