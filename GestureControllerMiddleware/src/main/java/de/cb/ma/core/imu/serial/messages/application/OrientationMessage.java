package de.cb.ma.core.imu.serial.messages.application;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class OrientationMessage extends SerialMessage {

   public OrientationMessage(final String yawValue, final String pitchValue, final String rollValue) {
      this.yawValue = yawValue;
      this.pitchValue = pitchValue;
      this.rollValue = rollValue;
   }

   private String yawValue;
   private String pitchValue;
   private String rollValue;

   public String getYawValue() {
      return yawValue;
   }

   public String getPitchValue() {
      return pitchValue;
   }

   public String getRollValue() {
      return rollValue;
   }

   @Override
   public String toString() {
      return "OrientationMessage [" +
            "yawValue='" + yawValue +
            ", pitchValue='" + pitchValue +
            ", rollValue='" + rollValue +
            ']';
   }
}
