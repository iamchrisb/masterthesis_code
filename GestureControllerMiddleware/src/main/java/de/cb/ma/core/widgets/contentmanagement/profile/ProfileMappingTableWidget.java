package de.cb.ma.core.widgets.contentmanagement.profile;

import de.cb.ma.core.database.dao.DAWCommandDao;
import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.database.model.Profile;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ProfileMappingTableWidget extends Pane {

   public interface ButtonCallback {
      void onSelected(int index, ProfileMappingModel dawCommand);
   }

   public interface PlayCallback extends ButtonCallback {
   }

   public interface DeleteCallback extends ButtonCallback {
   }

   public ProfileMappingTableWidget(GestureModelDao gestureModelDao, DAWCommandDao dawCommandDao, PlayCallback playCallback, DeleteCallback deleteCallback) {
      this.gestureModelDao = gestureModelDao;
      this.dawCommandDao = dawCommandDao;

      String title = "All profile mappings";
      final Label label = new Label(title);

      tableView = new TableView();

      TableColumn commandColumn = new TableColumn("Command");
      commandColumn.setMinWidth(200);
      commandColumn.setCellValueFactory(new PropertyValueFactory<ProfileMappingModel, String>("dawCommandName"));

      TableColumn idColumn = new TableColumn("ID");
      idColumn.setCellValueFactory(new PropertyValueFactory<ProfileMappingModel, String>("dawCommandId"));
      idColumn.setMinWidth(200);

      TableColumn gestureNameColumn = new TableColumn("Gesture name");
      gestureNameColumn.setCellValueFactory(new PropertyValueFactory<ProfileMappingModel, String>("gestureName"));
      gestureNameColumn.setMinWidth(200);

      TableColumn sendCol = new TableColumn<>("Test MIDI");
      sendCol.setSortable(false);
      sendCol.setMinWidth(100);

      TableColumn deleteColumn = new TableColumn<>("Actions");
      deleteColumn.setSortable(false);
      deleteColumn.setMinWidth(100);

      sendCol.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<ProfileMappingModel, Boolean>,
                  ObservableValue<Boolean>>() {

               @Override
               public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<ProfileMappingModel, Boolean> p) {
                  return new SimpleBooleanProperty(p.getValue() != null);
               }
            });

      sendCol.setCellFactory(
            p -> new ButtonCell("Play", playCallback));

      deleteColumn.setCellValueFactory(
            new Callback<TableColumn.CellDataFeatures<ProfileMappingModel, Boolean>,
                  ObservableValue<Boolean>>() {

               @Override
               public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<ProfileMappingModel, Boolean> p) {
                  return new SimpleBooleanProperty(p.getValue() != null);
               }
            });

      deleteColumn.setCellFactory(
            p -> new ButtonCell("Remove", deleteCallback));

      tableView.getColumns()
            .addAll(sendCol, commandColumn, gestureNameColumn, deleteColumn);

      VBox container = new VBox();

      container.getChildren()
            .addAll(label, tableView);

      this.getChildren()
            .add(container);
   }

   public void refresh(Profile selected) {
      clearTableView();
      final List<ProfileMappingModel> mappingModels = collectDAWMappingModels(selected);
      ObservableList<ProfileMappingModel> profileMappingModels = FXCollections.observableArrayList(mappingModels);
      tableView.setItems(profileMappingModels);
   }

   private void clearTableView() {
      tableView.getItems()
            .clear();
   }

   public List<ProfileMappingModel> collectDAWMappingModels(final Profile selectedProfile) {
      List<ProfileMappingModel> profileMappingModels = new LinkedList<>();

      final Set<Map.Entry<String, String>> entries = selectedProfile.getGestureMapping()
            .entrySet();

      for (Map.Entry<String, String> entry : entries) {
         final GestureModel gestureModel = gestureModelDao.findById(entry.getKey());
         final DAWCommand dawCommand = dawCommandDao.findById(entry.getValue());

         ProfileMappingModel dawCommandMappingModel = new ProfileMappingModel(gestureModel, dawCommand);
         profileMappingModels.add(dawCommandMappingModel);
      }

      return profileMappingModels;
   }

   private final TableView tableView;
   private ObservableList<ProfileMappingModel> model;

   //Define the button cell
   private class ButtonCell extends TableCell<ProfileMappingModel, Boolean> {
      final Button selectButton = new Button();
      final Button saveButton = new Button();

      HBox container = new HBox(10);

      ButtonCell(String text, ButtonCallback callback) {

         // add later
         saveButton.setText("Save (Draft)");

         selectButton.setText(text);
         selectButton.setOnAction(t -> {
            final ProfileMappingModel dawCommand = (ProfileMappingModel) tableView.getItems()
                  .get(getIndex());
            callback.onSelected(getIndex(), dawCommand);
         });

         container.getChildren()
               .addAll(selectButton);
      }

      //Display button if the row is not empty
      @Override
      protected void updateItem(Boolean t, boolean empty) {
         super.updateItem(t, empty);
         if (!empty) {
            setGraphic(container);
         }
      }
   }

   private final DAWCommandDao dawCommandDao;
   private final GestureModelDao gestureModelDao;
}
