package de.cb.ma.core.widgets.contentmanagement.profile;

import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.GestureModel;

public class ProfileMappingModel {

   private String dawCommandName;
   private String dawCommandId;
   private String gestureName;
   private String gestureId;
   private int midiDataByte1;
   private int midiDataByte2;
   private int midiChannel;
   private String midiMessageType;

   public ProfileMappingModel(final String dawCommandName, final String dawCommandId, final String gestureName, final String gestureId, final int midiDataByte1, final int midiDataByte2,
         final int midiChannel,
         final String midiMessageType) {
      this.dawCommandName = dawCommandName;
      this.dawCommandId = dawCommandId;
      this.gestureName = gestureName;
      this.gestureId = gestureId;
      this.midiDataByte1 = midiDataByte1;
      this.midiDataByte2 = midiDataByte2;
      this.midiChannel = midiChannel;
      this.midiMessageType = midiMessageType;
   }

   public ProfileMappingModel(GestureModel gestureModel, DAWCommand dawCommand) {
      this.dawCommandName = dawCommand.getCommand();
      this.dawCommandId = dawCommand.getId();
      this.gestureName = gestureModel.getName();
      this.gestureId = gestureModel.getId();
      this.midiDataByte1 = dawCommand.getMidiMessageParameter()
            .getDatabyte1();
      this.midiDataByte2 = dawCommand.getMidiMessageParameter()
            .getDatabyte2();
      this.midiChannel = dawCommand.getMidiMessageParameter()
            .getMidiChannel();
      this.midiMessageType = midiMessageType;
   }

   public String getDawCommandName() {
      return dawCommandName;
   }

   public void setDawCommandName(final String dawCommandName) {
      this.dawCommandName = dawCommandName;
   }

   public String getDawCommandId() {
      return dawCommandId;
   }

   public void setDawCommandId(final String dawCommandId) {
      this.dawCommandId = dawCommandId;
   }

   public String getGestureName() {
      return gestureName;
   }

   public void setGestureName(final String gestureName) {
      this.gestureName = gestureName;
   }

   public String getGestureId() {
      return gestureId;
   }

   public void setGestureId(final String gestureId) {
      this.gestureId = gestureId;
   }

   public String getMidiMessageType() {
      return midiMessageType;
   }

   public void setMidiMessageType(final String midiMessageType) {
      this.midiMessageType = midiMessageType;
   }

   public int getMidiDataByte1() {
      return midiDataByte1;
   }

   public void setMidiDataByte1(final int midiDataByte1) {
      this.midiDataByte1 = midiDataByte1;
   }

   public int getMidiDataByte2() {
      return midiDataByte2;
   }

   public void setMidiDataByte2(final int midiDataByte2) {
      this.midiDataByte2 = midiDataByte2;
   }

   public int getMidiChannel() {
      return midiChannel;
   }

   public void setMidiChannel(final int midiChannel) {
      this.midiChannel = midiChannel;
   }
}
