package de.cb.ma.core.widgets.contentmanagement;

import de.cb.ma.core.imu.motion.HandSign;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class GestureSaveWidget extends Pane {

   private ObservableList<HandSign.SIGN> HandSignObsList;
   private ComboBox selectHandSign;

   private final TextField nameTF;
   private final Button saveBtn;

   public GestureSaveWidget() {
      final Label title = new Label("enter gesture information");

      HBox nameContainer = new HBox(20);
      nameTF = new TextField();

      nameContainer.getChildren()
            .addAll(new Label("name"), nameTF);

      VBox container = new VBox(20);

      selectHandSign = new ComboBox<>(FXCollections.observableArrayList(HandSign.SIGN.values()));

      selectHandSign.getSelectionModel()
            .selectFirst();

      saveBtn = new Button("save");
      container.getChildren()
            .addAll(title, nameContainer, selectHandSign, saveBtn);

      this.getChildren()
            .addAll(container);
   }

   public void setButtonAction(EventHandler<ActionEvent> eventEventHandler) {
      saveBtn.setOnAction(eventEventHandler);
   }

   public String getGestureName() {
      return nameTF.getText();
   }

   public HandSign.SIGN getIdentifier() {
      return (HandSign.SIGN) selectHandSign.getSelectionModel()
            .getSelectedItem();
   }

}
