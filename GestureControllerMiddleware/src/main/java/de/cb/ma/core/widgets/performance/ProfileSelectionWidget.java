package de.cb.ma.core.widgets.performance;

import de.cb.ma.core.database.dao.ProfileDao;
import de.cb.ma.core.database.model.Profile;
import javafx.beans.value.ChangeListener;
import javafx.collections.FXCollections;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.List;

public class ProfileSelectionWidget extends Pane {

   public ProfileSelectionWidget(ProfileDao profileDao, ChangeListener<Profile> changeListener) {
      final VBox container = new VBox(20);

      final List<Profile> profiles = profileDao.findAll();

      profileSelection = new ComboBox<>(FXCollections.observableArrayList(profiles));

      profileSelection.valueProperty()
            .addListener(changeListener);

      if (profiles != null || !profiles.isEmpty()) {
         profileSelection.getSelectionModel()
               .selectFirst();
      }

      container.getChildren()
            .addAll(new Label("Choose profile"), profileSelection);

      this.getChildren()
            .addAll(container);

   }

   private final ComboBox<Profile> profileSelection;
}
