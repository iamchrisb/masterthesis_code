package de.cb.ma.core.widgets.analysis.charts;

import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.Pane;

public class DotPlotWidget extends Pane implements PlotWidget {

   private static final int xAxisRange[] = new int[] { 900, -900 };
   private static final int yAxisRange[] = new int[] { 900, -900 };

   public DotPlotWidget(final String legendName, final String chartTitle, final String xAxisLabel, final String yAxisLabel) {
      this(new String[] { legendName }, chartTitle, xAxisLabel, yAxisLabel);
   }

   @Override
   public void setWidth(double value) {
      this.setPrefWidth(value);
      this.setPrefHeight(value);

      dotChart.setPrefWidth(value);
      dotChart.setPrefHeight(value);

      this.setMinWidth(value);
      this.setMinHeight(value);

      dotChart.setMinWidth(value);
      dotChart.setMinHeight(value);

      this.setMaxWidth(value);
      this.setMaxHeight(value);

      dotChart.setMaxWidth(value);
      dotChart.setMaxHeight(value);
   }

   @Override
   public void setHeight(final double height) {

   }

   public DotPlotWidget(final String[] legendNames, final String chartTitle, final String xAxisLabel, final String yAxisLabel) {
      this.legendNames = legendNames;

      xAxis = new NumberAxis();
      yAxis = new NumberAxis();

      dotChart = new ScatterChart<>(xAxis, yAxis);

      xAxis.setLabel(xAxisLabel);
      xAxis.setUpperBound(xAxisRange[0]);
      xAxis.setLowerBound(xAxisRange[1]);

      yAxis.setLabel(yAxisLabel);
      yAxis.setUpperBound(yAxisRange[0]);
      yAxis.setLowerBound(yAxisRange[1]);

      yAxis.setAnimated(false);
      xAxis.setAnimated(false);

      dotChart.setTitle(chartTitle);

      for (String legendName : legendNames) {
         XYChart.Series dataSerie = new XYChart.Series();
         dataSerie.setName(legendName);

         dotChart.getData()
               .addAll(dataSerie);

         dataSerie.getData()
               .add(new XYChart.Data(0, 0));
      }

      this.getChildren()
            .add(dotChart);
   }

   public void addData(Number yValue, Number xValue) {
      int chartIndex = 0;
      Platform.runLater(() -> {
         addSingleData(new XYChart.Data<>(yValue, xValue), legendNames[chartIndex] + ": " + xValue, 0);
      });
   }

   public void addData(int chartIndex, Number yValue, Number xValue) {
      Platform.runLater(new Runnable() {
         @Override
         public void run() {
            addSingleData(new XYChart.Data<>(yValue, xValue), legendNames[chartIndex] + ": " + xValue, chartIndex);
         }
      });
   }

   private void addSingleData(final XYChart.Data<Number, Number> numberNumberData, final String value, final int index) {
      final ObservableList<XYChart.Series<Number, Number>> lineChartData = dotChart.getData();
      final XYChart.Series<Number, Number> dataSeries = lineChartData.get(index);

      final int currentDataSize = dataSeries
            .getData()
            .size();

      dataSeries.setName(value);

      dataSeries
            .getData()
            .add(numberNumberData);
   }

   public void addData(int[][] scatterData) {
      Platform.runLater(() -> {
         final ObservableList<XYChart.Series<Number, Number>> dotChartData = dotChart.getData();

         for (int i = 0; i < dotChartData.size(); i++) {
            final XYChart.Series<Number, Number> dataSeries = dotChartData.get(i);

            dataSeries.setName(legendNames[i]);

            //               .addAll(new XYChart.Data<>(yValue, xValue.get(i)));
            dataSeries
                  .getData()
                  .add(new XYChart.Data<>(scatterData[i][0], scatterData[i][1]));
         }
         //
      });
   }

   @Override
   public void updateRangeSlider(final double from, final double to) {
      // no function
   }

   @Override
   public void clearData() {
      final ObservableList<XYChart.Series<Number, Number>> data = dotChart.getData();

      for (XYChart.Series<Number, Number> datum : data) {
         datum.getData()
               .clear();
      }
   }

   private final NumberAxis xAxis;
   private final NumberAxis yAxis;
   private final ScatterChart<Number, Number> dotChart;
   private String[] legendNames;

   @Override
   public ObservableList<XYChart.Series<Number, Number>> getData() {
      return dotChart.getData();
   }

   @Override
   public void shrinkData(final Number from, final Number to) {
      final ObservableList<XYChart.Series<Number, Number>> dotChartData = dotChart.getData();

      clearData();
      dotChartData.clear();

      for (int i = 0; i < legendNames.length; i++) {
         String legendName = legendNames[i];

         XYChart.Series currentDateSeries = new XYChart.Series();
         currentDateSeries.setName(legendName);

         dotChart.getData()
               .addAll(currentDateSeries);

         for (int j = from.intValue(); j < to.intValue(); j++) {
            final XYChart.Data<Number, Number> numberData = dotChartData.get(i)
                  .getData()
                  .get(j);
            currentDateSeries.getData()
                  .add(numberData);
         }
      }

      for (XYChart.Series<Number, Number> dotChartDatum : dotChartData) {
         final ObservableList<XYChart.Data<Number, Number>> currentData = dotChartDatum.getData();
         //         while(currentData.iterator().hasNext()) {
         //
         //         };
         currentData.size();
      }
   }

   @Override
   public void setRangeSaveVisibility(final boolean value) {

   }
}
