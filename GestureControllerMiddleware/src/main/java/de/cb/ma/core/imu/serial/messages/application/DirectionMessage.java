package de.cb.ma.core.imu.serial.messages.application;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class DirectionMessage extends SerialMessage {

   public static Integer FORWARD = 0;
   public static Integer BACKWARD = 1;
   public static Integer LEFT = 2;
   public static Integer RIGHT = 3;
   public static Integer UP = 4;
   public static Integer DOWN = 5;

   private Integer identifier;

   public DirectionMessage(Integer identifier) {
      this.identifier = identifier;
   }

   public Integer getIdentifier() {
      return identifier;
   }
}
