package de.cb.ma.core.widgets.analysis;

import de.cb.ma.core.imu.motion.HandSign;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

public class FingerDisplayWidget extends Pane {

   Label indexFingerLabel = new Label("Index:  ");
   Label ringFingerLabel = new Label("Ring:    ");
   Label middleFingerLabel = new Label("Middle: ");
   Label pinkyFingerLabel = new Label("Pinky:  ");
   Label thumbLabel = new Label("Thumb: ");

   Label currentHandSignLabel = new Label("Current Handsign: ");
   Label currentHandSignValue = new Label();

   final Slider thumbLevel = new Slider(0, 1, 1);
   final Slider indexFingerLevel = new Slider(0, 1, 1);
   final Slider ringFingerLevel = new Slider(0, 1, 1);
   final Slider middleFingerLevel = new Slider(0, 1, 1);
   final Slider pinkyLevel = new Slider(0, 1, 1);

   public FingerDisplayWidget(final String title) {
      VBox container = new VBox();
      container.setSpacing(10);

      HBox hboxThumb = new HBox();
      HBox hboxIndexFinger = new HBox();
      HBox hboxMiddleFinger = new HBox();
      HBox hboxRingFinger = new HBox();
      HBox hboxPinky = new HBox();

      HBox currentHandSignContainer = new HBox();

      final Label titelLabel = new Label(title);

      container.getChildren()
            .addAll(titelLabel, hboxThumb, hboxIndexFinger, hboxMiddleFinger, hboxRingFinger, hboxPinky, currentHandSignContainer);

      hboxThumb.getChildren()
            .addAll(thumbLabel, thumbLevel);

      hboxIndexFinger.getChildren()
            .addAll(indexFingerLabel, indexFingerLevel);

      hboxMiddleFinger.getChildren()
            .addAll(middleFingerLabel, middleFingerLevel);

      hboxRingFinger.getChildren()
            .addAll(ringFingerLabel, ringFingerLevel);

      hboxPinky.getChildren()
            .addAll(pinkyFingerLabel, pinkyLevel);

      currentHandSignContainer.getChildren()
            .addAll(currentHandSignLabel, currentHandSignValue);

      hboxThumb.setSpacing(10);
      hboxIndexFinger.setSpacing(10);
      hboxMiddleFinger.setSpacing(10);
      hboxRingFinger.setSpacing(10);
      hboxPinky.setSpacing(10);
      currentHandSignContainer.setSpacing(10);

      showHandSign(HandSign.SIGN.UNDETECTED.name());

      this.getChildren()
            .add(container);
   }

   public void changeThumbSliderValue(final double v) {
      this.thumbLevel.adjustValue(v);
   }

   public void changeIndexSliderValue(final double v) {
      this.indexFingerLevel.adjustValue(v);
   }

   public void changeMiddleSliderValue(final double v) {
      this.middleFingerLevel.adjustValue(v);
   }

   public void changeRingSliderValue(final double v) {
      this.ringFingerLevel.adjustValue(v);
   }

   public void changePinkySliderValue(final double v) {
      this.pinkyLevel.adjustValue(v);
   }

   public void changeAllFingerSliderValues(final double thumbValue, final double indexValue, final double middleValue, final double ringValue, final double pinkyValue) {
      Platform.runLater(() -> {
         changeThumbSliderValue(mapValue(thumbValue));
         changeIndexSliderValue(mapValue(indexValue));
         changeMiddleSliderValue(mapValue(middleValue));
         changeRingSliderValue(mapValue(ringValue));
         changePinkySliderValue(mapValue(pinkyValue));
      });
   }

   private double mapValue(final double angle) {
      if (angle > 1) {
         return angle / 100;
      }
      if (angle < 0) {
         return 0;
      }
      return angle;
   }

   public void changeAllFingerSliderValues(double... angles) {
      changeAllFingerSliderValues(angles[0], angles[1], angles[2], angles[3], angles[4]);
   }

   public void showHandSign(final String currentHandSign) {
      Platform.runLater(() -> currentHandSignValue.setText(currentHandSign));
   }
}
