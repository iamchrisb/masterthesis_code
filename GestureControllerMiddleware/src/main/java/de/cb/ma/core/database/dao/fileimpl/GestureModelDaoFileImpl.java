package de.cb.ma.core.database.dao.fileimpl;

import de.cb.ma.core.database.dao.GestureModelDao;
import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.database.model.jackson.GestureModels;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GestureModelDaoFileImpl implements GestureModelDao {

   private ObjectMapper objectMapper;

   private File src;

   private GestureModels gestureModels;

   public GestureModelDaoFileImpl(String filePath, ObjectMapper objectMapper) {
      this.objectMapper = objectMapper;

      try {
         src = new File(filePath);
         gestureModels = objectMapper.readValue(src, GestureModels.class);
      } catch (JsonProcessingException | FileNotFoundException e) {
         e.printStackTrace();
         createFile();
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   private void createFile() {
      gestureModels = new GestureModels(new HashMap<String, GestureModel>());
      try {
         objectMapper.writeValue(src, gestureModels);
      } catch (IOException e1) {
         e1.printStackTrace();
      }
   }

   @Override
   public List<GestureModel> findAll() {
      return new LinkedList<>(gestureModels.getGestureModels()
            .values());
   }

   @Override
   public GestureModel findById(final String id) {
      return gestureModels.getGestureModels()
            .get(id);
   }

   @Override
   public GestureModel create(final GestureModel gestureModel) {
      gestureModels.getGestureModels()
            .put(gestureModel.getId(), gestureModel);
      return updateFile(gestureModel);
   }

   private GestureModel updateFile(GestureModel gestureModel) {
      try {
         objectMapper.writeValue(src, gestureModels);
      } catch (IOException e) {
         e.printStackTrace();
         return null;
      }
      return gestureModel;
   }

   @Override
   public GestureModel update(final GestureModel gestureModel) {
      gestureModels.getGestureModels()
            .put(gestureModel.getId(), gestureModel);
      return updateFile(gestureModel);
   }

   @Override
   public GestureModel delete(final GestureModel gestureModel) {
      gestureModels.getGestureModels()
            .remove(gestureModel.getId());
      return updateFile(gestureModel);
   }

}
