package de.cb.ma.core.imu.serial.messages.debug;

import de.cb.ma.core.imu.serial.messages.SerialMessage;

public class MagnetometerMessage extends SerialMessage {

   public MagnetometerMessage(final String xMagValue, final String yMaglValue, final String zMagValue) {
      this.xMagValue = xMagValue;
      this.yMagValue = yMaglValue;
      this.zMagValue = zMagValue;
   }

   private String xMagValue;
   private String yMagValue;
   private String zMagValue;

   public String getxMagValue() {
      return xMagValue;
   }

   public String getyMagValue() {
      return yMagValue;
   }

   public String getzMagValue() {
      return zMagValue;
   }

   @Override
   public String toString() {
      return "MagnetometerMessage [" +
            "xMagValue='" + xMagValue +
            ", yMagValue='" + yMagValue +
            ", zMagValue='" + zMagValue +
            ']';
   }
}
