package de.cb.ma.core.database.model.jackson;

import de.cb.ma.core.database.model.GestureModel;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GestureModels {

   Map<String, GestureModel> gestureModels;

   public GestureModels() {
      // JSON
   }

   public GestureModels(final Map<String, GestureModel> gestureModels) {
      this.gestureModels = gestureModels;
   }

   public Map<String, GestureModel> getGestureModels() {
      return gestureModels;
   }

   public void setGestureModels(final Map<String, GestureModel> gestureModels) {
      this.gestureModels = gestureModels;
   }
}
