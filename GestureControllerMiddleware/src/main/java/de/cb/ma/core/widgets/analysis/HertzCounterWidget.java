package de.cb.ma.core.widgets.analysis;

import de.cb.ma.util.Constants;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import java.util.Timer;
import java.util.TimerTask;

public class HertzCounterWidget extends Pane {

   private int tickFreqCounter;
   private Label hertzValue;

   Timer timer;

   public interface Callback {
      void onFrequencyCalculated(int passedSeconds, int ticks);
   }

   public HertzCounterWidget(Callback callback) {
      this.callback = callback;
      VBox panel = new VBox(20);

      HBox hertzCountContainer = new HBox(20);
      hertzValue = new Label();
      hertzCountContainer.getChildren()
            .addAll(new Label("Hertz: "), hertzValue);

      panel.getChildren()
            .addAll(new Label("Shows frequency"), hertzCountContainer);

      this.getChildren()
            .addAll(panel);

      timer = new Timer();
   }

   public HertzCounterWidget() {
      this(null);
   }

   /**
    * starts the counter
    */
   public void start() {
      timer.schedule(new TimerTask() {
         @Override
         public void run() {
            String counterAsString = Integer.toString(tickFreqCounter);

            if (callback != null) {
               callback.onFrequencyCalculated(internalSecondCounter, tickFreqCounter);
            }

            internalSecondCounter++;
            Platform.runLater(() -> hertzValue.setText(counterAsString));
            tickFreqCounter = 0;
         }
      }, 0, Constants.ONE_SECOND_PERIOD);
   }

   public void update() {
      tickFreqCounter++;
   }

   /**
    * resets hertz counter and restarts it
    */
   public void reset() {
      stop();
      start();
   }

   /**
    * kills the counter
    */
   public void stop() {
      internalSecondCounter = 0;
      timer.cancel();
   }

   private Callback callback;
   private int internalSecondCounter;
}
