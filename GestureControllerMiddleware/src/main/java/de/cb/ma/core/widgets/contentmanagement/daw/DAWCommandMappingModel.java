package de.cb.ma.core.widgets.contentmanagement.daw;

import de.cb.ma.core.database.model.DAWCommand;
import de.cb.ma.core.database.model.MidiMessageParameter;
import de.cb.ma.util.MidiHelper;

public class DAWCommandMappingModel {

   private String dawCommandName;
   private String dawCommandId;
   private int midiDataByte1;
   private int midiDataByte2;
   private int midiChannel;
   private String displayedMidiChannel;
   private MidiMessageParameter.MIDI_MESSAGE_TYPE midiMessageType;
   private final String displayDatabyte1Note;
   private final String displayDatabyte2Note;

   public DAWCommandMappingModel(final String dawCommandName, final String dawCommandId, final int midiDataByte1, final int midiDataByte2,
         final int midiChannel,
         final MidiMessageParameter.MIDI_MESSAGE_TYPE midiMessageType) {
      this.dawCommandName = dawCommandName;
      this.dawCommandId = dawCommandId;
      this.midiDataByte1 = midiDataByte1;
      this.midiDataByte2 = midiDataByte2;
      this.midiChannel = midiChannel;
      this.midiMessageType = midiMessageType;

      this.displayedMidiChannel = midiChannel + " (" + midiChannel + 1 + ")";
      this.displayDatabyte1Note = midiDataByte1 + " (" + MidiHelper.getNoteAsString(midiDataByte1) + ")";
      this.displayDatabyte2Note = midiDataByte2 + " (" + MidiHelper.getNoteAsString(midiDataByte2) + ")";
   }

   public DAWCommandMappingModel(DAWCommand dawCommand) {
      final int midiChannel = dawCommand.getMidiMessageParameter()
            .getMidiChannel();

      this.dawCommandName = dawCommand.getCommand();
      this.dawCommandId = dawCommand.getId();
      this.midiDataByte1 = dawCommand.getMidiMessageParameter()
            .getDatabyte1();
      this.midiDataByte2 = dawCommand.getMidiMessageParameter()
            .getDatabyte2();
      this.midiMessageType = dawCommand.getMidiMessageParameter()
            .getActionType();

      this.midiChannel = midiChannel;

      this.displayedMidiChannel = midiChannel + " (" + (midiChannel + 1) + ")";

      this.displayDatabyte1Note = midiDataByte1 + " (" + MidiHelper.getNoteAsString(midiDataByte1) + ")";
      this.displayDatabyte2Note = midiDataByte2 + " (" + MidiHelper.getNoteAsString(midiDataByte2) + ")";
   }

   public String getDawCommandName() {
      return dawCommandName;
   }

   public void setDawCommandName(final String dawCommandName) {
      this.dawCommandName = dawCommandName;
   }

   public String getDawCommandId() {
      return dawCommandId;
   }

   public void setDawCommandId(final String dawCommandId) {
      this.dawCommandId = dawCommandId;
   }

   public int getMidiDataByte1() {
      return midiDataByte1;
   }

   public void setMidiDataByte1(final int midiDataByte1) {
      this.midiDataByte1 = midiDataByte1;
   }

   public int getMidiDataByte2() {
      return midiDataByte2;
   }

   public void setMidiDataByte2(final int midiDataByte2) {
      this.midiDataByte2 = midiDataByte2;
   }

   public int getMidiChannel() {
      return midiChannel;
   }

   public void setMidiChannel(final int midiChannel) {
      this.midiChannel = midiChannel;
   }

   public MidiMessageParameter.MIDI_MESSAGE_TYPE getMidiMessageType() {
      return midiMessageType;
   }

   public void setMidiMessageType(final MidiMessageParameter.MIDI_MESSAGE_TYPE midiMessageType) {
      this.midiMessageType = midiMessageType;
   }

   public String getDisplayedMidiChannel() {
      return displayedMidiChannel;
   }

   public void setDisplayedMidiChannel(final String displayedMidiChannel) {
      this.displayedMidiChannel = displayedMidiChannel;
   }

   public String getDisplayDatabyte1Note() {
      return displayDatabyte1Note;
   }

   public String getDisplayDatabyte2Note() {
      return displayDatabyte2Note;
   }
}
