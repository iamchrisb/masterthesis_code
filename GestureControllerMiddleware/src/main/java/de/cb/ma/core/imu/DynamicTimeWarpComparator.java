package de.cb.ma.core.imu;

import de.cb.ma.core.database.model.GestureModel;
import de.cb.ma.core.imu.preprocessing.AmplitudeNormalizer;
import de.cb.ma.util.Pair;

import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.Validate;

import com.fastdtw.dtw.DTW;
import com.fastdtw.dtw.FastDTW;
import com.fastdtw.dtw.TimeWarpInfo;
import com.fastdtw.timeseries.TimeSeries;
import com.fastdtw.timeseries.TimeSeriesBase;
import com.fastdtw.util.DistanceFunction;

public class DynamicTimeWarpComparator implements GestureComparatorAlgo {

   public enum MODE {
      FAST_DTW,
      DTW
   }

   private TimeWarpInfo latestTimeWarpInfo;

   private DistanceFunction distanceFunction;
   private MODE mode;

   private AmplitudeNormalizer amplitudeNormalizerReference;
   private AmplitudeNormalizer amplitudeNormalizerObserved;

   public DynamicTimeWarpComparator(DistanceFunction distanceFunction, MODE mode) {
      this.distanceFunction = distanceFunction;
      this.mode = mode;

      amplitudeNormalizerReference = new AmplitudeNormalizer(-1000, 1000, 3);
      amplitudeNormalizerObserved = new AmplitudeNormalizer(-1000, 1000, 3);
   }

   public double compare(GestureModel referenceGesture, GestureModel observedGesture) {
      Validate.notNull(referenceGesture);
      Validate.notNull(observedGesture);

      //TODO implement data normalization

      final TimeSeriesBase.Builder firstTSBuilder = TimeSeriesBase.builder();
      final TimeSeriesBase.Builder secondTSBuilder = TimeSeriesBase.builder();

      final double[] g1TimeKeys = referenceGesture.getAccelerations()
            .getTimeKeys();
      final double[][] g1FrequencyValues = referenceGesture.getAccelerations()
            .getAmplitudeValues();

      for (int i = 0; i < g1FrequencyValues.length; i++) {
         double[] g1FrequencyValue = g1FrequencyValues[i];
         firstTSBuilder.add(g1TimeKeys[i], g1FrequencyValue);
      }

      final double[] g2TimeKeys = observedGesture.getAccelerations()
            .getTimeKeys();
      final double[][] g2FrequencyValues = observedGesture.getAccelerations()
            .getAmplitudeValues();

      for (int i = 0; i < g2FrequencyValues.length; i++) {
         double[] g2FrequencyValue = g2FrequencyValues[i];
         secondTSBuilder.add(g2TimeKeys[i], g2FrequencyValue);
      }

      final TimeSeries firstTimeSeries = firstTSBuilder.build();
      final TimeSeries secondTimeSeries = secondTSBuilder.build();

      final TimeWarpInfo warpInfo;
      if (mode.equals(MODE.DTW)) {
         warpInfo = DTW.compare(firstTimeSeries, secondTimeSeries, distanceFunction);
      } else {
         warpInfo = FastDTW.compare(firstTimeSeries, secondTimeSeries, distanceFunction);
      }

      this.latestTimeWarpInfo = warpInfo;

      return warpInfo.getDistance();
   }

   @Override
   public double compare(final List<Pair<Double, List<Double>>> referenceGesture, final List<Pair<Double, List<Double>>> observedGesture) {
      Validate.notEmpty(referenceGesture);
      Validate.notNull(referenceGesture);

      Validate.notNull(observedGesture);
      Validate.notEmpty(observedGesture);

      //TODO implement amplitude normalizer

      final TimeSeriesBase.Builder firstTSBuilder = TimeSeriesBase.builder();
      final TimeSeriesBase.Builder secondTSBuilder = TimeSeriesBase.builder();

      //      final List<Pair<Double, List<Double>>> normalizeObserved = amplitudeNormalizerObserved.normalize(observedGesture);
      //      final List<Pair<Double, List<Double>>> normalizeReferenced = amplitudeNormalizerObserved.normalize(referenceGesture);

      for (int i = 0; i < referenceGesture.size(); i++) {
         final Pair<Double, List<Double>> pair = referenceGesture.get(i);
         final Double[] doubles = pair.getValue()
               .toArray(new Double[pair.getValue()
                     .size()]);
         final double[] asArray = ArrayUtils.toPrimitive(doubles);
         firstTSBuilder.add(pair.getKey(), asArray);
      }

      for (int i = 0; i < observedGesture.size(); i++) {
         final Pair<Double, List<Double>> pair = observedGesture.get(i);
         final Double[] doubles = pair.getValue()
               .toArray(new Double[pair.getValue()
                     .size()]);
         final double[] asArray = ArrayUtils.toPrimitive(doubles);
         secondTSBuilder.add(pair.getKey(), asArray);
      }

      final TimeSeries firstTimeSeries = firstTSBuilder.build();
      final TimeSeries secondTimeSeries = secondTSBuilder.build();

      final TimeWarpInfo warpInfo;

      if (mode.equals(MODE.DTW)) {
         warpInfo = DTW.compare(firstTimeSeries, secondTimeSeries, distanceFunction);
      } else {
         warpInfo = FastDTW.compare(firstTimeSeries, secondTimeSeries, distanceFunction);
      }

      this.latestTimeWarpInfo = warpInfo;

      return warpInfo.getDistance();
   }

   public TimeWarpInfo getLatestTimeWarpInfo() {
      return latestTimeWarpInfo;
   }
}
