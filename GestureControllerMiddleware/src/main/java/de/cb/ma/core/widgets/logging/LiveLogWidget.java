package de.cb.ma.core.widgets.logging;

public interface LiveLogWidget {

   public void addLog(String logEntry);

   void addNewLine();
}
