package de.cb.ma.core.widgets.gesture;

import de.cb.ma.core.database.model.AmplitudeModel;
import de.cb.ma.core.widgets.analysis.charts.LineGraphPlotWidget;
import de.cb.ma.core.imu.preprocessing.AmplitudeNormalizer;
import de.cb.ma.util.Pair;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;

import java.util.Arrays;
import java.util.List;

public class DisplayGestureWidget extends Pane {

   public void displayGesture(final List<Pair<Double, List<Double>>> amplitudes) {
      final List<String> namesAsList = Arrays.asList(this.legendNames);

      observedGestureGraph.clearData();
      this.observedGesture = amplitudes;

      observedGestureGraph.createChart(amplitudes, namesAsList);

      normalizeAmpValues = amplitudeNormalizer.normalize(amplitudes);
      observedNormalizedGestureGraph.clearData();
      observedNormalizedGestureGraph.createChart(normalizeAmpValues, namesAsList);
   }

   public DisplayGestureWidget() {
      legendNames = new String[] { "x-Axis", "y-Axis",
            "z-Axis" };

      HBox graphContainer = new HBox(20);
      observedGestureGraph = new LineGraphPlotWidget(legendNames, "observed plot", "time in s (x)", "acceleration in mG (y)");
      //      observedGestureGraph.setRangeSaveVisibility(true);

      observedNormalizedGestureGraph = new LineGraphPlotWidget(legendNames, "normalized plot", "time in s (x)", "acceleration in mG (y)");

      graphContainer.getChildren()
            .addAll(observedGestureGraph.getElement(), observedNormalizedGestureGraph.getElement());

      this.getChildren()
            .addAll(graphContainer);

      amplitudeNormalizer = new AmplitudeNormalizer(-1000, 1000, 3);
   }

   private final LineGraphPlotWidget observedGestureGraph;
   private String[] legendNames;

   private List<Pair<Double, List<Double>>> observedGesture;

   public AmplitudeModel getAmplitudes() {
      final AmplitudeModel amplitudeSpectrum = new AmplitudeModel(3, this.observedGesture, Arrays.asList(this.legendNames));
      return amplitudeSpectrum;
   }

   private final AmplitudeNormalizer amplitudeNormalizer;
   private final LineGraphPlotWidget observedNormalizedGestureGraph;
   private List<Pair<Double, List<Double>>> normalizeAmpValues;
}
