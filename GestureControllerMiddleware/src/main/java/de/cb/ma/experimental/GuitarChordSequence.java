package de.cb.ma.experimental;

import java.util.ArrayList;

import javax.sound.midi.MidiEvent;
import javax.sound.midi.MidiSystem;
import javax.sound.midi.Sequence;
import javax.sound.midi.Sequencer;
import javax.sound.midi.ShortMessage;
import javax.sound.midi.Track;

public class GuitarChordSequence {   // this is the first one

   public static void main(String[] args) {

      GuitarTuning gt = new GuitarTuning();

      ArrayList<Integer> eMajor = new ArrayList<Integer>();

      eMajor.add(gt.midiNum(6, 0));
      eMajor.add(gt.midiNum(5, 2));
      eMajor.add(gt.midiNum(4, 2));
      eMajor.add(gt.midiNum(3, 1));
      eMajor.add(gt.midiNum(2, 0));
      eMajor.add(gt.midiNum(1, 0));

      ArrayList<Integer> aMajor = new ArrayList<Integer>();

      aMajor.add(gt.midiNum(6, 0));
      aMajor.add(gt.midiNum(5, 0));
      aMajor.add(gt.midiNum(4, 2));
      aMajor.add(gt.midiNum(3, 2));
      aMajor.add(gt.midiNum(2, 2));
      aMajor.add(gt.midiNum(1, 0));

      ArrayList<ArrayList<Integer>> chords = new ArrayList<ArrayList<Integer>>();

      chords.add(eMajor);
      chords.add(aMajor);

      final int instrument = 25; // SEE http://soundprogramming.net/file_formats/general_midi_instrument_list

	/* 25 Acoustic Guitar (nylon)
      26 Acoustic Guitar (steel)
	   27 Electric Guitar (jazz)
	   28 Electric Guitar (clean)
	   29 Electric Guitar (muted)
	   30 Overdriven Guitar
	   31 Distortion Guitar
	   32 Guitar harmonics */

      GuitarChordSequence mini = new GuitarChordSequence();
      mini.playChords(instrument, chords);

      System.exit(0);
   }

   public void playChords(int instrument, ArrayList<ArrayList<Integer>> chords) {

      try {

         Sequencer player = MidiSystem.getSequencer();
         player.open();

         Sequence seq = new Sequence(Sequence.PPQ, 4);
         Track track = seq.createTrack();

         MidiEvent event = null;

         ShortMessage first = new ShortMessage();
         first.setMessage(192, 1, instrument, 0);
         MidiEvent changeInstrument = new MidiEvent(first, 1);
         track.add(changeInstrument);

         final int beatDuration = 4; // how many beats to hold each chord

         // c is for chord
         for (int c = 0; c < chords.size(); c++) {

            // n is for note
            ArrayList<Integer> chord = chords.get(c);

            for (int n = 0; n < chord.size(); n++) {

               ShortMessage a = new ShortMessage();
               a.setMessage(ShortMessage.NOTE_ON, 1, chord.get(n), 100);
               MidiEvent noteOn = new MidiEvent(a, 1 + (c * beatDuration));
               track.add(noteOn);
            }

            for (int n = 0; n < chord.size(); n++) {
               ShortMessage b = new ShortMessage();
               b.setMessage(ShortMessage.NOTE_OFF, 1, chord.get(n), 100);
               MidiEvent noteOff = new MidiEvent(b, (c + 1) * beatDuration);
               track.add(noteOff);
            } // n for loop
         } // c for loop

         player.setSequence(seq);
         player.start();
         Thread.sleep(5000);
         player.close();

      } catch (Exception ex) {
         ex.printStackTrace();
      }
   } // close play

} // close class