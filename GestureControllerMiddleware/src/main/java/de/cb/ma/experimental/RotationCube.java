package de.cb.ma.experimental;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.PerspectiveCamera;
import javafx.scene.PointLight;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Box;
import javafx.scene.transform.Rotate;
import javafx.stage.Stage;

public class RotationCube extends Application {

   public static void main(String[] args) {
      launch(args);
   }

   Rotate rxBox = new Rotate(0, 0, 0, 0, Rotate.X_AXIS);
   Rotate ryBox = new Rotate(0, 0, 0, 0, Rotate.Y_AXIS);
   Rotate rzBox = new Rotate(0, 0, 0, 0, Rotate.Z_AXIS);

   @Override
   public void start(final Stage stage) throws Exception {
      // Create a Box
      Box box = new Box(200, 200, 200);
      //
      //      // Create a Sphere
      //      Sphere sphere = new Sphere(50);
      //      sphere.setTranslateX(300);
      //      sphere.setTranslateY(-5);
      //      sphere.setTranslateZ(400);
      //
      //      // Create a Cylinder
      //      Cylinder cylinder = new Cylinder(40, 120);
      //      cylinder.setTranslateX(500);
      //      cylinder.setTranslateY(-25);
      //      cylinder.setTranslateZ(600);

      // Create a Light
      PointLight light = new PointLight();
      light.setTranslateX(350);
      light.setTranslateY(100);
      light.setTranslateZ(300);

      // Create a Camera to view the 3D Shapes
      PerspectiveCamera camera = new PerspectiveCamera(false);
      camera.setTranslateX(100);
      camera.setTranslateY(-50);
      camera.setTranslateZ(300);

      // Add the Shapes and the Light to the Group
      //      Group root = new Group(box, sphere, cylinder, light);

      Group boxContainer = new Group(box);

      Group root = new Group(boxContainer, light);

      // Create a Scene with depth buffer enabled
      Scene scene = new Scene(root, 800, 800, true);
      // Add the Camera to the Scene
      scene.setCamera(camera);

      // Add the Scene to the Stage
      stage.setScene(scene);
      // Set the Title of the Stage
      stage.setTitle("An Example with Predefined 3D Shapes");
      // Display the Stage
      stage.show();

      boxContainer.setTranslateX(500);
      boxContainer.setTranslateY(250);
      boxContainer.setTranslateZ(600);

      PhongMaterial phongMaterial = new PhongMaterial();
      phongMaterial.setDiffuseColor(Color.TAN);
      box.setMaterial(phongMaterial);

      scene.setOnKeyPressed(new javafx.event.EventHandler<KeyEvent>() {
         @Override
         public void handle(final KeyEvent event) {

            //            final double yAngle = rzBox.getAngle();
            //            final double ryBoxAngle = ryBox.getAngle();
            Rotate rotation = new Rotate();

            switch (event.getCode()) {
               case RIGHT:
                  //                  rzBox.setAngle(yAngle + 1);
                  //                  box.getTransforms()
                  //                        .add(rzBox);
                  break;
               case LEFT:
                  //                  rzBox.setAngle(yAngle - 1);
                  //                  box.getTransforms()
                  //                        .add(rzBox);
               case DOWN:
                  //                  ryBox.setAngle(1);
                  //                  box.getTransforms()
                  //                        .add(ryBox);
               case UP:
                  //                  ryBox.setAngle(ryBoxAngle - 1);
                  //                  box.getTransforms()
                  //                        .add(ryBox);
               default:
                  break;
            }

            //            if (event.getCode()
            //                  .equals(KeyCode.RIGHT)) {
            //               final double rotate = box.getRotate();
            //               box.setRotate(rotate + 1);
            //            }
         }
      });
   }
}
