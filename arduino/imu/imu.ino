/* MPU9250 Basic Example Code
  by: Kris Winer
  date: April 1, 2014
  license: Beerware - Use this code however you'd like. If you
  find it useful you can buy me a beer some time.
  Modified by Brent Wilkins July 19, 2016

  Demonstrate basic MPU-9250 functionality including parameterizing the register
  addresses, initializing the sensor, getting properly scaled accelerometer,
  gyroscope, and magnetometer data out. Added display functions to allow display
  to on breadboard monitor. Addition of 9 DoF sensor fusion using open source
  Madgwick and Mahony filter algorithms. Sketch runs on the 3.3 V 8 MHz Pro Mini
  and the Teensy 3.1.

  SDA and SCL should have external pull-up resistors (to 3.3V).
  10k resistors are on the EMSENSR-9250 breakout board.

  Hardware setup:
  MPU9250 Breakout --------- Arduino
  VDD ---------------------- 3.3V
  VDDI --------------------- 3.3V
  SDA ----------------------- A4
  SCL ----------------------- A5
  GND ---------------------- GND
*/

#include <SPI.h>
#include <Wire.h>

#include "quaternionFilters.h"
#include "MPU9250.h"

#define AHRS true         // Set to false for basic data read
#define SerialDebug true  // Set to true to get Serial output for debugging

// Pin definitions
int intPin = 12;  // These can be changed, 2 and 3 are the Arduinos ext int pins
int myLed  = 13;  // Set up pin 13 led for toggling

MPU9250 myIMU;

void setup() {
  Wire.begin();
  // TWBR = 12;  // 400 kbit/sec I2C speed
  Serial.begin(38400);

  // Set up the interrupt pin, its set as active high, push-pull
  pinMode(intPin, INPUT);
  digitalWrite(intPin, LOW);
  pinMode(myLed, OUTPUT);
  digitalWrite(myLed, HIGH);

  // Read the WHO_AM_I register, this is a good test of communication
  byte c = myIMU.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);
  Serial.print("MPU9250 "); Serial.print("I AM "); Serial.print(c, HEX);
  Serial.print(" I should be "); Serial.println(0x71, HEX);

  if (c == 0x71) {
    // WHO_AM_I should always be 0x68
    Serial.println("MPU9250 is online...");

    // Start by performing self test and reporting values
    myIMU.MPU9250SelfTest(myIMU.SelfTest);
    Serial.print("x-axis self test: acceleration trim within : ");
    Serial.print(myIMU.SelfTest[0], 1); Serial.println("% of factory value");
    Serial.print("y-axis self test: acceleration trim within : ");
    Serial.print(myIMU.SelfTest[1], 1); Serial.println("% of factory value");
    Serial.print("z-axis self test: acceleration trim within : ");
    Serial.print(myIMU.SelfTest[2], 1); Serial.println("% of factory value");
    Serial.print("x-axis self test: gyration trim within : ");
    Serial.print(myIMU.SelfTest[3], 1); Serial.println("% of factory value");
    Serial.print("y-axis self test: gyration trim within : ");
    Serial.print(myIMU.SelfTest[4], 1); Serial.println("% of factory value");
    Serial.print("z-axis self test: gyration trim within : ");
    Serial.print(myIMU.SelfTest[5], 1); Serial.println("% of factory value");

    myIMU.calibrateMPU9250(myIMU.gyroBias, myIMU.accelBias);

    float dest1[3] = {0,0,0};
    float dest2[3] = {0,0,0};
    magcalMPU9250(dest1, dest2);

    Serial.print("Mag calibration dest1:" );
    printArray(dest1,3);
    Serial.println();

    Serial.print("Mag calibration dest2:" );
    printArray(dest2,3);
    Serial.println();
    
    myIMU.initMPU9250();
    // Initialize device for active mode read of acclerometer, gyroscope, and
    // temperature
    Serial.println("MPU9250 initialized for active data mode....");

    // Read the WHO_AM_I register of the magnetometer, this is a good test of
    // communication
    byte d = myIMU.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);
    Serial.print("AK8963 "); Serial.print("I AM "); Serial.print(d, HEX);
    Serial.print(" I should be "); Serial.println(0x48, HEX);

    // Get magnetometer calibration from AK8963 ROM
    myIMU.initAK8963(myIMU.magCalibration);
    // Initialize device for active mode read of magnetometer
    Serial.println("AK8963 initialized for active data mode....");
    if (SerialDebug) {
      //  Serial.println("Calibration values: ");
      Serial.print("X-Axis sensitivity adjustment value ");
      Serial.println(myIMU.magCalibration[0], 2);
      Serial.print("Y-Axis sensitivity adjustment value ");
      Serial.println(myIMU.magCalibration[1], 2);
      Serial.print("Z-Axis sensitivity adjustment value ");
      Serial.println(myIMU.magCalibration[2], 2);
    }

    Serial.print("Gyrobias: ");
    printArray(myIMU.gyroBias, 3);
    Serial.println();

    Serial.print("Accelbias: ");
    printArray(myIMU.accelBias, 3);
    Serial.println();
  }  else  {
    Serial.print("Could not connect to MPU9250: 0x");
    Serial.println(c, HEX);
    while (1) ; // Loop forever if communication doesn't happen
  }
}

void loop() {

  // If intPin goes high, all data registers have new data
  // On interrupt, check if data ready interrupt
  if (myIMU.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01) {

    

    myIMU.readAccelData(myIMU.accelCount);  // Read the x/y/z adc values
    myIMU.getAres();

    // Now we'll calculate the accleration value into actual g's
    // This depends on scale being set
    myIMU.ax = (float)myIMU.accelCount[0] * myIMU.aRes; // - accelBias[0];
    myIMU.ay = (float)myIMU.accelCount[1] * myIMU.aRes; // - accelBias[1];
    myIMU.az = (float)myIMU.accelCount[2] * myIMU.aRes; // - accelBias[2];

    myIMU.readGyroData(myIMU.gyroCount);  // Read the x/y/z adc values
    myIMU.getGres();

    // Calculate the gyro value into actual degrees per second
    // This depends on scale being set
    myIMU.gx = (float)myIMU.gyroCount[0] * myIMU.gRes;
    myIMU.gy = (float)myIMU.gyroCount[1] * myIMU.gRes;
    myIMU.gz = (float)myIMU.gyroCount[2] * myIMU.gRes;

    myIMU.readMagData(myIMU.magCount);  // Read the x/y/z adc values
    myIMU.getMres();
    // User environmental x-axis correction in milliGauss, should be
    // automatically calculated
    myIMU.magbias[0] = +470.;
    // User environmental x-axis correction in milliGauss TODO axis??
    myIMU.magbias[1] = +120.;
    // User environmental x-axis correction in milliGauss
    myIMU.magbias[2] = +125.;

    // Calculate the magnetometer values in milliGauss
    // Include factory calibration per data sheet and user environmental
    // corrections
    // Get actual magnetometer value, this depends on scale being set
    myIMU.mx = (float)myIMU.magCount[0] * myIMU.mRes * myIMU.magCalibration[0] -
               myIMU.magbias[0];
    myIMU.my = (float)myIMU.magCount[1] * myIMU.mRes * myIMU.magCalibration[1] -
               myIMU.magbias[1];
    myIMU.mz = (float)myIMU.magCount[2] * myIMU.mRes * myIMU.magCalibration[2] -
               myIMU.magbias[2];
  } // if (readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)

  // Must be called before updating quaternions!
  myIMU.updateTime();

  // Sensors x (y)-axis of the accelerometer is aligned with the y (x)-axis of
  // the magnetometer; the magnetometer z-axis (+ down) is opposite to z-axis
  // (+ up) of accelerometer and gyro! We have to make some allowance for this
  // orientationmismatch in feeding the output to the quaternion filter. For the
  // MPU-9250, we have chosen a magnetic rotation that keeps the sensor forward
  // along the x-axis just like in the LSM9DS0 sensor. This rotation can be
  // modified to allow any convenient orientation convention. This is ok by
  // aircraft orientation standards! Pass gyro rate as rad/s
  //  MadgwickQuaternionUpdate(ax, ay, az, gx*PI/180.0f, gy*PI/180.0f, gz*PI/180.0f,  my,  mx, mz);
  MahonyQuaternionUpdate(myIMU.ax, myIMU.ay, myIMU.az, myIMU.gx * DEG_TO_RAD,
                         myIMU.gy * DEG_TO_RAD, myIMU.gz * DEG_TO_RAD, myIMU.my,
                         myIMU.mx, myIMU.mz, myIMU.deltat);

  if (!AHRS) {
    myIMU.delt_t = millis() - myIMU.count;
    if (myIMU.delt_t > 100) {
      if (SerialDebug) {
        // Print resolutions

        float axValue = 1000 * myIMU.ax;
        float ayValue = 1000 * myIMU.ay;
        float azValue = 1000 * myIMU.az;

        float gxValue = myIMU.gx;
        float gyValue = myIMU.gy;
        float gzValue = myIMU.gz;

        float mxValue = myIMU.mx;
        float myValue = myIMU.my;
        float mzValue = myIMU.mz;

        /*
          Serial.println("Resolution accelerometer: " + String(myIMU.aRes));
          Serial.println("Resolution gyroscope: " + String(myIMU.gRes));
          Serial.println("Resolution magnetometer: " + String(myIMU.mRes));
        */
        // Print acceleration values in milligs!
        /* Serial.print("X-acceleration: "); Serial.print(axValue);
          Serial.print(" mg ");
          Serial.print("Y-acceleration: "); Serial.print(ayValue);
          Serial.print(" mg ");
          Serial.print("Z-acceleration: "); Serial.print(azValue);
          Serial.println(" mg ");

          // Print gyro values in degree/sec
          Serial.print("X-gyro rate: "); Serial.print(gxValue, 3);
          Serial.print(" degrees/sec ");
          Serial.print("Y-gyro rate: "); Serial.print(gyValue, 3);
          Serial.print(" degrees/sec ");
          Serial.print("Z-gyro rate: "); Serial.print(gzValue, 3);
          Serial.println(" degrees/sec");

          // Print mag values in degree/sec
          Serial.print("X-mag field: "); Serial.print(mxValue);
          Serial.print(" mG ");
          Serial.print("Y-mag field: "); Serial.print(myValue);
          Serial.print(" mG ");
          Serial.print("Z-mag field: "); Serial.print(mzValue);
          Serial.println(" mG");

          myIMU.tempCount = myIMU.readTempData();  // Read the adc values
          // Temperature in degrees Centigrade
          myIMU.temperature = ((float) myIMU.tempCount) / 333.87 + 21.0;
          // Print temperature in degrees Centigrade
          /*
           Serial.print("Temperature is ");  Serial.print(myIMU.temperature, 1);
           Serial.println(" degrees C");
        */

        //Serial.println("CMD:IMU:" + String((int)gxValue) + ";" + String((int)gyValue) + ";" + String((int)gzValue) + ";" + String((int)axValue) +  ";" + String((int)ayValue) + ";" + String((int)azValue));
      }

      myIMU.count = millis();
      digitalWrite(myLed, !digitalRead(myLed));  // toggle led
    } // if (myIMU.delt_t > 500)
  } else {
    // if (!AHRS)
    // Serial print and/or display at 0.5 s rate independent of data rates
    myIMU.delt_t = millis() - myIMU.count;

    if (myIMU.delt_t > 100) {
      if (SerialDebug) {

        float axValue = 1000 * myIMU.ax;
        float ayValue = 1000 * myIMU.ay;
        float azValue = 1000 * myIMU.az;

        float gxValue = myIMU.gx;
        float gyValue = myIMU.gy;
        float gzValue = myIMU.gz;

        float mxValue = myIMU.mx;
        float myValue = myIMU.my;
        float mzValue = myIMU.mz;

        Serial.println("Resolution accelerometer: " + String(myIMU.aRes));
        Serial.println("Resolution gyroscope: " + String(myIMU.gRes));
        Serial.println("Resolution magnetometer: " + String(myIMU.mRes));

        Serial.print("ax = "); Serial.print((int) axValue);
        Serial.print(" ay = "); Serial.print((int) ayValue);
        Serial.print(" az = "); Serial.print((int) azValue);
        Serial.println(" mg");

        Serial.print("gx = "); Serial.print(gxValue, 2);
        Serial.print(" gy = "); Serial.print(gyValue, 2);
        Serial.print(" gz = "); Serial.print(gzValue, 2);
        Serial.println(" deg/s");

        Serial.print("mx = "); Serial.print( (int) mxValue );
        Serial.print(" my = "); Serial.print( (int)myValue );
        Serial.print(" mz = "); Serial.print( (int)mzValue );
        Serial.println(" mG");

        // Serial.println("CMD:IMU:" + String((int)gxValue) + ";" + String((int)gyValue) + ";" + String((int)gzValue) + ";" + String((int)axValue) +  ";" + String((int)ayValue) + ";" + String((int)azValue));

        Serial.print("q0 = "); Serial.print(*getQ());
        Serial.print(" qx = "); Serial.print(*(getQ() + 1));
        Serial.print(" qy = "); Serial.print(*(getQ() + 2));
        Serial.print(" qz = "); Serial.println(*(getQ() + 3));
      }

      // Define output variables from updated quaternion---these are Tait-Bryan
      // angles, commonly used in aircraft orientation. In this coordinate system,
      // the positive z-axis is down toward Earth. Yaw is the angle between Sensor
      // x-axis and Earth magnetic North (or true North if corrected for local
      // declination, looking down on the sensor positive yaw is counterclockwise.
      // Pitch is angle between sensor x-axis and Earth ground plane, toward the
      // Earth is positive, up toward the sky is negative. Roll is angle between
      // sensor y-axis and Earth ground plane, y-axis up is positive roll. These
      // arise from the definition of the homogeneous rotation matrix constructed
      // from quaternions. Tait-Bryan angles as well as Euler angles are
      // non-commutative; that is, the get the correct orientation the rotations
      // must be applied in the correct order which for this configuration is yaw,
      // pitch, and then roll.
      // For more see
      // http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
      // which has additional links.

      myIMU.yaw   = atan2(2.0f * (*(getQ() + 1) * *(getQ() + 2) + *getQ() *
                                  *(getQ() + 3)), *getQ() * *getQ() + * (getQ() + 1) * *(getQ() + 1)
                          - * (getQ() + 2) * *(getQ() + 2) - * (getQ() + 3) * *(getQ() + 3));
      myIMU.pitch = -asin(2.0f * (*(getQ() + 1) * *(getQ() + 3) - *getQ() *
                                  *(getQ() + 2)));
      myIMU.roll  = atan2(2.0f * (*getQ() * *(getQ() + 1) + * (getQ() + 2) *
                                  *(getQ() + 3)), *getQ() * *getQ() - * (getQ() + 1) * *(getQ() + 1)
                          - * (getQ() + 2) * *(getQ() + 2) + * (getQ() + 3) * *(getQ() + 3));

      myIMU.pitch *= RAD_TO_DEG;
      myIMU.yaw   *= RAD_TO_DEG;

      // Declination of SparkFun Electronics (40°05'26.6"N 105°11'05.9"W) is
      //    8° 30' E  ± 0° 21' (or 8.5°) on 2016-07-19
      // - http://www.ngdc.noaa.gov/geomag-web/#declination
      //myIMU.yaw   -= 8.5;

      myIMU.yaw   -= 3.66666666666667; // for potsdam
      myIMU.roll  *= RAD_TO_DEG;

      if (SerialDebug) {
        Serial.print("Yaw, Pitch, Roll: ");
        Serial.print(myIMU.yaw, 2);
        Serial.print(", ");
        Serial.print(myIMU.pitch, 2);
        Serial.print(", ");
        Serial.println(myIMU.roll, 2);

        Serial.print("rate = ");
        Serial.print((float)myIMU.sumCount / myIMU.sum, 2);
        Serial.println(" Hz");
      }

      String stringyfiedYaw = String((int) myIMU.yaw);
      String stringyfiedPitch = String((int) myIMU.pitch);
      String stringyfiedRoll = String((int) myIMU.roll);

      float axValue = 1000 * myIMU.ax;
      float ayValue = 1000 * myIMU.ay;
      float azValue = 1000 * myIMU.az;

      String stringyfiedAxValue = String((int) axValue);
      String stringyfiedAyValue = String((int) ayValue);
      String stringyfiedAzValue = String((int) azValue);

      //Serial.println("CMD:IMU:" + stringyfiedYaw  + ";" + stringyfiedPitch + ";" + stringyfiedRoll + ";" + stringyfiedAxValue + ";" + stringyfiedAyValue + ";" + stringyfiedAzValue);
      Serial.println("MSG:APP:ORI:" + stringyfiedYaw  + ";" + stringyfiedPitch + ";" + stringyfiedRoll);
      Serial.println("MSG:DBG:ACC:" + stringyfiedAxValue + ";" + stringyfiedAyValue + ";" + stringyfiedAzValue);

      myIMU.count = millis();
      myIMU.sumCount = 0;
      myIMU.sum = 0;
    } // if (myIMU.delt_t > 500)
  } // if (AHRS)
}

/** 
 *  calibration functions 
 */
void magcalMPU9250(float * dest1, float * dest2) {
  uint16_t ii = 0, sample_count = 0;
  int32_t mag_bias[3] = {0, 0, 0}, mag_scale[3] = {0, 0, 0};
  int16_t mag_max[3] = {0x8000, 0x8000, 0x8000}, mag_min[3] = {0x7FFF, 0x7FFF, 0x7FFF}, mag_temp[3] = {0, 0, 0};

  Serial.println("Mag Calibration: Wave device in a figure eight until done!");
  delay(4000);

  sample_count = 128;
  for (ii = 0; ii < sample_count; ii++) {
    printIntArray(mag_temp, 3);
    //MPU9250readMagData(mag_temp);  // Read the mag data
    for (int jj = 0; jj < 3; jj++) {
      if (mag_temp[jj] > mag_max[jj]) mag_max[jj] = mag_temp[jj];
      if (mag_temp[jj] < mag_min[jj]) mag_min[jj] = mag_temp[jj];
    }
    delay(135);  // at 8 Hz ODR, new mag data is available every 125 ms
  }

  // Get hard iron correction
  mag_bias[0]  = (mag_max[0] + mag_min[0]) / 2; // get average x mag bias in counts
  mag_bias[1]  = (mag_max[1] + mag_min[1]) / 2; // get average y mag bias in counts
  mag_bias[2]  = (mag_max[2] + mag_min[2]) / 2; // get average z mag bias in counts

  Serial.print("mag max/min/bias x: "); Serial.println(String(mag_max[0]) + "/" + String(mag_min[0]) + "/" + String(mag_bias[0])); 
  Serial.print("mag-max/min/bias y: "); Serial.println(String(mag_max[1]) + "/" + String(mag_min[1]) + "/" + String(mag_bias[1]));
  Serial.print("mag-max/min/bias z: "); Serial.println(String(mag_max[2]) + "/" + String(mag_min[2]) + "/" + String(mag_bias[2]));
  /*
    dest1[0] = (float) mag_bias[0] * MPU9250mRes * MPU9250magCalibration[0]; // save mag biases in G for main program
    dest1[1] = (float) mag_bias[1] * MPU9250mRes * MPU9250magCalibration[1];
    dest1[2] = (float) mag_bias[2] * MPU9250mRes * MPU9250magCalibration[2];
  */

  dest1[0] = (float) mag_bias[0] * myIMU.mRes *  myIMU.magCalibration[0]; // save mag biases in G for main program
  dest1[1] = (float) mag_bias[1] * myIMU.mRes *  myIMU.magCalibration[1];
  dest1[2] = (float) mag_bias[2] * myIMU.mRes *  myIMU.magCalibration[2];

  // Get soft iron correction estimate
  mag_scale[0]  = (mag_max[0] - mag_min[0]) / 2; // get average x axis max chord length in counts
  mag_scale[1]  = (mag_max[1] - mag_min[1]) / 2; // get average y axis max chord length in counts
  mag_scale[2]  = (mag_max[2] - mag_min[2]) / 2; // get average z axis max chord length in counts

  float avg_rad = mag_scale[0] + mag_scale[1] + mag_scale[2];
  avg_rad /= 3.0;

  dest2[0] = avg_rad / ((float)mag_scale[0]);
  dest2[1] = avg_rad / ((float)mag_scale[1]);
  dest2[2] = avg_rad / ((float)mag_scale[2]);

  Serial.println("Mag Calibration done!");
}

/**
   util functions
*/

void printArray(float *ar, int len) {
  Serial.print("[");
  for (int i = 0; i < len; i++) {
    Serial.print(ar[i]);
    if (i < len - 1) {
      Serial.print(",");
    }
  }
  Serial.print("]");
}

void printIntArray(int *ar, int len) {
  Serial.print("[");
  for (int i = 0; i < len; i++) {
    Serial.print(ar[i]);
    if (i < len - 1) {
      Serial.print(",");
    }
  }
  Serial.print("]");
}

