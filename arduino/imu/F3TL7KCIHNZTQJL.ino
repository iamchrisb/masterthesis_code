/* 
  This sketch reads the acceleration from the Bean's on-board accelerometer. 
  
  Also, this sketch recognizes some pre defined gestures based on the accelerometer data.
  The machine learning method used is J48 decision tree built by Weka.
  The recognizable gestures are: NonGesture, SlapDown, SlapToTop and Punch (turn on device - Ref: ASL (American Sign Language)).

  Author: Bruno Silva Pontes
  Date: November 2015
  
  To use the Serial Monitor, set Arduino's serial port to "/tmp/tty.LightBlue-Bean"
  and the Bean as "Virtual Serial" in the OS X Bean Loader.
    
*/

//short int qtdGestos = 1;

boolean inicioExecucao = true;
int intervaloSerial = 50;
int maxWindowLength = 20;
short tooBigValue = 1000;
int maxMeasuresLength = 20;
int maxMeasuresGesture = 70;

void setup() {
  // Bean Serial is at a fixed baud rate. Changing the value in Serial.begin() has no effect.
  Serial.begin();   
  // Optional: Use Bean.setAccelerationRange() to set the sensitivity to something other than the default of ±2g.
}

void loop() {
  //Serial.println("inicio codigo");
  char stringMedicoes[800] = "" ;
  char stringAux[5] = "" ;
  boolean tempoEsgotadoGesto = false;
  unsigned long marcacaoTempoInicioMovimento, marcacaoTempoCorrente;
  short xAxisData[maxMeasuresGesture], yAxisData[maxMeasuresGesture], zAxisData[maxMeasuresGesture];
  AccelerationReading acceleration;
  int cont = 0, i =0;

  // "Dorme" pra ter um tempo pro usuario se preparar pra iniciar
  // Na primeira vez que vai executar dorme por mais tempo
  if(inicioExecucao){
      Bean.sleep(3000);
      inicioExecucao = false;
  }
  else
      Bean.sleep(intervaloSerial);
  
  // Liga o LED vermelho pra indicar pro usuario iniciar o gesto
  Bean.setLed(255,0,0);

  Serial.println("*** COMECE O GESTO ***");

  // Pega o tempo em que o movimento vai iniciar
  marcacaoTempoInicioMovimento = millis();

  // Initializes the arrays with a too big value
  for(i=0 ; i<maxMeasuresGesture ; i++){
    xAxisData[i] = tooBigValue;
    yAxisData[i] = tooBigValue;
    zAxisData[i] = tooBigValue;
  }
  
  // Fica pegando dados do acelerometro enquanto o gesto nao acaba
  while(tempoEsgotadoGesto == false){
      // Pega tempo atual
      marcacaoTempoCorrente = millis();
      
      // Get the current acceleration with range of ±2g, and a conversion of 3.91×10-3 g/unit or 0.03834(m/s^2)/units. 
      acceleration = Bean.getAcceleration();

      // Monta string no formato: (Classe):(X),(Y),(Z)
      //stringMedicoes = stringMedicoes + "1:" + acceleration.xAxis + "," + acceleration.yAxis + "," + acceleration.zAxis + "|" + marcacaoTempoCorrente + "\n";
      //strcat(stringMedicoes, "1:");
      sprintf(stringAux, "%hi", acceleration.xAxis);
      strcat(stringMedicoes, stringAux); strcat(stringMedicoes, ",");
      sprintf(stringAux, "%hi", acceleration.yAxis);
      strcat(stringMedicoes, stringAux); strcat(stringMedicoes, ",");
      sprintf(stringAux, "%hi", acceleration.zAxis);
      strcat(stringMedicoes, stringAux); 
      //strcat(stringMedicoes, "|");
      //sprintf(stringAux, "%lu", marcacaoTempoCorrente);
      //strcat(stringMedicoes, stringAux);
      xAxisData[cont] = acceleration.xAxis;
      yAxisData[cont] = acceleration.yAxis;
      zAxisData[cont] = acceleration.zAxis;
      cont++;
      // Verifica se ja deu tempo suficiente pro gesto ser feito
      if(marcacaoTempoCorrente - marcacaoTempoInicioMovimento > 1300)
          tempoEsgotadoGesto = true;
      else
          strcat(stringMedicoes, "$");

  }
  
  Serial.println("*** FIM DO GESTO ***");
  
  // Desliga o LED pra indicar que o tempo do gesto acabou
  Bean.setLed(0,0,0);  
  // Call the function that recognizes the time serie as a pre defined gesture
  RecognizesGesture(stringMedicoes, 5, xAxisData, yAxisData, zAxisData);  

  Bean.sleep(1000);
  
}

void RecognizesGesture(char* stringMedicoes, int numberWindows, short* xAxisData, short* yAxisData, short* zAxisData){
  char *stringParaImpressao;
  int timeSerieLength = 0, contWindow = 0, auxIndex = -1;
  char stringAux[50] = "" ;
  int modTimeSerie = 0, windowLength = 0, fractionalPart = 0, i = 0;
  short xAxisWindowData[maxWindowLength], yAxisWindowData[maxWindowLength], zAxisWindowData[maxWindowLength];
  char** measuresValues = NULL;
  int w = 0, auxCont = 0;
  char* stringTest = "testinggg";
  char* stringMedicoesAux = NULL;
  double YMeanWindow1, YMeanWindow3, ZSDWindow4;
  double XMeanWindow2;
  char* stringdouble;
  

  // Insert 1000 (very big impossible real value) in all indexes to know where the last measure was inserted
  for(i = 0 ; i < maxWindowLength ; i++){
     xAxisWindowData[i] = tooBigValue;
     yAxisWindowData[i] = tooBigValue;
     zAxisWindowData[i] = tooBigValue;
  }

  /*auxCont = 0;
  while(stringMedicoes[auxCont] != NULL){
    Serial.print("la vai stringMedicoes1 ");
    Serial.println(stringMedicoes[auxCont]);
    auxCont++;
  }*/

  //stringMedicoesAux = (char *)malloc((auxCont + 1) * sizeof(char));

  //strcpy(stringMedicoesAux, stringMedicoes);
  
  /*auxCont = 0;
  while(stringMedicoesAux[auxCont] != NULL){
    Serial.print("la vai AUXstringMedicoesAUX1 ");
    Serial.println(stringMedicoesAux[auxCont]);
    auxCont++;
  }*/
  
  // Separa o string grande (com todas medicoes de um gesto) em pedacos para 
  // poder imprimir usando o Serial.println pq o buffer do mesmo e muito pequeno
  // o $ eh o separador de medicoes na string
  stringParaImpressao = strtok(stringMedicoes, "$");
  while (stringParaImpressao != NULL) {
    //Serial.println(stringParaImpressao);
    stringParaImpressao = strtok(NULL, "$");
    timeSerieLength++;
  }

  sprintf(stringAux, "%d", timeSerieLength);
  strcat(stringAux, " = time serie length");
  Serial.println(stringAux);
  auxCont = 0;
  for(i=0 ; i<maxMeasuresGesture ; i++){
    if(xAxisData[i] < tooBigValue)
      auxCont++;
  }
  sprintf(stringAux, "%d", auxCont);
  strcat(stringAux, " = time serie length2");
  Serial.println(stringAux);

  
  // Find out the rest of the division of each time serie by numberWindows
  modTimeSerie = timeSerieLength % numberWindows;
  // Find out how many measures a window has
  windowLength = (timeSerieLength / numberWindows);
  sprintf(stringAux, "%d", windowLength);
  strcat(stringAux, " windowLength");
  Serial.println(stringAux);
  sprintf(stringAux, "%d", modTimeSerie);
  strcat(stringAux, " modTimeSerie");
  Serial.println(stringAux);

  //measuresValues = GetStringsMeasures(timeSerieLength, stringMedicoes);

  

  // Extract the machine learning model features
  for(i = 0 ; i < timeSerieLength ; i++){
      auxIndex = GetNextIndexToInsert(xAxisWindowData);
      xAxisWindowData[auxIndex] = xAxisData[i];
      yAxisWindowData[auxIndex] = yAxisData[i];
      zAxisWindowData[auxIndex] = zAxisData[i];
      
      // The first window may has more than numberMeasuresWindow if the division of the time serie length by
      // numberWindows is not exact
      if((i + 1) <= (windowLength + modTimeSerie)){
        // First window
        // It is the last measure of the first window
        // So write its features to file
        if((i + 1) == (windowLength + modTimeSerie)){
          // writesMachineLearningFeatures(machineLearningFile, xAxisWindowData, yAxisWindowData, zAxisWindowData)                  
          contWindow++;
          YMeanWindow1 = GetWindowMean(yAxisWindowData);
          // Clear the already written window data
          // Insert 1000 (very big impossible real value) in all indexes to know where the last measure was inserted
          for(w = 0 ; w < maxWindowLength ; w++){
            xAxisWindowData[w] = tooBigValue;
            yAxisWindowData[w] = tooBigValue;
            zAxisWindowData[w] = tooBigValue;
          } 
        }
      } else {
        // If it is the last measure of a window
        //aux = ((cont - modEachGesture[contTimeSerie])/numberMeasuresWindow)
        if((((i + 1) - modTimeSerie) % windowLength) == 0){
          contWindow++;
          //writesMachineLearningFeatures(machineLearningFile, xAxisWindowData, yAxisWindowData, zAxisWindowData)
          //if(contWindow == 5)
            //XMeanWindow5 = GetWindowMean(xAxisWindowData);
          if(contWindow == 3)
            YMeanWindow3 = GetWindowMean(yAxisWindowData);
          if(contWindow == 4)
            ZSDWindow4 = GetWindowSD(zAxisWindowData);
          if(contWindow == 2)
             XMeanWindow2 = GetWindowMean(xAxisWindowData);
          // Clear the already written window data
          // Insert 1000 (very big impossible real value) in all indexes to know where the last measure was inserted
          for(w = 0 ; w < maxWindowLength ; w++){
             xAxisWindowData[w] = tooBigValue;
             yAxisWindowData[w] = tooBigValue;
             zAxisWindowData[w] = tooBigValue;
          }
        }
      }
  }


  // Recognizes the gesture (Machine Learning Model)
  if(YMeanWindow1 <= -22.54){
    Serial.println("SlapDown");
  } else {
    if(XMeanWindow2 > 240.78){
      Serial.println("Punch");
    } else {
      if(ZSDWindow4 <= 21.53){
        Serial.println("NonGesture");
      } else{
        if(YMeanWindow3 > 120.33){
          Serial.println("NonGesture");
        } else {
          Serial.println("SlapToTop");
        }
      }
    }
  }

  /*if(XMeanWindow2 <= 240.78){
    Serial.println("NonGesture");
  } else {
    Serial.println("Punch");
  }*/

  // Checking values
  /*Serial.println("vai comecar os valores:");
  for(i=0; i<timeSerieLength; i++){
    sprintf(stringAux, "%hi", xAxisData[i]);
    strcat(stringAux, ", ");
    Serial.print(stringAux);
    sprintf(stringAux, "%hi", yAxisData[i]);
    strcat(stringAux, ", ");
    Serial.print(stringAux);
    sprintf(stringAux, "%hi", zAxisData[i]);
    Serial.println(stringAux);
  }*/

  //dtostrf(XMeanWindow5, 8, 2, stringdouble);
  //Serial.println("XMeanWindow5 ");
  //printDouble(XMeanWindow5, 10);
  /*dtostrf(ZMeanWindow3, 7, 2, stringdouble);
  Serial.print("ZMeanWindow3 ");
  Serial.println(stringdouble);
  dtostrf(YSDWindow4, 7, 2, stringdouble);
  Serial.print("YSDWindow4 ");
  Serial.println(stringdouble);
  
  sprintf(stringAux, "%0.1f", XMeanWindow5);
  strcat(stringAux, "  XMeanWindow5");
  Serial.println(stringAux);
  sprintf(stringAux, "%0.1f", ZMeanWindow3);
  strcat(stringAux, "  ZMeanWindow3");
  Serial.println(stringAux);
  sprintf(stringAux, "%0.1f", YSDWindow4);
  strcat(stringAux, "  YSDWindow4");
  Serial.println(stringAux);
*/
  
}
  

double GetWindowMean(short* axisWindowData){
  int contMeasures = 0, i = 0;
  short dataSum = 0;

  // Get the length of the window
  contMeasures = GetNextIndexToInsert(axisWindowData);

  for(i=0 ; i<contMeasures ; i++){
    dataSum = dataSum + axisWindowData[i];
  }

  return (dataSum/contMeasures);
}


// Gets a integer from a string with some values separated by comma (,)
int GetMeasureValue(int measureIndex, char* measures){
  String stringMeasureValue = measures;
}

// Return the first index where the value is equal to 1000 (it is not being used)
int GetNextIndexToInsert(short* values){
  int u = 0;

  for(u = 0 ; u < maxWindowLength ; u++){
    if(values[u] == tooBigValue){
      return u;  
    }
  }
  // should be error
  return -1;
}

double GetWindowSD(short* axisWindowData){
  double mean = 0.0, sum_deviation = 0.0;
  int contMeasures = 0, i=0;

  // Get the length of the window
  contMeasures = GetNextIndexToInsert(axisWindowData);

  mean = GetWindowMean(axisWindowData);

  for(i=0 ; i<contMeasures ; i++)
    sum_deviation += ((axisWindowData[i]-mean)*(axisWindowData[i]-mean));

  return sqrt(sum_deviation/contMeasures);
  
}

void printDouble( double val, unsigned int precision){
// prints val with number of decimal places determine by precision
// NOTE: precision is 1 followed by the number of zeros for the desired number of decimial places
// example: printDouble( 3.1415, 100); // prints 3.14 (two decimal places)

   Serial.print (int(val));  //prints the int part
   Serial.print("."); // print the decimal point
   unsigned int frac;
   if(val >= 0)
       frac = (val - int(val)) * precision;
   else
       frac = (int(val)- val ) * precision;
   Serial.println(frac,DEC) ;
}


/*char** GetStringsMeasures(int timeSerieLength, char* stringMedicoes){
  char** measuresString = NULL;
  int cont = 0, auxCont = 0;
  char *auxString;
  int i = 0;
  Serial.println("Got in GetStringsMeasures()");
  while(stringMedicoes[auxCont] != NULL){
    Serial.print("la vai stringMedicoes ");
    Serial.println(stringMedicoes[auxCont]);
    auxCont++;
  }
  Serial.println("passou do string medicoes");
  // Allocating space to the variable to be returned
  measuresString = (char**)malloc(timeSerieLength * sizeof(char*));
  if (measuresString == NULL)
    Serial.println("deu ruim");
  for(i=0 ; i<timeSerieLength ; i++){
    measuresString[i] = (char*)malloc(sizeof(char) * 6);
    if(measuresString[i] == NULL)
      Serial.println("deu ruim 2");
  }


  
  // Separa o string grande (com todas medicoes de um gesto) em pedacos para 
  // poder imprimir usando o Serial.println pq o buffer do mesmo e muito pequeno
  // o $ eh o separador de medicoes na string
  auxString = strtok(stringMedicoes, "$");
  Serial.println(auxString);
  if(auxString != NULL){
    strcpy(measuresString[cont], auxString);
    Serial.println("la vai measuresString[0]:"); 
    Serial.println(measuresString[cont]);
  }
  while (auxString != NULL) {
    //Serial.println(stringParaImpressao);
    auxString = strtok(NULL, "$");
    if(auxString != NULL){
      cont++;
      strcpy(measuresString[cont], auxString); 
      Serial.println("here it goes a measure");
      Serial.println(measuresString[cont]);
    }
      
    //timeSerieLength++;
  }
  
  
  return measuresString;
}
*/



