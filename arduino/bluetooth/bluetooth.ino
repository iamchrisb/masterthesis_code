#define softserial

// Eingebaute LED nutzen:
const int LED  = 13;


#ifdef softserial
  #include <SoftwareSerial.h>
  const int BTRX = 10;
  const int BTTX = 11;
  SoftwareSerial SerialBT(BTRX, BTTX);
#else 
  HardwareSerial SerialBT = Serial1;
#endif


// Die versendete Nachricht:
String msg; 


///////////////////////////////////////////////////
//
// setup
//    Verbindung mit HC-05 aufbauen
//
///////////////////////////////////////////////////

void setup() {
  SerialBT.begin(38400);
  SerialBT.println("Bluetooth-Verbindung steht");
  pinMode(LED, OUTPUT);
}

///////////////////////////////////////////////////
//
// loop
//    In jeder Iteration auf Nachricht warten,
//    Nachricht analysieren,
//    Aktion auslösen (LED ein/aus)
//
///////////////////////////////////////////////////

void loop() {
  if (SerialBT.available()){      // Daten liegen an
     msg = SerialBT.readString(); // Nachricht lesen
     if (msg == "ein") {
         digitalWrite(LED, HIGH);
         SerialBT.print("LED an Pin ");
         SerialBT.print(LED);
         SerialBT.println("ist eingeschaltet!");
      } 
      else
      if (msg == "aus") {
         digitalWrite(LED, LOW);
         SerialBT.print("LED an Pin ");
         SerialBT.print(LED);
         SerialBT.println("ist ausgeschaltet!");
      }
      else {
         SerialBT.print("Kommando ");
         SerialBT.print(msg);
         SerialBT.println("nicht bekannt");
      }
    }
}
