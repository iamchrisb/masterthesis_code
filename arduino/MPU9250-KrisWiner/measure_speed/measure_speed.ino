/* MPU9250 Basic Example Code
  by: Kris Winer
  date: April 1, 2014
  license: Beerware - Use this code however you'd like. If you
  find it useful you can buy me a beer some time.

  Demonstrate basic MPU-9250 functionality including parameterizing the register addresses, initializing the sensor,
  getting properly scaled accelerometer, gyroscope, and magnetometer data out. Added display functions to
  allow display to on breadboard monitor. Addition of 9 DoF sensor fusion using open source Madgwick and
  Mahony filter algorithms. Sketch runs on the 3.3 V 8 MHz Pro Mini and the Teensy 3.1.

  SDA and SCL should have external pull-up resistors (to 3.3V).
  10k resistors are on the EMSENSR-9250 breakout board.

  Hardware setup:
  MPU9250 Breakout --------- Arduino
  VDD ---------------------- 3.3V
  VDDI --------------------- 3.3V
  SDA ----------------------- A4
  SCL ----------------------- A5
  GND ---------------------- GND
*/

uint32_t delt_t = 0;                              // used to control display output rate
uint32_t count = 0, sumCount = 0;                 // used to control display output rate
float deltat = 0.0f, sum = 0.0f;                  // integration interval for both filter schemes
uint32_t lastUpdate = 0, firstUpdate = 0;         // used to calculate integration interval
uint32_t Now = 0;                                 // used to calculate integration interval

void setup(){
  Serial.begin(115200);
}

void loop() {
  measureSpeed();
}


void measureSpeed() {
  Now = micros();
  deltat = ((Now - lastUpdate) / 1000000.0f); // set integration time by time elapsed since last filter update
  lastUpdate = Now;

  sum += deltat; // sum for averaging filter update rate
  sumCount++;
  
  delt_t = millis() - count;
  if (delt_t > 500) {
    Serial.print("rate = "); Serial.print((float)sumCount/sum, 2); Serial.println(" Hz");
  }

  count = millis();
  sumCount = 0;
  sum = 0;
}

void myinthandler() {
  
}

