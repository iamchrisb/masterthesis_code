// Measure the voltage at 5V and the actual resistance of your
// resistor, and enter them below:

const float VCC = 4.98; // Measured voltage of Ardunio 5V line
const float R_DIV = 15000.0; // Measured resistance of resistor

const float MAX_ANGLE = 90.0;
const float MIN_ANGLE = 0;

// Upload the code, then try to adjust these values to more
// accurately calculate bend degree.

const float PINKY_RESISTANCE_STRAIGHT   = 11500.00;
const float PINKY_RESISTANCE_BEND       = 58000.00;

const float MIDDLE_RESISTANCE_STRAIGHT  = 12000.00;
const float MIDDLE_RESISTANCE_BEND      = 38000.00;

const float INDEX_RESISTANCE_STRAIGHT   = 15000.00;
const float INDEX_RESISTANCE_BEND       = 48000.00;

const float THUMB_RESISTANCE_STRAIGHT   = 11000.00;
const float THUMB_RESISTANCE_BEND       = 32000.00;

const float RING_RESISTANCE_STRAIGHT    = 11000.00;
const float RING_RESISTANCE_BEND        = 40000.00;

const float ANALOG_READ_MAX = 1023.0;

void flex() {
  // Read the ADC, and calculate voltage and resistance from it
  int flexADC_RING    = analogRead(FLEX_PIN_RING_FINGER);
  int flexADC_MIDDLE  = analogRead(FLEX_PIN_MIDDLE_FINGER);
  int flexADC_INDEX   = analogRead(FLEX_PIN_INDEX_FINGER);
  int flexADC_THUMB   = analogRead(FLEX_PIN_THUMB);
  int flexADC_PINKY   = analogRead(FLEX_PIN_PINKY);

  // use the voltage divider equation and solve it for the flex resistor flexR_[finger]

  float flexV_RING = flexADC_RING * VCC / ANALOG_READ_MAX;
  float flexR_RING = R_DIV * (VCC / flexV_RING - 1.0);

  float flexV_MIDDLE = flexADC_MIDDLE * VCC / ANALOG_READ_MAX;
  float flexR_MIDDLE = R_DIV * (VCC / flexV_MIDDLE - 1.0);

  float flexV_INDEX = flexADC_INDEX * VCC / ANALOG_READ_MAX;
  float flexR_INDEX = R_DIV * (VCC / flexV_INDEX - 1.0);

  float flexV_THUMB = flexADC_THUMB * VCC / ANALOG_READ_MAX;
  float flexR_THUMB = R_DIV * (VCC / flexV_THUMB - 1.0);

  float flexV_PINKY = flexADC_PINKY * VCC / ANALOG_READ_MAX;
  float flexR_PINKY = R_DIV * (VCC / flexV_PINKY - 1.0);

 // Use the calculated value from the variable resistor (flexsensor) to estimate the sensor's
  // bend angle:

  float ANGLE_RING = map(flexR_RING, RING_RESISTANCE_STRAIGHT, RING_RESISTANCE_BEND,
                         MIN_ANGLE, MAX_ANGLE);

  float ANGLE_PINKY = map(flexR_PINKY, PINKY_RESISTANCE_STRAIGHT , PINKY_RESISTANCE_BEND,
                          MIN_ANGLE, MAX_ANGLE);

  float ANGLE_MIDDLE = map(flexR_MIDDLE, MIDDLE_RESISTANCE_STRAIGHT, MIDDLE_RESISTANCE_BEND,
                           MIN_ANGLE, MAX_ANGLE);

  float ANGLE_INDEX = map(flexR_INDEX, INDEX_RESISTANCE_STRAIGHT, INDEX_RESISTANCE_BEND,
                          MIN_ANGLE, MAX_ANGLE);

  float ANGLE_THUMB = map(flexR_THUMB, THUMB_RESISTANCE_STRAIGHT, THUMB_RESISTANCE_BEND,
                          MIN_ANGLE, MAX_ANGLE);


  // the values will be transmitted in the main-sketch
  flexValues[3] = ANGLE_RING;
  flexValues[1]= ANGLE_INDEX;
  flexValues[4] = ANGLE_PINKY;
  flexValues[0] = ANGLE_THUMB;
  flexValues[2] = ANGLE_MIDDLE;
}

