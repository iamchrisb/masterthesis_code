/******************************************************************************
  Flex_Sensor_Example.ino
  Example sketch for SparkFun's flex sensors
  (https://www.sparkfun.com/products/10264)
  Jim Lindblom @ SparkFun Electronics
  April 28, 2016

  Create a voltage divider circuit combining a flex sensor with a 47k resistor.
  - The resistor should connect from A0 to GND.
  - The flex sensor should connect from A0 to 3.3V/5.0V
  As the resistance of the flex sensor increases (meaning it's being bent), the
  voltage at A0 should decrease.

  ------------------------------------------

  /* MPU9250 Basic Example Code
  by: Kris Winer
  date: April 1, 2014
  license: Beerware - Use this code however you'd like. If you
  find it useful you can buy me a beer some time.
  Modified by Brent Wilkins July 19, 2016

  Demonstrate basic MPU-9250 functionality including parameterizing the register
  addresses, initializing the sensor, getting properly scaled accelerometer,
  gyroscope, and magnetometer data out. Added display functions to allow display
  to on breadboard monitor. Addition of 9 DoF sensor fusion using open source
  Madgwick and Mahony filter algorithms. Sketch runs on the 3.3 V 8 MHz Pro Mini
  and the Teensy 3.1.

  SDA and SCL should have external pull-up resistors (to 3.3V).
  10k resistors are on the EMSENSR-9250 breakout board.

  Hardware setup:
  MPU9250 Breakout --------- Arduino
  VDD ---------------------- 3.3V
  VDDI --------------------- 3.3V
  SDA ----------------------- A4
  SCL ----------------------- A5
  GND ---------------------- GND

  Development environment specifics:
  Arduino 1.6.7
******************************************************************************/

/** BEND SENSOR PART **/
const int FLEX_PIN_THUMB = A3;
const int FLEX_PIN_INDEX_FINGER = A2;
const int FLEX_PIN_MIDDLE_FINGER = A1;
const int FLEX_PIN_RING_FINGER = A6;
const int FLEX_PIN_PINKY = A0;

// Measure the voltage at 5V and the actual resistance of your
// used resistor for the voltage divider circuite, and enter them below:
const float VCC = 4.98; // Measured voltage of Ardunio 5V line
const float R_DIV = 15000.0; // Measured resistance of 15k resistor

// Upload the code, then try to adjust these values to more
// accurately calculate bend degree.
const float PINKY_RESISTANCE_STRAIGHT   = 11500.00;
const float PINKY_RESISTANCE_BEND       = 38000.00;

const float MIDDLE_RESISTANCE_STRAIGHT  = 12000.00;
const float MIDDLE_RESISTANCE_BEND      = 38000.00;

const float INDEX_RESISTANCE_STRAIGHT   = 11500.00;
const float INDEX_RESISTANCE_BEND       = 38000.00;

const float THUMB_RESISTANCE_STRAIGHT   = 11000.00;
const float THUMB_RESISTANCE_BEND       = 30000.00;

const float RING_RESISTANCE_STRAIGHT    = 11000.00;
const float RING_RESISTANCE_BEND        = 38000.00;
/** BEND SENSOR PART **/

/** MPU PART **/
#include <SPI.h>
#include <Wire.h>

#include "quaternionFilters.h"
#include "MPU9250.h"

#define AHRS true         // Set to false for basic data read
#define SerialDebug false  // Set to true to get Serial output for debugging

// Pin definitions
int intPin = 12;  // These can be changed, 2 and 3 are the Arduinos ext int pins
int myLed  = 13;  // Set up pin 13 led for toggling

MPU9250 myIMU;
/** MPU PART **/

#define DEBUG true

void setup() {
  Wire.begin();
  // TWBR = 12;  // 400 kbit/sec I2C speed

  Serial.begin(38400);

  setup_flex();
  setup_mpu();
}

void loop() {
  read_flex();
  read_mpu();

  if (DEBUG) {
    //delay(500);
  }
}

void setup_flex() {
  pinMode(FLEX_PIN_THUMB, INPUT);
  pinMode(FLEX_PIN_INDEX_FINGER, INPUT);
  pinMode(FLEX_PIN_MIDDLE_FINGER, INPUT);
  pinMode(FLEX_PIN_RING_FINGER, INPUT);
  pinMode(FLEX_PIN_PINKY, INPUT);
}

void setup_mpu() {
  // Set up the interrupt pin, its set as active high, push-pull
  pinMode(intPin, INPUT);
  digitalWrite(intPin, LOW);
  pinMode(myLed, OUTPUT);
  digitalWrite(myLed, HIGH);

  // Read the WHO_AM_I register, this is a good test of communication
  byte c = myIMU.readByte(MPU9250_ADDRESS, WHO_AM_I_MPU9250);
  Serial.print("MPU9250 "); Serial.print("I AM "); Serial.print(c, HEX);
  Serial.print(" I should be "); Serial.println(0x71, HEX);

  if (c == 0x71) {
    // WHO_AM_I should always be 0x68
    Serial.println("MPU9250 is online...");

    // Start by performing self test and reporting values
    myIMU.MPU9250SelfTest(myIMU.SelfTest);
    Serial.print("x-axis self test: acceleration trim within : ");
    Serial.print(myIMU.SelfTest[0], 1); Serial.println("% of factory value");
    Serial.print("y-axis self test: acceleration trim within : ");
    Serial.print(myIMU.SelfTest[1], 1); Serial.println("% of factory value");
    Serial.print("z-axis self test: acceleration trim within : ");
    Serial.print(myIMU.SelfTest[2], 1); Serial.println("% of factory value");
    Serial.print("x-axis self test: gyration trim within : ");
    Serial.print(myIMU.SelfTest[3], 1); Serial.println("% of factory value");
    Serial.print("y-axis self test: gyration trim within : ");
    Serial.print(myIMU.SelfTest[4], 1); Serial.println("% of factory value");
    Serial.print("z-axis self test: gyration trim within : ");
    Serial.print(myIMU.SelfTest[5], 1); Serial.println("% of factory value");

    // Calibrate gyro and accelerometers, load biases in bias registers
    myIMU.calibrateMPU9250(myIMU.gyroBias, myIMU.accelBias);

    myIMU.initMPU9250();
    // Initialize device for active mode read of acclerometer, gyroscope, and
    // temperature
    Serial.println("MPU9250 initialized for active data mode....");

    // Read the WHO_AM_I register of the magnetometer, this is a good test of
    // communication
    byte d = myIMU.readByte(AK8963_ADDRESS, WHO_AM_I_AK8963);
    Serial.print("AK8963 "); Serial.print("I AM "); Serial.print(d, HEX);
    Serial.print(" I should be "); Serial.println(0x48, HEX);

    // Get magnetometer calibration from AK8963 ROM
    myIMU.initAK8963(myIMU.magCalibration);
    // Initialize device for active mode read of magnetometer
    Serial.println("AK8963 initialized for active data mode....");
    if (SerialDebug) {
      //  Serial.println("Calibration values: ");
      Serial.print("X-Axis sensitivity adjustment value ");
      Serial.println(myIMU.magCalibration[0], 2);
      Serial.print("Y-Axis sensitivity adjustment value ");
      Serial.println(myIMU.magCalibration[1], 2);
      Serial.print("Z-Axis sensitivity adjustment value ");
      Serial.println(myIMU.magCalibration[2], 2);
    }
  }  else  {
    Serial.print("Could not connect to MPU9250: 0x");
    Serial.println(c, HEX);
    while (1) ; // Loop forever if communication doesn't happen
  }
}

void read_mpu() {
  // If intPin goes high, all data registers have new data
  // On interrupt, check if data ready interrupt
  if (myIMU.readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01) {

    myIMU.readAccelData(myIMU.accelCount);  // Read the x/y/z adc values
    myIMU.getAres();

    // Now we'll calculate the accleration value into actual g's
    // This depends on scale being set
    myIMU.ax = (float)myIMU.accelCount[0] * myIMU.aRes; // - accelBias[0];
    myIMU.ay = (float)myIMU.accelCount[1] * myIMU.aRes; // - accelBias[1];
    myIMU.az = (float)myIMU.accelCount[2] * myIMU.aRes; // - accelBias[2];

    myIMU.readGyroData(myIMU.gyroCount);  // Read the x/y/z adc values
    myIMU.getGres();

    // Calculate the gyro value into actual degrees per second
    // This depends on scale being set
    myIMU.gx = (float)myIMU.gyroCount[0] * myIMU.gRes;
    myIMU.gy = (float)myIMU.gyroCount[1] * myIMU.gRes;
    myIMU.gz = (float)myIMU.gyroCount[2] * myIMU.gRes;

    myIMU.readMagData(myIMU.magCount);  // Read the x/y/z adc values
    myIMU.getMres();
    // User environmental x-axis correction in milliGauss, should be
    // automatically calculated
    myIMU.magbias[0] = +470.;
    // User environmental x-axis correction in milliGauss TODO axis??
    myIMU.magbias[1] = +120.;
    // User environmental x-axis correction in milliGauss
    myIMU.magbias[2] = +125.;

    // Calculate the magnetometer values in milliGauss
    // Include factory calibration per data sheet and user environmental
    // corrections
    // Get actual magnetometer value, this depends on scale being set
    myIMU.mx = (float)myIMU.magCount[0] * myIMU.mRes * myIMU.magCalibration[0] -
               myIMU.magbias[0];
    myIMU.my = (float)myIMU.magCount[1] * myIMU.mRes * myIMU.magCalibration[1] -
               myIMU.magbias[1];
    myIMU.mz = (float)myIMU.magCount[2] * myIMU.mRes * myIMU.magCalibration[2] -
               myIMU.magbias[2];
  } // if (readByte(MPU9250_ADDRESS, INT_STATUS) & 0x01)

  // Must be called before updating quaternions!
  myIMU.updateTime();

  // Sensors x (y)-axis of the accelerometer is aligned with the y (x)-axis of
  // the magnetometer; the magnetometer z-axis (+ down) is opposite to z-axis
  // (+ up) of accelerometer and gyro! We have to make some allowance for this
  // orientationmismatch in feeding the output to the quaternion filter. For the
  // MPU-9250, we have chosen a magnetic rotation that keeps the sensor forward
  // along the x-axis just like in the LSM9DS0 sensor. This rotation can be
  // modified to allow any convenient orientation convention. This is ok by
  // aircraft orientation standards! Pass gyro rate as rad/s
  //  MadgwickQuaternionUpdate(ax, ay, az, gx*PI/180.0f, gy*PI/180.0f, gz*PI/180.0f,  my,  mx, mz);
  MahonyQuaternionUpdate(myIMU.ax, myIMU.ay, myIMU.az, myIMU.gx * DEG_TO_RAD,
                         myIMU.gy * DEG_TO_RAD, myIMU.gz * DEG_TO_RAD, myIMU.my,
                         myIMU.mx, myIMU.mz, myIMU.deltat);

  if (!AHRS) {
    myIMU.delt_t = millis() - myIMU.count;
    if (myIMU.delt_t > 100) {
      if (SerialDebug) {
        // Print resolutions

        float axValue = 1000 * myIMU.ax;
        float ayValue = 1000 * myIMU.ay;
        float azValue = 1000 * myIMU.az;

        float gxValue = myIMU.gx;
        float gyValue = myIMU.gy;
        float gzValue = myIMU.gz;

        float mxValue = myIMU.mx;
        float myValue = myIMU.my;
        float mzValue = myIMU.mz;

        /*
          Serial.println("Resolution accelerometer: " + String(myIMU.aRes));
          Serial.println("Resolution gyroscope: " + String(myIMU.gRes));
          Serial.println("Resolution magnetometer: " + String(myIMU.mRes));
        */
        // Print acceleration values in milligs!
        /* Serial.print("X-acceleration: "); Serial.print(axValue);
          Serial.print(" mg ");
          Serial.print("Y-acceleration: "); Serial.print(ayValue);
          Serial.print(" mg ");
          Serial.print("Z-acceleration: "); Serial.print(azValue);
          Serial.println(" mg ");

          // Print gyro values in degree/sec
          Serial.print("X-gyro rate: "); Serial.print(gxValue, 3);
          Serial.print(" degrees/sec ");
          Serial.print("Y-gyro rate: "); Serial.print(gyValue, 3);
          Serial.print(" degrees/sec ");
          Serial.print("Z-gyro rate: "); Serial.print(gzValue, 3);
          Serial.println(" degrees/sec");

          // Print mag values in degree/sec
          Serial.print("X-mag field: "); Serial.print(mxValue);
          Serial.print(" mG ");
          Serial.print("Y-mag field: "); Serial.print(myValue);
          Serial.print(" mG ");
          Serial.print("Z-mag field: "); Serial.print(mzValue);
          Serial.println(" mG");

          myIMU.tempCount = myIMU.readTempData();  // Read the adc values
          // Temperature in degrees Centigrade
          myIMU.temperature = ((float) myIMU.tempCount) / 333.87 + 21.0;
          // Print temperature in degrees Centigrade
          /*
           Serial.print("Temperature is ");  Serial.print(myIMU.temperature, 1);
           Serial.println(" degrees C");
        */

        Serial.println("MSG:IMU:" + String((int)gxValue) + ";" + String((int)gyValue) + ";" + String((int)gzValue) + ";" + String((int)axValue) +  ";" + String((int)ayValue) + ";" + String((int)azValue));
      }

      myIMU.count = millis();
      digitalWrite(myLed, !digitalRead(myLed));  // toggle led
    } // if (myIMU.delt_t > 500)
  } else {
    // if (!AHRS)
    // Serial print and/or display at 0.5 s rate independent of data rates
    myIMU.delt_t = millis() - myIMU.count;

    if (myIMU.delt_t > 100) {
      if (SerialDebug) {

        float axValue = 1000 * myIMU.ax;
        float ayValue = 1000 * myIMU.ay;
        float azValue = 1000 * myIMU.az;

        float gxValue = myIMU.gx;
        float gyValue = myIMU.gy;
        float gzValue = myIMU.gz;

        float mxValue = myIMU.mx;
        float myValue = myIMU.my;
        float mzValue = myIMU.mz;

        Serial.println("Resolution accelerometer: " + String(myIMU.aRes));
        Serial.println("Resolution gyroscope: " + String(myIMU.gRes));
        Serial.println("Resolution magnetometer: " + String(myIMU.mRes));

        Serial.print("ax = "); Serial.print((int) axValue);
        Serial.print(" ay = "); Serial.print((int) ayValue);
        Serial.print(" az = "); Serial.print((int) azValue);
        Serial.println(" mg");

        Serial.print("gx = "); Serial.print(gxValue, 2);
        Serial.print(" gy = "); Serial.print(gyValue, 2);
        Serial.print(" gz = "); Serial.print(gzValue, 2);
        Serial.println(" deg/s");

        Serial.print("mx = "); Serial.print( (int) mxValue );
        Serial.print(" my = "); Serial.print( (int)myValue );
        Serial.print(" mz = "); Serial.print( (int)mzValue );
        Serial.println(" mG");

        // Serial.println("MSG:IMU:" + String((int)gxValue) + ";" + String((int)gyValue) + ";" + String((int)gzValue) + ";" + String((int)axValue) +  ";" + String((int)ayValue) + ";" + String((int)azValue));

        Serial.print("q0 = "); Serial.print(*getQ());
        Serial.print(" qx = "); Serial.print(*(getQ() + 1));
        Serial.print(" qy = "); Serial.print(*(getQ() + 2));
        Serial.print(" qz = "); Serial.println(*(getQ() + 3));
      }

      // Define output variables from updated quaternion---these are Tait-Bryan
      // angles, commonly used in aircraft orientation. In this coordinate system,
      // the positive z-axis is down toward Earth. Yaw is the angle between Sensor
      // x-axis and Earth magnetic North (or true North if corrected for local
      // declination, looking down on the sensor positive yaw is counterclockwise.
      // Pitch is angle between sensor x-axis and Earth ground plane, toward the
      // Earth is positive, up toward the sky is negative. Roll is angle between
      // sensor y-axis and Earth ground plane, y-axis up is positive roll. These
      // arise from the definition of the homogeneous rotation matrix constructed
      // from quaternions. Tait-Bryan angles as well as Euler angles are
      // non-commutative; that is, the get the correct orientation the rotations
      // must be applied in the correct order which for this configuration is yaw,
      // pitch, and then roll.
      // For more see
      // http://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles
      // which has additional links.

      myIMU.yaw   = atan2(2.0f * (*(getQ() + 1) * *(getQ() + 2) + *getQ() *
                                  *(getQ() + 3)), *getQ() * *getQ() + * (getQ() + 1) * *(getQ() + 1)
                          - * (getQ() + 2) * *(getQ() + 2) - * (getQ() + 3) * *(getQ() + 3));
      myIMU.pitch = -asin(2.0f * (*(getQ() + 1) * *(getQ() + 3) - *getQ() *
                                  *(getQ() + 2)));
      myIMU.roll  = atan2(2.0f * (*getQ() * *(getQ() + 1) + * (getQ() + 2) *
                                  *(getQ() + 3)), *getQ() * *getQ() - * (getQ() + 1) * *(getQ() + 1)
                          - * (getQ() + 2) * *(getQ() + 2) + * (getQ() + 3) * *(getQ() + 3));

      myIMU.pitch *= RAD_TO_DEG;
      myIMU.yaw   *= RAD_TO_DEG;

      // Declination of SparkFun Electronics (40°05'26.6"N 105°11'05.9"W) is
      //    8° 30' E  ± 0° 21' (or 8.5°) on 2016-07-19
      // - http://www.ngdc.noaa.gov/geomag-web/#declination
      //myIMU.yaw   -= 8.5;

      myIMU.yaw   -= 3.66666666666667; // for potsdam
      myIMU.roll  *= RAD_TO_DEG;

      if (SerialDebug) {
        Serial.print("Yaw, Pitch, Roll: ");
        Serial.print(myIMU.yaw, 2);
        Serial.print(", ");
        Serial.print(myIMU.pitch, 2);
        Serial.print(", ");
        Serial.println(myIMU.roll, 2);

        Serial.print("rate = ");
        Serial.print((float)myIMU.sumCount / myIMU.sum, 2);
        Serial.println(" Hz");
      }

      String stringyfiedYaw = String((int) myIMU.yaw);
      String stringyfiedPitch = String((int) myIMU.pitch);
      String stringyfiedRoll = String((int) myIMU.roll);

      Serial.println("MSG:APP:ORI:" + stringyfiedYaw  + ";" + stringyfiedPitch + ";" + stringyfiedRoll);
      //TODO "drum" with intensity and region
      Serial.println("MSG:APP:DIR:" + String(0));
      Serial.println("MSG:APP:DRUM:" + String(0) + ";" + String(30));

      if (DEBUG) {
        imu_debug();
      }

      myIMU.count = millis();
      myIMU.sumCount = 0;
      myIMU.sum = 0;
    } // if (myIMU.delt_t > 500)
  } // if (AHRS)
}

/**
   print certain imu values for debugging stuff
*/
void imu_debug() {
  myIMU.tempCount = myIMU.readTempData();  // Read the adc values
  // Temperature in degrees Centigrade
  myIMU.temperature = ((float) myIMU.tempCount) / 333.87 + 21.0;

  float axValue = 1000 * myIMU.ax;
  float ayValue = 1000 * myIMU.ay;
  float azValue = 1000 * myIMU.az;

  String stringyfiedAxValue = String((int) axValue);
  String stringyfiedAyValue = String((int) ayValue);
  String stringyfiedAzValue = String((int) azValue);

  float gxValue = myIMU.gx;
  float gyValue = myIMU.gy;
  float gzValue = myIMU.gz;

  float mxValue = myIMU.mx;
  float myValue = myIMU.my;
  float mzValue = myIMU.mz;


  Serial.println("MSG:DBG:GYRO:" + String((int)gxValue) + ";" + String((int)gyValue) + ";" + String((int)gzValue));
  Serial.println("MSG:DBG:ACC:" + stringyfiedAxValue + ";" + stringyfiedAyValue + ";" + stringyfiedAzValue);
  Serial.println("MSG:DBG:MAG:" + String((int) mxValue) + ";" +  String((int) myValue) + ";" +  String((int) mzValue));
  Serial.println("MSG:DBG:TMP:" + String(myIMU.temperature));
  Serial.println("MSG:DBG:FREQ:" + String((float)myIMU.sumCount / myIMU.sum));
}

void read_flex() {
  // Read the ADC, and calculate voltage and resistance from it
  int flexADC_RING    = analogRead(FLEX_PIN_RING_FINGER);
  int flexADC_MIDDLE  = analogRead(FLEX_PIN_MIDDLE_FINGER);
  int flexADC_INDEX   = analogRead(FLEX_PIN_INDEX_FINGER);
  int flexADC_THUMB   = analogRead(FLEX_PIN_THUMB);
  int flexADC_PINKY   = analogRead(FLEX_PIN_PINKY);

  float flexV_RING = flexADC_RING * VCC / 1023.0;
  float flexR_RING = R_DIV * (VCC / flexV_RING - 1.0);

  float flexV_MIDDLE = flexADC_MIDDLE * VCC / 1023.0;
  float flexR_MIDDLE = R_DIV * (VCC / flexV_MIDDLE - 1.0);

  float flexV_INDEX = flexADC_INDEX * VCC / 1023.0;
  float flexR_INDEX = R_DIV * (VCC / flexV_INDEX - 1.0);

  float flexV_THUMB = flexADC_THUMB * VCC / 1023.0;
  float flexR_THUMB = R_DIV * (VCC / flexV_THUMB - 1.0);

  float flexV_PINKY = flexADC_PINKY * VCC / 1023.0;
  float flexR_PINKY = R_DIV * (VCC / flexV_PINKY - 1.0);

  // Use the calculated resistance to estimate the sensor's
  // bend angle:
  float ANGLE_RING = map(flexR_RING, RING_RESISTANCE_STRAIGHT, RING_RESISTANCE_BEND,
                         0, 90.0);
  float ANGLE_PINKY = map(flexR_PINKY, PINKY_RESISTANCE_STRAIGHT , PINKY_RESISTANCE_BEND,
                          0, 90.0);
  float ANGLE_MIDDLE = map(flexR_MIDDLE, MIDDLE_RESISTANCE_STRAIGHT, MIDDLE_RESISTANCE_BEND,
                           0, 90.0);
  float ANGLE_INDEX = map(flexR_INDEX, INDEX_RESISTANCE_STRAIGHT, INDEX_RESISTANCE_BEND,
                          0, 90.0);
  float ANGLE_THUMB = map(flexR_THUMB, THUMB_RESISTANCE_STRAIGHT, THUMB_RESISTANCE_BEND,
                          0, 90.0);

  String AVERAGE_BEND_ANGLE = String((int) 0);

  Serial.println("MSG:APP:BEND:" + String((int)ANGLE_THUMB) + ";" + String((int)ANGLE_INDEX) + ";" + String((int)ANGLE_MIDDLE) + ";" + String((int)ANGLE_RING) + ";" + String((int)ANGLE_PINKY) + ";" + AVERAGE_BEND_ANGLE);
}


