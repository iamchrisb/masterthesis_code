/******************************************************************************
Flex_Sensor_Example.ino
Example sketch for SparkFun's flex sensors
  (https://www.sparkfun.com/products/10264)
Jim Lindblom @ SparkFun Electronics
April 28, 2016

Create a voltage divider circuit combining a flex sensor with a 47k resistor.
- The resistor should connect from A0 to GND.
- The flex sensor should connect from A0 to 3.3V
As the resistance of the flex sensor increases (meaning it's being bent), the
voltage at A0 should decrease.

Development environment specifics:
Arduino 1.6.7
******************************************************************************/
const int FLEX_PIN_THUMB = A3; // Pin connected to voltage divider output
const int FLEX_PIN_INDEX_FINGER = A2;
const int FLEX_PIN_MIDDLE_FINGER = A1;
const int FLEX_PIN_RING_FINGER = A6;
const int FLEX_PIN_PINKY = A0;

// Measure the voltage at 5V and the actual resistance of your
// 47k resistor, and enter them below:
const float VCC = 4.98; // Measured voltage of Ardunio 5V line
const float R_DIV = 15000.0; // Measured resistance of 47k resistor

// Upload the code, then try to adjust these values to more
// accurately calculate bend degree.

const float PINKY_RESISTANCE_STRAIGHT   = 11500.00;
const float PINKY_RESISTANCE_BEND       = 38000.00;

const float MIDDLE_RESISTANCE_STRAIGHT  = 12000.00;
const float MIDDLE_RESISTANCE_BEND      = 38000.00;  

const float INDEX_RESISTANCE_STRAIGHT   = 11500.00;
const float INDEX_RESISTANCE_BEND       = 38000.00;

const float THUMB_RESISTANCE_STRAIGHT   = 11000.00;
const float THUMB_RESISTANCE_BEND       = 38000.00;

const float RING_RESISTANCE_STRAIGHT    = 11000.00;
const float RING_RESISTANCE_BEND        = 38000.00;

void setup()
{
  Serial.begin(38400);
  pinMode(FLEX_PIN_THUMB, INPUT);
  pinMode(FLEX_PIN_INDEX_FINGER, INPUT);
  pinMode(FLEX_PIN_MIDDLE_FINGER, INPUT);
  pinMode(FLEX_PIN_RING_FINGER, INPUT);
  pinMode(FLEX_PIN_PINKY, INPUT);
}

void loop()
{
  // Read the ADC, and calculate voltage and resistance from it
  int flexADC_RING    = analogRead(FLEX_PIN_RING_FINGER);
  int flexADC_MIDDLE  = analogRead(FLEX_PIN_MIDDLE_FINGER);
  int flexADC_INDEX   = analogRead(FLEX_PIN_INDEX_FINGER);
  int flexADC_THUMB   = analogRead(FLEX_PIN_THUMB);
  int flexADC_PINKY   = analogRead(FLEX_PIN_PINKY);

  
  Serial.println("ANALOG VALUE 0: " + String(flexADC_0));
  Serial.println("ANALOG VALUE 1: " + String(flexADC_1));
  Serial.println("ANALOG VALUE 2: " + String(flexADC_2));
  Serial.println("ANALOG VALUE 3: " + String(flexADC_3));
  Serial.println("ANALOG VALUE 4: " + String(flexADC_4));
  Serial.println();
  

  float flexV_RING = flexADC_RING * VCC / 1023.0;
  float flexR_RING = R_DIV * (VCC / flexV_RING - 1.0);
  
  float flexV_MIDDLE = flexADC_MIDDLE * VCC / 1023.0;
  float flexR_MIDDLE = R_DIV * (VCC / flexV_MIDDLE - 1.0);

  float flexV_INDEX = flexADC_INDEX * VCC / 1023.0;
  float flexR_INDEX = R_DIV * (VCC / flexV_INDEX - 1.0);

  float flexV_THUMB = flexADC_THUMB * VCC / 1023.0;
  float flexR_THUMB = R_DIV * (VCC / flexV_THUMB - 1.0);

  float flexV_PINKY = flexADC_PINKY * VCC / 1023.0;
  float flexR_PINKY = R_DIV * (VCC / flexV_PINKY - 1.0);
  
  // Use the calculated resistance to estimate the sensor's
  // bend angle:
  
  /*
  Serial.println("Resistance: " + String(flexR_0) + " ohms");
  Serial.println("Bend angle0: " + String(angle_0) + " degrees");
  Serial.println();
  */

  float ANGLE_RING = map(flexR_RING, RING_RESISTANCE_STRAIGHT, RING_RESISTANCE_BEND,
                      0, 90.0);
                      
  float ANGLE_PINKY = map(flexR_PINKY, PINKY_RESISTANCE_STRAIGHT , PINKY_RESISTANCE_BEND,
                      0, 90.0);

  float ANGLE_MIDDLE = map(flexR_MIDDLE, MIDDLE_RESISTANCE_STRAIGHT, MIDDLE_RESISTANCE_BEND,
                      0, 90.0);

  float ANGLE_INDEX = map(flexR_INDEX, INDEX_RESISTANCE_STRAIGHT, INDEX_RESISTANCE_BEND,
                      0, 90.0);

  float ANGLE_THUMB = map(flexR_THUMB,THUMB_RESISTANCE_STRAIGHT, THUMB_RESISTANCE_BEND,
                      0, 90.0);
  /*
    Serial.println("Resistance: " + String(flexR_1) + " ohms");
    Serial.println("Bend angle1: " + String(angle_1) + " degrees");
    Serial.println();
  */
  

  //Serial.println("CMD:" + String((int)angle_0) + "-" + String((int)angle_1) + "-" + String((int)angle_2) + "-" + String((int)angle_3) + "-" + String((int)angle_4));
  //Serial.println("CMD:FLX:" + String((int)angle_0) + ";" + String((int)angle_1) + ";" + String((int)angle_2) + ";" + String((int)angle_3) + ";" + String((int)angle_4));
  Serial.println(String((int)ANGLE_PINKY) + ";" + String((int)ANGLE_RING) + ";" + String((int)ANGLE_MIDDLE) + ";" + String((int)ANGLE_INDEX) + ";" + String((int)ANGLE_THUMB));

  //delay(500);
}

