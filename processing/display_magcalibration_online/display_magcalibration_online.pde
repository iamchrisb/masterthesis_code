import processing.serial.*;

Serial  myPort;
short   portIndex = 3;
int     lf = 10;       //ASCII linefeed
String  inString;      //String for testing serial communication
int     calibrating;

float   dt;
float   x_gyr;  //Gyroscope data
float   y_gyr;
float   z_gyr;
float   x_acc;  //Accelerometer data
float   y_acc;
float   z_acc;
float   x_fil;  //Filtered data
float   y_fil;
float   z_fil;


float x, y, z;
int translateZ = -800; 
int translateX = 500;
int translateY = 200;

float angle = 0;

int centerRadius = 10;

boolean isLines = false;
boolean isLive = false;

int[][] points;

int[] livePoint = new int[3];

void setup() {
  size(1400, 800, P3D);
  noStroke();
  colorMode(RGB, 256); 

  //  println("in setup");
  String portName = Serial.list()[portIndex];
  println(Serial.list());
  //  println(" Connecting to -> " + Serial.list()[portIndex]);
  myPort = new Serial(this, portName, 115200);
  myPort.clear();
  myPort.bufferUntil(lf);
}

void draw() {
  clear();

/*
  translate(translateX, translateY, translateZ);

  pushMatrix();
  rotateY(angle);
  angle = angle + 0.01;

  drawSphere();
  if (isLive) {
    drawLive();
  } else {
    if (isLines) {
      drawLines();
    } else { 
      drawPoints();
    }
  }
  popMatrix();
  */
}

void clear() { 
  background(0);
}

void drawPoints() {
  if (points == null) {
    return;
  }

  for (int i = 0; i < points.length; i++) {
    int[] currentPoint = points[i];
    stroke(255, 0, 0);
    pushMatrix();
    translate(currentPoint[0], currentPoint[1], currentPoint[2]);
    //point(currentPoint[0], currentPoint[1], currentPoint[2]);
    box(1);
    popMatrix();
    stroke(255, 0, 0, 80);
    line(0, 0, 0, currentPoint[0], currentPoint[1], currentPoint[2]);
  }
}

void drawLines() {
  if (points == null) {
    return;
  }

  for (int i = 1; i < points.length; i++) {
    int[] currentPoint = points[i];
    int[] lastPoint = points[i-1];
    stroke(255, 0, 0);
    line(lastPoint[0], lastPoint[1], lastPoint[2], currentPoint[0], currentPoint[1], currentPoint[2]);
    stroke(255, 0, 0, 80);
    line(0, 0, 0, currentPoint[0], currentPoint[1], currentPoint[2]);
  }
}

void drawSphere () {
  noFill();
  stroke(255, 255, 255, 100);
  sphere(centerRadius);
}

void drawLive() {
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      translateZ += 100;
      background(0);
    } else if (keyCode == DOWN) {
      translateZ -= 100;
      background(0);
    } else if (keyCode == RIGHT) {
      translateX -= 100;
      background(0);
    } else if (keyCode == LEFT) {
      translateX += 100;
      background(0);
    }
  }
}

void serialEvent(Serial p) {

  inString = (myPort.readString());


  try {
    // Parse the data
    println(inString);
  } 
  catch (Exception e) {
    println("Caught Exception");
  }
}