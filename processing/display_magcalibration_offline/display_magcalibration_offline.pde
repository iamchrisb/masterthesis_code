float x, y, z;
int translateZ = -800; 
int translateX = 500;
int translateY = 200;

float angle = 0;

int centerRadius = 10;

boolean isLines = false;
boolean isLive = false;

int[][] points;

int[] livePoint = new int[3];

void setup() {
  int screenW = int(displayWidth * 0.85);
  println(screenW);
  int screenH = int(displayHeight * 0.85);
  println(screenH);
  
  size(1400, 800, P3D);
  background(0);
  lights();

  selectInput("Select a file to process:", "fileSelected");
  smooth();
}

void draw() {
  clear();

  translate(translateX, translateY, translateZ);

  pushMatrix();
  rotateY(angle);
  angle = angle + 0.01;

  drawSphere();
  if (isLive) {
    drawLive();
  } else {
    if (isLines) {
      drawLines();
    } else { 
      drawPoints();
    }
  }
  popMatrix();
}

void clear() { 
  background(0);
}

void drawPoints() {
  if (points == null) {
    return;
  }

  for (int i = 0; i < points.length; i++) {
    int[] currentPoint = points[i];
    stroke(255, 0, 0);
    pushMatrix();
    translate(currentPoint[0], currentPoint[1], currentPoint[2]);
    //point(currentPoint[0], currentPoint[1], currentPoint[2]);
    box(1);
    popMatrix();
    stroke(255, 0, 0, 80);
    line(0, 0, 0, currentPoint[0], currentPoint[1], currentPoint[2]);
  }
}

void drawLines() {
  if (points == null) {
    return;
  }

  for (int i = 1; i < points.length; i++) {
    int[] currentPoint = points[i];
    int[] lastPoint = points[i-1];
    stroke(255, 0, 0);
    line(lastPoint[0], lastPoint[1], lastPoint[2], currentPoint[0], currentPoint[1], currentPoint[2]);
    stroke(255, 0, 0, 80);
    line(0, 0, 0, currentPoint[0], currentPoint[1], currentPoint[2]);
  }
}

void drawSphere () {
  noFill();
  stroke(255, 255, 255, 100);
  sphere(centerRadius);
}

void drawLive() {
   
}

void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      translateZ += 100;
      background(0);
    } else if (keyCode == DOWN) {
      translateZ -= 100;
      background(0);
    } else if (keyCode == RIGHT) {
      translateX -= 100;
      background(0);
    } else if (keyCode == LEFT) {
      translateX += 100;
      background(0);
    }
  }
}

void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or the user hit cancel.");
  } else {
    println("User selected " + selection.getAbsolutePath());

    String[] lines = loadStrings(selection.getAbsolutePath());
    println("there are " + lines.length + " lines");

    points = new int[lines.length][3];

    for (int i = 0; i < lines.length; i++) {
      String currentLine = lines[i];
      if (currentLine != null) {
        String[] values = split(currentLine, ",");
        int x = Integer.parseInt(values[0]);
        int y = Integer.parseInt(values[1]);
        int z = Integer.parseInt(values[2]);
        points[i][0] = x;
        points[i][1] = y;
        points[i][2] = z;
      }
    }
  }
}